import minimalmodbus
import logging
import sys
from datetime import datetime
import serial
import time
import pandas as pd
import os.path
import inquirer
import ipdb

# ask for user input
questions = [
    inquirer.List('master',
                  message="What's the master name?",
                  choices=['Dell Edge', 'Laptop', 'PLC Modbus'],
                  ),
]
answers = inquirer.prompt(questions)
ipdb.set_trace()
master = raw_input("What's the Master? \n a.Dell Edge \n b. Laptop \n c.PLC Modbus \n")
if master.lower() == 'a':
    master = 'Dell_Edge'
elif master.lower() == 'b':
    master = 'Laptop'
elif master.lower() == 'c':
    master = 'PLC_Modbus'
ipdb.set_trace()
baud_rate = raw_input("What's the baud rate? ")
num_of_sensors = raw_input("How many sensor are you testing? ")
wiring_schema = raw_input("What's the wiring schema? ")
power_injection = raw_input("what's the power injection? ")
time_sleep = float(raw_input("What's the sleep time you want to have between sensor readings? "))

datetime_now = datetime.now().strftime("%m-%d-%Y_%H:%M:%S")

minimalmodbus.TIMEOUT = 2

logger = logging.getLogger()

parity_str = ["parity_none", "parity_odd", "parity_even"]
port = '/dev/ttyS{}'

sensors = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 153, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
           90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133]

checksum_errors = {}
other_errors = {}
sensor_data = {}
message_loop = 0

# create summary file
filename = '{n}_sensors_test_with_{m}_{b}_{w}_{p}_{d}'.format(
    n=num_of_sensors, m=master, b=baud_rate, w=wiring_schema, p=power_injection, d=datetime_now)

with open("{}.csv".format(filename), 'wb') as textfile:
    textfile.write("datetime: {} \n".format(datetime_now))
    textfile.write("master: {} \n".format(master))
    textfile.write("baud rate: {} \n".format(baud_rate))
    textfile.write("number of sensors from user input: {} \n".format(num_of_sensors))
    textfile.write("number of sensors actual: {} \n".format(len(sensors)))
    textfile.write("wiring schema: {} \n".format(wiring_schema))
    textfile.write("power injection: {} \n".format(power_injection))
    textfile.write("number of loops: {} \n".format(message_loop))
    textfile.write("time sleep between sensor readings: {} \n".format(time_sleep))
    textfile.write("\n")
    textfile.close()

# create detailed log file
with open("{}.csv".format(filename), 'r') as textfile, open("{}_detailed_logs.txt".format(filename), 'wb') as textfile2:
    textfile2.write(textfile.read())
    textfile2.write('sensor_number, sensor_values \n')
    textfile2.close()


def create_file(checksum_errors_dict, other_errors_dict):
    """
    summarize information from checksum errors and other types of erros
    """

    checksum_error = pd.DataFrame(list(checksum_errors_dict.items()), columns=[
        'sensor_number', 'num_of_checksum_errors'])
    other_error = pd.DataFrame(list(other_errors_dict.items()), columns=['sensor_number', 'num_of_other_types_errors'])
    data = checksum_error.merge(other_error, left_on='sensor_number', right_on='sensor_number', how='outer')
    data.fillna(0, inplace=True)
    return data

p = '/dev/cu.usbserial-FTAUZCNF'

try:
    while True:
        print('================ message_loop :{a}==========================='.format(a=message_loop))
        message_loop += 1
        for s in sensors:
            try:
                instrument = minimalmodbus.Instrument(p, s)
                instrument.serial.baudrate = baud_rate
                instrument.serial.parity = serial.PARITY_NONE
                instrument.serial.bytesize = 8
                instrument.serial.stopbits = 1
                instrument.serial.timeouts = 0.5

                time.sleep(time_sleep)

                # sensor data
                raw_data = instrument.read_registers(5200, 22)
                # append raw data into detailed logs file
                with open("{}_detailed_logs.txt".format(filename), 'a') as textfile2:
                    textfile2.write("{}, {}".format(s, raw_data))
                    textfile2.write("\n")
                    textfile2.close()

            except BaseException as e:
                print(e)
                with open("{}_detailed_logs.txt".format(filename), 'a') as textfile2:
                    textfile2.write("{}, {}".format(s, e))
                    textfile2.write("\n")
                    textfile2.close()
                if s not in checksum_errors.keys():
                    checksum_errors[s] = 0
                if s not in other_errors.keys():
                    other_errors[s] = 0
                if 'checksum' in str(e).lower():
                    checksum_errors[s] += 1
                elif len(str(e)) > 1:
                    other_errors[s] += 1
                time.sleep(time_sleep)

except KeyboardInterrupt:
    data = create_file(checksum_errors, other_errors)
    data.to_csv("{}.csv".format(filename), index=False, mode='a')
    sys.exit(1)
