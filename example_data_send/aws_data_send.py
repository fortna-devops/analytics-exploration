#!/usr/bin/env python3

import csv
import json
import io

import boto3
import s3fs

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

MY_DATA_SCHEMA = pa.schema([
    pa.field('_id', pa.string()),
    pa.field('index', pa.float64()),
    pa.field('guid', pa.string()),
    pa.field('isActive', pa.bool_()),
    pa.field('balance', pa.string()),
    pa.field('picture', pa.string()),
    pa.field('age', pa.int16()),
    pa.field('eyeColor', pa.string()),
    pa.field('name', pa.string()),
    pa.field('gender', pa.string()),
    pa.field('company', pa.string()),
    pa.field('email', pa.string()),
    pa.field('phone', pa.string()),
    pa.field('address', pa.string()),
    pa.field('about', pa.string()),
    pa.field('registered', pa.string()),
    pa.field('latitude', pa.float64()),
    pa.field('longitude', pa.float64()),
    pa.field('greeting', pa.string()),
    pa.field('favoriteFruit', pa.string())
])


def _convert_json_to_df(json_dict):
    """
    converts json to dataframe
    """
    #json_dict = json.loads(json_str)
    converted_json = pd.DataFrame.from_dict(json_dict, orient='index')
    df = converted_json.transpose()
    return df


class DataWriter():

    def __init__(self, s3_bucket_name, boto_session=None):
        """

        """
        if not boto_session:
            boto_session = boto3.Session()
        self.boto_session = boto_session
        # initialize an s3 filesystem
        self.bucketname = s3_bucket_name
        self.fs = s3fs.S3FileSystem()
        self.s3_client = self.boto_session.client('s3')

    def write_data(self, data, path, parquet=False):
        """
        writes dataframe to cloud at specified path
        """
        filepath = f's3://{self.bucketname}/{path}'

        if parquet:
            # for production, write as parquet for space saving
            table = pa.Table.from_pandas(data, schema=MY_DATA_SCHEMA)
            import pdb
            pdb.set_trace()
            pq.write_to_dataset(table, filepath, filesystem=self.fs, compression='gzip')
        else:
            # for development, keep as csvs for easier debugging
            csv_str = data.to_csv(index=False).encode()
            import pdb
            pdb.set_trace()
            self.s3_client.put_object(
                Bucket=self.bucketname,
                Body=csv_str,
                Key=path)

            # only need to use one of these ways to write data
            # with self.fs.open(filepath, 'wb') as f:
            #     f.write(io.StringIO(csv_str).getvalue().encode())


dw = DataWriter("mhs-robots-test")
f = open("example.json")
json_data = json.load(f)
bigdf = pd.DataFrame()
for datapoint in json_data:
    df = _convert_json_to_df(datapoint)
    bigdf = bigdf.append(df)
dw.write_data(bigdf, "sometest/testparquet", parquet=True)
# dw.write_data(bigdf, "sometest/testfolder/example.csv", parquet=False)
