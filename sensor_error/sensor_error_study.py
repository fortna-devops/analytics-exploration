#!/usr/bin/env python3

import importlib
# import_path = importlib.import_module('analytics-sdk-lite.analytics_sdk_core.data_init.data_pull')

from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from collections import OrderedDict
import csv
import os

SITES = ["mhspredict-site-mhs-emulated", "mhspredict-site-mhs-dataart", "mhspredict-site-mhs-testloop-atlanta"]
#, "mhspredict-site-amazon-zappos", "mhspredict-site-dhl-milano"]
DATE = '2019-05-28'

location_table_query = "SELECT sensor FROM location WHERE sensor NOT LIKE '%PLC%'"
sensor_table_query = "SELECT sensor, count(*) FROM sensor WHERE date between date '{date}' and date '{date}' GROUP BY sensor".format(date = DATE)
error_table_query = "SELECT sensor, count(*) FROM error WHERE date between date '{date}' and date '{date}' GROUP BY sensor".format(date = DATE)

FIELDS = ['sensor_entries', 'error_entries', 'total_entries', 'percent_of_expected', 'error_rate']
FIELD_NAMES = ['sensor', 'sensor_entries', 'error_entries', 'total_entries', 'percent_of_expected', 'error_rate']

def reconstruct_query_to_dict(raw_query_results):
	"""
	transforms the raw results from a list of ordered dictionaries to 
	a dictionary with key as sensor and value as count

	:param raw_query_results: raw query results
	:type raw_query_results: list of ordered dicts

	:returns: dictionary where key is sensor and value is whatever count
	:rtype: dict
	"""

	reconstructed_query = {}
	for row in raw_query_results:
		sensor_num = row['sensor']
		count = row['_col1']

		reconstructed_query[sensor_num] = count

	return reconstructed_query


def merge_data(sensors, sensor_entries, error_entries):
	"""
	combines the count of sensor table entries and count error 
	of error table entries together into one dict and have
	each dict as a list item to have a list of all of the sensors
	"""

	organized_data = []

	for sensor_row in sensors:
		sensor_info = OrderedDict()
		sensor_num = sensor_row['sensor']

		sensor_info['sensor'] = sensor_num

		try:
			sensor_info['sensor_entries'] = int(sensor_entries[sensor_num])
		except:
			sensor_info['sensor_entries'] = 0

		if sensor_num not in error_entries:
			sensor_info['error_entries'] = 0
		else:
			sensor_info['error_entries'] = int(error_entries[sensor_num])

		organized_data.append(sensor_info)

	return organized_data


def find_totals(data):
	"""
	calculate total entries (sensor table entries + error table entries)
	"""

	for row in data:
		total_entries = row['sensor_entries'] + row['error_entries']
		row['total_entries'] = total_entries

	return data


def find_percent_of_expected(data):
	"""
	calculates the percent of data that was received where the expected
	is one point every 10 seconds (8640 points/sensor/day)
	"""

	for row in data:
		percent_of_expected = row['total_entries']/8640
		row['percent_of_expected'] = percent_of_expected

	return data


def find_error_rates(data):
	"""
	calculates the error rates for each sensor (error entries/total entries)
	"""

	for row in data:
		try:
			error_rate = row['error_entries'] / row['total_entries']
			row['error_rate'] = error_rate
		except:
			row['error_rate'] = 0

	return data


def average_all(data):

	average_row = OrderedDict()
	average_row['sensor'] = 'average'

	for field in FIELDS:
		sum_field = 0
		for row in data:
			sum_field += row[field]
		average_field = sum_field / len(data)
		average_row[field] = average_field
	data.append(average_row)
	return data


for site in SITES:

	dqe = DQE(s3_bucket_name=site)

	all_sensors = dqe.execute_generic_query(location_table_query)
	raw_sensor_entries = dqe.execute_generic_query(sensor_table_query)
	raw_error_entries = dqe.execute_generic_query(error_table_query)

	sensor_entries = reconstruct_query_to_dict(raw_sensor_entries)
	error_entries = reconstruct_query_to_dict(raw_error_entries)

	all_data = merge_data(all_sensors, sensor_entries, error_entries)
	data_with_totals = find_totals(all_data)
	data_with_percent_of_expected = find_percent_of_expected(data_with_totals)
	data_with_all_stats = find_error_rates(data_with_percent_of_expected)

	final_data = average_all(data_with_all_stats)

	site_name = site.split('mhspredict-site-')[1]
	filename = "{date}/{site}.csv".format(date=DATE, site=site_name)
	os.makedirs(os.path.dirname(filename), exist_ok=True)
	with open(filename, 'w') as csvfile:
		writer = csv.DictWriter(csvfile, fieldnames=FIELD_NAMES)
		writer.writeheader()
		for row in final_data:
			writer.writerow(row)
	csvfile.close()


