"""
Exploratory Analysis of Predicting Motor Current
"""
import os

import pandas as pd
import numpy as np

import plotly.offline as pltly
import plotly.graph_objs as go



from analytics_toolbox.data_init import data_handles
from analytics_toolbox.machine_learning import data_preparation, modeling
from analytics_toolbox.preprocessing import data_cleaning


# merged dataframes for PLC & Sensor
data_path = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))),'data')
print(data_path)
sr10path_sens = os.path.join(data_path,'SR1-9','2_26Z_RMS_IPSX_RMS_V_IPSX_RMS_GTEMP_Fconveyor_on_2018-06-11_2018-07-12.csv')
sr10path_plc = os.path.join(data_path,'SR1-9','2_PLC-SR1-9belt_speedmotor_currentmotor_freqconveyor_on_2018-06-11_2018-07-12.csv')


sr10_plc = pd.read_csv(sr10path_plc)
sr10_sens = pd.read_csv(sr10path_sens)
# sr9_plc = pd.read_csv('../data/SR-9/2_PLC-SR1-9belt_speedmotor_currentmotor_freqconveyor_on_2018-06-11_2018-07-12.csv')
# sr9_sens = pd.read_csv('../data/SR-9/2_26Z_RMS_IPSX_RMS_V_IPSX_RMS_G,TEMP_Fconveyor_on_2018-06-11_2018-07-12.csv')

motor_df = data_handles.merge_disparate_dfs([data_handles.set_datetime_index(sr10_plc),data_handles.set_datetime_index(sr10_sens)])


# # data preparation

range_input = {'motor_current':[0.0,999],
			   'conveyor_on':[0.5,999]}


motor_df = data_cleaning.range_check_df(motor_df,range_input)

motor_df = data_preparation.scale_data(motor_df)

motor_df = motor_df.dropna(inplace=False)

df_train, df_test = data_preparation.split_train_test(motor_df,train_ratio=3/5)

features_train,target_train = data_preparation.separate_target(df_train,target_colnames=['motor_current'])
features_test,target_test = data_preparation.separate_target(df_test,target_colnames=['motor_current'])

print(features_train.columns)
print(target_train.columns)


# model training
# MultilayerPerceptronFitPredict
est = modeling.MultilayerPerceptronFitPredict(features_train,target_train,features_test,target_test)

res = est - target_test['motor_current']

tracepred = go.Scatter(x=target_test.index,y=est,mode='lines',name='predicted')
tracetruth = go.Scatter(x=target_test.index,y=target_test['motor_current'],mode='lines',name='measured')


trace_res = go.Scatter(x=target_test.index,y=res,mode='lines',name='residuals')

data = [tracepred,tracetruth,trace_res]


layout = go.Layout(
    title='SR1-9: MLP Model Predictions vs Measured: Res Abs Average Error: {}'.format(np.average(abs(res))),
    xaxis=dict(
        title='Time',
        titlefont=dict(
            family='Courier New, monospace',
            size=18,
            color='#7f7f7f'
        )
    ),
    yaxis=dict(
        title='Scaled Current [0,1]',
        titlefont=dict(
            family='Courier New, monospace',
            size=18,
            color='#7f7f7f'
        )
    )
)
fig = go.Figure(data=data,layout=layout)

pltly.plot(fig,filename='MLP_SR9targetrespred')



# model predictions


# visualizations
