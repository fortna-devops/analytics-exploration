## Analytics Exploration

Repository for proof of concept of different approaches to creating analytics for predictive maintenance.

- LSTM (long term short term memory)
- FFT (fast fourier transform)
	- require high sample rate, best suited for local analytics
	- fault detection based on specific frequency amplitudes
	- should be used in steady state operation
- WAVET (wavelet transform)
	- require high sample rate, best suited for local analytics
	- can be used in transient load conditions 
- AAKR (auto associative kernel regression)
- VAR (vector autoregression)
	- assumes linearity
	- use multivariate time series to predict values

