import numpy as np
import scipy.stats as st
import math

# Mean Time Between Failure
MTBF = 500000
# Failure Rate : https://en.wikipedia.org/wiki/Failure_rate  
# Failure_Rate = 1/MTBF
operating_time_of_interest = 120
num_units = 5000
confidence_level = 0.9
simulation_iteration = 10000
# expected_failure_nums = N * Failure_Rate * Within_Time
# safety_stock =  (Maximum daily usage * Maximum lead time in days) – (Average daily usage * Average lead time in days)

"""
------------------------
Exponential Distribution
------------------------
The exponential distribution is one of the widely used continuous distributions. 
It is often used to model the time elapsed between events. 

---------------------
Poisson Distribution 
---------------------
given an equipment population (n), the failure rate of each equipment (λ) 
and a time interval (t) during which no further spares will be available, 
the Poisson distribution can be used to predict the number of failures that will occur, 
 with a certain probability (or confidence), and thus the number of depot spares required for time interval t.
"""



def generate_rv_exponential_distribution(MTBF):
    """
    generate ramdom variable from exponential distribution

    :param MTBF: mean time between failure
    :MTBF type: float

    :returns: time between failure generate from exponential distribution
    :rtype: float
    """
    U = np.random.uniform()
    time_between_failure = math.log(1-np.random.uniform())/(-1/MTBF)
    return time_between_failure


def probability_stock_level(average_number_event, n):
    """
    compute the the probability of failure units is smaller than the stock level based on the confidence level

    :param average_number_event: expected number of failure units
    :average_number_event type: int

    :param n: number of united supported
    :n type: int

    :returns: probability of safe stock
    :rtype: float, between 0 and 1 
    """
    return np.exp(-average_number_event) * (average_number_event)**n / math.factorial(n)

def compute_optimal_stock_level(MTBF, time_interval, num_units, confidence_level):

    """
    compute optimal stock level based on mean time between failure (hours, time interval (hours)
    number of units to be supported and desird confidence

    verify result here: https://reliabilityanalyticstoolkit.appspot.com/poisson_spares_analysis


    :param MTBF: mean time between failure
    :MTBF type: float, unit in hours

    :param time_interval: time interval for observe
    :time_interval type: int, unit in hours 

    :param num_units: number of units used on site 
    :num_units type: int

    :param confidence_level: probability of having few items failure
    :confidence_level: float, number between 0 and 1

    """
    z_score = st.norm.ppf(confidence_level)
    stock = 0
    # simulate TBF based on the expoential distribution
    average_number_event = 1/MTBF * num_units * time_interval

    if average_number_event < 720: 
        try: 
            probability_initial_stock = probability_stock_level(average_number_event, stock)
            while probability_initial_stock < confidence_level:
                stock = stock + 1
                probability_initial_stock = probability_initial_stock + probability_stock_level(average_number_event, stock)
        except OverflowError:
            pass
    else: 
        stock = ((z_score + np.sqrt(z_score**2 + 4 * (average_number_event)))** 2)/4
    return math.ceil(stock)

def run_simulation(num_iterations, MTBF, time_interval, num_units, confidence_level):
    """
    run simulation
    """
    optimal_stock_list = []
    for i in range(num_iterations):
        TBF = generate_rv_exponential_distribution(MTBF)
        average_number_event = 1/TBF * num_units * time_interval
        optimal_stock = compute_optimal_stock_level(TBF,time_interval, num_units, confidence_level)
        optimal_stock_list.append(optimal_stock)

    return optimal_stock_list

def optimal_stock_level_after_simulation(optimal_stock_list):
    """
    compute the average of 5 percentile to 95 percentile for the optimal stock level
    """
    
    len_list = len(optimal_stock_list)
    percentile_5 = math.floor(len_list*0.05)
    percentile_95 = math.ceil(len_list*0.95)
    sorted_list = sorted(optimal_stock_list)
    filtered_list = sorted_list[percentile_5:percentile_95]
    optimal_stock = math.ceil(sum(filtered_list)/len(filtered_list))


    return optimal_stock

optimal_stock_list = run_simulation(simulation_iteration, MTBF, operating_time_of_interest, num_units, confidence_level )
optimal_stock = optimal_stock_level_after_simulation(optimal_stock_list)
import ipdb 
ipdb.set_trace()


