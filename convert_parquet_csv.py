import pandas as pd
import ipdb
from pathlib import Path
import os

# data_dir = Path('Fedex-Louisville-Feb/sensor/')
# # os.mkdir('ELOU-Feb')
# # os.mkdir('ELOU-Feb/sensor')
# for i, parquet_path in enumerate(data_dir.glob('*.parquet')):
#     with open('ELOU-Feb/sensor/table_{}.csv'.format(i), 'w') as csv_file:
#         df = pd.read_parquet(parquet_path)
#         df.to_csv(csv_file, index=False)
# ipdb.set_trace()


data_dir = Path('Fedex-Louisville-Feb/plc/')
os.mkdir('ELOU-Feb/plc')
for i, parquet_path in enumerate(data_dir.glob('*.parquet')):
    with open('ELOU-Feb/plc/table_{}.csv'.format(i), 'w') as csv_file:
        df = pd.read_parquet(parquet_path)
        df.to_csv(csv_file, index=False)
ipdb.set_trace()
