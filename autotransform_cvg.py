#!/usr/bin/env python3
"""
auto transfrom local cvg data and upload to aws
..author: Coco Wu <jwu@mhsinc.net>

"""
from analytics_toolbox.data_init import data_writing
import pyarrow as pa
import pyarrow.parquet as pq
import boto3
from botocore.exceptions import ClientError
import pandas as pd
import ipdb

# read data
raw_data = pd.read_csv('last21days.csv')
location = pd.read_csv('sensor_location.csv')
tags = pd.read_csv('tags.csv', index_col='sensor')


def inverse_lookup(lookupvalue, dataframe):
    """
    look up the index and column name by the actual values inside dataframe
    dataframe has duplicate value will only give one index and one column
    """
    for i in dataframe.columns:
        for j in dataframe.index:
            if dataframe.loc[j][i] == lookupvalue:
                return (j, i)


sensor_table_dict = {}
for column in raw_data.columns:
    if column == 'UTC_minus_4':
        continue
    sensor_num, column_name = inverse_lookup(column, tags)
    if sensor_num in sensor_table_dict.keys():
        sensor_table = pd.DataFrame()
        sensor_table[column_name] = raw_data[column]
        sensor_table_dict[sensor_num] = pd.concat([sensor_table_dict[sensor_num], sensor_table[column_name]], axis=1)
    else:
        sensor_table = pd.DataFrame()
        sensor_table['timestamp'] = raw_data['UTC_minus_4']
        sensor_table['sensor'] = sensor_num
        sensor_table[column_name] = raw_data[column]
        sensor_table_dict[sensor_num] = sensor_table
# downsample hour
# for sensor_num, sensor_values in sensor_table_dict.items():
#     sensor_table_dict[sensor_num] = sensor_table_dict[sensor_num].set_index(
#         pd.DatetimeIndex(sensor_table_dict[sensor_num]['timestamp'])).resample('60T').mean().dropna()
#     sensor_table_dict[sensor_num]['timestamp'] = sensor_table_dict[sensor_num].index
#     sensor_table_dict[sensor_num]['sensor'] = sensor_table_dict[sensor_num]['sensor'].astype(int)
###

all_data = sensor_table_dict.values()
result = pd.concat(all_data)
result['gateway'] = 'DXM1'
result['port'] = '/dev/dxm100'
result['rms_velocity_z'] = result['rms_velocity_z'] / 10000
result['temperature'] = result['temperature'].round(2)
result['rms_velocity_x'] = result['rms_velocity_x'] / 10000
result['peak_acceleration_z'] = 999.9
result['peak_acceleration_x'] = 999.9
result['peak_frequency_z'] = 999.9
result['peak_frequency_x'] = 999.9
result['rms_acceleration_z'] = result['rms_acceleration_z'] / 1000
result['rms_acceleration_x'] = result['rms_acceleration_x'] / 1000
result['kurtosis_z'] = 999.9
result['kurtosis_x'] = 999.9
result['crest_acceleration_z'] = 999.9
result['crest_acceleration_x'] = 999.9
result['peak_velocity_z'] = result['peak_velocity_z'] / 10000
result['peak_velocity_x'] = result['peak_velocity_x'] / 10000
result['hf_rms_acceleration_z'] = 999.9
result['hf_rms_acceleration_x'] = 999.9
result['timestamp'] = pd.to_datetime(result['timestamp'])
result['sensor'] = result['sensor'].astype(str)
result = result.set_index(pd.DatetimeIndex(result['timestamp']))
dw = data_writing.DataWriter(s3_bucket_name='mhspredict-site-dhl-cincinnati')
dw.write_parquet_ts_sensor_data(result, table_name='sensor_minute', dir_name='dhl-cincinnati/sensor-minute')
