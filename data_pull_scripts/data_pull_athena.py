from datetime import datetime
import pytest
import pandas as pd
import numpy as np
import pdb




from analytics_toolbox.data_init import data_retrieval
from analytics_toolbox.units_manager import Q_


S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

aqe = data_retrieval.AthenaQueryEngine(s3_bucket_name=S3BUCKET_NAME)

sensor_tags = [
	'rms_velocity_z',
	'temperature',
	'rms_velocity_x',
	'peak_acceleration_z',	
	'peak_acceleration_x',
	'peak_frequency_z',
	'peak_frequency_x',
	'rms_acceleration_z',
	'rms_acceleration_x',
	'kurtosis_z',
	'kurtosis_x',
	'crest_acceleration_z',
	'crest_acceleration_x',
	'peak_velocity_z',
	'peak_velocity_x',
	'hf_rms_acceleration_z',
	'hf_rms_acceleration_x'
	]

plc_tags = ["belt_speed",
"vfd_current",
"vfd_frequency",
"vfd_voltage",
"vfd_fault_code",
"sorter_induct_rate",
"sorter_reject_rate",
"status"]


# t1 = datetime(day=5,month=8,year=2018,hour=12)
# t2 = datetime(day=6,month=9,year=2018,hour=12)
# timetuple = (t1,t2)
# df_sensors = aqe.execute_timeseries_data_query(taglist=sensor_tags,
# 	timerange_tuple=timetuple,
# 	return_as_df=True)

# df_plcs = aqe.execute_timeseries_data_query(taglist=plc_tags,
# 	timerange_tuple=timetuple,
# 	return_as_df=True)

# df_sensors.to_csv('fedex_sensor_one_month.csv')
# df_plcs.to_csv('fedex_plc_one_month.csv')

# S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

# aqe = data_retrieval.AthenaQueryEngine(s3_bucket_name=S3BUCKET_NAME)

# tags = ['temperature','vfd_fault_code','vfd_current','rms_velocity_x']


# t1 = datetime(day=16,month=8,year=2018,hour=12,minute=30)
# t2 = datetime(day=16,month=8,year=2018,hour=12,minute=33)
# timetuple = (t1,t2)
# df = aqe.execute_timeseries_data_query(taglist=tags,
# 	timerange_tuple=timetuple,
# 	conveyor='SS1-4',
# 	equip='G',
# 	return_as_df=True)

# print(df)

S3BUCKET_NAME = 'mhspredict-site-dhl-miami'

aqe = data_retrieval.AthenaQueryEngine(s3_bucket_name=S3BUCKET_NAME)

tags = ['temperature','rms_velocity_x','vfd_current']


conveyors = [
"PF_F1_2",
"T2_F2_1", 
"XR_F1_2PT", 
"T2_F1_4PT", 
"T2_F1_3", 
"T2_F1_2PT", 
"T2_F1_1", 
"PF_F1_5PT", 
"LF_F1_5PT", 
"LF_F1_4", 
"LF_F1_3PT", 
"PF_F1_4", 
"PF_F1_3PT", 
"LF_F1_2", 
"LF_F1_1", 
"PF_F2_3PT", 
"PF_F2_4", 
"T1_F1_2PT", 
"PF_F1_6", 
"T1_F1_5", 
"T1_F1_4PT", 
"HC_F1_2PT", 
"HC_F1_3PT", 
"PF_F1_7PT", 
"LF_F1_7PT", 
"T2_F1_6PT", 
"T2_F2_2PT"
]


for c in conveyors[:1]:

	cname = c.replace('_','-')
	print(cname)


	t1 = datetime(day=16,month=9,year=2018,hour=12,minute=30)
	t2 = datetime(day=16,month=9,year=2018,hour=12,minute=31)
	timetuple = (t1,t2)
	df = aqe.execute_timeseries_data_query(taglist=tags,
		timerange_tuple=timetuple,
		conveyor=cname,
		equip='all',
		return_as_df=True)

	df.to_csv('{c}_alldata.csv'.format(c=c))


# aqe = data_retrieval.AthenaQueryEngine(s3_bucket_name=S3BUCKET_NAME)

# q = """SELECT "sensor","temperature_thr_r","temperature_thr_y" FROM \"{}\".locations where sensor!= 'sensor' LIMIT 3""".format(S3BUCKET_NAME)

# df = aqe.execute_generic_query(
# 	raw_query_string=q,
# 	table_name='locations',
# 	return_as_df=True
# )

# print(df)

# S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

# aqe = data_retrieval.AthenaQueryEngine(s3_bucket_name=S3BUCKET_NAME)

# q = """SELECT "sensor","temperature_thr_r","temperature_thr_y" FROM \"{}\".locations where sensor!= 'sensor' LIMIT 3""".format(S3BUCKET_NAME)

# df = aqe.execute_generic_query(
# 	raw_query_string=q,
# 	table_name='locations',
# 	return_as_df=True
# )

