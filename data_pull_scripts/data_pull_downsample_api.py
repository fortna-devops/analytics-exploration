
"""
A script to pull 120 second resolution data for multiple days.
Outputs wide format csvs

:author: Olutosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
import logging
import json
import requests
from datetime import datetime, timedelta
import pdb

import os



import pandas as pd
from pandas.io.json import json_normalize 



from analytics_toolbox.data_init import data_retrieval


# create logger with 'spam_application'
logger = logging.getLogger('data_pull')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('datapull.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


def create_metric_query(list_tags):
	"""
	creates correct string format for api query

	:list_tags: tag names of interest
	:type: list

	:returns: tag names joints by %2C (uri encoding hex)
	:rtype: str
	"""

	return "%2C".join(list_tags)

def make_dt_obj(dt_str):
	"""
	creates a datetime object from formatted string timestamp

	:return: python datetime object corresponding to date
	:rtype: datetime object
	"""

	return datetime.strptime(dt_str, '%Y-%m-%dT%H:%M:%S.%fZ')

def make_dt_str(dt_obj):
	"""
	create a formatted string from datetime object

	:return: date time string of format '%Y-%m-%dT%H:%M:%S.%fZ'
	:rtype: str
	"""

	return dt_obj.strftime('%Y-%m-%dT%H:%M:%S.%fZ')




URL = "https://cjt8zux7vl.execute-api.us-east-1.amazonaws.com/prod/data"
BEARER_TOKEN = "eyJraWQiOiJvbVRSMzRcL3hjdHQ0Y3NXQmNVZytQVlV5Z25sdlZGWG9ZYTg2RkowWlpjWT0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiZ1JNREZMUjVyWm5INGIzRXB0VU9IUSIsInN1YiI6Ijg1NzhjNDIzLTk5ZmEtNDBiYS1iOTM1LTM3ZWY2YmNkMTY1NiIsImF1ZCI6IjVkdXV2NDQxOGJqYTVlbjlkZDM5a2RrNm1zIiwiY29nbml0bzpncm91cHMiOlsiRmVkRXgiLCJESEwiLCJBRE1JTiJdLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1Mzc3NTMwNjMsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX2VFb21iaWRnRyIsImNvZ25pdG86dXNlcm5hbWUiOiJvc29udXlpIiwiZXhwIjoxNTM3NzU2NjYzLCJpYXQiOjE1Mzc3NTMwNjMsImVtYWlsIjoib2x1d2F0b3NpbnNvbnV5aUBtaHNpbmMubmV0In0.mYhuqtMUW7r2JVgMFJkrvblADXU-Iip81WAJLAJ4i9P95215q3HReH3wB-jXvl2rotl_tpUJG1NrGl9eFt6jd2neMtYVIXSqfYUzzp_a1G63fRfYIVPHsgmilkK5wlmbrB92VYtth2zhSNYZotNBG6cF5vhi6nTp66s_VFTaIlKu_B3OD5WA7QCI8YCwGkKouuRF0NX1NMOIjNPlzIk7ZKkNYbcQqQEexcpFI9lAgeUQVeCy73KCtf9gstf6daKFXbnvmw8BNNym-Ux4wt2a8_eBxqfgyjbu7F-SwtKjhCd2ENdNFhblmkqfit5n-LDZ1wyicUihYMhk2SO5FaDvtA"
HEADERS = {
    'Cache-Control': "no-cache",
    'Authorization' : "Bearer {}".format(BEARER_TOKEN)
    }


site_num = "2"

start_time = "2018-06-11T13:51:42.944Z"
end_time = "2018-07-12T13:51:42.944Z"

###### Pull for PLC data #####


# sensor = "PLC-SR2-8"

# taglist = ["belt_speed",
# 		   "motor_current",
# 		   "motor_freq",
# 		   "conveyor_on"]

# query_taglist = create_metric_query(taglist)

# querys_params = {"site":site_num,
# 			   "sensor":sensor,
# 			   "metric":query_taglist,
# 			   "time1":start_time,
# 			   "time2":end_time,
# 			   "convert2metric":"0"}


##### Pull for vibration data ######

# sensor = "33"

# taglist = ["Z_RMS_IPS","X_RMS_V_IPS","X_RMS_G,TEMP_F","conveyor_on"]

# query_taglist = create_metric_query(taglist)

# querys_params = {"site":site_num,
# 			   "sensor":sensor,
# 			   "metric":query_taglist,
# 			   "time1":start_time,
# 			   "time2":end_time,
# 			   "convert2metric":"0"}





# response = requests.request("GET", url, headers=headers, params=querys_params)

# json_obj = json.loads(response.text)

# if 'Unauthorized' in json_obj.values():
# 	raise ValueError("Unauthorized response, update bearer token")

# json_data = json_obj["data"]

# data_df = json_normalize(json_data)
# data_df.columns = [col.replace('values.','') for col in data_df.columns]
# data_df.to_csv("data/{}{}_{}_{}.csv".format(
# 				sensor,"".join(taglist),start_time[:10],end_time[:10]),
# 			   index=False)

def complete_data_pull(start_time,end_time,querys_params):


	# the top level start time of the whole query
	total_start_obj = make_dt_obj(start_time)
	total_end_obj = make_dt_obj(end_time)
	tdelta = total_end_obj - total_start_obj
	days = tdelta.days

	sensor = querys_params["sensor"]

	# holder data frame for putting data all together
	aggy_df = pd.DataFrame()

	logger.info("****New Pull***\nQuery Parameters:{}\n".format(querys_params))

	for day_offset in range(days):
		# this will start at an offset of 0
		new_start_time_obj = total_start_obj + timedelta(days=day_offset)
		new_start_time_str = make_dt_str(new_start_time_obj)


		new_end_time_obj = new_start_time_obj + timedelta(days=1)
		new_end_time_str = make_dt_str(new_end_time_obj)

		querys_params["time1"] = new_start_time_str
		querys_params["time2"] = new_end_time_str

		logger.info("Pulling from {t1} to {t2}".format(t1=new_start_time_str,t2=new_end_time_str))

		response = requests.request("GET", URL, headers=HEADERS, params=querys_params)
		response_json = json.loads(response.text)

		if 'Unauthorized' in response_json.values():
			raise ValueError("Unauthorized response, update bearer token")

		try:
			data_df = json_normalize(response_json["data"])
		except:
			logger.error('Something was wrong with trying to get data from {}'.format(response_json))
			return

		data_df.columns = [col.replace('values.','') for col in data_df.columns]

		aggy_df = aggy_df.append(data_df)

		site_names = {'1':'DHL_Miami','2':'FedEx_Louisville'} 


	conveyor_name = querys_params["conveyor_name"]
	site_readable_name = site_names[querys_params["site"]]
	outdir = './data/{s}/{c}'.format(s=site_readable_name,c=conveyor_name)
	if not os.path.exists(outdir):
		os.makedirs(outdir)





	aggy_df.to_csv("data/{site}/{cname}/{site}{cname}_{etype}_{stime}_{etime}.csv".format(
					site=site_readable_name,
					cname=conveyor_name,
					etype=querys_params["equipment_type"],
					stime=start_time[:10],
					etime=end_time[:10]),
				   index=False)



site_num = "1"

start_time = "2018-09-05T13:51:42.944Z"
end_time = "2018-09-23T13:51:42.944Z"

# sensor = "33"

taglist_sen = ["Z_RMS_IPS","X_RMS_V_IPS","X_RMS_G,TEMP_F"]

taglist_plc = ["belt_speed",
		   "motor_current",
		   "motor_freq",
		   "conveyor_on"]

query_taglist_plc = create_metric_query(taglist_plc)

query_taglist_sen = create_metric_query(taglist_sen)

# querys_params = {"site":site_num,
# 			   "sensor":sensor,
# 			   "metric":query_taglist,
# 			   "time1":start_time,
# 			   "time2":end_time,
# 			   "convert2metric":"0"}


S3BUCKET_NAME = 'mhspredict-site-dhl-miami'

aqe = data_retrieval.AthenaQueryEngine(s3_bucket_name=S3BUCKET_NAME)

conveyors = [
"PF_F1_2",
"T2_F2_1", 
"XR_F1_2PT", 
"T2_F1_4PT", 
"T2_F1_3", 
"T2_F1_2PT", 
"T2_F1_1", 
"PF_F1_5PT", 
"LF_F1_5PT", 
"LF_F1_4", 
"LF_F1_3PT", 
"PF_F1_4", 
"PF_F1_3PT", 
"LF_F1_2", 
"LF_F1_1", 
"PF_F2_3PT", 
"PF_F2_4", 
"T1_F1_2PT", 
"PF_F1_6", 
"T1_F1_5", 
"T1_F1_4PT", 
"HC_F1_2PT", 
"HC_F1_3PT", 
"PF_F1_7PT", 
"LF_F1_7PT", 
"T2_F1_6PT", 
"T2_F2_2PT"
]


for c in conveyors[12:]:

	cname = c.replace('_','-')
	print(cname)



	c_sensors_df = aqe.get_conveyor_sensors(cname)
	print(c_sensors_df)

	q_params = []
	# sensors = ["28","PLC-SR1-10","26","PLC-SR1-9"]
	sensors = list(c_sensors_df['sensor'])
	equips = list(c_sensors_df['location'])
	# TODO: ask Nik/Lyudmila why the PLC sensor names are not in the lcoation table
	sensors.append('PLC-{conveyor_name}'.format(conveyor_name=cname))
	equips.append('PLC')

	# pdb.set_trace()

	for s,equip in zip(sensors,equips):
		params = {}
		params["site"] = site_num
		params["sensor"] = s
		if "PLC" in str(s).upper():
			params["metric"] = query_taglist_plc
		else:
			params["metric"] = query_taglist_sen
		params["time1"] = start_time
		params["time2"] = end_time
		params["convert2metric"] = "0"
		params["equipment_type"] = equip
		params["conveyor_name"] = cname

		q_params.append(params)


	for q_param in q_params:
		complete_data_pull(start_time,end_time,q_param)









