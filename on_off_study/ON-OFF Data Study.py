
# coding: utf-8

# In[210]:


from analytics_toolbox.data_init import data_retrieval, data_handles
from datetime import datetime
import numpy as np
import pandas as pd
from datetime import timedelta
import calendar
import random
from scipy.optimize import curve_fit
import warnings
import numpy as np
import pandas as pd
import scipy.stats as st
import statsmodels as sm
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100


# ## Which table should we query to calculate number of on-off transitions? 

# In[211]:


S3BUCKET_NAME = 'mhspredict-site-dhl-miami'


# In[212]:


t1 = datetime(day=1, month=3, year=2019, hour=0)
t2 = datetime(day=2, month=3, year=2019, hour=23)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[213]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[214]:


tags = [
    'temperature',
    'rms_velocity_x',
    'rms_velocity_z',
    'rms_acceleration_x',
    'rms_acceleration_z',
    'kurtosis_x',
    'kurtosis_z',
    'crest_acceleration_z',
    'crest_acceleration_x'
]


# In[215]:


conveyor_list = dqe.get_conveyors(s3_bucket_name=S3BUCKET_NAME)


# In[216]:


conveyor_list.remove('Ambient Environment')


# In[217]:


def construct_new_cols(data):
    newcols = []
    for colname in list(data.columns):
        try: 
            newcol = colname.split('.')[1]
        except:
            newcol = colname
        newcols.append(newcol)
    return newcols
    


# In[218]:


def on_off_status(data):
    if data['rms_acceleration_x'] > 0.02:
        return 'ON'
    else:
        return 'OFF'


# In[219]:


def plc_on_off_status(data):
    if data['rpm'] > 100:
        return 'ON'
    else:
        return 'OFF'


# In[220]:


def num_on_off_trans(sensor_list, d1, d2, ts1, ts2):
    if len(sensor_list)>1:
        raw_query = "SELECT * FROM sensor_minute where sensor in {sl} and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(sl=tuple(sensor_list), d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
    else:
        raw_query = "SELECT * FROM sensor_minute where sensor = '{s1}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(s1=sensor_list[0], d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
    data = dqe.execute_generic_query(raw_query, return_as_df=True)
    if data.empty:
        num_on_off = 'NA'
    else:
        data = data[data['sensor'] == float(list(sensor_list)[0])].reset_index()
        data['status'] = data.apply(on_off_status, axis = 1)
        data = data.sort_values(by='timestamp')
        num_on_off = sum(data['status'] != data['status'].shift()) - 1
    return num_on_off   


# In[221]:


def num_on_off_trans_plc(conveyor,d1,d2,ts1,ts2):
    raw_query = "SELECT * FROM plc where sensor = 'PLC-{c}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(c = conveyor, d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
    data = dqe.execute_generic_query(raw_query, return_as_df=True)
    if data.empty:
        num_on_off = 'NA'
    else:
        data['status'] = data.apply(plc_on_off_status, axis = 1)
        data = data.sort_values(by='timestamp')
        num_on_off = sum(data['status'] != data['status'].shift()) - 1 
    return num_on_off  


# In[222]:


def gauss(x,mu,sigma,A):
    return A*exp(-(x-mu)**2/2/sigma**2)


# In[223]:


def bimodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)


# In[224]:


def calculate_bimodal_dis_params(data):
    y,x,_=hist(data,100,alpha=.3,label='data')
    x=(x[1:]+x[:-1])/2 # for len(x)==len(y)
    params,cov=curve_fit(bimodal,x,y)
    sigma=sqrt(diag(cov))

    params = pd.DataFrame(data={'params':params,'sigma':sigma},index=bimodal.__code__.co_varnames[1:])
    return params


# In[163]:


# Calculate distbution params for plc
for conveyor in ['T1-F1-5']: 
    raw_query = "select * from plc where sensor = 'PLC-{c}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(c=conveyor, d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
    data = dqe.execute_generic_query(raw_query, return_as_df=True)
    if data.empty: 
        continue
    data = data['rpm']


# In[67]:


# check if every conveyor applies to on-off rule (rms_acceleration > 0.02)
for conveyor in conveyor_list: 
    temp_data_dict = dqe.execute_timeseries_data_query(
    taglist=tags,
    timerange_tuple=timetuple,
    conveyor=conveyor,
    equip="all",
    return_as_df=True)
    
    for equipment, df in temp_data_dict.items():
        #rename columns
        newcols = construct_new_cols(df)
        df.columns = newcols
        plt.hist(df['rms_acceleration_x'], bins='auto')
        plt.savefig("{c}_{e}_rms_accerleration_distribution.png".format(c=conveyor, e = equipment))
        plt.close()


# In[382]:


# graph on-off distribution for plc rpm tag
for conveyor in conveyor_list: 
    raw_query = "select * from plc where sensor = 'PLC-{c}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(c=conveyor, d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
    data = dqe.execute_generic_query(raw_query, return_as_df=True)
    if data.empty: 
        continue
    plt.hist(data['rpm'], bins='auto')
    plt.savefig("{c}_plc_rpm_distribution.png".format(c=conveyor))
    plt.close()


# ### single conveyor analysis - Raw Sensor Table

# In[225]:


temp_data_dict = dqe.execute_timeseries_data_query(
    taglist=tags,
    timerange_tuple=timetuple,
    conveyor=conveyor_list[0],
    equip="all",
    return_as_df=True)


# In[226]:


data1 = temp_data_dict[list(temp_data_dict.keys())[0]]
data2 = temp_data_dict[list(temp_data_dict.keys())[1]]


# In[227]:


newcols = construct_new_cols(data1)


# In[228]:


data1.columns = newcols


# In[229]:


data1.head()
sensor_num = data1['sensor'].unique()


# In[230]:


data1['status'] = data1.apply(on_off_status, axis = 1)


# In[231]:


data1['status'].value_counts()
sensor_on_percentage = data1['status'].value_counts()['ON']/len(data1)
print(sensor_on_percentage)


# In[268]:


conveyor_list[0]


# In[315]:


# d1 = t1.strftime('%Y-%m-%d')
# d2 = t2.strftime('%Y-%m-%d')
# ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
# ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
sensor_conveyor = 'PLC-'+ conveyor_list[1]


# In[316]:


q_location = "SELECT * from location where conveyor = '{c}'".format(c = conveyor_list[1])
res_location = dqe.execute_generic_query(q_location, return_as_df = True)


# In[317]:


res_location


# In[318]:


q_sensor = "SELECT * FROM sensor where sensor = '60' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
res_sensor = dqe.execute_generic_query(q_sensor, return_as_df=True)


# In[319]:


res_sensor['status'] = res_sensor.apply(on_off_status, axis = 1)
res_sensor['status'].value_counts()
sensor_on_percentage = res_sensor['status'].value_counts()['ON']/len(res_sensor)
print(sensor_on_percentage)


# In[320]:


res_sensor = res_sensor.sort_values(by='timestamp')
len(res_sensor) - (res_sensor['status'].eq(res_sensor['status'].shift()).sum() + 1)


# In[321]:


# get plc data for the same conveyor to verify
q_plc = "SELECT * FROM plc where sensor = '{c}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(c=sensor_conveyor, d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
res_plc = dqe.execute_generic_query(q_plc, return_as_df=True)


# In[303]:


### unfortunately.... miami plc status is not accurate........T^T
res_plc['status'].unique() 


# In[304]:


# use rpm instead... as we can see the value clearly separate into two parts. around 0 and around 1800
res_plc['rpm'].value_counts()


# In[283]:


# use rpm > or < than 100 as the separation points for status on and off
plc_on = res_plc[res_plc['rpm'] > 100]
plc_on_percentage = len(plc_on)/ len(res_plc)
print ('the plc on percentage is {}'.format(plc_on_percentage))
print ('the sensor on percentage is {}'.format(sensor_on_percentage))


# In[322]:


res_plc['status'] = res_plc.apply(plc_on_off_status, axis = 1)


# In[238]:


## order by timestamp first
data1 = data1.sort_values(by='timestamp')


# In[102]:


## calculate how many status change (two ways)

len(data1) - (data1['status'].eq(data1['status'].shift()).sum() + 1)
sum(data1['status'] != data1['status'].shift()) - 1


# In[103]:


(sum(data1['status'] != data1['status'].shift()) - 1)/len(data1)


# In[323]:


## order by timestamp first
res_plc = res_plc.sort_values(by='timestamp')


# In[324]:


sum(res_plc['status'] != res_plc['status'].shift()) - 1


# In[308]:


(sum(res_plc['status'] != res_plc['status'].shift()) - 1)/len(res_plc)


# ### single conveyor analysis - Sensor Minute Table
# 

# In[260]:


conveyor = conveyor_list[0]
print ("Following start the analysis for conveyor {c}".format(c = conveyor))


# In[325]:


q_sminute = "SELECT * FROM sensor_minute where sensor = '60' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
res_minute = dqe.execute_generic_query(q_sminute, return_as_df=True)


# In[326]:


res_minute['status'] = res_minute.apply(on_off_status, axis = 1)
res_minute['status'].value_counts()
sensor_on_minute_percentage = res_minute['status'].value_counts()['ON']/len(res_minute)
print(sensor_on_minute_percentage)


# In[327]:


res_minute = res_minute.sort_values(by='timestamp')
len(res_minute) - (res_minute['status'].eq(res_minute['status'].shift()).sum() + 1)


# In[79]:


# t1 = datetime(day=10, month=1, year=2019, hour=0)
# t2 = datetime(day=10, month=1, year=2019, hour=6)
# timetuple = (t1, t2)
# dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[80]:


# d1 = t1.strftime('%Y-%m-%d')
# d2 = t2.strftime('%Y-%m-%d')
# ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
# ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[81]:


def calculate_on_percentage(conveyor):
    on_dict = {}
    # query from plc
    q_plc = "SELECT * FROM plc where sensor = 'PLC-{c}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(c = conveyor, d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
    res_plc = dqe.execute_generic_query(q_plc, return_as_df=True)
    if res_plc.empty:
        plc_on_percentage = 'NA'
    else:
        res_plc['status'] = res_plc.apply(plc_on_off_status, axis = 1)
        plc_on_percentage = round(res_plc['status'].value_counts()['ON']/len(res_plc),4)
    # query location
    q_sensor_list = "SELECT sensor, location, equipment FROM location where conveyor = '{}'".format(conveyor)
    sensorlist = dqe.execute_generic_query(q_sensor_list, return_as_df=True)
    sensorlist['sensor'] = sensorlist['sensor'].astype('str')
    sensor_list = [x for x in list(sensorlist['sensor']) if not x.startswith('PLC')]
    loc_list = [x for x in list(sensorlist['location']) if not x.startswith('PLC')]
    # query sensor minute table
    if len(sensor_list) > 1: 
        q_data_minute = "SELECT * FROM sensor_minute where sensor in {sl} and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(sl=tuple(sensor_list), d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
        res_data_minute = dqe.execute_generic_query(q_data_minute, return_as_df=True)
    else: 
        q_data_minute = "SELECT * FROM sensor_minute where sensor  = '{s1}' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(s1=sensor_list[0], d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
        res_data_minute = dqe.execute_generic_query(q_data_minute, return_as_df=True)
    if res_data_minute.empty:
        sensor_on_percentage = 'NA'
    else: 
        res_data_minute['sensor'] = res_data_minute['sensor'].astype('str')
        res_data_minute_eq = pd.merge(res_data_minute,sensorlist, on = 'sensor')
        res_data_minute_eq['status'] = res_data_minute_eq.apply(on_off_status, axis = 1)
        loc_order_list = []
        sensor_on_percentage_list = []
        for location in loc_list: 
            res_data_minute_eq_one = res_data_minute_eq[res_data_minute_eq['location'] == location]
            if len(res_data_minute_eq_one['status'].value_counts().index.tolist()) > 1:
                sensor_on_percentage = round(res_data_minute_eq_one['status'].value_counts()['ON']/len(res_data_minute_eq_one),4)
            else:
                sensor_on_percentage = 0
            loc_order_list.append(location)
            sensor_on_percentage_list.append(sensor_on_percentage)  
    on_dict['sensor_on_percentage'] = sensor_on_percentage_list
    on_dict['location'] = loc_order_list
    on_dict['plc_on_percentage'] = [plc_on_percentage] * len(loc_order_list)
    on_dict['conveyor'] = [conveyor] * len(loc_order_list)
    return on_dict


# In[82]:


frames = []
for conveyor in conveyor_list:
    on_dict = calculate_on_percentage(conveyor=conveyor)
    dict2pd = pd.DataFrame(on_dict)
    frames.append(dict2pd)
result_6hour = pd.concat(frames)
    


# In[63]:


result.groupby(['location'])[['sensor_on_percentage', 'plc_on_percentage']].mean()                           


# In[75]:


result_filterNA = result[result['plc_on_percentage'] != 'NA'].reset_index()


# In[76]:


result_filterNA['plc_on_percentage'] = result_filterNA['plc_on_percentage'].astype('float')


# In[78]:


result_filterNA.groupby('location').mean()


# In[83]:


result_6hour_filterNA = result_6hour[result_6hour['plc_on_percentage'] != 'NA'].reset_index()
result_6hour_filterNA['plc_on_percentage'] = result_6hour_filterNA['plc_on_percentage'].astype('float')
result_6hour_filterNA.groupby('location').mean()


# In[70]:


res_data1_minute['status'].value_counts()
sensor_on_minute_percentage = res_data1_minute['status'].value_counts()['ON']/len(res_data1_minute)
print('the status on percentage using sensor minute data is {}'.format(sensor_on_minute_percentage))
print ('the sensor on percentage using sensor data is {}'.format(sensor_on_percentage))
print ('the plc on percentage is {}'.format(plc_on_percentage))


# In[106]:


## sort by timestamp first
res_data1_minute = res_data1_minute.sort_values(by='timestamp')


# In[107]:


## calculate how many status change (two ways)
len(res_data1_minute) - (res_data1_minute['status'].eq(res_data1_minute['status'].shift()).sum() + 1)
sum(res_data1_minute['status'] != res_data1_minute['status'].shift()) - 1


# In[108]:


(sum(res_data1_minute['status'] != res_data1_minute['status'].shift()) - 1)/len(res_data1_minute)


# ### Sensor Hour Table 

# In[328]:


q_hour = "SELECT * FROM sensor_hour where sensor = '60' and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
res_hour = dqe.execute_generic_query(q_hour, return_as_df=True)


# In[329]:


res_hour['status'] = res_hour.apply(on_off_status, axis = 1)
res_hour['status'].value_counts()
sensor_on_hour_percentage = res_hour['status'].value_counts()['ON']/len(res_hour)
print(sensor_on_hour_percentage)


# In[330]:


res_hour = res_hour.sort_values(by='timestamp')
len(res_hour) - (res_hour['status'].eq(res_hour['status'].shift()).sum() + 1)


# ## Which time frame makes the most sense to determine issues on the basis of number of state transitions between ON/OFF?
# 
# ## 1 hour, 2 hour, 6 hour, 12 hour, 24 hour?

# ### query data for 1 hour 

# In[8]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=1, month=1, year=2019, hour=1)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[9]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[10]:


q_data_minute_1hr = "SELECT * FROM sensor_minute where sensor in {sl} and date between date '{d1}' and date '{d2}' and timestamp between timestamp '{ts1}' and timestamp '{ts2}'".format(sl=sensor_list, d1 = d1, d2 = d2, ts1 = ts1, ts2 = ts2)
res_data_minute_1hr = dqe.execute_generic_query(q_data_minute_1hr, return_as_df=True)


# In[119]:


res_data1_minute_1hr = res_data_minute_1hr[res_data_minute_1hr['sensor'] == float(sensor_num[0])].reset_index() 


# In[121]:


res_data1_minutle_1hr['status'] = res_data1_minute_1hr.apply(on_off_status, axis = 1)


# In[123]:


res_data1_minute_1hr = res_data1_minute_1hr.sort_values(by='timestamp')


# In[125]:


sum(res_data1_minute_1hr['status'] != res_data1_minute_1hr['status'].shift()) - 1


# In[131]:


num_on_off_trans(sensor_list, d1, d2, ts1, ts2)


# ### query data for 2 hour 

# In[147]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=1, month=1, year=2019, hour=2)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[133]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[134]:


num_on_off_trans(sensor_list, d1, d2, ts1, ts2)


# ### query data for 3 hour 

# In[135]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=1, month=1, year=2019, hour=3)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[136]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[137]:


num_on_off_trans(sensor_list, d1, d2, ts1, ts2)


# ### query data for 5 hour 

# In[140]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=1, month=1, year=2019, hour=5)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[141]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[142]:


num_on_off_trans(sensor_list, d1, d2, ts1, ts2)


# ### query data for 12 hour

# In[144]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=1, month=1, year=2019, hour=12)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[145]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[146]:


num_on_off_trans(sensor_list, d1, d2, ts1, ts2)


# ### query data for 24 hours 

# In[148]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=2, month=1, year=2019, hour=0)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[149]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[150]:


num_on_off_trans(sensor_list, d1, d2, ts1, ts2)


# In[46]:


t1 = datetime(day=1, month=1, year=2019, hour=0)
t2 = datetime(day=1, month=1, year=2019, hour=23)
timetuple = (t1, t2)
d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[ ]:


dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


# In[18]:


conveyor_list


# ## automatic run all conveyors to calculate number of on off transitions - Motor

# In[74]:


f = open("DHL_miami_Bearing_Jan15.txt", "w")
f.write("conveyor, 1hr_transition, 2hr_transition, 3hr_transition, 5hr_transition, 12hr_transition, 24hr_transition")
f.write("\n")
conveyor_count = 0
for conveyor in conveyor_list:
    conveyor_count = conveyor_count + 1
    # Check if it has equipment or not 
    equipment_df = dqe.get_conveyor_sensors(conveyor = conveyor)    
    if 'B' in  equipment_df.location.unique():
        f.write("{}".format(conveyor))
        hours_list = [1,2,3,5,12,24]
        for hour in hours_list: 
        # construct timetuple to query 
            t1 = datetime(day=15, month=1, year=2019, hour=0)
            t2 = t1 + timedelta(hours = hour)
            timetuple = (t1, t2)
            d1 = t1.strftime('%Y-%m-%d')
            d2 = t2.strftime('%Y-%m-%d')
            ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
            ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
            # run query 
            sensor_list = list()
            sensor_list.append(str(temp_data_dict['sensor'][0]))
            result = num_on_off_trans(sensor_list, d1, d2, ts1, ts2)
            f.write(",{}".format(result))
        f.write("\n")
    print ('finished {c}/{d}'.format(c = conveyor_count, d = len(conveyor_list)))
f.close()


# In[79]:


f = open("DHL_miami_PLC_Feb1.txt", "w")
f.write("conveyor, 1hr_transition, 2hr_transition, 3hr_transition, 5hr_transition, 12hr_transition, 24hr_transition")
f.write("\n")
conveyor_count = 0
for conveyor in conveyor_list:
    conveyor_count = conveyor_count + 1
    f.write("{}".format(conveyor))
    hours_list = [1,2,3,5,12,24]
    for hour in hours_list: 
    # construct timetuple to query 
        t1 = datetime(day=1, month=2, year=2019, hour=0)
        t2 = t1 + timedelta(hours = hour)
        timetuple = (t1, t2)
        d1 = t1.strftime('%Y-%m-%d')
        d2 = t2.strftime('%Y-%m-%d')
        ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
        ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
        # run on off transition function
        result = num_on_off_trans_plc(conveyor, d1, d2, ts1, ts2)
        f.write(",{}".format(result))
    f.write("\n")
    print ('finished {c}/{d}'.format(c = conveyor_count, d = len(conveyor_list)))
f.close()


# In[90]:


abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
def generate_PLC_on_off_file(day, month):
    f = open("DHL_miami_PLC_{}{}.txt".format(month, day), "w")
    f.write("conveyor, 1hr_transition, 2hr_transition, 3hr_transition, 5hr_transition, 12hr_transition, 24hr_transition")
    f.write("\n")
    conveyor_count = 0
    for conveyor in conveyor_list:
        conveyor_count = conveyor_count + 1
        f.write("{}".format(conveyor))
        hours_list = [1,2,3,5,12,24]
        for hour in hours_list: 
        # construct timetuple to query 
            t1 = datetime(day=day, month=abbr_to_num[month], year=2019, hour=0)
            t2 = t1 + timedelta(hours = hour)
            timetuple = (t1, t2)
            d1 = t1.strftime('%Y-%m-%d')
            d2 = t2.strftime('%Y-%m-%d')
            ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
            ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
            # run on off transition function
            result = num_on_off_trans_plc(conveyor, d1, d2, ts1, ts2)
            f.write(",{}".format(result))
        f.write("\n")
        print ('finished {c}/{d}'.format(c = conveyor_count, d = len(conveyor_list)))
    f.close()


# In[91]:


generate_PLC_on_off_file(3,'Feb')


# In[92]:


generate_PLC_on_off_file(5,'Feb')


# In[93]:



generate_PLC_on_off_file(11,'Feb')


# In[94]:



generate_PLC_on_off_file(14,'Feb')


# In[95]:



generate_PLC_on_off_file(19,'Feb')


# In[96]:



generate_PLC_on_off_file(27,'Feb')


# In[97]:



generate_PLC_on_off_file(3,'Mar')


# In[98]:



generate_PLC_on_off_file(5,'Mar')


# In[99]:



generate_PLC_on_off_file(9,'Mar')


# In[100]:



generate_PLC_on_off_file(13,'Mar')


# In[101]:



generate_PLC_on_off_file(19,'Mar')


# In[102]:



generate_PLC_on_off_file(23,'Mar')


# In[103]:



generate_PLC_on_off_file(30,'Mar')


# In[334]:


concatePLC = pd.read_csv('DHL_miami_PLC/DHL_miami_PLC_all.csv')


# In[358]:


concateMotor = pd.read_csv('DHL_miami_Motor/DHL_miami_motor_all.csv')


# In[361]:


concateGearbox = pd.read_csv('DHL_miami_Gearbox/DHL_miami_gearbox_all.csv')


# In[362]:


concateBearing = pd.read_csv('DHL_miami_Bearing/DHL_miami_bearing_all.csv')


# In[337]:


PLC_avg_on_off = concatePLC.median()
PLC_avg_on_off


# In[354]:


concatePLC.describe().loc[['mean', 'std']]


# In[394]:


aaa = concatePLC.describe().loc[['std']]
bbb = concateMotor.describe().loc[['std']]
ccc = concateGearbox.describe().loc[['std']]
ddd = concateBearing.describe().loc[['std']]


# In[395]:


frames = [aaa, bbb, ccc, ddd]
result = pd.concat(frames)


# In[396]:


equip_sereis = pd.Series(['PLC', 'Motor', 'Gearbox', 'Bearing'])


# In[397]:


equip = pd.DataFrame({'equip':equip_sereis})


# In[398]:


equip.join(result.reset_index(drop=True)).set_index('equip').T


# In[399]:


equip.join(result.reset_index(drop=True)).set_index('equip').T.plot(kind = 'bar', figsize= (10,5))


# In[353]:


aaa.T.plot(kind = 'bar', secondary_y= 'std', figsize = (10,5))


# In[364]:


bbb.T.plot(kind = 'bar', secondary_y= 'std', figsize = (10,5))


# In[172]:


miami_loc_q = 'SELECT distinct conveyor, facility_organization from location'
miamilocation = dqe.execute_generic_query(miami_loc_q, return_as_df = True)


# In[173]:


miamilocation


# In[175]:


PLC_with_facility_raw = pd.merge(miamilocation, concatePLC, on='conveyor')


# In[178]:


PLC_with_facility = pd.merge(miamilocation, PLC_avg_on_off, on='conveyor')


# In[179]:


PLC_with_facility.groupby(['facility_organization']).median()


# In[180]:


PLC_with_facility_raw.groupby(['facility_organization']).mean()


# In[183]:


PLC_with_facility_raw.groupby(['facility_organization']).std()


# In[298]:


def generate_motor_on_off_file(day, month):
    f = open("DHL_miami_Motor_{}{}.txt".format(month, day), "w")
    f.write("conveyor, 1hr_transition, 2hr_transition, 3hr_transition, 5hr_transition, 12hr_transition, 24hr_transition")
    f.write("\n")
    conveyor_count = 0
    for conveyor in conveyor_list:
        conveyor_count = conveyor_count + 1
        # Check if it has equipment or not 
        equipment_df = dqe.get_conveyor_sensors(conveyor = conveyor)    
        if 'M' in  equipment_df.location.unique():
            f.write("{}".format(conveyor))
            hours_list = [1,2,3,5,12,24]
            for hour in hours_list: 
            # construct timetuple to query 
                t1 = datetime(day=day, month=abbr_to_num[month], year=2019, hour=0)
                t2 = t1 + timedelta(hours = hour)
                timetuple = (t1, t2)
                d1 = t1.strftime('%Y-%m-%d')
                d2 = t2.strftime('%Y-%m-%d')
                ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
                ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
                # run query 
                sensor_list = list()
                sensor_list.append(str(int(equipment_df[equipment_df['location'] == 'M']['sensor'])))
                result = num_on_off_trans(sensor_list, d1, d2, ts1, ts2)
                f.write(",{}".format(result))
            f.write("\n")
        print ('finished {c}/{d}'.format(c = conveyor_count, d = len(conveyor_list)))
    f.close()


# In[285]:


def generate_gearbox_on_off_file(day, month):
    f = open("DHL_miami_Gearbox_{}{}.txt".format(month, day), "w")
    f.write("conveyor, 1hr_transition, 2hr_transition, 3hr_transition, 5hr_transition, 12hr_transition, 24hr_transition")
    f.write("\n")
    conveyor_count = 0
    for conveyor in conveyor_list:
        conveyor_count = conveyor_count + 1
        # Check if it has equipment or not 
        equipment_df = dqe.get_conveyor_sensors(conveyor = conveyor)    
        if 'G' in  equipment_df.location.unique():
            f.write("{}".format(conveyor))
            hours_list = [1,2,3,5,12,24]
            for hour in hours_list: 
            # construct timetuple to query 
                t1 = datetime(day=day, month=abbr_to_num[month], year=2019, hour=0)
                t2 = t1 + timedelta(hours = hour)
                timetuple = (t1, t2)
                d1 = t1.strftime('%Y-%m-%d')
                d2 = t2.strftime('%Y-%m-%d')
                ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
                ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
                # run function to calculate number of times on-off-transitions
                sensor_list = list()
                sensor_list.append(str(int(equipment_df[equipment_df['location'] == 'G']['sensor'])))
                result = num_on_off_trans(sensor_list, d1, d2, ts1, ts2)
                f.write(",{}".format(result))
            f.write("\n")
        print ('finished {c}/{d}'.format(c = conveyor_count, d = len(conveyor_list)))
    f.close()


# In[258]:


def generate_bearing_on_off_file(day, month):
    f = open("DHL_miami_Bearing_{}{}.txt".format(month, day), "w")
    f.write("conveyor, 1hr_transition, 2hr_transition, 3hr_transition, 5hr_transition, 12hr_transition, 24hr_transition")
    f.write("\n")
    conveyor_count = 0
    for conveyor in conveyor_list:
        conveyor_count = conveyor_count + 1
        # Check if it has equipment or not 
        equipment_df = dqe.get_conveyor_sensors(conveyor = conveyor)    
        if 'B' in  equipment_df.location.unique():
            f.write("{}".format(conveyor))
            hours_list = [1,2,3,5,12,24]
            for hour in hours_list: 
            # construct timetuple to query 
                t1 = datetime(day=day, month=abbr_to_num[month], year=2019, hour=0)
                t2 = t1 + timedelta(hours = hour)
                timetuple = (t1, t2)
                d1 = t1.strftime('%Y-%m-%d')
                d2 = t2.strftime('%Y-%m-%d')
                ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
                ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
                # run function to calculate number of times on-off-transitions
                if len(equipment_df[equipment_df['location'] == 'B']['sensor']) > 1:
                    sensor_list = list(equipment_df[equipment_df['location'] == 'B']['sensor'])
                    result = num_on_off_trans(sensor_list, d1, d2, ts1, ts2)
                else: 
                    sensor_list = list()
                    sensor_list.append(str(int(equipment_df[equipment_df['location'] == 'B']['sensor'])))
                    result = num_on_off_trans(sensor_list, d1, d2, ts1, ts2)
                f.write(",{}".format(result))
            f.write("\n")
        print ('finished {c}/{d}'.format(c = conveyor_count, d = len(conveyor_list)))
    f.close()


# In[259]:


generate_bearing_on_off_file(3,'Feb')


# In[260]:


generate_bearing_on_off_file(5,'Feb')
generate_bearing_on_off_file(11,'Feb')


# In[261]:


generate_bearing_on_off_file(14,'Feb')


# In[262]:


generate_bearing_on_off_file(19,'Feb')


# In[263]:


generate_bearing_on_off_file(27,'Feb')


# In[264]:


generate_bearing_on_off_file(3,'Mar')


# In[265]:


generate_bearing_on_off_file(5,'Mar')


# In[266]:


generate_bearing_on_off_file(9,'Mar')


# In[267]:


generate_bearing_on_off_file(13,'Mar')


# In[268]:


generate_bearing_on_off_file(19,'Mar')


# In[269]:


generate_bearing_on_off_file(23,'Mar')


# In[270]:


generate_bearing_on_off_file(30,'Mar')


# In[271]:


generate_bearing_on_off_file(24,'Mar')


# In[272]:


generate_bearing_on_off_file(25,'Mar')


# In[273]:


generate_bearing_on_off_file(27,'Mar')


# In[286]:


generate_gearbox_on_off_file(3,'Jan')


# In[288]:


generate_gearbox_on_off_file(4,'Jan')


# In[289]:


generate_gearbox_on_off_file(7,'Jan')


# In[290]:


generate_gearbox_on_off_file(9,'Jan')


# In[291]:


generate_gearbox_on_off_file(10,'Jan')


# In[292]:


generate_gearbox_on_off_file(12,'Jan')


# In[293]:


generate_gearbox_on_off_file(20,'Jan')


# In[294]:


generate_gearbox_on_off_file(24,'Jan')


# In[295]:


generate_gearbox_on_off_file(1, 'Feb')


# In[299]:


generate_motor_on_off_file(20, 'Jan')


# In[300]:


generate_motor_on_off_file(24, 'Jan')


# In[301]:


generate_motor_on_off_file(1, 'Feb')


# In[333]:


concatePLC = pd.read_csv('DHL_miami_PLC/DHL_miami_PLC_all.csv')
PLC_with_facility_raw = pd.merge(miamilocation, concatePLC, on='conveyor')
PLC_with_facility_raw.groupby(['facility_organization']).median()


# In[334]:


concateMotor = pd.read_csv('DHL_miami_Motor/DHL_miami_motor_all.csv')
Motor_with_facility_raw = pd.merge(miamilocation, concateMotor, on='conveyor')
Motor_with_facility_raw.groupby(['facility_organization']).median()


# In[331]:


concateGearbox = pd.read_csv('DHL_miami_Gearbox/DHL_miami_gearbox_all.csv')
Gearbox_with_facility_raw = pd.merge(miamilocation, concateGearbox, on='conveyor')
Gearbox_with_facility_raw.groupby(['facility_organization']).median()


# In[332]:


concateBearing = pd.read_csv('DHL_miami_Bearing/DHL_miami_bearing_all.csv')
Bearing_with_facility_raw = pd.merge(miamilocation, concateBearing, on='conveyor')
Bearing_with_facility_raw.groupby(['facility_organization']).median()


# In[332]:


PLC_with_facility_raw.describe()


# In[331]:


PLC_with_facility_raw.describe()['mean']


# In[336]:


Motor_with_facility_raw.describe()


# In[337]:


Gearbox_with_facility_raw.describe()


# In[338]:


Bearing_with_facility_raw.describe()


# # Decision: Motor 5 Hour From Sensor Minute Table

# In[ ]:


# 26/30 conveyors has equipment M, the rest of them have bearing


# In[ ]:


# query motor data to find out distribution of on off transitions in 5 hours


# In[ ]:


d1 = t1.strftime('%Y-%m-%d')
d2 = t2.strftime('%Y-%m-%d')
ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')


# In[111]:


startdate = datetime(day=1, month=1, year=2019, hour=0)
enddate = datetime(day=17, month=4, year=2019, hour=23)


# In[168]:


frames = []
for conveyor in conveyor_list: 
    equipment_df = dqe.get_conveyor_sensors(conveyor = conveyor)    
    if 'M' in  equipment_df.location.unique():
        on_off_dict = {}
        result = []
        for i in range(20): 
            t1 = startdate + (enddate - startdate) * random.random()
            t2 = t1 + timedelta(hours = 5)
            d1 = t1.strftime('%Y-%m-%d')
            d2 = t2.strftime('%Y-%m-%d')
            ts1 = t1.strftime('%Y-%m-%d %H:%M:%S')
            ts2 = t2.strftime('%Y-%m-%d %H:%M:%S')
            sensor_list = list()
            sensor_list.append(str(int(equipment_df[equipment_df['location'] == 'M']['sensor'])))
            on_off_num = num_on_off_trans(sensor_list, d1, d2, ts1, ts2)
            result.append(on_off_num)
        on_off_dict['on_off_num'] = result
        on_off_dict['conveyor'] = [conveyor] * len(result)
        frame = pd.DataFrame(on_off_dict)
        frames.append(frame)


# In[169]:


data = pd.concat(frames)


# In[170]:


data = data[data['on_off_num'] != 'NA'].reset_index()


# In[172]:


data.to_csv('on_off_trans_dhl_miami.csv', index = False)


# In[400]:


plt.figure(figsize=(10, 5))
sns.distplot(data['on_off_num'].astype(int))


# In[177]:


# Create models from data
def best_fit_distribution(data, bins=200, ax=None):
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [        
        st.alpha,st.anglit,st.arcsine,st.beta,st.betaprime,st.bradford,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.expon,st.exponnorm,st.exponweib,st.exponpow,st.f,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.genpareto,st.gennorm,st.genexpon,
        st.genextreme,st.gausshyper,st.gamma,st.gengamma,st.genhalflogistic,st.gilbrat,st.gompertz,st.gumbel_r,
        st.gumbel_l,st.halfcauchy,st.halflogistic,st.halfnorm,st.halfgennorm,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.johnsonsu,st.ksone,st.kstwobign,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.lomax,st.maxwell,st.mielke,st.nakagami,st.ncx2,st.ncf,
        st.nct,st.norm,st.pareto,st.pearson3,st.powerlaw,st.powerlognorm,st.powernorm,st.rdist,st.reciprocal,
        st.rayleigh,st.rice,st.semicircular,st.t,st.triang,st.truncexpon,st.truncnorm,st.tukeylambda,
        st.uniform,st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max,st.wrapcauchy
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0))

                # if axis pass in add to plot
                try:
                    if ax:
                        pd.Series(pdf, x).plot(ax=ax)
                    end
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)


# In[178]:


def make_pdf(dist, params, size=10000):
    """Generate distributions's Probability Distribution Function """

    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]

    # Get sane start and end points of distribution
    start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

    # Build PDF and turn into pandas Series
    x = np.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pd.Series(y, x)

    return pdf


# In[179]:


get_ipython().run_line_magic('matplotlib', 'inline')

matplotlib.rcParams['figure.figsize'] = (16.0, 12.0)
matplotlib.style.use('ggplot')

# Load data from statsmodels datasets
adata = data['on_off_num'].astype(int)

# Plot for comparison
plt.figure(figsize=(12,8))
ax = adata.plot(kind = 'density',alpha=0.5, color='r')
# Save plot limits
adataYLim = ax.get_ylim()

# Find best fit distribution
best_fit_name, best_fit_params = best_fit_distribution(adata, 200, ax)
best_dist = getattr(st, best_fit_name)

# Update plots
ax.set_ylim(adataYLim)
ax.set_title(u'Num on off transitions\n All Fitted Distributions')
ax.set_xlabel(u'Num_on off transitions')
ax.set_ylabel('Frequency')

# Make PDF with best params 
pdf = make_pdf(best_dist, best_fit_params)

# Display
plt.figure(figsize=(12,8))
ax = pdf.plot(lw=2, label='PDF', legend=True)
data.plot(kind = 'density', alpha=0.5, label='Data', legend=True, ax=ax)

param_names = (best_dist.shapes + ', loc, scale').split(', ') if best_dist.shapes else ['loc', 'scale']
param_str = ', '.join(['{}={:0.2f}'.format(k,v) for k,v in zip(param_names, best_fit_params)])
dist_str = '{}({})'.format(best_fit_name, param_str)

ax.set_title(u' Num on off transitions with best fit distribution \n' + dist_str)
ax.set_xlabel(u'Num on off transitions')
ax.set_ylabel('Frequency')


# In[198]:


def comp_params(data, distribution):
    dist = getattr(st, distribution)
    param = dist.fit(data)
    return param


# In[196]:


def comp_residual_upper_limit(params, sensitive_level):
    residual_upper_level = np.round(st.beta(params).ppf(sensitive_level), 3)
    return residual_upper_level


# In[200]:


params = comp_params(data['on_off_num'].astype(int), 'beta')


# In[202]:


params


# In[209]:


st.beta(params[0], params[1]).ppf(0.95)*1443


# In[411]:


st.beta(params[0], params[1], scale = 1443.06, loc = -0).ppf(0.95)


# In[409]:


st.uniform.cdf([0, 1, 2, 3, 4, 5], loc = 1)


# In[410]:


st.uniform.cdf([0, 1, 2, 3, 4, 5], loc = 1)*4

