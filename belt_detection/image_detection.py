import logging
import signal
import math
import datetime
import cv2
import numpy as np

import s3fs
import configargparse
from fractions import Fraction


logger = logging.getLogger()
logger.setLevel(logging.INFO)


parser = configargparse.ArgumentParser(default_config_files=['config.ini'])
parser.add_argument("-c", "--config", is_config_file=True, help="path to config file")
parser.add_argument("-carl", "--contour_area_range_lower", env_var='CONTOUR_AREA_RANGE_LOWER',
                    type=int, required=True, help="range of the contour area lower limit")
parser.add_argument("-caru", "--contour_area_range_upper", env_var='CONTOUR_AREA_RANGE_UPPER',
                    type=int, required=True, help="range of the contour area upper limit")
parser.add_argument("-p2i", "--pixel_2_inch_ori", env_var='PIXEL_2_INCH_ORI',
                    required=True, help="pixel to inch transfer")
parser.add_argument("-til", "--threshold_inch_lower", env_var='THRESHOLD_INCH_LOWER', type=float,
                    required=True, help="distance threshold between conveyor and edge for alarms lower limit")
parser.add_argument("-tiu", "--threshold_inch_upper", env_var='THRESHOLD_INCH_UPPER', type=float,
                    required=True, help="distance threshold between conveyor and edge for alarms upper limit")
parser.add_argument("-b", "--belt_inch", env_var='BELT_INCH', required=True, type=float, help="belt inches")
parser.add_argument("-cx", "--cropx", env_var='CROPX', required=True, type=int, help="crop x from image left side")
parser.add_argument("-cxw", "--cropxw", env_var='CROPXW', required=True, type=int,  help="crop x from image right side")
parser.add_argument("-cy", "--cropy", env_var='CROPY', required=True, type=int,  help="crop y from image")
parser.add_argument("-cannyl", "--cannyfilter_lower", env_var='CANNYFILTER_LOWER', type=int,
                    required=True, default=60, help="canny threshold for getting contours lower limit")
parser.add_argument("-cannyu", "--cannyfilter_upper", env_var='CANNYFILTER_UPPER', type=int,
                    required=True, default=70, help="canny threshold for getting contours upper limit")
parser.add_argument("-rotate", "--rotate_angle", env_var='ROTATE_ANGLE', required=False,
                    type=int, default=3, help="how much degrees to rotate image clockwise")
parser.add_argument("-curl", "--cameraurl", env_var='CAMERAURL', required=True, help='url for connect to ip camera')

args = parser.parse_args()
contour_area_range_lower = args.contour_area_range_lower
contour_area_range_upper = args.contour_area_range_upper
pixel_2_inch_ori = float(Fraction(args.pixel_2_inch_ori))
threshold_inch_lower = args.threshold_inch_lower
threshold_inch_upper = args.threshold_inch_upper
belt_inch = args.belt_inch
cropx = args.cropx
cropxw = args.cropxw
cropy = args.cropy
cannyfilter_lower = args.cannyfilter_lower
cannyfilter_upper = args.cannyfilter_upper
rotate_angle = args.rotate_angle
cameraurl = args.cameraurl


def _signal_handler(signum, _frame):
    """
    Warm shutdown.
    """
    global is_working
    logging.info(f'Got signal {signum}')
    is_working = False

for sig in (signal.SIGINT, signal.SIGTERM):
    signal.signal(sig, _signal_handler)

is_working = True
method = "left-to-right"
cap = cv2.VideoCapture(cameraurl)


def resize_contours_keep_middle(contour1, contour2):
    """
    resize the contours to the same shape by keeping the middle part of larger contour
    """
    # resive to the same shape is not
    if contour1.shape != contour2.shape:
        # detect which contour is large and which contour is small
        if len(contour1) > len(contour2):
            contour1_is_larger = True
            larger_contour = contour1
            smaller_contour = contour2
        else:
            contour1_is_larger = False
            larger_contour = contour2
            smaller_contour = contour1

        diff = len(larger_contour) - len(smaller_contour)
        # if diff is the even
        if diff % 2 == 0:
            start = 0 + diff // 2
            end = len(larger_contour) - diff // 2
            larger_contour = larger_contour[start:end]
        else:
            start = 0 + diff // 2
            end = len(larger_contour) - (diff - diff // 2)
            larger_contour = larger_contour[start:end]
        if contour1_is_larger:
            return larger_contour, smaller_contour
        else:
            return smaller_contour, larger_contour
    else:
        return contour1, contour2


def resize_contours_remove_by_step(contour1, contour2):
    """
    resize the contours to the same shape by removing middle elemnt once per certain number based on the diff in sizes
    Note: the returning contours is not exactly the same ship. need to run resize again
    """
    if contour1.shape != contour2.shape:
        # detect which contour is large and which contour is small
        if len(contour1) > len(contour2):
            contour1_is_larger = True
            larger_contour = contour1
            smaller_contour = contour2
        else:
            contour1_is_larger = False
            larger_contour = contour2
            smaller_contour = contour1

        diff = len(larger_contour) - len(smaller_contour)
        step = math.ceil(len(larger_contour) / diff)
        larger_contour = np.delete(larger_contour, np.arange(step, len(larger_contour), step), axis=0)
        if contour1_is_larger:
            return larger_contour, smaller_contour
        else:
            return smaller_contour, larger_contour
    else:
        return contour1, contour2


def clean_contours(contour):
    """
    clean the second half of an array. b/c contour is an rectangle shape, so there are half points are replicated in this case
    alternative to sort_contours func
    """
    dim_contour = contour.shape[0]
    max_index = np.argmax(contour.reshape(dim_contour, 2,)[:, 1])
    contour = contour.reshape(dim_contour, 2,)[0:max_index + 1]
    _, index = np.unique(contour[:, 1], return_index=True)
    contour = contour[index]
    return contour


def compute_euclidean_distance(x1, y1, x2, y2):
    """
    caclulate ecludian distance
    """
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)


def calculate_distance(contour_1, contour_2):
    """
    calculate maximum distance between contours
    """
    max_distance = -1
    max_index = -1
    for index, (dpt1, dpt2) in enumerate(zip(contour_1, contour_2)):
        dpts1 = dpt1.reshape(2,)
        dpts1_x = dpts1[0]
        dpts1_y = dpts1[1]
        dpts2 = dpt2.reshape(2,)
        dpts2_x = dpts2[0]
        dpts2_y = dpts2[1]
        distance = compute_euclidean_distance(dpts1_x, dpts1_y, dpts2_x, dpts2_y)
        if distance > max_distance:
            max_distance = distance
            max_index = index
    return max_distance, max_index


def get_edge_cordinates(contour_1, contour_2, index):
    """
    get the edge cordinates for two contours
    """
    left_x, up_y = contour_1[index]
    right_xw, down_yh = contour_2[index]
    return left_x, up_y, right_xw, down_yh


def rotate_image(image_2_rotate, angle):
    """
    rotate image by a specific angle clockwise
    """
    image_center = tuple(np.array(image_2_rotate.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result_im = cv2.warpAffine(image_2_rotate, rot_mat, image_2_rotate.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result_im


def sort_contours(cnts, method="left-to-right"):
    """
    sort contours by the method defined
    """
    # initialize the reverse flag and sort index
    reverse = False
    i = 0

    # handle if we need to sort in reverse
    if method in ("right-to-left", "left-to-right"):
        reverse = True

    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method in ("top-to-bottom", "bottom-to-top"):
        i = 1

    # construct the list of bounding boxes and sort them from top to
    # bottom
    bounding_boxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, bounding_boxes) = zip(*sorted(zip(cnts, bounding_boxes),
                                         key=lambda b: b[1][i], reverse=reverse))

    # return the list of sorted contours and bounding boxes
    return cnts, bounding_boxes


def label_contour(image_2_label, c, i, color=(0, 255, 0), thickness=2):
    """
    label contour based on method
    """
    # compute the center of the contour area and draw a circle
    # representing the center
    M = cv2.moments(c)
    cx = int(M["m10"] / M["m00"])
    cy = int(M["m01"] / M["m00"])

    # draw the contour and label number on the image
    cv2.drawContours(image_2_label, [c], -1, color, thickness)
    cv2.putText(image_2_label, f"#{i+1}", (cx - 20, cy), cv2.FONT_HERSHEY_SIMPLEX,
                1.0, (255, 255, 255), 2)

    # return the image with the contour number drawn on it
    return image_2_label

s3 = s3fs.S3FileSystem()
while is_working:
    result, ori_image = cap.read()
    if not result:
        logger.error('ERROR')
        break
    ori_image = np.int16(ori_image)
    ori_image = rotate_image(ori_image, rotate_angle)
    contrast = 48
    brightness = 0
    ori_image = ori_image * (contrast / 127 + 1) - contrast + brightness
    ori_image = np.clip(ori_image, 0, 255)
    ori_image = np.uint8(ori_image)
    ori_clone = ori_image.copy()
    ctr = ori_image.shape[0] // 2
    ctr_y = ori_image.shape[1] // 2
    crop_x = int(ctr - (belt_inch // pixel_2_inch_ori) // 2 + cropx)
    crop_xw = int(ctr + (belt_inch // pixel_2_inch_ori) // 2 + cropxw)
    crop_y = ctr_y + cropy
    crop_yh = ctr_y
    ori_image = ori_image[crop_y:crop_yh, crop_x:crop_xw]
    im = cv2.cvtColor(ori_image, cv2.COLOR_BGR2GRAY)
    # ### gussian blur to remove some noise
    im = cv2.GaussianBlur(im, (5, 5), 0)
    # edge method 3: canny
    canny = cv2.Canny(im, cannyfilter_lower, cannyfilter_upper)
    canny = cv2.dilate(canny, None, iterations=1)
    canny = cv2.erode(canny, None, iterations=1)
    # find contours

    contours, hierarchy = cv2.findContours(canny.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)

    # filter un-relevant contours by the contourArea defined above and sort contours matrix y axis (from top to bottom)
    edge_contours = []
    for contour in contours:
        if (cv2.contourArea(contour) >= contour_area_range_lower) and (cv2.contourArea(contour) <= contour_area_range_upper):
            contour = clean_contours(contour)
            edge_contours.append(contour)
    # sort the contours using the method defined
    if len(edge_contours) <= 1:
        continue
    (edge_contours, boundingBoxes) = sort_contours(edge_contours, method=method)

    contour1, contour2 = resize_contours_remove_by_step(edge_contours[0], edge_contours[1])
    contour1, contour2 = resize_contours_keep_middle(contour1, contour2)
    assert contour1.shape == contour2.shape
    # calculate distance
    max_distance_left, index_left = calculate_distance(contour1, contour2)
    # unit transfer from pixels to inches
    real_distance_left = round(max_distance_left * pixel_2_inch_ori, 2)
    # draw boxes around max distance area, green indicates safe distance value, red means alarming distance value
    x, y, xw, yh = get_edge_cordinates(contour1, contour2, index_left)
    x_position_ori = crop_x + x
    xw_position_ori = crop_x + xw
    y_position_ori = crop_y + y
    if (threshold_inch_lower > real_distance_left) or (threshold_inch_upper < real_distance_left):
        cv2.rectangle(ori_clone, (x_position_ori, y_position_ori),
                      (xw_position_ori, y_position_ori - 10), (0, 0, 255), 2)
        cv2.putText(ori_clone, f"{real_distance_left} inches", (x_position_ori - 20, y_position_ori + 30), cv2.FONT_HERSHEY_PLAIN, 2.0, (0, 0, 255), 2)
        time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        date = datetime.datetime.now().strftime('%Y-%m-%d')
        cv2.imwrite('alarms.jpg', ori_clone)
        filename = f'alarms-detected-edge-{time}.jpg'
        with open('alarms.jpg', 'rb') as f:
            image_data = f.read()
        with s3.open(f'belt-detection/date={date}/{filename}', 'wb') as f:
            f.write(image_data)
        print("upload one alarm image to s3 bucket belt-detection")


def handler(_event, _context):
    """
    This is a dummy handler for AWS and will not be invoked.
    Instead the code above will be executed in an infinite loop.
    """
    return
