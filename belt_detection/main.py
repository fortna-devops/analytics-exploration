import cv2
import numpy as np


# Settings.
MIN_BOX_SQUARE = 6000
SHOW_BOXES = True

# Capture video from file
#cap = cv2.VideoCapture("mist.mp4")
cap = cv2.VideoCapture('rtsp://10.21.6.21/user=admin&password=&channel=1&stream=0.sdp?real_stream')

while True:
    result, original_frame = cap.read()
    if not result:
        print('ERROR')
        break

    frame_np = np.int16(original_frame)
    contrast   = 48
    brightness = 0
    frame_np = frame_np*(contrast/127 + 1) - contrast + brightness
    frame_np = np.clip(frame_np, 0, 255)
    frame = np.uint8(frame_np)

    # Apply mask and filters.
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, (0, 0, 0), (180, 255, 40))
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # Calculate boxes.
    boxes = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]

    r_x, r_y, r_xw, r_yh = None, None, None, None
    for box in boxes:
        x, y, w, h = cv2.boundingRect(box)
        xw, yh = x + w, y + h
        # Filter boxes by square.
        if MIN_BOX_SQUARE is not None and w * h < MIN_BOX_SQUARE:
            continue
        # Calculate box.
        if r_x is None:
            r_x, r_y, r_xw, r_yh = x, y, xw, yh
        if r_x > x:
            r_x = x
        if r_y > y:
            r_y = y
        if xw > r_xw:
            r_xw = xw
        if yh > r_yh:
            r_yh = yh
        if SHOW_BOXES:
            cv2.rectangle(original_frame, (x, y), (xw, yh), (0, 0, 255), 2)

    cv2.imshow('Detection', original_frame)

    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
