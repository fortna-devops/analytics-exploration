-- edit: got rid of []
-- edit: replaced 'GO' with ';'
-- edit: INCREMENT(1,1) changed to AUTO_INCREMENT
-- edit: got rid of NOT FOR REPLICATION constraint
-- edit: got rid of "WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON PRIMARY",
-- "",
-- and the like
/*for now i do not think it is worth researching every detail of this to replicate it in the mysql database*/
-- edit: deleted "dbo." prefix to table names
-- edit: deleted SET ANSI_NULLS ON; b/c this is done by default on mysql
-- edit: replaced SET QUOTED_IDENTIFIER ON; with SET SESSION sql_mode = 'ANSI_QUOTES';
/*this is the equivalent in MySQL but I don't know about the scope portion because there was also the option
of SESSION instead of GLOBAL*/
-- edit: replaced money type with DECIMAL(13,2)


SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroup(
	AccessGroupPK int NOT NULL AUTO_INCREMENT,
	AccessGroupID varchar(25) NOT NULL,
	AccessGroupName varchar(50) NULL,
	AppAccess varchar(15) NULL,
	AppAccessDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	MenuName varchar(200) NULL,
	ServiceRequest_Link varchar(200) NULL,
	ServiceRequestStatus_Link varchar(200) NULL,
	Comments varchar(4000) NULL,
 CONSTRAINT PK_AccessGroup PRIMARY KEY NONCLUSTERED 
(
	AccessGroupPK ASC
),
 CONSTRAINT IX_AccessGroup UNIQUE CLUSTERED 
(
	AccessGroupID ASC
)
);
/****** Object:  Table AccessGroupActions    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupActions(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NOT NULL,
	DisplayOrder int NULL,
	ActionPK int NOT NULL,
	ActionID varchar(100) NULL,
	ActionName varchar(50) NULL,
	ActionDesc varchar(500) NULL,
	MinAmount decimal(19, 6) NOT NULL,
	MaxAmount decimal(19, 6) NOT NULL,
	UseAmount bit NOT NULL,
	ModuleID char(2) NULL,
	Enabled bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ActionValue varchar(100) NULL,
	UseValue bit NULL,
	SystemHidden bit NULL,
 CONSTRAINT PK_AccessGroupActions PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AccessGroupAsset    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupAsset(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NOT NULL,
	AssetPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AccessGroupAsset PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AccessGroupAssetMgr    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupAssetMgr(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NOT NULL,
	AssetPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AccessGroupAssetMgr PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AccessGroupDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupDocument(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_AccessGroupDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AccessGroupForm    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupForm(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	FormPage varchar(100) NOT NULL,
	ActionPage varchar(100) NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AccessGroupForm PRIMARY KEY CLUSTERED 
(
	PK ASC
),
 CONSTRAINT DF_AccessGroupForm_AG_Module UNIQUE NONCLUSTERED 
(
	AccessGroupPK ASC,
	ModuleID ASC
)
);
/****** Object:  Table AccessGroupRepairCenter    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupRepairCenter(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NOT NULL,
	RepairCenterPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AccessGroupRepairCenter PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AccessGroupReportGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AccessGroupReportGroup(
	PK int NOT NULL AUTO_INCREMENT,
	AccessGroupPK int NULL,
	ReportGroupPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AccessGroupReportGroup_PK PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Account    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Account(
	AccountPK int NOT NULL AUTO_INCREMENT,
	AccountID varchar(25) NOT NULL,
	AccountName varchar(50) NULL,
	ParentPK int NULL,
	Description varchar(150) NULL,
	Type varchar(25) NOT NULL,
	TypeDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	Budget DECIMAL(13,2) NOT NULL,
	Active bit NOT NULL,
	Comments varchar(4000) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	PMCycleStartDate datetime NULL,
	RiskLevel smallint NULL,
	PMCounter smallint NULL,
 CONSTRAINT PK_Account PRIMARY KEY NONCLUSTERED 
(
	AccountPK ASC
),
 CONSTRAINT IX_Account UNIQUE CLUSTERED 
(
	AccountID ASC
)
);
/****** Object:  Table Actions    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Actions(
	ActionPK int NOT NULL AUTO_INCREMENT,
	Active bit NOT NULL,
	DisplayOrder int NULL,
	ActionID varchar(100) NULL,
	ActionName varchar(50) NULL,
	ActionDesc varchar(500) NULL,
	MinAmount decimal(19, 6) NOT NULL,
	MaxAmount decimal(19, 6) NOT NULL,
	UseAmount bit NOT NULL,
	ModuleID char(2) NULL,
	Enabled bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ActionValue varchar(100) NULL,
	UseValue bit NULL,
	SystemHidden bit NULL,
 CONSTRAINT PK_Actions PRIMARY KEY NONCLUSTERED 
(
	ActionPK ASC
),
 CONSTRAINT IX_Actions UNIQUE CLUSTERED 
(
	ActionID ASC
)
);
/****** Object:  Table AppCache    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AppCache(
	PK int NOT NULL AUTO_INCREMENT,
	FileName nvarchar(2000) NOT NULL,
	Type nvarchar(50) NOT NULL,
	Custom bit NOT NULL,
 CONSTRAINT PK_AppCache PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Asset    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Asset(
	AssetPK int NOT NULL AUTO_INCREMENT,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	InternalAssetNum varchar(25) NULL,
	IsLocation bit NOT NULL,
	IsClone bit NOT NULL,
	RequesterCanView bit NOT NULL,
	IsUp bit NOT NULL,
	Status varchar(25) NULL,
	StatusDesc varchar(50) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(100) NULL,
	ClassificationName varchar(150) NULL,
	Model varchar(50) NULL,
	Serial varchar(50) NULL,
	System varchar(25) NULL,
	SystemDesc varchar(50) NULL,
	OperatorPK int NULL,
	OperatorID varchar(25) NULL,
	OperatorName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	Vicinity varchar(100) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	GenLedger varchar(25) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	VendorPK int NULL,
	VendorID varchar(25) NULL,
	VendorName varchar(100) NULL,
	ManufacturerPK int NULL,
	ManufacturerID varchar(25) NULL,
	ManufacturerName varchar(100) NULL,
	WarrantyExpire datetime NULL,
	ContactPK int NULL,
	ContactID varchar(25) NULL,
	ContactName varchar(50) NULL,
	Address varchar(80) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(15) NULL,
	Country varchar(50) NULL,
	DisplayMapOnWO bit NOT NULL,
	PurchasedDate datetime NULL,
	PurchaseOrder varchar(25) NULL,
	PurchaseCost DECIMAL(13,2) NOT NULL,
	CurrentValue DECIMAL(13,2) NOT NULL,
	InstallDate datetime NULL,
	StartupDate datetime NULL,
	ReplaceDate datetime NULL,
	ReplacementCost DECIMAL(13,2) NOT NULL,
	Life int NULL,
	DisposalDate datetime NULL,
	LicenseNumber varchar(50) NULL,
	Instructions varchar(2000) NULL,
	InstructionsToWO bit NOT NULL,
	InsuranceCarrier varchar(50) NULL,
	InsurancePolicy varchar(50) NULL,
	LeaseNumber varchar(50) NULL,
	RegistrationDate datetime NULL,
	xcoordinate varchar(15) NULL,
	ycoordinate varchar(15) NULL,
	zcoordinate varchar(15) NULL,
	parcelid varchar(25) NULL,
	IsPredictive bit NOT NULL,
	Technology varchar(25) NULL,
	TechnologyDesc varchar(50) NULL,
	IsMeter bit NOT NULL,
	Meter1Reading decimal(19, 6) NOT NULL,
	Meter1Units varchar(25) NULL,
	Meter1UnitsDesc varchar(50) NULL,
	Meter1TrackHistory bit NULL,
	Meter2Reading decimal(19, 6) NOT NULL,
	Meter2Units varchar(25) NULL,
	Meter2UnitsDesc varchar(50) NULL,
	Meter2TrackHistory bit NULL,
	LastMaintained datetime NULL,
	LastMaintained_WOPK int NULL,
	Icon varchar(200) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	PurchaseType varchar(25) NULL,
	PurchaseTypeDesc varchar(50) NULL,
	DrawingUpdatesNeeded bit NULL,
	DisplayOrder int NULL,
	ClassIndustry varchar(25) NULL,
	ClassIndustryDesc varchar(50) NULL,
	RiskAssessmentRequired bit NULL,
	AssessedByPK int NULL,
	AssessedByID varchar(25) NULL,
	AssessedByName varchar(50) NULL,
	LastAssessed datetime NULL,
	RiskAssessmentGroup varchar(25) NULL,
	RiskAssessmentGroupDesc varchar(50) NULL,
	RiskFactor1 varchar(100) NULL,
	RiskFactor1Score smallint NULL,
	RiskFactor2 varchar(100) NULL,
	RiskFactor2Score smallint NULL,
	RiskFactor3 varchar(100) NULL,
	RiskFactor3Score smallint NULL,
	RiskFactor4 varchar(100) NULL,
	RiskFactor4Score smallint NULL,
	RiskFactor5 varchar(100) NULL,
	RiskFactor5Score smallint NULL,
	RiskScore decimal(18, 0) NULL,
	RiskLevel smallint NULL,
	PMRequired bit NULL,
	PlanForImprovement bit NULL,
	HIPPARelated bit NULL,
	StatementOfConditions bit NULL,
	StatementOfConditionsCompliant bit NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	County varchar(50) NULL,
	YearBuilt datetime NULL,
	MajorRenovations varchar(50) NULL,
	SquareFootage decimal(19, 6) NULL,
	ConstructionCode varchar(25) NULL,
	ConstructionCodeDesc varchar(50) NULL,
	NumberOfStories smallint NULL,
	ISOProtection varchar(25) NULL,
	ISOProtectionDesc varchar(50) NULL,
	AutoSprinkler varchar(25) NULL,
	AutoSprinklerDesc varchar(50) NULL,
	SmokeAlarm varchar(25) NULL,
	SmokeAlarmDesc varchar(50) NULL,
	HeatAlarm varchar(25) NULL,
	HeatAlarmDesc varchar(50) NULL,
	FloodZone varchar(50) NULL,
	QuakeZone varchar(50) NULL,
	Ext100Feet varchar(50) NULL,
	OperatingUnits smallint NULL,
	EstimatedValue float NULL,
	ResponsibilityRepairPK int NULL,
	ResponsibilityRepairID varchar(25) NULL,
	ResponsibilityRepairName varchar(50) NULL,
	ResponsibilityPMPK int NULL,
	ResponsibilityPMID varchar(25) NULL,
	ResponsibilityPMName varchar(50) NULL,
	ResponsibilitySafetyPK int NULL,
	ResponsibilitySafetyID varchar(25) NULL,
	ResponsibilitySafetyName varchar(50) NULL,
	ServiceRepairPK int NULL,
	ServiceRepairID varchar(25) NULL,
	ServiceRepairName varchar(50) NULL,
	ServicePMPK int NULL,
	ServicePMID varchar(25) NULL,
	ServicePMName varchar(50) NULL,
	Meter1RollDown bit NULL,
	Meter2RollDown bit NULL,
	Meter1RollDownMethod char(1) NULL,
	Meter2RollDownMethod char(1) NULL,
	Meter1ReadingLife decimal(19, 6) NULL,
	Meter2ReadingLife decimal(19, 6) NULL,
	IsLinear bit NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	OrigPKforCloning int NULL,
	PMCycleStartBy varchar(25) NULL,
	PMCycleStartByDesc varchar(50) NULL,
	PMCycleStartDate datetime NULL,
	PMCounter smallint NULL,
	ModelNumber varchar(50) NULL,
	ModelNumberMFG varchar(50) NULL,
	ModelLine varchar(25) NULL,
	ModelLineDesc varchar(50) NULL,
	ModelSeries varchar(25) NULL,
	ModelSeriesDesc varchar(50) NULL,
	SystemPlatform varchar(25) NULL,
	SystemPlatformDesc varchar(50) NULL,
	ResourceForScheduling bit NULL,
	RotatingPartPK int NULL,
	RotatingPartID varchar(50) NULL,
	RotatingPartName varchar(500) NULL,
	RotatingRepairCenterPK int NULL,
	RotatingLocationPK int NULL,
	RotatingLaborPK int NULL,
	GISLayer varchar(100) NULL,
	GISKeyValue varchar(100) NULL,
	GISTable varchar(25) NULL,
	ReceiveDate datetime NULL,
	Latitude decimal(19, 7) NULL,
	Longitude decimal(19, 7) NULL,
	AssetFromPK int NULL,
	AssetFromID varchar(100) NULL,
	AssetFromName varchar(150) NULL,
	AssetToPK int NULL,
	AssetToID varchar(100) NULL,
	AssetToName varchar(150) NULL,
	FormName varchar(50) NULL,
	MaintainableToolPK int NULL,
	MaintainableToolID varchar(50) NULL,
	MaintainableToolName varchar(100) NULL,
	MaintainableToolLocationPK int NULL,
	RiskLevelAutoCalc bit NULL,
	PMRequiredAutoCalc bit NULL,
	RotatingBin varchar(100) NULL,
 CONSTRAINT PK_Asset PRIMARY KEY NONCLUSTERED 
(
	AssetPK ASC
)
);
/****** Object:  Table AssetAncestor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetAncestor(
	System char(5) NOT NULL,
	AssetPK int NOT NULL,
	AncestorPK int NOT NULL,
	AncestorLevel int NULL,
	AssetLevel int NULL,
 CONSTRAINT PK_AssetAncestor PRIMARY KEY CLUSTERED 
(
	System ASC,
	AssetPK ASC,
	AncestorPK ASC
)
);
/****** Object:  Table AssetComponent    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetComponent(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	ComponentPK int NOT NULL,
	ComponentName varchar(50) NULL,
	ComponentID varchar(50) NULL,
	DisplayOrder smallint NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetComponent PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetComponentSpecification    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetComponentSpecification(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	ComponentPK int NOT NULL,
	SpecificationPK int NOT NULL,
	SpecificationName varchar(50) NULL,
	DisplayOrder smallint NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(50) NULL,
	ValueDate datetime NULL,
	ValueHi decimal(19, 6) NULL,
	ValueLow decimal(19, 6) NULL,
	TrackHistory bit NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetComponentSpecification PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetContract    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetContract(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	CompanyPK int NOT NULL,
	PeriodStart datetime NULL,
	PeriodEnd datetime NULL,
	VendorContractNum varchar(50) NULL,
	ContractSummary varchar(7900) NULL,
	ContractPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetContract PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetCost    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetCost(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	CostGroup varchar(25) NULL,
	ReferenceDate datetime NULL,
	CostLaborActual DECIMAL(13,2) NOT NULL,
	CostEmployeeActual DECIMAL(13,2) NOT NULL,
	CostContractorActual DECIMAL(13,2) NOT NULL,
	CostPartActual DECIMAL(13,2) NOT NULL,
	CostMiscActual DECIMAL(13,2) NOT NULL,
	CostTotalActual DECIMAL(13,2) NOT NULL,
	ChargeLaborActual DECIMAL(13,2) NOT NULL,
	ChargeEmployeeActual DECIMAL(13,2) NOT NULL,
	ChargeContractorActual DECIMAL(13,2) NOT NULL,
	ChargePartActual DECIMAL(13,2) NOT NULL,
	ChargeMiscActual DECIMAL(13,2) NOT NULL,
	ChargeTotalActual DECIMAL(13,2) NOT NULL,
	CostLaborActual_AO DECIMAL(13,2) NOT NULL,
	CostEmployeeActual_AO DECIMAL(13,2) NOT NULL,
	CostContractorActual_AO DECIMAL(13,2) NOT NULL,
	CostPartActual_AO DECIMAL(13,2) NOT NULL,
	CostMiscActual_AO DECIMAL(13,2) NOT NULL,
	CostTotalActual_AO DECIMAL(13,2) NOT NULL,
	ChargeLaborActual_AO DECIMAL(13,2) NOT NULL,
	ChargeEmployeeActual_AO DECIMAL(13,2) NOT NULL,
	ChargeContractorActual_AO DECIMAL(13,2) NOT NULL,
	ChargePartActual_AO DECIMAL(13,2) NOT NULL,
	ChargeMiscActual_AO DECIMAL(13,2) NOT NULL,
	ChargeTotalActual_AO DECIMAL(13,2) NOT NULL,
 CONSTRAINT PK_AssetCost PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetDocument(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_AssetDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetGroup(
	AssetGroupPK int NOT NULL AUTO_INCREMENT,
	AssetGroupID varchar(25) NOT NULL,
	AssetGroupName varchar(100) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetGroup PRIMARY KEY NONCLUSTERED 
(
	AssetGroupPK ASC
)
);
/****** Object:  Table AssetGroupAsset    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetGroupAsset(
	PK int NOT NULL AUTO_INCREMENT,
	AssetGroupPK int NOT NULL,
	AssetPK int NOT NULL,
	DisplayOrder int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetGroupAsset PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetHierarchy    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetHierarchy(
	System char(5) NOT NULL,
	AssetPK int NOT NULL,
	ParentPK int NOT NULL,
	AssetLevel int NULL,
	HasChildren bit NULL,
	OrderAlpha int NULL,
	OrderType int NULL,
	Location1PK int NULL,
	Location1ID varchar(100) NULL,
	Location1Name varchar(150) NULL,
	Location2PK int NULL,
	Location2ID varchar(100) NULL,
	Location2Name varchar(150) NULL,
	Location3PK int NULL,
	Location3ID varchar(100) NULL,
	Location3Name varchar(150) NULL,
	Location4PK int NULL,
	Location4ID varchar(100) NULL,
	Location4Name varchar(150) NULL,
	Location5PK int NULL,
	Location5ID varchar(100) NULL,
	Location5Name varchar(150) NULL,
	Location6PK int NULL,
	Location6ID varchar(100) NULL,
	Location6Name varchar(150) NULL,
	Location7PK int NULL,
	Location7ID varchar(100) NULL,
	Location7Name varchar(150) NULL,
	Location8PK int NULL,
	Location8ID varchar(100) NULL,
	Location8Name varchar(150) NULL,
	ParentLocation varchar(150) NULL,
	ParentLocationAll varchar(4000) NULL,
	ParentEquipment varchar(150) NULL,
	ParentEquipmentAll varchar(4000) NULL,
	IsOpenWO bit NULL,
	IsOpenWOChild bit NULL,
	IsOpenPD bit NULL,
	IsOpenPDChild bit NULL,
	Dirty bit NULL,
	DirtyBelow bit NULL,
 CONSTRAINT PK_AssetHierarchy PRIMARY KEY CLUSTERED 
(
	System ASC,
	AssetPK ASC,
	ParentPK ASC
)
);
/****** Object:  Table AssetLabor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetLabor(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	LaborPK int NOT NULL,
	Priority smallint NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	AutoAssign bit NULL,
	BackupResource bit NULL,
 CONSTRAINT PK_AssetLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetLease    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetLease(
	LeasePK int NOT NULL AUTO_INCREMENT,
	LeaseID varchar(25) NOT NULL,
	LeaseName varchar(150) NOT NULL,
	AssetPK int NOT NULL,
	AssetID varchar(25) NOT NULL,
	AssetName varchar(150) NOT NULL,
	LeaseNumber varchar(100) NOT NULL,
	Status varchar(100) NOT NULL,
	StartDate datetime NOT NULL,
	ExpireSet bit NOT NULL,
	Expiration datetime NOT NULL,
	SpaceType varchar(100) NOT NULL,
	TotalSF int NOT NULL,
	RenewOption bit NOT NULL,
	RenewValue int NOT NULL,
	RenewType varchar(50) NOT NULL,
	DaysBeforeNotice int NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	SFType varchar(2) NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
 CONSTRAINT PK_AssetLease PRIMARY KEY CLUSTERED 
(
	LeasePK ASC
)
);
/****** Object:  Table AssetLeasePayments    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetLeasePayments(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(50) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	LeasePK int NOT NULL,
	LeaseID varchar(25) NOT NULL,
	LeaseName varchar(150) NOT NULL,
	Amount DECIMAL(13,2) NOT NULL,
	PaymentDate datetime NOT NULL,
	ContactPK int NULL,
	ContactID varchar(25) NULL,
	ContactName varchar(150) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
 CONSTRAINT PK_AssetLeasePayments PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table AssetLeaseResponse    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetLeaseResponse(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	LeasePK int NOT NULL,
	LeaseID varchar(25) NOT NULL,
	LeaseName varchar(150) NOT NULL,
	ResponseID varchar(25) NULL,
	ResponseName varchar(150) NULL,
	PartyPK int NULL,
	PartyID varchar(25) NULL,
	PartyName varchar(150) NULL,
	ContactPK int NULL,
	ContactID varchar(25) NULL,
	ContactName varchar(150) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetLeaseResponse PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table AssetLeaseSF    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetLeaseSF(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	LeasePK int NOT NULL,
	LeaseID varchar(25) NOT NULL,
	LeaseName varchar(150) NOT NULL,
	Name varchar(50) NOT NULL,
	FeetValue int NOT NULL,
	Value numeric(18, 4) NOT NULL,
	Type int NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionIntitals varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetLeaseSF PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table AssetMeterHistory    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetMeterHistory(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	ReadingDate datetime NULL,
	Meter1Reading decimal(19, 6) NOT NULL,
	Meter2Reading decimal(19, 6) NOT NULL,
	WOPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetMeterHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetMove    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetMove(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	MoveDate datetime NULL,
	OldRepairCenterPK int NULL,
	NewRepairCenterPK int NULL,
	OldParentPK int NULL,
	NewParentPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	OldParentLocationAll varchar(4000) NULL,
	NewParentLocationAll varchar(4000) NULL,
 CONSTRAINT PK_AssetMove PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetNote    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetNote(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetPart    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetPart(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	PartPK int NOT NULL,
	Qty int NULL,
	Comments varchar(2000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetPart PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetPhoto    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetPhoto(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	PhotoDate datetime NOT NULL,
	Photo varchar(200) NULL,
	Active bit NOT NULL,
	Followup bit NOT NULL,
	ReportOrder smallint NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetPhoto PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetRequester    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetRequester(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	LaborPK int NOT NULL,
	StartDate datetime NULL,
	EndDate datetime NULL,
	IsLease bit NOT NULL,
	ExpireDate datetime NULL,
	Active bit NOT NULL,
	IsPrimary bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	Percentage decimal(19, 6) NULL,
 CONSTRAINT PK_AssetRequester PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetRiskValues    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetRiskValues(
	RiskPK int NOT NULL AUTO_INCREMENT,
	RiskID varchar(25) NOT NULL,
	RiskGroup varchar(255) NOT NULL,
	RiskGroupOrder int NOT NULL,
	RiskCaption varchar(255) NOT NULL,
	RiskScore int NOT NULL,
	RiskOrder int NOT NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowversionDate datetime NOT NULL,
 CONSTRAINT PK__AssetRiskValues__6D95DDF5 PRIMARY KEY CLUSTERED 
(
	RiskPK ASC
)
);
/****** Object:  Table AssetSpecification    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetSpecification(
	PK bigint NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	SpecificationPK int NOT NULL,
	SpecificationName varchar(50) NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(6000) NULL,
	ValueDate datetime NULL,
	ValueHi decimal(19, 6) NULL,
	ValueLow decimal(19, 6) NULL,
	TrackHistory bit NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ValueOptimal decimal(19, 6) NULL,
	ValueOutOfRangeWO bit NULL,
	WOGenerated bit NULL,
	ProcedurePK int NULL,
	WOPKUsedToChangeSpec int NULL,
 CONSTRAINT PK_AssetSpecification PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetStatusHistory    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetStatusHistory(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	ShutdownDate datetime NULL,
	InServiceDate datetime NULL,
	DownTime decimal(19, 6) NOT NULL,
	DownTimeUnits char(10) NOT NULL,
	DownHours decimal(19, 6) NOT NULL,
	Reason varchar(2000) NULL,
	WOPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetStatusHistory_PK PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetStructure    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetStructure(
	PK int NOT NULL AUTO_INCREMENT,
	StructurePosition smallint NOT NULL,
	ClassificationPK int NOT NULL,
 CONSTRAINT PK_AssetStructure PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetTask    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetTask(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	TaskPK int NULL,
	TaskAction varchar(7000) NULL,
	WOTaskPK int NULL,
	LastCompleted datetime NULL,
	Comments varchar(1000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetTask PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table AssetTreeStatus    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetTreeStatus(
	IsOrdered bit NOT NULL,
	AncestorOnTheFly bit NULL
);
/****** Object:  Table AssetValueHistory    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE AssetValueHistory(
	PK int NOT NULL AUTO_INCREMENT,
	AssetPK int NOT NULL,
	ValuationDate datetime NOT NULL,
	CurrentValue DECIMAL(13,2) NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssetValueHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table BatchReportList    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE BatchReportList(
	BatchReportPK int NOT NULL AUTO_INCREMENT,
	RandomString varchar(50) NOT NULL,
	ReportID varchar(50) NOT NULL,
	ReportPK int NOT NULL,
	DisplayOrder char(10) NOT NULL,
	Datetime datetime NOT NULL,
	Group1Total nvarchar(500) NULL,
	Group2Total nvarchar(500) NULL,
	Group3Total nvarchar(500) NULL,
	Group4Total nvarchar(500) NULL,
	Group5Total nvarchar(500) NULL,
	GrandTotal nvarchar(500) NULL,
	RowLastPage int NULL,
	LastGroup1 varchar(255) NULL,
	LastGroup2 varchar(255) NULL,
	LastGroup3 varchar(255) NULL,
	LastGroup4 varchar(255) NULL,
	LastGroup5 varchar(255) NULL,
 CONSTRAINT PK_BatchReportList PRIMARY KEY CLUSTERED 
(
	BatchReportPK ASC
)
);
/****** Object:  Table Bulletin    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Bulletin(
	BulletinPK int NOT NULL AUTO_INCREMENT,
	BulletinName varchar(500) NOT NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	ActiveDate datetime NOT NULL,
	InActiveDate datetime NOT NULL,
	BulletinText varchar(4000) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Bulletin PRIMARY KEY NONCLUSTERED 
(
	BulletinPK ASC
)
);
/****** Object:  Table Cache    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Cache(
	CachePK int NOT NULL AUTO_INCREMENT,
	CacheID varchar(50) NOT NULL,
	ParentPK int NOT NULL,
	Type varchar(50) NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate varchar(25) NOT NULL,
 CONSTRAINT PK_Cache PRIMARY KEY CLUSTERED 
(
	CachePK ASC
)
);
/****** Object:  Table Category    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Category(
	CategoryPK int NOT NULL AUTO_INCREMENT,
	CategoryID varchar(25) NOT NULL,
	CategoryName varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	Budget DECIMAL(13,2) NOT NULL,
	Active bit NOT NULL,
	Comments varchar(4000) NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	MCModule varchar(25) NULL,
	MCModuleDesc varchar(50) NULL,
	ParentPK int NULL,
	ParentID varchar(25) NULL,
	ParentName varchar(50) NULL,
	FormName varchar(50) NULL,
 CONSTRAINT PK_Category PRIMARY KEY NONCLUSTERED 
(
	CategoryPK ASC
),
 CONSTRAINT IX_Category UNIQUE CLUSTERED 
(
	CategoryID ASC
)
);
/****** Object:  Table ChatFolders    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ChatFolders(
	ChatFoldersPK int NOT NULL AUTO_INCREMENT,
	UserPK int NOT NULL,
	FolderName varchar(50) NOT NULL,
 CONSTRAINT PK_ChatFolders PRIMARY KEY CLUSTERED 
(
	ChatFoldersPK ASC
)
);
/****** Object:  Table ChatLog    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ChatLog(
	ChatLogPK int NOT NULL AUTO_INCREMENT,
	ChatLogID varchar(50) NOT NULL,
	UserPK int NOT NULL,
	MyName varchar(150) NOT NULL,
	MyPhoto varchar(500) NOT NULL,
	FriendPK int NOT NULL,
	FriendName varchar(150) NOT NULL,
	FriendPhoto varchar(500) NOT NULL,
	Message varchar(500) NOT NULL,
	MessageTime datetime NOT NULL,
	MessageViewed bit NOT NULL,
	IsOfflineMsg bit NOT NULL,
	WhoSentPK int NOT NULL,
	ExecuteScript varchar(8000) NOT NULL,
	ChatActive bit NOT NULL,
 CONSTRAINT PK_ChatLog PRIMARY KEY CLUSTERED 
(
	ChatLogPK ASC
)
);
/****** Object:  Table ChatLogINI    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ChatLogINI(
	ChatLogINIPK int NOT NULL AUTO_INCREMENT,
	ChatLogID varchar(50) NOT NULL,
	FriendPK int NOT NULL,
	INIKey varchar(50) NOT NULL,
	INIValue varchar(500) NOT NULL,
	INICaption varchar(150) NOT NULL,
 CONSTRAINT PK_ChatLogINI PRIMARY KEY CLUSTERED 
(
	ChatLogINIPK ASC
)
);
/****** Object:  Table ChatTo    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ChatTo(
	pkChatTo int NOT NULL AUTO_INCREMENT,
	ChatMsgPK int NOT NULL,
	LaborPK int NOT NULL,
	LaborName varchar(150) NOT NULL,
	ToType int NOT NULL,
 CONSTRAINT PK_ChatTo PRIMARY KEY CLUSTERED 
(
	pkChatTo ASC
)
);
/****** Object:  Table Classification    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Classification(
	ClassificationPK int NOT NULL AUTO_INCREMENT,
	ClassificationID varchar(50) NOT NULL,
	ClassificationName varchar(100) NULL,
	IsLocation bit NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	AssetCount int NOT NULL,
	Icon varchar(200) NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	IsMeter bit NULL,
	Meter1Units varchar(25) NULL,
	Meter1UnitsDesc varchar(50) NULL,
	Meter1TrackHistory bit NULL,
	Meter2Units varchar(25) NULL,
	Meter2UnitsDesc varchar(50) NULL,
	Meter2TrackHistory bit NULL,
	Photo varchar(200) NULL,
	DisplayOrder int NULL,
	PMCycleStartBy varchar(25) NULL,
	PMCycleStartByDesc varchar(50) NULL,
	PMCycleStartDate datetime NULL,
	PMCounter smallint NULL,
	RiskLevelDefault smallint NULL,
	RiskLevel smallint NULL,
	Model varchar(50) NULL,
	ModelNumber varchar(50) NULL,
	ModelNumberMFG varchar(50) NULL,
	ModelLine varchar(25) NULL,
	ModelLineDesc varchar(50) NULL,
	ModelSeries varchar(25) NULL,
	ModelSeriesDesc varchar(50) NULL,
	SystemPlatform varchar(25) NULL,
	SystemPlatformDesc varchar(50) NULL,
	ManufacturerPK int NULL,
	ManufacturerID varchar(25) NULL,
	ManufacturerName varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	LockFieldsinAssetModule bit NULL,
	LockNameinAssetModule bit NULL,
	FormName varchar(50) NULL,
	ClassIndustry varchar(25) NULL,
	ClassIndustryDesc varchar(50) NULL,
	RiskAssessmentRequired bit NULL,
	AssessedByPK int NULL,
	AssessedByID varchar(25) NULL,
	AssessedByName varchar(50) NULL,
	LastAssessed datetime NULL,
	RiskAssessmentGroup varchar(25) NULL,
	RiskAssessmentGroupDesc varchar(50) NULL,
	RiskFactor1 varchar(100) NULL,
	RiskFactor1Score smallint NULL,
	RiskFactor2 varchar(100) NULL,
	RiskFactor2Score smallint NULL,
	RiskFactor3 varchar(100) NULL,
	RiskFactor3Score smallint NULL,
	RiskFactor4 varchar(100) NULL,
	RiskFactor4Score smallint NULL,
	RiskFactor5 varchar(100) NULL,
	RiskFactor5Score smallint NULL,
	RiskScore decimal(18, 0) NULL,
	PMRequired bit NULL,
	PlanForImprovement bit NULL,
	HIPPARelated bit NULL,
	StatementOfConditions bit NULL,
	StatementOfConditionsCompliant bit NULL,
 CONSTRAINT PK_Classification PRIMARY KEY NONCLUSTERED 
(
	ClassificationPK ASC
),
 CONSTRAINT IX_Classification UNIQUE CLUSTERED 
(
	ClassificationID ASC
)
);
/****** Object:  Table ClassificationAncestor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationAncestor(
	System char(5) NOT NULL,
	ClassificationPK int NOT NULL,
	AncestorPK int NOT NULL,
	AncestorLevel int NULL,
	ClassificationLevel int NULL,
 CONSTRAINT PK_ClassificationAncestor PRIMARY KEY CLUSTERED 
(
	System ASC,
	ClassificationPK ASC,
	AncestorPK ASC
)
);
/****** Object:  Table ClassificationCause    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationCause(
	ProblemPK int NOT NULL,
	ClassificationPK int NOT NULL,
	FailurePK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	PK int NOT NULL AUTO_INCREMENT,
 CONSTRAINT PK_ClassificationCause PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationComponent    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationComponent(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	ComponentPK int NOT NULL,
	ComponentName varchar(50) NULL,
	ComponentID varchar(50) NULL,
	DisplayOrder smallint NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationComponent PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationComponentSpecification    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationComponentSpecification(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	ComponentPK int NOT NULL,
	SpecificationPK int NOT NULL,
	SpecificationName varchar(50) NULL,
	DisplayOrder smallint NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(50) NULL,
	ValueDate datetime NULL,
	ValueHi decimal(19, 6) NULL,
	ValueLow decimal(19, 6) NULL,
	TrackHistory bit NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationComponentSpecification PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationContract    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationContract(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	CompanyPK int NOT NULL,
	PeriodStart datetime NULL,
	PeriodEnd datetime NULL,
	VendorContractNum varchar(50) NULL,
	ContractSummary varchar(4000) NULL,
	ContractPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationContract PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationDocument(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_ClassificationDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationHierarchy    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationHierarchy(
	System char(5) NOT NULL,
	ClassificationPK int NOT NULL,
	ParentPK int NOT NULL,
	ClassificationLevel int NULL,
	HasChildren bit NULL,
	OrderAlpha int NULL,
	OrderType int NULL,
	ParentLocation varchar(150) NULL,
	ParentLocationAll varchar(4000) NULL,
 CONSTRAINT PK_ClassificationHierarchy PRIMARY KEY CLUSTERED 
(
	System ASC,
	ClassificationPK ASC,
	ParentPK ASC
)
);
/****** Object:  Table ClassificationLabor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationLabor(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	LaborPK int NOT NULL,
	Priority smallint NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	AutoAssign bit NULL,
	BackupResource bit NULL,
 CONSTRAINT PK_ClassificationLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationPart    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationPart(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	PartPK int NOT NULL,
	Qty int NULL,
	Comments varchar(2000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationPart PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationPM    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationPM(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	PMPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationPM PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationProblem    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationProblem(
	ClassificationPK int NOT NULL,
	FailurePK int NOT NULL,
	ProcedurePK int NULL,
	AvailableToRequester bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	PK int NOT NULL AUTO_INCREMENT,
 CONSTRAINT PK_ClassificationProblem PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationProcedure    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationProcedure(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	ProcedurePK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationProcedure PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationRemedy    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationRemedy(
	CausePK int NOT NULL,
	ClassificationPK int NOT NULL,
	FailurePK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	PK int NOT NULL AUTO_INCREMENT,
	ProblemPK int NOT NULL,
 CONSTRAINT PK_ClassificationRemedy PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationSpecification    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationSpecification(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	SpecificationPK int NOT NULL,
	SpecificationName varchar(50) NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(6000) NULL,
	ValueDate datetime NULL,
	ValueHi decimal(19, 6) NULL,
	ValueLow decimal(19, 6) NULL,
	TrackHistory bit NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ValueOptimal decimal(19, 6) NULL,
	ValueOutOfRangeWO bit NULL,
	ProcedurePK int NULL,
 CONSTRAINT PK_ClassificationSpecification PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationTask    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationTask(
	PK int NOT NULL AUTO_INCREMENT,
	ClassificationPK int NOT NULL,
	TaskPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ClassificationTask PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ClassificationTreeStatus    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ClassificationTreeStatus(
	System char(5) NOT NULL,
	IsOrdered bit NOT NULL
);
/****** Object:  Table Company    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Company(
	CompanyPK int NOT NULL AUTO_INCREMENT,
	CompanyID varchar(25) NOT NULL,
	CompanyName varchar(100) NULL,
	Active bit NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	URL varchar(200) NULL,
	Division varchar(50) NULL,
	CustomerNum varchar(50) NULL,
	DunsNum varchar(50) NULL,
	PayTaxToVendor bit NOT NULL,
	TaxRate decimal(19, 6) NOT NULL,
	TaxCode varchar(25) NULL,
	TaxCodeDesc varchar(50) NULL,
	TaxExempt varchar(25) NULL,
	TaxExemptDesc varchar(50) NULL,
	TaxExemptNumber varchar(50) NULL,
	ECommerceEnabled bit NOT NULL,
	FOBPoint varchar(50) NULL,
	FreightTerms varchar(25) NULL,
	FreightTermsDesc varchar(50) NULL,
	ShipVia varchar(25) NULL,
	ShipViaDesc varchar(50) NULL,
	BankName varchar(50) NULL,
	BankRefNo varchar(50) NULL,
	PayTo varchar(50) NULL,
	PaymentTerms varchar(25) NULL,
	PaymentTermsDesc varchar(50) NULL,
	PaymentOnReceipt bit NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	Attention varchar(50) NULL,
	Address varchar(50) NULL,
	Address2 varchar(50) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(50) NULL,
	Country varchar(50) NULL,
	RemitToAddress varchar(50) NULL,
	RemitToAddress2 varchar(50) NULL,
	RemitToCity varchar(50) NULL,
	RemitToState varchar(50) NULL,
	RemitToZip varchar(50) NULL,
	RemitToCountry varchar(50) NULL,
	Phone varchar(20) NULL,
	Fax varchar(20) NULL,
	Legal varchar(2000) NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	UseAsLaborResource bit NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	Currency varchar(25) NULL,
	SMSEmail varchar(50) NULL,
	PhoneSales varchar(20) NULL,
	PhoneCustSupport varchar(20) NULL,
	PhoneTechSupport varchar(20) NULL,
 CONSTRAINT PK_Company PRIMARY KEY NONCLUSTERED 
(
	CompanyPK ASC
),
 CONSTRAINT IX_Company UNIQUE CLUSTERED 
(
	CompanyID ASC
)
);
/****** Object:  Table CompanyDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE CompanyDocument(
	PK int NOT NULL AUTO_INCREMENT,
	CompanyPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_CompanyDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table CompanyLabor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE CompanyLabor(
	PK int NOT NULL AUTO_INCREMENT,
	CompanyPK int NOT NULL,
	LaborPK int NOT NULL,
	PrimaryContact bit NOT NULL,
	POContact bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_CompanyLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table CompanyNote    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE CompanyNote(
	PK int NOT NULL AUTO_INCREMENT,
	CompanyPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_CompanyNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Component    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Component(
	ComponentPK int NOT NULL AUTO_INCREMENT,
	ComponentName varchar(50) NOT NULL,
	Description varchar(150) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	Active bit NOT NULL,
	Comments varchar(4000) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Component PRIMARY KEY NONCLUSTERED 
(
	ComponentPK ASC
)
);
/****** Object:  Table ComponentSpecification    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ComponentSpecification(
	PK int NOT NULL AUTO_INCREMENT,
	ComponentPK int NOT NULL,
	SpecificationPK int NOT NULL,
	SpecificationName varchar(50) NULL,
	DisplayOrder smallint NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(50) NULL,
	ValueDate datetime NULL,
	ValueHi decimal(19, 6) NULL,
	ValueLow decimal(19, 6) NULL,
	TrackHistory bit NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ComponentSpecification PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Contract    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Contract(
	ContractPK int NOT NULL AUTO_INCREMENT,
	ContractID varchar(25) NOT NULL,
	ContractName varchar(100) NULL,
	CompanyPK int NULL,
	CompanyID varchar(25) NULL,
	CompanyName varchar(100) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	PeriodStart datetime NULL,
	PeriodEnd datetime NULL,
	VendorContractNum varchar(50) NULL,
	ContractSummary varchar(4000) NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	AssetPK int NULL,
	AssetID varchar(25) NULL,
	AssetName varchar(100) NULL,
	Term varchar(50) NULL,
	Type varchar(50) NULL,
	Cost DECIMAL(13,2) NULL,
	POPK int NULL,
	POID varchar(25) NULL,
	POName varchar(100) NULL,
	PONumber varchar(50) NULL,
	Expired bit NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFChar11 varchar(50) NULL,
	UDFChar12 varchar(50) NULL,
	UDFChar13 varchar(50) NULL,
	UDFChar14 varchar(50) NULL,
	UDFChar15 varchar(50) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	DepartmentPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(100) NULL,
	LaborPK int NULL,
	AnnualCost DECIMAL(13,2) NULL,
	UDFBit3 bit NOT NULL,
	UDFBit4 bit NOT NULL,
	UDFBit5 bit NOT NULL,
 CONSTRAINT PK_Contract PRIMARY KEY NONCLUSTERED 
(
	ContractPK ASC
),
 CONSTRAINT IX_Contract UNIQUE CLUSTERED 
(
	ContractID ASC
)
);
/****** Object:  Table ContractDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ContractDocument(
	PK int NOT NULL AUTO_INCREMENT,
	ContractPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_ServiceContractDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ContractNote    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ContractNote(
	PK int NOT NULL AUTO_INCREMENT,
	ContractPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ContractNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table CriteriaGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE CriteriaGroup(
	CriteriaGroupPK int NOT NULL AUTO_INCREMENT,
	CriteriaGroupName varchar(255) NULL,
	ModuleID char(2) NULL,
	DisplayOrder smallint NULL,
	Icon varchar(200) NULL,
	RepairCenterPK int NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_CriteriaGroup PRIMARY KEY NONCLUSTERED 
(
	CriteriaGroupPK ASC
)
);
/****** Object:  Table CriteriaGroupReport    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE CriteriaGroupReport(
	PK int NOT NULL AUTO_INCREMENT,
	CriteriaGroupPK int NOT NULL,
	ReportPK int NOT NULL,
 CONSTRAINT PK_CriteriaGroupReport PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table DatabaseGroups    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE DatabaseGroups(
	GroupName varchar(100) NOT NULL,
	ContainerGUID varchar(50) NOT NULL
);
/****** Object:  Table DataDict    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE DataDict(
	DataDictPK int NOT NULL AUTO_INCREMENT,
	TABLE_CATALOG varchar(256) NULL,
	TABLE_SCHEMA varchar(256) NULL,
	TABLE_NAME varchar(256) NULL,
	COLUMN_NAME varchar(256) NULL,
	ORDINAL_POSITION smallint NULL,
	COLUMN_DEFAULT varchar(4000) NULL,
	IS_NULLABLE varchar(50) NULL,
	DATA_TYPE varchar(256) NULL,
	CHARACTER_MAXIMUM_LENGTH int NULL,
	FIELD_LABEL varchar(50) NULL,
	HOTKEY char(10) NULL,
	FIELD_SIZE int NULL,
	TEMPLATE_DISPLAY bit NULL,
	TEMPLATE_ORDER smallint NULL,
	TEMPLATE_ALIGN varchar(50) NULL,
	REQUIRED bit NULL,
	LOOKUP_TABLE bit NULL,
	LOOKUP_TABLE_NAME varchar(50) NULL,
	FK_LOOKUP varchar(25) NULL,
	ONCHANGE_JS varchar(50) NULL,
	ONKEYPRESS_JS varchar(50) NULL,
	ONFOCUS_JS varchar(50) NULL,
	ONBLUR_JS varchar(50) NULL,
	MCDEFAULT varchar(50) NULL,
	MCPOPUPONAPPLY varchar(50) NULL,
	CUSTOM bit NULL,
	AUDIT bit NULL,
	USE_FOR_AUDIT_NAME bit NULL,
	USE_TIME_IF_DATE bit NULL,
	REPORT_NOSELECT bit NULL,
	REPORT_LABEL varchar(50) NULL,
	TOTALIFSELECTED bit NULL,
	ROWVERSIONDATE datetime NULL,
	AVAILABLEFOREDIT bit NULL,
	SCREENSHOT1 varchar(255) NULL,
	SCREENSHOT2 varchar(255) NULL,
	SCREENSHOT3 varchar(255) NULL,
	REPORT_EDIT char(2) NULL,
	COPYTOMODULEONVALIDATE char(2) NULL,
	COPYTOFIELDONVALIDATE varchar(100) NULL,
	FORM_HIDDEN bit NULL,
	FORM_REQUIRED bit NULL,
	FORM_LABEL1 varchar(50) NULL,
	FORM_LABEL2 varchar(50) NULL,
	FORM_LABEL3 varchar(50) NULL,
	FORM_LABEL4 varchar(50) NULL,
	FORM_LABEL5 varchar(50) NULL,
	FORM_LABEL6 varchar(50) NULL,
	FORM_LABEL7 varchar(50) NULL,
	FORM_LABEL8 varchar(50) NULL,
	FORM_LABEL9 varchar(50) NULL,
	FORM_LABEL10 varchar(50) NULL,
	FORM_FIELD_ID varchar(50) NULL,
	FORM_FIELD_VALIDATE bit NULL,
	BATCHUPDATABLE bit NULL,
	FORM_HIDDEN2 bit NOT NULL,
 CONSTRAINT PK_DataDict PRIMARY KEY NONCLUSTERED 
(
	DataDictPK ASC
),
 CONSTRAINT IX_DataDict UNIQUE CLUSTERED 
(
	TABLE_NAME ASC,
	COLUMN_NAME ASC
)
);
/****** Object:  Table Debug    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Debug(
	PK int NOT NULL AUTO_INCREMENT,
	debugout1 varchar(8000) NULL,
	debugout2 varchar(8000) NULL,
	debugout3 varchar(8000) NULL,
	debugout4 nvarchar(4000) NULL,
 CONSTRAINT PK_Debug PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table DebugSQL    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE DebugSQL(
	PK int NOT NULL AUTO_INCREMENT,
	TransactionDate datetime NOT NULL,
	container_guid uniqueidentifier NULL,
	container_name nvarchar(50) NULL,
	user_guid uniqueidentifier NULL,
	LaborPK int NULL,
	LaborName nvarchar(50) NULL,
	ADOFunction nvarchar(100) NULL,
	SQL nvarchar(max) NULL,
	SQLParams nvarchar(max) NULL,
	ProcessingTime decimal(19, 6) NULL,
	DB_DOK bit NULL,
	DB_DError nvarchar(4000) NULL,
	SQLDBCS nvarchar(max) NULL,
	URL nvarchar(4000) NULL,
	ScriptName nvarchar(100) NULL,
	DBServer nvarchar(100) NULL,
	DBName nvarchar(100) NULL,
	LanguageCode nvarchar(50) NULL,
	Logo nvarchar(100) NULL,
	IPAddress nvarchar(50) NULL,
 CONSTRAINT PK_DebugSQL PRIMARY KEY CLUSTERED 
(
	PK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table Department    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Department(
	DepartmentPK int NOT NULL AUTO_INCREMENT,
	DepartmentID varchar(25) NOT NULL,
	DepartmentName varchar(100) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	Active bit NOT NULL,
	CustomerNum varchar(50) NULL,
	URL varchar(200) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	Attention varchar(50) NULL,
	Address varchar(50) NULL,
	Address2 varchar(50) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(50) NULL,
	Phone varchar(20) NULL,
	Fax varchar(20) NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	CostRegular DECIMAL(13,2) NULL,
	CostOvertime DECIMAL(13,2) NULL,
	CostOther DECIMAL(13,2) NULL,
	ChargePercentage decimal(19, 6) NULL,
	ChargeRate DECIMAL(13,2) NULL,
	Budget DECIMAL(13,2) NULL,
	PMCycleStartDate datetime NULL,
	RiskLevel smallint NULL,
	PMCounter smallint NULL,
 CONSTRAINT PK_Department PRIMARY KEY NONCLUSTERED 
(
	DepartmentPK ASC
),
 CONSTRAINT IX_Department UNIQUE CLUSTERED 
(
	DepartmentID ASC
)
);
/****** Object:  Table DepartmentDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE DepartmentDocument(
	PK int NOT NULL AUTO_INCREMENT,
	DepartmentPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_DepartmentDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table DepartmentLabor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE DepartmentLabor(
	PK int NOT NULL AUTO_INCREMENT,
	DepartmentPK int NOT NULL,
	LaborPK int NOT NULL,
	PrimaryContact bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_DepartmentLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table DepartmentNote    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE DepartmentNote(
	PK int NOT NULL AUTO_INCREMENT,
	DepartmentPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_DepartmentNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Document    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Document(
	DocumentPK int NOT NULL AUTO_INCREMENT,
	DocumentID varchar(50) NOT NULL,
	DocumentName varchar(150) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	LaborPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(50) NULL,
	DocumentType varchar(25) NOT NULL,
	DocumentTypeDesc varchar(200) NULL,
	LocationType varchar(25) NOT NULL,
	LocationTypeDesc varchar(50) NULL,
	Location varchar(500) NOT NULL,
	PrintWithWO bit NOT NULL,
	SentWithEmail bit NOT NULL,
	Active bit NOT NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	DocumentText varchar(max) NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_Document PRIMARY KEY NONCLUSTERED 
(
	DocumentPK ASC
),
 CONSTRAINT IX_Document UNIQUE CLUSTERED 
(
	DocumentID ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table EmailGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE EmailGroup(
	EmailGroupPK int NOT NULL AUTO_INCREMENT,
	EmailGroupName varchar(500) NULL,
	RepairCenterPK int NULL,
	EmailTo varchar(max) NULL,
	EmailCC varchar(max) NULL,
	EmailBCC varchar(max) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_EmailGroup PRIMARY KEY NONCLUSTERED 
(
	EmailGroupPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table Escalation    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Escalation(
	RulePK int NOT NULL,
	PK int NOT NULL,
	Count int NOT NULL,
	NextRepeat datetime NOT NULL,
 CONSTRAINT PK_Escalation PRIMARY KEY CLUSTERED 
(
	RulePK ASC,
	PK ASC
)
);
/****** Object:  Table Event    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Event(
	EventPK int NOT NULL AUTO_INCREMENT,
	Event varchar(50) NULL,
	WOPK int NULL,
	PartPK int NULL,
	LocationPK int NULL,
	Quantity decimal(19, 6) NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Event PRIMARY KEY NONCLUSTERED 
(
	EventPK ASC
)
);
/****** Object:  Table Failure    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Failure(
	FailurePK int NOT NULL AUTO_INCREMENT,
	FailureID varchar(25) NOT NULL,
	FailureName varchar(50) NULL,
	ParentPK int NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AvailableToRequester bit NOT NULL,
	Active bit NOT NULL,
	Comments varchar(4000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	Severity varchar(25) NULL,
	SeverityDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
 CONSTRAINT PK_Failure PRIMARY KEY NONCLUSTERED 
(
	FailurePK ASC
),
 CONSTRAINT IX_Failure UNIQUE CLUSTERED 
(
	FailureID ASC
)
);
/****** Object:  Table FavoriteGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE FavoriteGroup(
	FavoriteGroupPK int NOT NULL AUTO_INCREMENT,
	FavoriteGroupName varchar(100) NOT NULL,
	LaborPK int NOT NULL,
	FavoriteGroupColor varchar(10) NULL,
	GlobalGroupView bit NOT NULL,
	GlobalGroupUpdate bit NOT NULL,
	GlobalGroupOwnerPK int NULL,
	DefaultGroup bit NOT NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_FavoriteGroup PRIMARY KEY NONCLUSTERED 
(
	FavoriteGroupPK ASC
)
);
/****** Object:  Table FavoriteGroupKeys    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE FavoriteGroupKeys(
	PK int NOT NULL AUTO_INCREMENT,
	FavoriteGroupPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	KeyValue int NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_FavoriteGroupKeys PRIMARY KEY NONCLUSTERED 
(
	PK ASC
),
 CONSTRAINT IX_FavoriteGroupKeys_2 UNIQUE CLUSTERED 
(
	FavoriteGroupPK ASC,
	ModuleID ASC,
	KeyValue ASC
)
);
/****** Object:  Table Filestore    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Filestore(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	ParentPK int NOT NULL,
	ParentID varchar(25) NOT NULL,
	Tablename varchar(50) NOT NULL,
	OriginalFilename varchar(500) NOT NULL,
	FilenameOnDisk varchar(255) NOT NULL,
	Type int NOT NULL,
	Version decimal(18, 3) NOT NULL,
	Category varchar(255) NOT NULL,
	ContactPK int NOT NULL,
	ContactID varchar(25) NOT NULL,
	ContactName varchar(150) NOT NULL,
	Title varchar(255) NOT NULL,
	Description varchar(max) NOT NULL,
	RelatedPK int NOT NULL,
	RelatedID varchar(25) NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Filestore PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table FRCriteria    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE FRCriteria(
	FRCriteriaPK int NOT NULL AUTO_INCREMENT,
	FRPK int NOT NULL,
	DisplayTable varchar(50) NOT NULL,
	DisplayField varchar(50) NOT NULL,
	SQLWhereTable varchar(50) NOT NULL,
	SQLWhereField varchar(50) NOT NULL,
	DefaultCritValue varchar(2000) NULL,
	CritName varchar(2000) NULL,
	Operator varchar(50) NULL,
	isMulti bit NOT NULL,
	AskLater bit NOT NULL,
	LabelOverride varchar(255) NULL,
	DisplayOrder int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_FRCriteria PRIMARY KEY NONCLUSTERED 
(
	FRCriteriaPK ASC
)
);
/****** Object:  Table FRFields    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE FRFields(
	FRFieldPK int NOT NULL AUTO_INCREMENT,
	FRPK int NOT NULL,
	AGFunction varchar(50) NULL,
	RFTable varchar(50) NOT NULL,
	RFField varchar(50) NOT NULL,
	Alias varchar(50) NULL,
	DisplayOrder int NOT NULL,
	Display bit NOT NULL,
	NotUserSelectable bit NOT NULL,
	LabelOverride varchar(255) NULL,
	TotalIfSelected bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NULL,
 CONSTRAINT PK_FRFields PRIMARY KEY NONCLUSTERED 
(
	FRFieldPK ASC
)
);
/****** Object:  Table FRMain    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE FRMain(
	FRPK int NOT NULL AUTO_INCREMENT,
	ModuleID char(2) NOT NULL,
	Table_Name varchar(50) NOT NULL,
	FromSQL varchar(500) NULL,
	JoinSQL varchar(500) NULL,
	WhereSQL varchar(500) NULL,
 CONSTRAINT PK_FRMain PRIMARY KEY NONCLUSTERED 
(
	FRPK ASC
)
);
/****** Object:  Table GISLayer    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GISLayer(
	GISViewerName varchar(100) NOT NULL,
	GISLayerGroupName varchar(100) NULL,
	GISLayerName varchar(100) NOT NULL,
	GISLayerLabel varchar(100) NULL,
	GISLayerInitialStateOn bit NULL,
	GISLayerFileName varchar(200) NULL,
	GISLayerDBConnectionString varchar(500) NULL,
	GISLayerSQL varchar(1000) NULL,
	GISLayerKeyField varchar(100) NULL,
	GISLayerShowLabel bit NULL,
	GISLayerLabelField varchar(100) NULL,
	GISLayerLabelExpression varchar(500) NULL,
	GISLayerLabelFont varchar(25) NULL,
	GISLayerLabelFontSize smallint NULL,
	GISLayerLabelFontBold bit NULL,
	GISLayerLabelShowOutline bit NULL,
	GISLayerLabelOutlineColor varchar(25) NULL,
	GISLayerSymbolPointStyle varchar(50) NULL,
	GISLayerSymbolSize smallint NULL,
	GISLayerSymbolLineColor varchar(25) NULL,
	GISLayerSymbolFillColor varchar(25) NULL,
	GISLayerSymbolFillStyle varchar(25) NULL,
	GISLayerSymbolBitmap varchar(200) NULL,
	GISLayerIdentifyCalloutText varchar(500) NULL,
	GISLayerOrder smallint NULL,
	Active bit NULL,
	Icon varchar(200) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	SynchLayerWithGISTable bit NULL,
	GISTable varchar(25) NULL,
	GISTableNameField varchar(50) NULL,
	AssetIDPrefix varchar(25) NULL,
	ClassificationID varchar(25) NULL,
	ParentID varchar(50) NULL,
	TransferFieldsAsSpecs bit NULL,
	GISTableFromField varchar(50) NULL,
	GISTableToField varchar(50) NULL,
 CONSTRAINT PK_GISLayer PRIMARY KEY CLUSTERED 
(
	GISViewerName ASC,
	GISLayerName ASC
)
);
/****** Object:  Table GISLayerGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GISLayerGroup(
	GISViewerName varchar(100) NOT NULL,
	GISLayerGroupName varchar(100) NOT NULL,
	GISLayerGroupOrder smallint NULL,
	Active bit NULL,
	Icon varchar(200) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GISLayerGroup PRIMARY KEY CLUSTERED 
(
	GISViewerName ASC,
	GISLayerGroupName ASC
)
);
/****** Object:  Table GISLayerLabor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GISLayerLabor(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	GISLayerName varchar(100) NOT NULL,
	Display bit NULL,
 CONSTRAINT PK_GISLayerLabor PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table GISViewer    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GISViewer(
	GISViewerName varchar(100) NOT NULL,
	GISViewerURL varchar(2000) NULL,
	GISViewerWidth smallint NULL,
	GISViewerHeight smallint NULL,
	GISViewerBackColor varchar(25) NULL,
	ExternalViewer bit NULL,
	Active bit NULL,
	Icon varchar(200) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GISViewer PRIMARY KEY CLUSTERED 
(
	GISViewerName ASC
)
);
/****** Object:  Table GlobalAddress    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GlobalAddress(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	ParentPK int NOT NULL,
	ParentID varchar(25) NOT NULL,
	Tablename varchar(50) NOT NULL,
	Address1 varchar(255) NOT NULL,
	Address2 varchar(50) NOT NULL,
	City varchar(50) NOT NULL,
	State varchar(50) NOT NULL,
	Zipcode varchar(50) NOT NULL,
	Country varchar(50) NOT NULL,
	AddressType varchar(50) NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GlobalAddress PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table GlobalContacts    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GlobalContacts(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	Tablename varchar(50) NOT NULL,
	Firstname varchar(50) NOT NULL,
	Middlename varchar(50) NOT NULL,
	Lastname varchar(50) NOT NULL,
	Email varchar(255) NOT NULL,
	Website varchar(255) NOT NULL,
	Category varchar(255) NOT NULL,
	IsDefault bit NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GlobalContacts PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table GlobalNotes    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GlobalNotes(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	ParentPK int NOT NULL,
	ParentID varchar(25) NOT NULL,
	Tablename varchar(50) NOT NULL,
	Subject varchar(255) NOT NULL,
	Note varchar(max) NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GlobalNotes PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table GlobalPhones    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GlobalPhones(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	ParentPK int NOT NULL,
	ParentID varchar(25) NOT NULL,
	Tablename varchar(50) NOT NULL,
	Phone varchar(50) NOT NULL,
	Ext varchar(10) NOT NULL,
	PhoneType varchar(50) NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GlobalPhones PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table GlobalShare    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE GlobalShare(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	GlobalID varchar(25) NOT NULL,
	GlobalName varchar(50) NOT NULL,
	ParentPK int NOT NULL,
	ParentID varchar(25) NOT NULL,
	TableName varchar(50) NOT NULL,
	ChildPK int NOT NULL,
	ChildID varchar(50) NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitals varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_GlobalShare PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
)
);
/****** Object:  Table Help    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Help(
	HelpPK int NOT NULL AUTO_INCREMENT,
	ModuleID char(2) NULL,
	TABLE_NAME varchar(256) NULL,
	COLUMN_NAME varchar(256) NULL,
	ORDINAL_POSITION smallint NULL,
	FIELD_LABEL varchar(50) NULL,
	TEMPLATE_ORDER smallint NULL,
	HelpText varchar(max) NULL,
 CONSTRAINT PK_Help PRIMARY KEY CLUSTERED 
(
	HelpPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table History    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE History(
	pkHistory int NOT NULL AUTO_INCREMENT,
	hUID int NOT NULL,
	hPID int NOT NULL,
	hPIDChar varchar(50) NOT NULL,
	hType varchar(50) NOT NULL,
	hAction varchar(500) NOT NULL,
	hGlobalID varchar(50) NOT NULL,
	hModified datetime NOT NULL,
	hActive int NOT NULL,
	hTimestamp datetime NOT NULL,
 CONSTRAINT PK_History PRIMARY KEY CLUSTERED 
(
	pkHistory ASC
)
);
/****** Object:  Table Holiday    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Holiday(
	HolidayPK int NOT NULL AUTO_INCREMENT,
	HolidayName varchar(50) NULL,
	IsFixedDate bit NOT NULL,
	HolidayDate datetime NULL,
	HolidayMonth decimal(19, 6) NULL,
	HolidayDay decimal(19, 6) NULL,
	Country varchar(25) NULL,
	CountryDesc varchar(50) NULL,
	Icon varchar(200) NULL,
	Graphic varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Holiday PRIMARY KEY NONCLUSTERED 
(
	HolidayPK ASC
),
 CONSTRAINT IX_Holiday_1 UNIQUE CLUSTERED 
(
	HolidayName ASC
)
);
/****** Object:  Table ImportErrors    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ImportErrors(
	PK int NOT NULL AUTO_INCREMENT,
	RecordID varchar(100) NULL,
	ErrorMessage varchar(7500) NULL,
	Type char(1) NULL,
	ScriptKey int NULL
);
/****** Object:  Table InOutTransaction    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE InOutTransaction(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NULL,
	WOPK int NULL,
	AccountPK int NULL,
	PartPK int NULL,
	ToolPK int NULL,
	AssetPK int NULL,
	LocationPK int NULL,
	TransactionDate datetime NULL,
	TransactionTime varchar(25) NULL,
	Notes varchar(255) NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NULL,
	CheckedIn bit NULL,
	handleby int NULL
);
/****** Object:  Table InterfaceUser    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE InterfaceUser(
	InterfaceUserPK int NOT NULL AUTO_INCREMENT,
	InterfaceName varchar(50) NOT NULL,
	Logon varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	role_code char(15) NULL,
	LaborPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(50) NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_InterfaceUser PRIMARY KEY NONCLUSTERED 
(
	InterfaceUserPK ASC
),
 CONSTRAINT IX_InterfaceUser_1 UNIQUE CLUSTERED 
(
	Logon ASC
),
 CONSTRAINT IX_InterfaceUser_2 UNIQUE NONCLUSTERED 
(
	InterfaceName ASC
)
);
/****** Object:  Table KeyAssetLocation    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KeyAssetLocation(
	PK int NOT NULL AUTO_INCREMENT,
	KeyPK int NULL,
	AssetPK int NULL,
	IsActive bit NULL,
	LModifiedDate datetime NULL,
	RowVersionDate datetime NULL,
 CONSTRAINT PK_KeyAssetLocation PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table KeyData    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KeyData(
	KeyDataPK int NOT NULL AUTO_INCREMENT,
	KeyDataID varchar(100) NULL,
	KeyPK int NULL,
	LaborPK int NULL,
	Status varchar(35) NULL,
	IssuedDate datetime NULL,
	DueDate datetime NULL,
	DateReturned datetime NULL,
	IssuedQty smallint NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NULL,
 CONSTRAINT PK_KeyData PRIMARY KEY NONCLUSTERED 
(
	KeyDataPK ASC
)
);
/****** Object:  Table Keys    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Keys(
	KeyPK int NOT NULL AUTO_INCREMENT,
	KeyID char(100) NULL,
	KeyName varchar(200) NULL,
	KeyDescription varchar(200) NULL,
	CompanyPK int NULL,
	CompanyID varchar(25) NULL,
	CompanyName varchar(100) NULL,
	Qty smallint NULL,
	IssuedQty smallint NULL,
	Comments varchar(100) NULL,
	IsActive bit NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NULL,
 CONSTRAINT PK_Keys PRIMARY KEY NONCLUSTERED 
(
	KeyPK ASC
)
);
/****** Object:  Table KeyTransaction    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KeyTransaction(
	PK int NOT NULL AUTO_INCREMENT,
	KeyPK int NOT NULL,
	KeyID varchar(100) NULL,
	KeyName varchar(200) NULL,
	LaborPK int NULL,
	Actions varchar(100) NULL,
	IssuedQty smallint NULL,
	TransactionDate datetime NULL,
 CONSTRAINT PK_KeyTransaction PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table KPI    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KPI(
	KPIPK int NOT NULL AUTO_INCREMENT,
	Name varchar(100) NOT NULL,
	Units varchar(50) NULL,
	Decimals smallint NOT NULL,
	Format char(1) NOT NULL,
	DisplayOrder int NOT NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	IsPercent bit NOT NULL,
	LowRange decimal(19, 6) NOT NULL,
	HighRange decimal(19, 6) NOT NULL,
	CriticalValue decimal(19, 6) NOT NULL,
	AlertValue decimal(19, 6) NOT NULL,
	OptimalValue decimal(19, 6) NOT NULL,
	ShowOptimalDial bit NOT NULL,
	LastValue decimal(19, 6) NOT NULL,
	LastVariance decimal(19, 6) NOT NULL,
	LastChecked datetime NULL,
	SQL1 varchar(2000) NULL,
	SQL1Where varchar(500) NULL,
	SQL1Tables varchar(100) NULL,
	Operator1 varchar(25) NULL,
	SQL2 varchar(2000) NULL,
	SQL2Where varchar(500) NULL,
	SQL2Tables varchar(100) NULL,
	Operator2 varchar(25) NULL,
	SQL3 varchar(2000) NULL,
	SQL3Where varchar(500) NULL,
	SQL3Tables varchar(100) NULL,
	Operator3 varchar(25) NULL,
	SQL4 varchar(2000) NULL,
	SQL4Where varchar(500) NULL,
	SQL4Tables varchar(100) NULL,
	Operator4 varchar(25) NULL,
	SQL5 varchar(2000) NULL,
	SQL5Where varchar(500) NULL,
	SQL5Tables varchar(100) NULL,
	ChartControl varchar(25) NULL,
	Active bit NOT NULL,
	Comments varchar(1000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	CustomImage1 varchar(200) NULL,
	CustomImage2 varchar(200) NULL,
	CustomImage3 varchar(200) NULL,
	UnacceptableValue decimal(19, 6) NULL,
	Description varchar(4000) NULL,
	OnclickURL varchar(500) NULL,
	ShareID varchar(50) NOT NULL,
	ShareRemoteID varchar(50) NOT NULL,
 CONSTRAINT PK_KPI PRIMARY KEY NONCLUSTERED 
(
	KPIPK ASC
)
);
/****** Object:  Table KPICustomImages    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KPICustomImages(
	KPICustomImagesPK int NOT NULL AUTO_INCREMENT,
	RepairCenterPK int NULL,
	CustomImage1 varchar(200) NULL,
	CustomImage2 varchar(200) NULL,
	CustomImage3 varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_KPICustomImages PRIMARY KEY NONCLUSTERED 
(
	KPICustomImagesPK ASC
)
);
/****** Object:  Table KPIGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KPIGroup(
	KPIGroupPK int NOT NULL AUTO_INCREMENT,
	RepairCenterPK int NULL,
	Name varchar(50) NULL,
	ModuleID char(2) NULL,
	DisplayOrder int NOT NULL,
	Active bit NOT NULL,
	Comments varchar(1000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_KPIGroup PRIMARY KEY NONCLUSTERED 
(
	KPIGroupPK ASC
)
);
/****** Object:  Table KPIGroupKPI    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KPIGroupKPI(
	PK int NOT NULL AUTO_INCREMENT,
	KPIPK int NOT NULL,
	KPIGroupPK int NOT NULL,
 CONSTRAINT PK_KPIGroupKPI PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table KPIHistory    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE KPIHistory(
	KPIPK int NULL,
	DateStamp datetime NULL,
	KPIValue decimal(19, 6) NULL,
	NValue int NULL
);
/****** Object:  Table Labor    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Labor(
	LaborPK int NOT NULL AUTO_INCREMENT,
	LaborID varchar(25) NOT NULL,
	LaborName varchar(50) NULL,
	FirstName varchar(35) NULL,
	MiddleName varchar(35) NULL,
	LastName varchar(35) NULL,
	Initials varchar(5) NULL,
	CraftPK int NULL,
	CraftID varchar(25) NULL,
	CraftName varchar(50) NULL,
	LaborType varchar(25) NOT NULL,
	LaborTypeDesc varchar(50) NULL,
	Crew varchar(25) NULL,
	CrewDesc varchar(50) NULL,
	Shift varchar(2) NULL,
	Class varchar(25) NULL,
	Senority varchar(25) NULL,
	SkillLevel varchar(25) NULL,
	SSN varchar(25) NULL,
	Active bit NOT NULL,
	SystemUser bit NOT NULL,
	AccessGroupPK int NULL,
	AccessGroupID varchar(25) NULL,
	AccessGroupName varchar(50) NULL,
	Access bit NOT NULL,
	CompanyPK int NULL,
	CompanyID varchar(25) NULL,
	CompanyName varchar(100) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	WorkLocationPK int NULL,
	WorkLocationID varchar(100) NULL,
	WorkLocationName varchar(150) NULL,
	JobTitle varchar(50) NULL,
	Address varchar(80) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(15) NULL,
	Country varchar(50) NULL,
	PhoneHome varchar(30) NULL,
	PhoneWork varchar(30) NULL,
	PhoneMobile varchar(30) NULL,
	Pager varchar(50) NULL,
	PagerService varchar(50) NULL,
	PagerPIN varchar(50) NULL,
	PagerNumeric bit NOT NULL,
	Fax varchar(30) NULL,
	Email varchar(50) NULL,
	PagerEmail varchar(50) NULL,
	EmailNotify varchar(25) NULL,
	EmailNotifyDesc varchar(50) NULL,
	PrinterName varchar(50) NULL,
	URL varchar(200) NULL,
	CostRegular DECIMAL(13,2) NOT NULL,
	CostOvertime DECIMAL(13,2) NOT NULL,
	CostOther DECIMAL(13,2) NOT NULL,
	ChargePercentage decimal(19, 6) NOT NULL,
	ChargeRate DECIMAL(13,2) NOT NULL,
	HireDate datetime NULL,
	TermDate datetime NULL,
	BirthDate datetime NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	LastEval datetime NULL,
	NextEval datetime NULL,
	MemberID varchar(50) NULL,
	User_Guid uniqueidentifier NULL,
	role_code varchar(15) NULL,
	role_desc varchar(50) NULL,
	PDAID varchar(25) NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	EmpStatus varchar(25) NULL,
	EmpStatusDesc varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	AvailForAutoAssign bit NULL,
	OnCall bit NULL,
	ContactName varchar(50) NULL,
	UsesTimesheet bit NULL,
	MenuName varchar(200) NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
	Is_Online bit NULL,
	Last_Refresh datetime NULL,
	Online_Status int NULL,
	Online_Desc varchar(500) NULL,
	IPAddress varchar(50) NULL,
	ServiceRequest_Link varchar(200) NULL,
	ServiceRequestStatus_Link varchar(200) NULL,
	RecordViews int NOT NULL,
	UseScheduleFromShift bit NULL,
	ShiftForSchedulePK int NULL,
	ShiftForScheduleID varchar(25) NULL,
	ShiftForScheduleName varchar(50) NULL,
	AppCache_Date datetime NULL,
	DataCache_Date datetime NULL,
	AppCache_Clear bit NULL,
	EquipmentList varchar(2000) NULL,
 CONSTRAINT PK_Labor PRIMARY KEY NONCLUSTERED 
(
	LaborPK ASC
),
 CONSTRAINT IX_Labor UNIQUE CLUSTERED 
(
	LaborID ASC
)
);
/****** Object:  Table LaborAddressBook    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborAddressBook(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NULL,
	EmailName varchar(50) NULL,
	EmailAddress varchar(255) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborAddressBook PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborAsset    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborAsset(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	AssetPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborAsset PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborAssetFilter    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborAssetFilter(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	AssetPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborAssetFilter PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborAssetMgr    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborAssetMgr(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	AssetPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborAssetMgr PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborDocument(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_LaborDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborNote    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborNote(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	LaborPK_Note int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborPreference    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborPreference(
	PreferenceName varchar(50) NOT NULL,
	LaborPK int NOT NULL,
	PreferenceValue varchar(1000) NOT NULL,
	PreferenceValueDesc varchar(150) NULL,
	PreferenceValuePK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborPreference PRIMARY KEY CLUSTERED 
(
	PreferenceName ASC,
	LaborPK ASC
)
);
/****** Object:  Table LaborReportGroup    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborReportGroup(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NULL,
	ReportGroupPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborReportGroup PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborTimeOff    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborTimeOff(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	Shift varchar(25) NULL,
	ShiftDesc varchar(50) NULL,
	WorkDate datetime NOT NULL,
	Hours decimal(19, 6) NOT NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	WorkDate2 datetime NULL,
	FromShift bit NULL,
 CONSTRAINT PK_LaborWorkExceptions PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborTraining    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborTraining(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	TrainingPK int NOT NULL,
	TrainingID varchar(25) NULL,
	TrainingName varchar(50) NULL,
	Certification varchar(50) NULL,
	CertificationDate datetime NULL,
	CertificationExpiration datetime NULL,
	Hours decimal(19, 6) NOT NULL,
	TravelCost DECIMAL(13,2) NOT NULL,
	TuitionCost DECIMAL(13,2) NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborTraining PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborTypes    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborTypes(
	LaborType varchar(25) NOT NULL,
	ModuleID char(2) NOT NULL,
 CONSTRAINT PK_LaborTypes PRIMARY KEY CLUSTERED 
(
	LaborType ASC
)
);
/****** Object:  Table LaborWorkHours    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborWorkHours(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	WorkDay datetime NOT NULL,
	WorkHours decimal(19, 6) NOT NULL,
	Type varchar(2) NULL,
	TypePK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_LaborHours PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table LaborWorkSchedule    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LaborWorkSchedule(
	PK int NOT NULL AUTO_INCREMENT,
	LaborPK int NOT NULL,
	Shift varchar(25) NULL,
	ShiftDesc varchar(50) NULL,
	BeginDate datetime NOT NULL,
	EndDate datetime NOT NULL,
	Sunday decimal(19, 6) NOT NULL,
	Monday decimal(19, 6) NOT NULL,
	Tuesday decimal(19, 6) NOT NULL,
	Wednesday decimal(19, 6) NOT NULL,
	Thursday decimal(19, 6) NOT NULL,
	Friday decimal(19, 6) NOT NULL,
	Saturday decimal(19, 6) NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	FromShift bit NULL,
 CONSTRAINT PK_LaborWorkHours PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Languages    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Languages(
	PK int NOT NULL AUTO_INCREMENT,
	Code varchar(10) NOT NULL,
	Name varchar(4000) NOT NULL,
	Active bit NOT NULL,
 CONSTRAINT PK_Languages PRIMARY KEY CLUSTERED 
(
	PK ASC
),
 CONSTRAINT IX_languages UNIQUE NONCLUSTERED 
(
	Code ASC
)
);
/****** Object:  Table Location    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Location(
	LocationPK int NOT NULL AUTO_INCREMENT,
	LocationID varchar(25) NOT NULL,
	LocationName varchar(50) NULL,
	Type varchar(25) NOT NULL,
	TypeDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	Budget DECIMAL(13,2) NULL,
	BuyerCompanyPK int NULL,
	BuyerCompanyID varchar(25) NULL,
	BuyerCompanyName varchar(50) NULL,
	BuyerPK int NULL,
	BuyerID varchar(25) NULL,
	BuyerName varchar(50) NULL,
	ShipToPK int NULL,
	ShipToID varchar(25) NULL,
	ShipToName varchar(50) NULL,
	ShipToAttention varchar(50) NULL,
	ShipToAddress1 varchar(80) NULL,
	ShipToAddress2 varchar(80) NULL,
	ShipToAddress3 varchar(80) NULL,
	BillToPK int NULL,
	BillToID varchar(25) NULL,
	BillToName varchar(50) NULL,
	BillToAttention varchar(50) NULL,
	BillToAddress1 varchar(80) NULL,
	BillToAddress2 varchar(80) NULL,
	BillToAddress3 varchar(80) NULL,
	Address varchar(80) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(15) NULL,
	Country varchar(50) NULL,
	Phone varchar(30) NULL,
	Fax varchar(30) NULL,
	Email varchar(50) NULL,
	EmailNotify varchar(25) NULL,
	EmailNotifyDesc varchar(50) NULL,
	URL varchar(200) NULL,
	Active bit NOT NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	SMSEmail varchar(50) NULL,
 CONSTRAINT PK_Location PRIMARY KEY NONCLUSTERED 
(
	LocationPK ASC
),
 CONSTRAINT IX_Location UNIQUE CLUSTERED 
(
	LocationID ASC
)
);
/****** Object:  Table LocationDocument    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LocationDocument(
	PK int NOT NULL AUTO_INCREMENT,
	LocationPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_LocationDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Log    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Log(
	PK int NOT NULL AUTO_INCREMENT,
	Date datetime NOT NULL,
	Message nvarchar(max) NOT NULL,
 CONSTRAINT PK_Log PRIMARY KEY CLUSTERED 
(
	PK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table LookupTable    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LookupTable(
	LookupTable varchar(25) NOT NULL,
	Description varchar(50) NOT NULL,
	CodeWidth int NOT NULL,
	DescriptionWidth int NOT NULL,
	Internal bit NOT NULL,
	Enabled bit NOT NULL,
	SkipValidation bit NOT NULL,
	CanModify bit NOT NULL,
	NoCascadeUpdate bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	CodeValueEnabled bit NULL,
	NotViewableInLookupTableMgr bit NULL,
 CONSTRAINT PK_LookupTable PRIMARY KEY CLUSTERED 
(
	LookupTable ASC
)
);
/****** Object:  Table LookupTableValues    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE LookupTableValues(
	LookupTable varchar(25) NOT NULL,
	CodeName varchar(50) NOT NULL,
	CodeDesc varchar(100) NOT NULL,
	CodeValue decimal(19, 6) NOT NULL,
	CodeIcon varchar(200) NULL,
	SystemCode bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	AvailableToRequester bit NOT NULL,
 CONSTRAINT PK_LookupTableValues PRIMARY KEY CLUSTERED 
(
	LookupTable ASC,
	CodeName ASC
)
);
/****** Object:  Table MC_ExceptionLog    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MC_ExceptionLog(
	PK int NOT NULL AUTO_INCREMENT,
	TransactionDate datetime NOT NULL,
	container_guid uniqueidentifier NULL,
	container_name nvarchar(50) NULL,
	user_guid uniqueidentifier NULL,
	LaborPK int NULL,
	LaborName nvarchar(50) NULL,
	ADOFunction nvarchar(100) NULL,
	SQL nvarchar(max) NULL,
	SQLParams nvarchar(max) NULL,
	DB_DError nvarchar(4000) NULL,
	SQLDBCS nvarchar(max) NULL,
	URL nvarchar(4000) NULL,
	ScriptName nvarchar(100) NULL,
	DBServer nvarchar(100) NULL,
	DBName nvarchar(100) NULL,
	LanguageCode nvarchar(50) NULL,
	IPAddress nvarchar(50) NULL,
 CONSTRAINT PK_MC_ExceptionLog PRIMARY KEY CLUSTERED 
(
	PK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MC_FieldOptions    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MC_FieldOptions(
	GlobalPK int NOT NULL AUTO_INCREMENT,
	TableName varchar(50) NOT NULL,
	FieldName varchar(100) NOT NULL,
	Required bit NOT NULL,
	Options varchar(50) NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayonSR bit NOT NULL,
	DisplayOrder int NULL,
	Filtered bit NOT NULL,
	CanbeFiltered bit NOT NULL,
 CONSTRAINT PK_MC_FieldOptions PRIMARY KEY CLUSTERED 
(
	GlobalPK ASC
),
 CONSTRAINT IX_MC_FieldOptions UNIQUE NONCLUSTERED 
(
	TableName ASC,
	FieldName ASC
)
);
/****** Object:  Table MC_InterfaceLog    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MC_InterfaceLog(
	PK int NOT NULL AUTO_INCREMENT,
	ProcessDate datetime NULL,
	FileName varchar(250) NULL,
	RecordNumber int NULL,
	ErrorMessage varchar(7000) NULL,
	Processed char(1) NULL,
 CONSTRAINT PK_MC_InterfaceLog PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table MCApprovals    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCApprovals(
	MCApprovalsPK int NOT NULL AUTO_INCREMENT,
	Levels int NULL,
	POPK int NOT NULL,
	WOPK int NOT NULL,
	ModuleID varchar(5) NULL,
	ApprovalLevel int NULL,
	Approved bit NULL,
	ApprovedByPK int NULL,
	ApprovedByInitials varchar(5) NULL,
	RowVersionUserPK int NOT NULL,
	RowVersionInitials varchar(5) NOT NULL,
	RowversionDate datetime NOT NULL,
 CONSTRAINT PK__MCApprovals__4D6048C8 PRIMARY KEY CLUSTERED 
(
	MCApprovalsPK ASC
)
);
/****** Object:  Table MCAudit    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCAudit(
	PK int NOT NULL AUTO_INCREMENT,
	Type char(2) NOT NULL,
	Table_Name varchar(30) NULL,
	Audit_PK int NULL,
	Audit_Name varchar(50) NULL,
	Audit_Info varchar(7800) NULL,
	RowVersionUserPK int NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCAudit PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table MCAutoEmail    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCAutoEmail(
	MCAutoEmailPK int NOT NULL AUTO_INCREMENT,
	RulePK int NOT NULL,
	PKFieldName varchar(50) NOT NULL,
	PK int NOT NULL,
	TransactionDate datetime NOT NULL,
	Processed char(1) NOT NULL,
	PKChildFieldName varchar(50) NULL,
	PKChild int NULL,
	PKOtherFieldName varchar(50) NULL,
	PKOther int NULL,
	EmailToAddresses varchar(max) NULL,
	EmailTemplate varchar(max) NULL,
	EmailTemplateHTML varchar(max) NULL,
	SensorSnoozeEndDate datetime NULL,
	StatusMessage varchar(500) NULL,
	ActionType char(1) NULL,
	EventUserPK int NULL,
 CONSTRAINT PK_MCAutoEmail_MCAutoEmailPK PRIMARY KEY CLUSTERED 
(
	MCAutoEmailPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCAutoMessage    Script Date: 9/4/2019 1:29:07 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCAutoMessage(
	MCAutoMessagePK int NOT NULL AUTO_INCREMENT,
	RulePK int NOT NULL,
	PKFieldName varchar(50) NOT NULL,
	PK int NOT NULL,
	PKChildFieldName varchar(50) NULL,
	PKChild int NULL,
	PKOtherFieldName varchar(50) NULL,
	PKOther int NULL,
	EventUserPK int NULL,
	TransactionDate datetime NOT NULL,
	Processed char(1) NOT NULL,
	StatusMessage varchar(500) NULL,
 CONSTRAINT PK_MCAutoMessage PRIMARY KEY NONCLUSTERED 
(
	MCAutoMessagePK ASC
)
);
/****** Object:  Table MCChartData    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCChartData(
	PK int NOT NULL AUTO_INCREMENT,
	ChartID varchar(10) NULL,
	DataPK int NULL,
	DataName varchar(50) NULL,
	DataValues varchar(8000) NULL,
	IsY2 int NULL
);
/****** Object:  Table MCChartLabels    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCChartLabels(
	PK int NOT NULL AUTO_INCREMENT,
	ChartID varchar(10) NULL,
	DataPK int NULL,
	LabelID varchar(4000) NULL,
	CraftID varchar(4000) NULL
);
/****** Object:  Table MCEdition    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCEdition(
	EditionName varchar(100) NOT NULL,
	EditionDesc varchar(4000) NULL,
	EditionStandards varchar(2000) NULL,
	DisplayOrder smallint NULL,
	MenuExplorer varchar(200) NULL,
	MenuOpen varchar(200) NULL,
	MenuNew varchar(200) NULL,
	ServiceRequester varchar(200) NULL,
	CustomerSQL varchar(max) NULL,
	CurrentEdition bit NOT NULL,
	BaseEdition bit NOT NULL,
	CustomerSpecific bit NOT NULL,
 CONSTRAINT PK_MCEdition PRIMARY KEY CLUSTERED 
(
	EditionName ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCEmailLog    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCEmailLog(
	EmailLogPK int NOT NULL AUTO_INCREMENT,
	ReportSchedulePK int NULL,
	RulePK int NULL,
	WOPK int NULL,
	AssetPK int NULL,
	AssetContractPK int NULL,
	AssetSpecificationPK bigint NULL,
	POPK int NULL,
	LaborPK int NULL,
	LaborTrainingPK int NULL,
	ProjectPK int NULL,
	PredictivePK int NULL,
	PartPK int NULL,
	PartLocationPK int NULL,
	PartVendorPK int NULL,
	CompanyPK int NULL,
	PMPK int NULL,
	EmailDate datetime NOT NULL,
	EmailPriority smallint NULL,
	EmailFromName varchar(50) NULL,
	EmailFromAddress varchar(100) NULL,
	EmailSubject varchar(500) NULL,
	EmailTo varchar(max) NULL,
	EmailBody varchar(max) NULL,
	Success bit NULL,
	EmailError varchar(2000) NULL,
 CONSTRAINT PK_MCEmailLog PRIMARY KEY NONCLUSTERED 
(
	EmailLogPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCEvent    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCEvent(
	EventPK int NOT NULL AUTO_INCREMENT,
	EventID varchar(25) NOT NULL,
	EventName varchar(100) NULL,
	EventOrder smallint NULL,
	ModuleID char(2) NULL,
	SQLforEmailTemplate varchar(2000) NULL,
	EmailTemplate varchar(max) NULL,
	EmailTemplateHTML varchar(max) NULL,
	Active bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	EventType varchar(25) NULL,
	ReportID varchar(50) NULL,
	SQLforSensor varchar(2000) NULL,
	SensorPKFieldName varchar(50) NULL,
	SensorPKChildFieldName varchar(50) NULL,
	SensorPKOtherFieldName varchar(50) NULL,
	SystemHidden bit NULL,
	PrimaryTable varchar(256) NULL,
 CONSTRAINT PK_MCEvent PRIMARY KEY NONCLUSTERED 
(
	EventPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCExport    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCExport(
	PK int NOT NULL AUTO_INCREMENT,
	ExportName varchar(50) NULL,
	ExportData varchar(max) NULL,
	ExportDate datetime NOT NULL,
 CONSTRAINT PK_MCExport PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCForm    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCForm(
	FormName varchar(100) NOT NULL,
	EditionName varchar(100) NOT NULL,
	FormDesc varchar(1000) NULL,
	FormPage varchar(100) NOT NULL,
	ModuleID char(2) NOT NULL,
	ModuleIDforTabs varchar(25) NULL,
	EntityDatabases varchar(2000) NULL,
	DisplayOrder smallint NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCForm PRIMARY KEY CLUSTERED 
(
	FormName ASC
)
);
/****** Object:  Table MCICalendar    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCICalendar(
	ICalendarPK int NOT NULL AUTO_INCREMENT,
	SQL varchar(8000) NULL,
	Active bit NOT NULL,
	RowVersionUserPK int NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCICalendar PRIMARY KEY CLUSTERED 
(
	ICalendarPK ASC
)
);
/****** Object:  Table MCINI    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCINI(
	INI_KEY varchar(255) NOT NULL,
	INI_VALUE varchar(2500) NOT NULL
);
/****** Object:  Table MCLanguage    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCLanguage(
	LanguagePK int NOT NULL AUTO_INCREMENT,
	LanguageName varchar(100) NULL,
	LanguageOrder smallint NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	LanguageCode varchar(50) NULL,
	BaseTranslation nvarchar(max) NULL,
	CustomTranslation nvarchar(max) NULL,
 CONSTRAINT PK_MCLanguage PRIMARY KEY NONCLUSTERED 
(
	LanguagePK ASC
),
 CONSTRAINT IX_MCLanguage UNIQUE CLUSTERED 
(
	LanguageName ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCMenu    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCMenu(
	MenuName varchar(200) NOT NULL,
	DisplayOrder int NULL,
	MenuDropDownCurrent bit NULL,
	MenuOpenCurrent bit NULL,
	MenuNewCurrent bit NULL,
	MenuHTML varchar(max) NOT NULL,
	MenuDropDown varchar(max) NULL,
	MenuOpen varchar(max) NULL,
	MenuNew varchar(max) NULL,
	IsActive bit NOT NULL,
 CONSTRAINT PK_MCMenu PRIMARY KEY CLUSTERED 
(
	MenuName ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCMenuXP    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCMenuXP(
	MCMenuPK int NOT NULL AUTO_INCREMENT,
	Module varchar(3) NOT NULL,
	PageID varchar(50) NOT NULL,
	MenuName varchar(50) NOT NULL,
	DisplayOrder int NOT NULL,
	MenuHTML varchar(max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	MCMenuPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCModule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCModule(
	ModuleID char(2) NOT NULL,
	ModuleName varchar(50) NULL,
	TitleforDocumentList varchar(50) NULL,
	CreateModObjOnStartup bit NOT NULL,
	UsesFindAndReplace bit NOT NULL,
	RootURL varchar(100) NULL,
	FormPage varchar(100) NULL,
	ActionPage varchar(100) NULL,
	ValidateDir varchar(100) NULL,
	LookupDir varchar(100) NULL,
	Icon varchar(200) NULL,
	IsPhoto bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	ModuleIDforTabs varchar(25) NULL,
 CONSTRAINT PK_MCModule PRIMARY KEY CLUSTERED 
(
	ModuleID ASC
)
);
/****** Object:  Table MCModuleActions    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCModuleActions(
	ActionPK int NOT NULL AUTO_INCREMENT,
	ActionName varchar(100) NULL,
	DisplayOrder smallint NOT NULL,
	Icon varchar(200) NULL,
	UseActionLoader bit NOT NULL,
	ActionLoader varchar(max) NULL,
	ActionPage varchar(1000) NULL,
	ActionParameters varchar(1000) NULL,
	ActionPageHeight int NOT NULL,
	ActionPageWidth int NOT NULL,
	IsCustomForClient bit NOT NULL,
	IsEnabled bit NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	IsTool bit NULL,
	ActionType char(2) NULL,
	LaunchNewBrowserWindow bit NULL,
 CONSTRAINT PK_MCModuleActions PRIMARY KEY NONCLUSTERED 
(
	ActionPK ASC
),
 CONSTRAINT IX_MCModuleActions_1 UNIQUE CLUSTERED 
(
	ActionName ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCModuleActionsModule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCModuleActionsModule(
	PK int NOT NULL AUTO_INCREMENT,
	ActionPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCModuleActionsModule PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table MCModuleLookupCodes    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCModuleLookupCodes(
	LookupModuleID char(25) NOT NULL,
	ModuleID char(2) NOT NULL,
	LookupTable char(25) NULL,
	LookupFieldPK char(25) NULL,
	LookupFieldID char(25) NULL,
	LookupFieldName char(25) NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCModuleLookupCodes PRIMARY KEY CLUSTERED 
(
	LookupModuleID ASC
)
);
/****** Object:  Table MCModuleTable    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCModuleTable(
	PK int NOT NULL AUTO_INCREMENT,
	ModuleID char(2) NOT NULL,
	Table_Name varchar(256) NOT NULL,
	Icon varchar(200) NULL,
 CONSTRAINT PK_MCModuleTable PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table MCNotifications    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCNotifications(
	MCNotificationsPK int NOT NULL AUTO_INCREMENT,
	RowVersionUserPK int NOT NULL,
	RowversionDate datetime NOT NULL,
	BodyMessage varchar(max) NULL,
	NotificationType varchar(50) NULL,
	Caption varchar(500) NULL,
	ClickAction varchar(8000) NULL,
	CaptionStyle varchar(500) NULL,
	DivStyle varchar(500) NULL,
	DivHeight int NULL,
	DivWidth int NULL,
	BodyStyle varchar(500) NULL,
	NotificationSound varchar(500) NULL,
	NotificationImage varchar(500) NULL,
	CustomScript varchar(max) NULL,
	EventRecord int NULL,
	EventModule char(2) NULL,
	EventUserPK int NULL,
	ReportID varchar(50) NULL,
	OutputMessage varchar(max) NULL,
	ShowedMessage bit NOT NULL,
 CONSTRAINT PK__MCNotifications__640C73BB PRIMARY KEY NONCLUSTERED 
(
	MCNotificationsPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCRule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRule(
	RulePK int NOT NULL AUTO_INCREMENT,
	RuleName varchar(500) NOT NULL,
	RuleOrder int NULL,
	RepairCenterPK int NULL,
	RuleType varchar(25) NULL,
	Active bit NOT NULL,
	ReportPK int NULL,
	SQLWhere varchar(max) NULL,
	SQLEWhere varchar(max) NULL,
	EventPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	KPIPK int NULL,
	Action_EmailNotify bit NULL,
	Action_CreateWO bit NULL,
	Action_ChangeWOStatus bit NULL,
	Action_AssignLabor bit NULL,
	Action_UpdateWO bit NULL,
	Action_CreatePO bit NULL,
	Action_ChangePOStatus bit NULL,
	Action_UpdatePO bit NULL,
	Action_UpdateAS bit NULL,
	Action_MoveAS bit NULL,
	DeleteAfterRuleInvoked bit NULL,
	RuleSQL varchar(8000) NULL,
	CustomEventSQL varchar(8000) NULL,
	RuleASP varchar(8000) NULL,
	RuleJS varchar(8000) NULL,
	RuleJSCmd int NULL,
	RuleJSCmdReportPK int NULL,
	RuleJSCmdReportCritOption char(1) NULL,
	Action_SMS bit NULL,
	Action_Notify bit NULL,
	Action_RunJSCmd bit NULL,
	Action_RunSQL bit NULL,
	Action_RunURL bit NULL,
	Action_Call bit NULL,
	Action_Status bit NULL,
	Action_SubStatus bit NULL,
	Action_Priority bit NULL,
	Action_Date bit NULL,
	Action_Active bit NULL,
	PK int NULL,
	EIntervalPeriod int NULL,
	EIntervalType char(1) NULL,
	ETimeStart datetime NULL,
	ETimeEnd datetime NULL,
	EOperator char(1) NULL,
	EIntervalPeriod2 int NULL,
	EIntervalType2 char(1) NULL,
	EDate varchar(50) NULL,
	ERepeat int NULL,
	ERepeatIntervalPeriod int NULL,
	ERepeatIntervalType char(1) NULL,
	EMonday bit NULL,
	ETuesday bit NULL,
	EWednesday bit NULL,
	EThursday bit NULL,
	EFriday bit NULL,
	ESaturday bit NULL,
	ESunday bit NULL,
	EMax int NULL,
 CONSTRAINT PK_AssignmentRule PRIMARY KEY NONCLUSTERED 
(
	RulePK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCRuleEmail    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRuleEmail(
	RulePK int NOT NULL,
	EmailFromName varchar(50) NULL,
	EmailFromAddress varchar(100) NULL,
	EmailToAlways varchar(max) NULL,
	EmailToLastResort varchar(max) NULL,
	EmailSubject varchar(500) NULL,
	EmailPriority smallint NULL,
	EmailReport bit NOT NULL,
	RepairCenter bit NOT NULL,
	RepairCenterSupervisor bit NOT NULL,
	Shop bit NOT NULL,
	ShopSupervisor bit NOT NULL,
	Supervisor bit NOT NULL,
	Assigned bit NOT NULL,
	Requester bit NOT NULL,
	StockRoom bit NOT NULL,
	StockRoomSupervisor bit NOT NULL,
	ToolRoom bit NOT NULL,
	ToolRoomSupervisor bit NOT NULL,
	Contact bit NOT NULL,
	Operator bit NOT NULL,
	Occupant bit NOT NULL,
	Department bit NOT NULL,
	Tenant bit NOT NULL,
	Vendor bit NOT NULL,
	Buyer bit NOT NULL,
	ShipTo bit NOT NULL,
	BillTo bit NOT NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	Custom3 bit NOT NULL,
	Custom4 bit NOT NULL,
	Custom5 bit NOT NULL,
	Custom6 bit NOT NULL,
	Custom7 bit NOT NULL,
	Custom8 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ReportPKList varchar(100) NULL,
	UseReportCriteriaList varchar(100) NULL,
	RPTCriteriaUseParentList varchar(100) NULL,
	EmailTemplate varchar(max) NULL,
	EmailTemplateHTML varchar(max) NULL,
	ReportInBody bit NULL,
	SensorSnoozeIntervalType char(1) NULL,
	SensorSnoozeIntervalPeriod int NULL,
	TakenBy bit NULL,
	PlainTextEmail bit NULL,
	EmailToAlwaysEGPK int NULL,
	EmailToLastResortEGPK int NULL,
	ADevice char(1) NULL,
	IsAssignedLead bit NULL,
	IsAssignedPDA bit NULL,
	IsAssignedEmp bit NULL,
	IsAssignedCon bit NULL,
	IsRequesterSupervisor bit NULL,
	IsContactLocation bit NULL,
	IsDepartmentSupervisor bit NULL,
	IsProjectSupervisor bit NULL,
	IsZone bit NULL,
	IsZoneSupervisor bit NULL,
	IsOccupantAllActive bit NULL,
	IsVendorPO bit NULL,
	IsVendorLabor bit NULL,
	IsShipToPO bit NULL,
	IsShipToShipping bit NULL,
	IsBillToPO bit NULL,
	IsBillToBilling bit NULL,
	Taken bit NULL,
	Pager bit NULL,
	AttachMiscAttach bit NULL,
	RepairCenterSMS bit NULL,
	RepairCenterSupervisorSMS bit NULL,
	ShopSMS bit NULL,
	ShopSupervisorSMS bit NULL,
	SupervisorSMS bit NULL,
	AssignedSMS bit NULL,
	PagerSMS bit NULL,
	RequesterSMS bit NULL,
	StockRoomSMS bit NULL,
	StockRoomSupervisorSMS bit NULL,
	ToolRoomSMS bit NULL,
	ToolRoomSupervisorSMS bit NULL,
	ContactSMS bit NULL,
	OperatorSMS bit NULL,
	OccupantSMS bit NULL,
	DepartmentSMS bit NULL,
	TenantSMS bit NULL,
	VendorSMS bit NULL,
	BuyerSMS bit NULL,
	ShipToSMS bit NULL,
	BillToSMS bit NULL,
	TakenSMS bit NULL,
	EmailFromNameSMS varchar(50) NULL,
	EmailFromAddressSMS varchar(100) NULL,
	EmailToAlwaysSMS varchar(max) NULL,
	EmailToLastResortSMS varchar(max) NULL,
	EmailToAlwaysEGPKSMS int NULL,
	EmailToLastResortEGPKSMS int NULL,
	EmailTemplateSMS varchar(max) NULL,
	NotifyMessageCenter bit NULL,
	MessageCenterSubject varchar(500) NULL,
	NotifyAlert bit NULL,
	AlertOnlyIfOnline bit NULL,
	ClientCommandOnlyIfOnline bit NULL,
	AlertCaption varchar(500) NULL,
	AlertCaptionStyle varchar(500) NULL,
	AlertDivStyle varchar(500) NULL,
	AlertDivHeight int NULL,
	AlertDivWidth int NULL,
	AlertBodyStyle varchar(500) NULL,
	AlertNotificationSound varchar(500) NULL,
	AlertNotificationImage varchar(500) NULL,
	MembersAll bit NULL,
	MembersAllNoEvent bit NULL,
	MemberEvent bit NULL,
	RepairCenterNotify bit NULL,
	RepairCenterSupervisorNotify bit NULL,
	ShopNotify bit NULL,
	ShopSupervisorNotify bit NULL,
	SupervisorNotify bit NULL,
	AssignedNotify bit NULL,
	RequesterNotify bit NULL,
	StockRoomSupervisorNotify bit NULL,
	ToolRoomSupervisorNotify bit NULL,
	OperatorNotify bit NULL,
	BuyerNotify bit NULL,
	TakenNotify bit NULL,
	EmailTemplateHTMLNotify varchar(max) NULL,
	AlertJS varchar(8000) NULL,
	AlertJSCmd int NULL,
	AlertJSCmdReportPK int NULL,
	AlertJSCmdReportCritOption char(1) NULL,
	NotifyClientCommand bit NULL,
	IncludePhotoSMS bit NULL,
	IncludeEventLaborPhotoSMS bit NULL,
	RepairCenterCALL bit NULL,
	RepairCenterSupervisorCALL bit NULL,
	ShopCALL bit NULL,
	ShopSupervisorCALL bit NULL,
	SupervisorCALL bit NULL,
	AssignedCALL bit NULL,
	PagerCALL bit NULL,
	RequesterCALL bit NULL,
	StockRoomCALL bit NULL,
	StockRoomSupervisorCALL bit NULL,
	ToolRoomCALL bit NULL,
	ToolRoomSupervisorCALL bit NULL,
	ContactCALL bit NULL,
	OperatorCALL bit NULL,
	OccupantCALL bit NULL,
	DepartmentCALL bit NULL,
	TenantCALL bit NULL,
	VendorCALL bit NULL,
	BuyerCALL bit NULL,
	ShipToCALL bit NULL,
	BillToCALL bit NULL,
	TakenCALL bit NULL,
	CallToAlways varchar(max) NULL,
	CallToAlwaysEGPK int NULL,
	CallTemplate varchar(max) NULL,
 CONSTRAINT PK_MCRuleEmail PRIMARY KEY CLUSTERED 
(
	RulePK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCRuleEmailReport    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRuleEmailReport(
	PK int NOT NULL AUTO_INCREMENT,
	RulePK int NOT NULL,
	ReportPK int NULL,
	OutputFormat char(1) NULL,
	UseReportCriteria bit NOT NULL,
	RPTCriteriaUseParent bit NOT NULL,
	IsActive bit NOT NULL,
 CONSTRAINT PK_MCRuleEmailReport PRIMARY KEY CLUSTERED 
(
	RulePK ASC
)
);
/****** Object:  Table MCRuleLabor    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRuleLabor(
	PK int NOT NULL AUTO_INCREMENT,
	RulePK int NOT NULL,
	LaborPK int NOT NULL,
	Priority smallint NULL,
	OnCall bit NOT NULL,
	BackupResource bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_AssignmentRuleLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table MCRuleLaborAlert    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRuleLaborAlert(
	PK int NOT NULL AUTO_INCREMENT,
	RulePK int NOT NULL,
	LaborPK int NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCRuleLaborAlert PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table MCRulePOP3    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRulePOP3(
	MCRulePOP3PK int NOT NULL AUTO_INCREMENT,
	RuleName varchar(500) NOT NULL,
	RuleOrder int NULL,
	RemoteHost varchar(100) NULL,
	UserName varchar(50) NULL,
	Password varchar(50) NULL,
	PortNumber int NULL,
	DomainAccept varchar(2000) NULL,
	DomainDeny varchar(2000) NULL,
	RepairCenterPK int NULL,
	AssetPK int NULL,
	ProcedurePK int NULL,
	SecurityWord varchar(50) NULL,
	AuthMethod int NOT NULL,
	ImageUploadCommands bit NOT NULL,
	ReportCommands bit NOT NULL,
	ReportCommandsEmailMustExistinLA bit NOT NULL,
	ReportCommandsEmailAccept varchar(2000) NULL,
	ReportListAccept varchar(200) NULL,
	ReportListDeny varchar(200) NULL,
	SaveAttachmentsToImageServer bit NOT NULL,
	Active bit NOT NULL,
	Processing datetime NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_MCRulePOP3 PRIMARY KEY NONCLUSTERED 
(
	MCRulePOP3PK ASC
),
 CONSTRAINT IX_MCRulePOP3 UNIQUE CLUSTERED 
(
	RuleName ASC
)
);
/****** Object:  Table MCRuleWO    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCRuleWO(
	RulePK int NOT NULL,
	Reason varchar(1000) NULL,
	Status varchar(15) NULL,
	StatusDesc varchar(50) NULL,
	SubStatus varchar(50) NULL,
	AuthStatus varchar(15) NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NULL,
	StatusDate datetime NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	StockRoomPK int NULL,
	StockRoomID varchar(25) NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetDate datetime NULL,
	TargetHours decimal(19, 6) NULL,
	ActualHours decimal(19, 6) NULL,
	ResponseHours decimal(19, 6) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	WarrantyBox bit NULL,
	ShutdownBox bit NULL,
	LockoutTagoutBox bit NULL,
	AttachmentsBox bit NULL,
	SurveyBox bit NULL,
	Survey_ID int NULL,
	PrintedBox bit NULL,
	Instructions varchar(1000) NULL,
	LaborReport varchar(3000) NULL,
	Chargeable bit NULL,
	FollowupWork bit NULL,
	ProblemPK int NULL,
	ProblemID varchar(25) NULL,
	ProblemName varchar(50) NULL,
	FailurePK int NULL,
	FailureID varchar(25) NULL,
	FailureName varchar(50) NULL,
	SolutionPK int NULL,
	SolutionID varchar(25) NULL,
	SolutionName varchar(50) NULL,
	Component varchar(25) NULL,
	ComponentDesc varchar(50) NULL,
	FaultLocation varchar(25) NULL,
	FaultLocationDesc varchar(50) NULL,
	FailedWO bit NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	CompletePercent decimal(19, 6) NULL,
	Complete datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	AssetStatusHistoryPK int NULL,
	IsOpen bit NULL,
	IsApproved bit NULL,
	IsAssigned bit NULL,
	IsGenerated bit NULL,
	IsPredictive bit NULL,
	IsPartsReserved bit NULL,
	Meter1Reading decimal(19, 6) NULL,
	Meter2Reading decimal(19, 6) NULL,
	PDAID varchar(25) NULL,
	PDAWO bit NULL,
	PMPK int NULL,
	PMID varchar(25) NULL,
	PMName varchar(100) NULL,
	PMUndoPK int NULL,
	PredictivePK int NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	IsAssignmentsCompleted bit NULL,
	Responded datetime NULL,
	Finalized datetime NULL,
	IssuedInitials varchar(5) NULL,
	RespondedInitials varchar(5) NULL,
	CompletedInitials varchar(5) NULL,
	FinalizedInitials varchar(5) NULL,
	ClosedInitials varchar(5) NULL,
	TakenByInitials varchar(5) NULL,
	TakenByPK int NULL,
	TakenByID varchar(25) NULL,
	TakenByName varchar(50) NULL,
 CONSTRAINT PK_MCRuleWO PRIMARY KEY CLUSTERED 
(
	RulePK ASC
)
);
/****** Object:  Table MCScriptAction    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCScriptAction(
	ScriptActionPK int NOT NULL AUTO_INCREMENT,
	ScriptAction varchar(100) NOT NULL,
	ScriptActionCode varchar(max) NOT NULL,
	DisplayOrder int NULL,
	Enabled bit NOT NULL,
	UserDefined bit NOT NULL,
 CONSTRAINT PK_MCScriptAction PRIMARY KEY NONCLUSTERED 
(
	ScriptActionPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MCTab    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCTab(
	Tab_Group varchar(50) NOT NULL,
	ModuleID char(2) NULL,
	Tab_Number smallint NOT NULL,
	Tab_Label1 varchar(50) NULL,
	Tab_Label2 varchar(50) NULL,
	Tab_Label3 varchar(50) NULL,
	Tab_Label4 varchar(50) NULL,
	Tab_Label5 varchar(50) NULL,
	Tab_Label6 varchar(50) NULL,
	Tab_Label7 varchar(50) NULL,
	Tab_Label8 varchar(50) NULL,
	Tab_Label9 varchar(50) NULL,
	Tab_Label10 varchar(50) NULL,
	Tab_Label_JSDynamic bit NULL,
	Tab_Icon varchar(200) NULL,
	Tab_ClickEvent varchar(200) NULL,
	Tab_DIV_ID varchar(50) NULL,
	Tab_CreateOnStartup bit NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	IsSystem bit NULL,
 CONSTRAINT PK_MCTab PRIMARY KEY CLUSTERED 
(
	Tab_Group ASC,
	Tab_Number ASC
)
);
/****** Object:  Table MCTable    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCTable(
	PK int NOT NULL AUTO_INCREMENT,
	Table_Name varchar(256) NOT NULL,
	Table_Desc varchar(500) NULL,
	UseTable_DescOnAudit bit NOT NULL,
	AuditInserts bit NOT NULL,
	AuditUpdates bit NOT NULL,
	AuditDeletes bit NOT NULL,
	PrimaryTable bit NULL,
	CustomerSpecificTable bit NULL,
 CONSTRAINT PK_MCTable PRIMARY KEY NONCLUSTERED 
(
	PK ASC
),
 CONSTRAINT IX_MCTable UNIQUE CLUSTERED 
(
	Table_Name ASC
)
);
/****** Object:  Table MCTimeZone    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MCTimeZone(
	CodeName varchar(50) NULL,
	CodeDesc varchar(50) NULL,
	CodeValue varchar(50) NULL,
	ctz varchar(50) NULL
);
/****** Object:  Table MessageCenter    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MessageCenter(
	pkChatOfflineMsg int NOT NULL AUTO_INCREMENT,
	ChatOfflineMsgID varchar(50) NOT NULL,
	UserPK int NOT NULL,
	FriendPK int NOT NULL,
	FriendName varchar(150) NOT NULL,
	FriendPhoto varchar(500) NOT NULL,
	Subject varchar(500) NOT NULL,
	Message varchar(max) NOT NULL,
	MsgRead bit NOT NULL,
	MessageTime datetime NOT NULL,
	MsgStatus int NOT NULL,
	MsgActive int NOT NULL,
	MsgFiles bit NOT NULL,
	MsgUrgent bit NOT NULL,
	WOPK int NOT NULL,
	WOID varchar(50) NOT NULL,
	FolderID varchar(50) NOT NULL,
	IsGenerated bit NOT NULL,
	HasReports bit NOT NULL,
	ReportID1 varchar(50) NOT NULL,
	ReportID2 varchar(50) NOT NULL,
	ReportID3 varchar(50) NOT NULL,
	ReportID4 varchar(50) NOT NULL,
	ReportID5 varchar(50) NOT NULL,
 CONSTRAINT PK_ChatOfflineMsg PRIMARY KEY CLUSTERED 
(
	pkChatOfflineMsg ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table MeterEntry    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE MeterEntry(
	PK int NOT NULL AUTO_INCREMENT,
	MeterDate datetime NULL,
	BranchID varchar(100) NULL,
	P1M1 decimal(19, 6) NULL,
	P1M2 decimal(19, 6) NULL,
	P2M1 decimal(19, 6) NULL,
	P2M2 decimal(19, 6) NULL,
	P3M1 decimal(19, 6) NULL,
	IPAddress varchar(100) NULL,
	TransactionDate datetime NULL,
 CONSTRAINT PK_MeterEntry PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Part    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Part(
	PartPK int NOT NULL AUTO_INCREMENT,
	PartID varchar(50) NOT NULL,
	PartName varchar(500) NULL,
	PartDescription varchar(2000) NULL,
	Active bit NOT NULL,
	Hazardous bit NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(100) NULL,
	ClassificationName varchar(150) NULL,
	Model varchar(50) NULL,
	ManufacturerPK int NULL,
	ManufacturerID varchar(25) NULL,
	ManufacturerName varchar(50) NULL,
	ShelfLifeDays smallint NOT NULL,
	ShippingWeight decimal(19, 6) NOT NULL,
	InternalPartNumber varchar(50) NULL,
	IssueUnits varchar(25) NULL,
	IssueUnitsDesc varchar(50) NULL,
	IssueUnitCost DECIMAL(13,2) NOT NULL,
	IssueUnitChargePrice DECIMAL(13,2) NOT NULL,
	IssueUnitChargePercentage decimal(19, 6) NOT NULL,
	AverageOrderUnitPrice DECIMAL(13,2) NOT NULL,
	LastOrderUnitPrice DECIMAL(13,2) NOT NULL,
	LastOrdered datetime NULL,
	LastIssued datetime NULL,
	Photo varchar(200) NULL,
	Comments varchar(4000) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	UpdateStockRooms bit NULL,
	UpdateCostPR bit NULL,
	UpdateCostPRFrom varchar(25) NULL,
	UpdateCostPRFromDesc varchar(50) NULL,
	UpdateChargePR bit NULL,
	UpdateStockRooms2 bit NULL,
	CostRule varchar(25) NULL,
	CostRuleDesc varchar(50) NULL,
	DirectIssue bit NULL,
	AvailableToRequester bit NULL,
	RotatingPart bit NULL,
	OrderUnits varchar(25) NULL,
	OrderUnitsDesc varchar(50) NULL,
	ConversionToIssueUnits decimal(19, 6) NULL,
	LastOrderedPOPK int NULL,
	LastOrderedPOID varchar(25) NULL,
	LastIssuedWOPK int NULL,
	LastIssuedWOID varchar(50) NULL,
	WarrantyDays int NULL,
	WarrantyFrom varchar(25) NULL,
	WarrantyFromDesc varchar(50) NULL,
	ManufacturerNumber varchar(50) NULL,
	PartURL varchar(1000) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NOT NULL,
	UDFBit4 bit NOT NULL,
	UDFBit5 bit NOT NULL,
 CONSTRAINT PK_Part PRIMARY KEY NONCLUSTERED 
(
	PartPK ASC
),
 CONSTRAINT IX_Part UNIQUE CLUSTERED 
(
	PartID ASC
)
);
/****** Object:  Table PartAlternate    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartAlternate(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NOT NULL,
	PartAlternatePK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PartAlternate PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PartDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartDocument(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_PartDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PartLocation    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartLocation(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NOT NULL,
	LocationPK int NOT NULL,
	Bin varchar(100) NULL,
	Lot varchar(25) NULL,
	Stock bit NOT NULL,
	MinIssueUnitQty int NOT NULL,
	MaxIssueUnitQty int NOT NULL,
	ReorderOrderUnitQty int NOT NULL,
	OrderUnits varchar(25) NULL,
	OrderUnitsDesc varchar(50) NULL,
	ConversionToIssueUnits decimal(19, 6) NULL,
	IssueUnits varchar(25) NULL,
	IssueUnitsDesc varchar(50) NULL,
	Available decimal(19, 6) NOT NULL,
	Reserved decimal(19, 6) NOT NULL,
	OnHand decimal(19, 6) NOT NULL,
	OnOrder decimal(19, 6) NOT NULL,
	OnOrderOpenPO decimal(19, 6) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ABCCode varchar(25) NULL,
	ABCCodeDesc varchar(50) NULL,
	CycleCount varchar(25) NULL,
	CycleCountDesc varchar(50) NULL,
	PhysicalLast datetime NULL,
	AverageOrderUnitPrice DECIMAL(13,2) NOT NULL,
	LastOrderUnitPrice DECIMAL(13,2) NOT NULL,
	IssueUnitCost DECIMAL(13,2) NOT NULL,
	IssueUnitChargePrice DECIMAL(13,2) NOT NULL,
	IssueUnitChargePercentage decimal(19, 6) NOT NULL,
	LastOrdered datetime NULL,
	LastIssued datetime NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	CostRule varchar(25) NULL,
	CostRuleDesc varchar(50) NULL,
	Reimbursable bit NULL,
	UpdateCostPR bit NULL,
	UpdateCostPRFrom varchar(25) NULL,
	UpdateCostPRFromDesc varchar(50) NULL,
	UpdateChargePR bit NULL,
	LastOrderedPOPK int NULL,
	LastOrderedPOID varchar(25) NULL,
	LastIssuedWOPK int NULL,
	LastIssuedWOID varchar(50) NULL,
	OnHandPending decimal(19, 6) NULL,
	PlugOnHand bit NULL,
	ItemCountLaborPK int NULL,
	ItemCountLaborID varchar(25) NULL,
	ItemCountLaborName varchar(50) NULL,
	OnHandStatic decimal(19, 6) NULL,
	OnHandStaticDate datetime NULL,
	AverageOrderUnitPriceStatic DECIMAL(13,2) NULL,
	OnHandPendingDiff decimal(19, 6) NULL,
 CONSTRAINT PK_PartLocation PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PartNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartNote(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PartNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PartPendingRecalc    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartPendingRecalc(
	PartPendingRecalcPK int NOT NULL AUTO_INCREMENT,
	WOPK int NULL,
	POPK int NULL,
	PartPK int NULL,
	LocationPK int NULL,
	ReCalcOnOrder bit NULL,
	ReCalcOnOrderOpenPO bit NULL,
	ReCalcReserved bit NULL,
	ProcessedDate datetime NULL,
	Bin varchar(100) NULL,
 CONSTRAINT PK_PartPendingRecalc PRIMARY KEY NONCLUSTERED 
(
	PartPendingRecalcPK ASC
)
);
/****** Object:  Table PartSpecification    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartSpecification(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NOT NULL,
	SpecificationPK int NOT NULL,
	SpecificationName varchar(50) NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(50) NULL,
	ValueDate datetime NULL,
	ValueHi decimal(19, 6) NULL,
	ValueLow decimal(19, 6) NULL,
	TrackHistory bit NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ValueOptimal decimal(19, 6) NULL,
	ValueOutOfRangeWO bit NULL,
	WOGenerated bit NULL,
	ProcedurePK int NULL,
 CONSTRAINT PK_PartSpecification PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PartTransaction    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartTransaction(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NOT NULL,
	LocationPK int NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	TransactionDate datetime NULL,
	Bin varchar(100) NULL,
	Lot varchar(25) NULL,
	Qty int NOT NULL,
	AssetPK int NULL,
	AccountPK int NULL,
	LaborPK int NULL,
	WOPK int NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	MasterType varchar(25) NULL,
	MasterTypeDesc varchar(50) NULL,
	Balance int NULL,
	BalanceAllLocations int NULL,
	LocationPK2 int NULL,
	Bin2 varchar(25) NULL,
	Lot2 varchar(25) NULL,
	TransactionOrder int NULL,
	WOPartPK int NULL,
	PurchaseOrderInvoicePK int NULL,
	PurchaseOrderInvoiceDetailPK int NULL,
	POPK int NULL,
	PurchaseOrderDetailPK int NULL,
	OrderUnits varchar(25) NULL,
	OrderUnitsDesc varchar(50) NULL,
	ConversionToIssueUnits decimal(19, 6) NULL,
	OrderUnitQty int NULL,
	OrderUnitPrice DECIMAL(13,2) NULL,
	QtyIssuedFIFOLIFO int NULL,
	QtyAvailableFIFOLIFO int NULL,
	QtyReservedFIFOLIFO int NULL,
	WODeleteDate datetime NULL,
	WODeleteLaborPK int NULL,
	WODeleteLaborInitials varchar(5) NULL,
	WOPartDeleteDate datetime NULL,
	WOPartDeleteLaborPK int NULL,
	WOPartDeleteLaborInitials varchar(5) NULL,
	PODeleteDate datetime NULL,
	PODeleteLaborPK int NULL,
	PODeleteLaborInitials varchar(5) NULL,
	PODetailDeleteDate datetime NULL,
	PODetailDeleteLaborPK int NULL,
	PODetailDeleteLaborInitials varchar(5) NULL,
	ReceiptDeleteDate datetime NULL,
	ReceiptDeleteLaborPK int NULL,
	ReceiptDeleteLaborInitials varchar(5) NULL,
	ReceiptLineDeleteDate datetime NULL,
	ReceiptLineDeleteLaborPK int NULL,
	ReceiptLineDeleteLaborInitials varchar(5) NULL,
	AssetDeleteDate datetime NULL,
	AssetDeleteLaborPK int NULL,
	AssetDeleteLaborInitials varchar(5) NULL,
	LocationDeleteDate datetime NULL,
	LocationDeleteLaborPK int NULL,
	LocationDeleteLaborInitials varchar(5) NULL,
	InterfaceBit1 bit NULL,
	InterfaceBit2 bit NULL,
	InterfaceBit3 bit NULL,
	InterfaceBit4 bit NULL,
	InterfaceBit5 bit NULL,
	InterfaceChar1 varchar(100) NULL,
	InterfaceChar2 varchar(100) NULL,
	InterfaceChar3 varchar(100) NULL,
	InterfaceChar4 varchar(100) NULL,
	InterfaceChar5 varchar(100) NULL,
	InterfaceDate1 datetime NULL,
	InterfaceDate2 datetime NULL,
	InterfaceDate3 datetime NULL,
	InterfaceDate4 datetime NULL,
	InterfaceDate5 datetime NULL,
	LaborToPK int NULL,
 CONSTRAINT PK_PartTransaction PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PartVendor    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PartVendor(
	PK int NOT NULL AUTO_INCREMENT,
	PartPK int NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	Priority smallint NOT NULL,
	CompanyPK int NULL,
	VendorPartNumber varchar(100) NULL,
	VendorPartCatalogNo varchar(50) NULL,
	VendorPartURL varchar(200) NULL,
	OrderUnits varchar(25) NULL,
	OrderUnitsDesc varchar(50) NULL,
	ConversionToIssueUnits decimal(19, 6) NOT NULL,
	OrderUnitPrice DECIMAL(13,2) NOT NULL,
	Discount float NOT NULL,
	IsTax bit NOT NULL,
	TaxRate decimal(19, 6) NOT NULL,
	TaxCode varchar(25) NULL,
	TaxCodeDesc varchar(50) NULL,
	DaysToDeliver int NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PartVendor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
),
 CONSTRAINT IX_PartVendor_4 UNIQUE NONCLUSTERED 
(
	PartPK ASC,
	LocationPK ASC,
	Priority ASC
)
);
/****** Object:  Table PhoneGroup    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PhoneGroup(
	PhoneGroupPK int NOT NULL AUTO_INCREMENT,
	PhoneGroupName varchar(500) NULL,
	RepairCenterPK int NULL,
	Phone varchar(max) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PhoneGroup PRIMARY KEY NONCLUSTERED 
(
	PhoneGroupPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table PM    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PM(
	PMPK int NOT NULL AUTO_INCREMENT,
	PMID varchar(25) NOT NULL,
	PMName varchar(100) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetHours decimal(19, 6) NULL,
	AverageHours decimal(19, 6) NOT NULL,
	TimesDone int NOT NULL,
	TimesCounter smallint NOT NULL,
	PMCounter smallint NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	WOStatus varchar(25) NULL,
	WOStatusDesc varchar(50) NULL,
	ShutdownBox bit NOT NULL,
	LockoutTagoutBox bit NOT NULL,
	AttachmentsBox bit NOT NULL,
	Chargeable bit NOT NULL,
	RecalcSchedule bit NOT NULL,
	IsScheduled bit NOT NULL,
	AssetStatusRequirement char(1) NOT NULL,
	Frequency varchar(25) NULL,
	FrequencyDesc varchar(50) NULL,
	NumberInterval smallint NOT NULL,
	DayOfWeek smallint NOT NULL,
	DayOfMonth smallint NOT NULL,
	WeekOfMonth smallint NOT NULL,
	MonthOfYear smallint NOT NULL,
	MonthlyChoice smallint NOT NULL,
	YearlyChoice smallint NOT NULL,
	OnSunday bit NOT NULL,
	OnMonday bit NOT NULL,
	OnTuesday bit NOT NULL,
	OnWednesday bit NOT NULL,
	OnThursday bit NOT NULL,
	OnFriday bit NOT NULL,
	OnSaturday bit NOT NULL,
	OnAny bit NOT NULL,
	UseLastCompleted bit NOT NULL,
	StartAfter datetime NOT NULL,
	DateRangeType smallint NOT NULL,
	EndOccurrences smallint NULL,
	StopAfter datetime NULL,
	SeasonStartMonth smallint NULL,
	SeasonStartDay smallint NULL,
	SeasonEndMonth smallint NULL,
	SeasonEndDay smallint NULL,
	NotifyOnEnd bit NOT NULL,
	ScheduleDisabled bit NOT NULL,
	LastGeneratedDate datetime NULL,
	LastCompletedDate datetime NULL,
	NextScheduledDate datetime NULL,
	Meter1Interval decimal(19, 6) NOT NULL,
	Meter2Interval decimal(19, 6) NOT NULL,
	AutomationMethod smallint NOT NULL,
	AutomationDaysPrior smallint NOT NULL,
	ASAutomationMethod smallint NOT NULL,
	Autofilter_RecordType smallint NOT NULL,
	Autofilter_ClassificationPK int NULL,
	Autofilter_ClassificationID varchar(25) NULL,
	Autofilter_ClassificationName varchar(50) NULL,
	Autofilter_AssetPK int NULL,
	Autofilter_AssetID varchar(100) NULL,
	Autofilter_AssetName varchar(150) NULL,
	Autofilter_RepairCenterPK int NULL,
	Autofilter_RepairCenterID varchar(25) NULL,
	Autofilter_RepairCenterName varchar(50) NULL,
	WOGroup bit NOT NULL,
	WOGroupRC bit NOT NULL,
	WOGroupManageAsSingle bit NOT NULL,
	LastWOPK int NULL,
	LastGroupPK int NULL,
	CostTotalEstimated DECIMAL(13,2) NOT NULL,
	CostPartEstimated DECIMAL(13,2) NOT NULL,
	CostMiscEstimated DECIMAL(13,2) NOT NULL,
	CostLaborEstimated DECIMAL(13,2) NOT NULL,
	CostLaborEmployeeEstimated DECIMAL(13,2) NOT NULL,
	CostLaborContractorEstimated DECIMAL(13,2) NOT NULL,
	Active bit NOT NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	ScheduleType smallint NULL,
	UseLastCompleted2 bit NULL,
	Fixed_DayOfMonth1 smallint NULL,
	Fixed_DayOfMonth2 smallint NULL,
	Fixed_DayOfMonth3 smallint NULL,
	Fixed_DayOfMonth4 smallint NULL,
	Fixed_DayOfMonth5 smallint NULL,
	Fixed_DayOfMonth6 smallint NULL,
	Fixed_DayOfMonth7 smallint NULL,
	Fixed_DayOfMonth8 smallint NULL,
	Fixed_DayOfMonth9 smallint NULL,
	Fixed_DayOfMonth10 smallint NULL,
	Fixed_DayOfMonth11 smallint NULL,
	Fixed_DayOfMonth12 smallint NULL,
	Fixed_DayOfMonth13 smallint NULL,
	Fixed_DayOfMonth14 smallint NULL,
	Fixed_DayOfMonth15 smallint NULL,
	Fixed_DayOfMonth16 smallint NULL,
	Fixed_DayOfMonth17 smallint NULL,
	Fixed_DayOfMonth18 smallint NULL,
	Fixed_DayOfMonth19 smallint NULL,
	Fixed_DayOfMonth20 smallint NULL,
	Fixed_DayOfMonth21 smallint NULL,
	Fixed_DayOfMonth22 smallint NULL,
	Fixed_DayOfMonth23 smallint NULL,
	Fixed_DayOfMonth24 smallint NULL,
	Fixed_MonthOfYear1 smallint NULL,
	Fixed_MonthOfYear2 smallint NULL,
	Fixed_MonthOfYear3 smallint NULL,
	Fixed_MonthOfYear4 smallint NULL,
	Fixed_MonthOfYear5 smallint NULL,
	Fixed_MonthOfYear6 smallint NULL,
	Fixed_MonthOfYear7 smallint NULL,
	Fixed_MonthOfYear8 smallint NULL,
	Fixed_MonthOfYear9 smallint NULL,
	Fixed_MonthOfYear10 smallint NULL,
	Fixed_MonthOfYear11 smallint NULL,
	Fixed_MonthOfYear12 smallint NULL,
	Fixed_MonthOfYear13 smallint NULL,
	Fixed_MonthOfYear14 smallint NULL,
	Fixed_MonthOfYear15 smallint NULL,
	Fixed_MonthOfYear16 smallint NULL,
	Fixed_MonthOfYear17 smallint NULL,
	Fixed_MonthOfYear18 smallint NULL,
	Fixed_MonthOfYear19 smallint NULL,
	Fixed_MonthOfYear20 smallint NULL,
	Fixed_MonthOfYear21 smallint NULL,
	Fixed_MonthOfYear22 smallint NULL,
	Fixed_MonthOfYear23 smallint NULL,
	Fixed_MonthOfYear24 smallint NULL,
	UseAssetDefaults bit NULL,
	UseAssetDefaultsRC bit NULL,
	UseAssetDefaultsSH bit NULL,
	UseAssetDefaultsSU bit NULL,
	UseAssetDefaultsAC bit NULL,
	UseAssetDefaultsDP bit NULL,
	UseAssetDefaultsTN bit NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
	AutomationDaysPriorFromProcedure bit NULL,
	UseLastComplete_WOStatus varchar(25) NULL,
	UseLastComplete2_WOStatus varchar(25) NULL,
 CONSTRAINT PK_PM PRIMARY KEY NONCLUSTERED 
(
	PMPK ASC
),
 CONSTRAINT IX_PM UNIQUE CLUSTERED 
(
	PMID ASC
)
);
/****** Object:  Table PMAsset    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMAsset(
	PK int NOT NULL AUTO_INCREMENT,
	PMPK int NOT NULL,
	AssetPK int NOT NULL,
	RouteOrder smallint NOT NULL,
	PMCounter smallint NOT NULL,
	TimesCounter smallint NOT NULL,
	PMEnded bit NOT NULL,
	TimesDone int NOT NULL,
	LastGeneratedDate datetime NULL,
	LastCompletedDate datetime NULL,
	LastProcedurePK int NULL,
	NextScheduledDate datetime NULL,
	NextProcedurePK int NULL,
	Meter1ReadingLastInterval decimal(19, 6) NULL,
	Meter1NextInterval decimal(19, 6) NULL,
	Meter2ReadingLastInterval decimal(19, 6) NULL,
	Meter2NextInterval decimal(19, 6) NULL,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	StockRoomPK int NOT NULL,
	StockRoomID varchar(25) NOT NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	GenerateMarker bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	WOPK int NULL,
	IsOpenWO bit NULL,
	ScheduleDisabled bit NULL,
	PMCycleStartDate datetime NULL,
	PMCycleStartDateCalc bit NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
	AutomationDaysPrior smallint NULL,
 CONSTRAINT PK_PMAsset PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PMAssetTemp    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMAssetTemp(
	PMAssetTempPK uniqueidentifier NOT NULL,
	PK int NOT NULL,
	PMPK int NOT NULL,
	AssetPK int NOT NULL,
	RouteOrder smallint NOT NULL,
	PMCounter smallint NOT NULL,
	TimesCounter smallint NOT NULL,
	PMEnded bit NOT NULL,
	TimesDone int NOT NULL,
	LastGeneratedDate datetime NULL,
	LastCompletedDate datetime NULL,
	LastProcedurePK int NULL,
	NextScheduledDate datetime NULL,
	NextProcedurePK int NULL,
	Meter1ReadingLastInterval decimal(19, 6) NULL,
	Meter1NextInterval decimal(19, 6) NULL,
	Meter2ReadingLastInterval decimal(19, 6) NULL,
	Meter2NextInterval decimal(19, 6) NULL,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	StockRoomPK int NOT NULL,
	StockRoomID varchar(25) NOT NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	GenerateMarker bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	WOPK int NULL,
	IsOpenWO bit NULL,
	ScheduleDisabled bit NULL,
	PMCycleStartDate datetime NULL,
	PMCycleStartDateCalc bit NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
	AutomationDaysPrior smallint NULL
);
/****** Object:  Table PMAssetUndo    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMAssetUndo(
	PMAssetUndoPK int NOT NULL AUTO_INCREMENT,
	PMUndoPK int NOT NULL,
	PMAssetPK int NOT NULL,
	PMCounter smallint NOT NULL,
	TimesCounter smallint NOT NULL,
	PMEnded bit NOT NULL,
	TimesDone int NOT NULL,
	LastGeneratedDate datetime NULL,
	LastCompletedDate datetime NULL,
	LastProcedurePK int NULL,
	NextScheduledDate datetime NULL,
	NextProcedurePK int NULL,
	Meter1ReadingLastInterval decimal(19, 6) NULL,
	Meter1NextInterval decimal(19, 6) NULL,
	Meter2ReadingLastInterval decimal(19, 6) NULL,
	Meter2NextInterval decimal(19, 6) NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PMAssetUndo PRIMARY KEY NONCLUSTERED 
(
	PMAssetUndoPK ASC
)
);
/****** Object:  Table PMDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMDocument(
	PK int NOT NULL AUTO_INCREMENT,
	PMPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_PMDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PMNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMNote(
	PK int NOT NULL AUTO_INCREMENT,
	PMPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PMNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PMProcedure    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMProcedure(
	PK int NOT NULL AUTO_INCREMENT,
	PMPK int NOT NULL,
	PMType varchar(25) NULL,
	PMTypeDesc varchar(50) NULL,
	ProcedurePK int NOT NULL,
	AverageHours decimal(19, 6) NULL,
	Sequence smallint NULL,
	OneTimeDate datetime NULL,
	OneTimeDateGenerated bit NOT NULL,
	Meter1 decimal(19, 6) NOT NULL,
	Meter1Generated bit NOT NULL,
	Meter2 decimal(19, 6) NOT NULL,
	Meter2Generated bit NOT NULL,
	Event varchar(25) NULL,
	EventDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	AutomationDaysPrior smallint NULL,
 CONSTRAINT PK_PMProcedure PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PMUndo    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMUndo(
	PMUndoPK int NOT NULL AUTO_INCREMENT,
	GenerateDate datetime NOT NULL,
	RepairCenters varchar(1000) NULL,
	GenDateOption bit NOT NULL,
	GenerateToDate datetime NULL,
	AutoCreate bit NOT NULL,
	Undo bit NOT NULL,
	Processing bit NOT NULL,
	StartTime datetime NULL,
	EndTime datetime NULL,
	SQLErrorCode int NULL,
	WOCount decimal(19, 6) NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PMUndo_PMUndoPK PRIMARY KEY CLUSTERED 
(
	PMUndoPK ASC
)
);
/****** Object:  Table PMUndoRepairCenter    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PMUndoRepairCenter(
	PK int NOT NULL AUTO_INCREMENT,
	PMUndoPK int NOT NULL,
	RepairCenterPK int NOT NULL,
 CONSTRAINT PK_PMUndoRepairCenter_PK PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Predictive    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Predictive(
	PredictivePK int NOT NULL AUTO_INCREMENT,
	PredictiveID varchar(50) NULL,
	Reason varchar(2000) NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	SubStatus varchar(50) NULL,
	AuthStatus varchar(15) NOT NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NOT NULL,
	StatusDate datetime NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetDate datetime NULL,
	TargetHours decimal(19, 6) NULL,
	LastSurveyDate datetime NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NOT NULL,
	PriorityDesc varchar(50) NULL,
	Technology varchar(25) NULL,
	TechnologyDesc varchar(50) NULL,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	WarrantyBox bit NULL,
	ShutdownBox bit NOT NULL,
	LockoutTagoutBox bit NOT NULL,
	AttachmentsBox bit NOT NULL,
	PrintedBox bit NOT NULL,
	LaborReport varchar(2000) NULL,
	FollowupReport varchar(2000) NULL,
	ProblemPK int NULL,
	ProblemID varchar(25) NULL,
	ProblemName varchar(50) NULL,
	FailurePK int NULL,
	FailureID varchar(25) NULL,
	FailureName varchar(50) NULL,
	SolutionPK int NULL,
	SolutionID varchar(25) NULL,
	SolutionName varchar(50) NULL,
	Component varchar(25) NULL,
	ComponentDesc varchar(50) NULL,
	FaultLocation varchar(25) NULL,
	FaultLocationDesc varchar(50) NULL,
	FailedPredictive bit NOT NULL,
	TakenByPK int NULL,
	TakenByInitials varchar(25) NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	Complete datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	IsOpen bit NOT NULL,
	IsApproved bit NOT NULL,
	Meter1Reading decimal(19, 6) NULL,
	Meter2Reading decimal(19, 6) NULL,
	PDAID varchar(25) NULL,
	PDAPredictive bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	Currency varchar(25) NULL,
	CurrencyPrefix varchar(10) NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
 CONSTRAINT PK_Predictive PRIMARY KEY NONCLUSTERED 
(
	PredictivePK ASC
),
 CONSTRAINT IX_Predictive UNIQUE CLUSTERED 
(
	PredictiveID ASC
)
);
/****** Object:  Table PredictiveNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PredictiveNote(
	PK int NOT NULL AUTO_INCREMENT,
	PredictivePK int NOT NULL,
	NoteDate datetime NOT NULL,
	Note varchar(7500) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	Custom1 bit NULL,
	Custom2 bit NULL,
 CONSTRAINT PK_PredictiveNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PredictivePhoto    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PredictivePhoto(
	PK int NOT NULL AUTO_INCREMENT,
	PredictivePK int NOT NULL,
	PhotoDate datetime NOT NULL,
	Photo varchar(200) NULL,
	Active bit NOT NULL,
	Followup bit NOT NULL,
	ReportOrder smallint NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PredictivePhoto PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PredictiveStatusHistory    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PredictiveStatusHistory(
	PK int NOT NULL AUTO_INCREMENT,
	PredictivePK int NOT NULL,
	IsAuthStatus bit NOT NULL,
	StatusDate datetime NOT NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PredictiveStatusHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PredictiveTask    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PredictiveTask(
	PK int NOT NULL AUTO_INCREMENT,
	PredictivePK int NOT NULL,
	TaskNo int NULL,
	TaskAction varchar(7000) NULL,
	Complete bit NOT NULL,
	Fail bit NOT NULL,
	Rate int NULL,
	Measurement varchar(50) NULL,
	Initials varchar(5) NULL,
	HoursEstimated decimal(19, 6) NULL,
	HoursActual decimal(19, 6) NULL,
	CraftPK int NULL,
	Spec bit NOT NULL,
	Header bit NOT NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	LineStyle varchar(25) NULL,
	LineStyleDesc varchar(50) NULL,
	AssetSpecificationPK bigint NULL,
	SpecificationPK int NULL,
	Meter1 bit NOT NULL,
	Meter2 bit NOT NULL,
	TaskPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PredictiveTask PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Preference    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Preference(
	PreferenceName varchar(50) NOT NULL,
	PreferenceCategory varchar(50) NOT NULL,
	Description varchar(1000) NULL,
	DisplayOrder smallint NOT NULL,
	ShowToRepairCenter bit NOT NULL,
	ShowToLabor bit NOT NULL,
	DefaultValue varchar(1000) NULL,
	DefaultValueDesc varchar(150) NULL,
	DefaultValuePK int NULL,
	DataType char(1) NULL,
	PreferenceHelp varchar(2000) NULL,
	DemoLaborPK int NULL,
	LookupType char(3) NULL,
	LookupReference varchar(25) NULL,
	IsCustomForClient bit NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	GlobalPreference bit NULL,
	ReportIDforCriteria varchar(50) NULL,
	CriteriaPrefix varchar(500) NULL,
	PopupTool nvarchar(100) NULL,
	SystemHidden bit NULL,
 CONSTRAINT PK_Preference PRIMARY KEY CLUSTERED 
(
	PreferenceName ASC
)
);
/****** Object:  Table PreferenceCategory    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PreferenceCategory(
	PreferenceCategory varchar(50) NOT NULL,
	PreferenceCategoryDesc varchar(500) NULL,
	DisplayOrder smallint NOT NULL,
	IsCustomForClient bit NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PreferenceCategory PRIMARY KEY CLUSTERED 
(
	PreferenceCategory ASC
)
);
/****** Object:  Table PreferenceModule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PreferenceModule(
	PreferenceName varchar(50) NOT NULL,
	ModuleID char(2) NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PreferenceModule PRIMARY KEY CLUSTERED 
(
	PreferenceName ASC,
	ModuleID ASC
)
);
/****** Object:  Table ProcedureDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureDocument(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(50) NULL,
	ClassificationName varchar(100) NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_ProcedureDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedureLabor    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureLabor(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	LaborPK int NOT NULL,
	LaborID varchar(25) NOT NULL,
	LaborName varchar(50) NULL,
	RecordType smallint NOT NULL,
	LaborType varchar(25) NULL,
	LaborTypeDesc varchar(50) NULL,
	EstimatedHours decimal(19, 6) NOT NULL,
	RegularHours decimal(19, 6) NOT NULL,
	OvertimeHours decimal(19, 6) NOT NULL,
	OtherHours decimal(19, 6) NOT NULL,
	TotalHours decimal(19, 6) NOT NULL,
	WorkDate datetime NULL,
	TimeIn datetime NULL,
	TimeOut datetime NULL,
	RegularCost DECIMAL(13,2) NOT NULL,
	OvertimeCost DECIMAL(13,2) NOT NULL,
	OtherCost DECIMAL(13,2) NOT NULL,
	TotalCost DECIMAL(13,2) NOT NULL,
	CostRegular DECIMAL(13,2) NOT NULL,
	CostOvertime DECIMAL(13,2) NOT NULL,
	CostOther DECIMAL(13,2) NOT NULL,
	ChargeRate DECIMAL(13,2) NOT NULL,
	ChargePercentage decimal(19, 6) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(50) NULL,
	ClassificationName varchar(100) NULL,
	AutoCalcCost bit NULL,
	AutoCalcCharge bit NULL,
 CONSTRAINT PK_ProcedureLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedureLibrary    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureLibrary(
	ProcedurePK int NOT NULL AUTO_INCREMENT,
	ProcedureID varchar(25) NOT NULL,
	ProcedureName varchar(100) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetHours decimal(19, 6) NULL,
	AverageHours decimal(19, 6) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	WOAuthStatus varchar(25) NULL,
	WOAuthStatusDesc varchar(50) NULL,
	ShutdownBox bit NOT NULL,
	LockoutTagoutBox bit NOT NULL,
	AttachmentsBox bit NOT NULL,
	SurveyBox bit NOT NULL,
	Survey_ID int NULL,
	Chargeable bit NOT NULL,
	Instructions varchar(2000) NULL,
	CostTotalEstimated DECIMAL(13,2) NOT NULL,
	CostPartEstimated DECIMAL(13,2) NOT NULL,
	CostMiscEstimated DECIMAL(13,2) NOT NULL,
	CostLaborEstimated DECIMAL(13,2) NOT NULL,
	CostEmployeeEstimated DECIMAL(13,2) NOT NULL,
	CostContractorEstimated DECIMAL(13,2) NOT NULL,
	ChargeLaborEstimated DECIMAL(13,2) NOT NULL,
	ChargeEmployeeEstimated DECIMAL(13,2) NOT NULL,
	ChargeContractorEstimated DECIMAL(13,2) NOT NULL,
	ChargePartEstimated DECIMAL(13,2) NOT NULL,
	ChargeMiscEstimated DECIMAL(13,2) NOT NULL,
	ChargeTotalEstimated DECIMAL(13,2) NOT NULL,
	Active bit NOT NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	AvailableToRequester bit NULL,
	ProjectPlan bit NULL,
	AvailableToRequester1 bit NULL,
	FormName varchar(50) NULL,
	ShareID varchar(50) NOT NULL,
	ShareRemoteID varchar(50) NOT NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
 CONSTRAINT PK_ProcedureLibrary PRIMARY KEY NONCLUSTERED 
(
	ProcedurePK ASC
),
 CONSTRAINT IX_ProcedureLibrary UNIQUE CLUSTERED 
(
	ProcedureID ASC
)
);
/****** Object:  Table ProcedureMiscCost    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureMiscCost(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	RecordType smallint NOT NULL,
	MiscCostPK int NULL,
	MiscCostID varchar(25) NULL,
	MiscCostName varchar(50) NOT NULL,
	MiscCostDesc varchar(1000) NULL,
	InvoiceNumber varchar(50) NULL,
	MiscCostDate datetime NOT NULL,
	EstimatedCost DECIMAL(13,2) NOT NULL,
	ActualCost DECIMAL(13,2) NOT NULL,
	ChargePercentage decimal(19, 6) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	CompanyPK int NULL,
	CompanyID varchar(25) NULL,
	CompanyName varchar(100) NULL,
	LaborPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(50) NULL,
	Comments varchar(1000) NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(50) NULL,
	ClassificationName varchar(100) NULL,
	AutoCalcCost bit NULL,
	AutoCalcCharge bit NULL,
 CONSTRAINT PK_ProcedureMiscCost PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedureNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureNote(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ProcedureNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedurePart    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedurePart(
	PK int NOT NULL AUTO_INCREMENT,
	RecordType smallint NOT NULL,
	ProcedurePK int NOT NULL,
	PartPK int NOT NULL,
	PartID varchar(50) NULL,
	PartName varchar(500) NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	DirectIssue bit NOT NULL,
	OutOfPocket bit NOT NULL,
	QuantityEstimated decimal(19, 6) NOT NULL,
	QuantityActual decimal(19, 6) NOT NULL,
	IssueUnitCost DECIMAL(13,2) NOT NULL,
	IssueUnitChargePrice DECIMAL(13,2) NOT NULL,
	IssueUnitChargePercentage decimal(19, 6) NOT NULL,
	OtherCost DECIMAL(13,2) NOT NULL,
	TotalCost DECIMAL(13,2) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(50) NULL,
	ClassificationName varchar(100) NULL,
	AutoCalcCost bit NULL,
	AutoCalcCharge bit NULL,
	Bin varchar(100) NULL,
 CONSTRAINT PK_ProcedurePart PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedureProcedure    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureProcedure(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	ProjectPlanProcedurePK int NOT NULL,
	ProcedureSequence int NULL,
	ClassificationPK int NULL,
	ClassificationCount int NULL,
	ClassificationAll bit NULL,
	AssetPK int NULL,
	DependencySequences varchar(1000) NULL,
	DependencyPKs varchar(1000) NULL,
	DependencyRestrictionEvent varchar(25) NULL,
	DependencyRestrictionEventDesc varchar(50) NULL,
	DependencyAutoIssueEvent varchar(25) NULL,
	DependencyAutoIssueEventDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ProcedureProcedure PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedureTask    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureTask(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	TaskNo int NULL,
	TaskAction varchar(7000) NULL,
	HoursEstimated decimal(19, 6) NULL,
	HoursActual decimal(19, 6) NULL,
	CraftPK int NULL,
	Spec bit NOT NULL,
	Header bit NOT NULL,
	LineStyle varchar(25) NULL,
	LineStyleDesc varchar(50) NULL,
	AssetSpecificationPK bigint NULL,
	SpecificationPK int NULL,
	Meter1 bit NOT NULL,
	Meter2 bit NOT NULL,
	TaskPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	AssetPK int NULL,
	ClassificationPK int NULL,
	ClassificationCount int NULL,
	ClassificationAll bit NULL,
	TrackTask bit NULL,
	ClassificationForTaskPK int NULL,
	ClassificationForTaskID varchar(50) NULL,
	ClassificationForTaskName varchar(100) NULL,
 CONSTRAINT PK_ProcedureTask PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProcedureTool    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProcedureTool(
	PK int NOT NULL AUTO_INCREMENT,
	ProcedurePK int NOT NULL,
	ToolPK int NOT NULL,
	ToolID varchar(25) NOT NULL,
	ToolName varchar(50) NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	RecordType smallint NOT NULL,
	QuantityEstimated int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ClassificationPK int NULL,
	ClassificationID varchar(50) NULL,
	ClassificationName varchar(100) NULL,
 CONSTRAINT PK_ProcedureTool PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Project    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Project(
	ProjectPK int NOT NULL AUTO_INCREMENT,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	AuthStatus varchar(15) NOT NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NOT NULL,
	StatusDate datetime NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	StartEstimated datetime NULL,
	EndEstimated datetime NULL,
	StartActual datetime NULL,
	EndActual datetime NULL,
	TargetDate datetime NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	IsOpen bit NOT NULL,
	IsApproved bit NOT NULL,
	Comments varchar(4000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	CostRegular DECIMAL(13,2) NULL,
	CostOvertime DECIMAL(13,2) NULL,
	CostOther DECIMAL(13,2) NULL,
	ChargePercentage decimal(19, 6) NULL,
	ChargeRate DECIMAL(13,2) NULL,
	Budget DECIMAL(13,2) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	TakenByPK int NULL,
	TakenByID varchar(25) NULL,
	TakenByName varchar(50) NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	AutoAddAssets bit NULL,
	PMCycleStartDate datetime NULL,
 CONSTRAINT PK_Project PRIMARY KEY NONCLUSTERED 
(
	ProjectPK ASC
),
 CONSTRAINT IX_Project UNIQUE CLUSTERED 
(
	ProjectID ASC
)
);
/****** Object:  Table ProjectAsset    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProjectAsset(
	PK int NOT NULL AUTO_INCREMENT,
	ProjectPK int NOT NULL,
	AssetPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ProjectAsset PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProjectDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProjectDocument(
	PK int NOT NULL AUTO_INCREMENT,
	ProjectPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_ProjectDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Projection    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Projection(
	ProjectionPK int NOT NULL AUTO_INCREMENT,
	ProjectionName varchar(100) NULL,
	GenerateDate datetime NOT NULL,
	RepairCenters varchar(1000) NULL,
	GenDateOption bit NOT NULL,
	GenerateToDate datetime NULL,
	WOCount decimal(19, 6) NOT NULL,
	IsPreview bit NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Projection PRIMARY KEY CLUSTERED 
(
	ProjectionPK ASC
)
);
/****** Object:  Table ProjectNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProjectNote(
	PK int NOT NULL AUTO_INCREMENT,
	ProjectPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ProjectNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ProjectStatusHistory    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ProjectStatusHistory(
	PK int NOT NULL AUTO_INCREMENT,
	ProjectPK int NOT NULL,
	IsAuthStatus bit NOT NULL,
	StatusDate datetime NOT NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ProjectStatusHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrder    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrder(
	POPK int IDENTITY(1000,1) NOT NULL,
	POID varchar(25) NOT NULL,
	POName varchar(50) NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	AuthStatus varchar(15) NOT NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NOT NULL,
	StatusDate datetime NULL,
	PODate datetime NOT NULL,
	BuyerCompanyPK int NULL,
	BuyerCompanyID varchar(25) NULL,
	BuyerCompanyName varchar(50) NULL,
	BuyerPK int NULL,
	BuyerID varchar(25) NULL,
	BuyerName varchar(50) NULL,
	FollowupDate datetime NULL,
	ShipDate datetime NULL,
	TrackingNo varchar(50) NULL,
	VendorPK int NOT NULL,
	VendorID varchar(25) NOT NULL,
	VendorName varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	InvoiceNumber varchar(30) NULL,
	FreightTerms varchar(25) NULL,
	FreightTermsDesc varchar(50) NULL,
	ShippingMethod varchar(25) NULL,
	ShippingMethodDesc varchar(50) NULL,
	Subtotal DECIMAL(13,2) NOT NULL,
	FreightCharge DECIMAL(13,2) NOT NULL,
	TaxAmount decimal(19, 6) NOT NULL,
	Total DECIMAL(13,2) NOT NULL,
	Terms varchar(25) NULL,
	TermsDesc varchar(50) NULL,
	TermsInfo varchar(2000) NULL,
	ShipToPK int NULL,
	ShipToID varchar(25) NULL,
	ShipToName varchar(50) NULL,
	ShipToAttention varchar(50) NULL,
	ShipToAddress1 varchar(80) NULL,
	ShipToAddress2 varchar(80) NULL,
	ShipToAddress3 varchar(80) NULL,
	BillToPK int NULL,
	BillToID varchar(25) NULL,
	BillToName varchar(50) NULL,
	BillToAttention varchar(50) NULL,
	BillToAddress1 varchar(80) NULL,
	BillToAddress2 varchar(80) NULL,
	BillToAddress3 varchar(80) NULL,
	TakenByPK int NULL,
	TakenByInitials varchar(25) NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	Comments varchar(2000) NULL,
	IsOpen bit NOT NULL,
	IsApproved bit NOT NULL,
	IsPartsOrdered bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	DiscountPercentage decimal(19, 6) NULL,
	Discount DECIMAL(13,2) NULL,
	SubTotalPreDiscount DECIMAL(13,2) NULL,
	SubTotalPostDiscount DECIMAL(13,2) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	Currency varchar(25) NULL,
	CurrencySymbol varchar(10) NULL,
	PrintedBox bit NULL,
	IsAveraged bit NULL,
	FormName varchar(50) NULL,
	SubStatus varchar(25) NULL,
	SubStatusDesc varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NOT NULL,
	UDFBit4 bit NOT NULL,
	UDFBit5 bit NOT NULL,
	Budget DECIMAL(13,2) NULL,
 CONSTRAINT PK_PurchaseOrder_POPK PRIMARY KEY CLUSTERED 
(
	POPK ASC
),
 CONSTRAINT IX_PurchaseOrder_POID UNIQUE NONCLUSTERED 
(
	POID ASC
)
);
/****** Object:  Table PurchaseOrderDetail    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderDetail(
	PK int NOT NULL AUTO_INCREMENT,
	POPK int NOT NULL,
	LineItemNo int NOT NULL,
	PartPK int NOT NULL,
	PartID varchar(50) NULL,
	PartName varchar(500) NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	DirectIssue bit NOT NULL,
	VendorPartNumber varchar(50) NULL,
	WOPK int NULL,
	WOID varchar(50) NULL,
	WOPartPK int NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	LaborPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(50) NULL,
	OrderUnits varchar(25) NOT NULL,
	OrderUnitsDesc varchar(50) NULL,
	ConversionToIssueUnits decimal(19, 6) NOT NULL,
	IssueUnits varchar(25) NULL,
	IssueUnitsDesc varchar(50) NULL,
	OrderUnitPrice DECIMAL(13,2) NOT NULL,
	OrderUnitQty int NOT NULL,
	OrderUnitQtyBackOrdered int NOT NULL,
	OrderUnitQtyReceived int NOT NULL,
	Discount float NOT NULL,
	Subtotal DECIMAL(13,2) NOT NULL,
	IsTax bit NOT NULL,
	TaxRate decimal(19, 6) NOT NULL,
	TaxAmount decimal(19, 6) NOT NULL,
	LineItemTotal DECIMAL(13,2) NOT NULL,
	Comments varchar(2000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(50) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(50) NULL,
	RowVersionDate datetime NOT NULL,
	FreightCharge DECIMAL(13,2) NULL,
	OrderUnitQtyCanceled int NULL,
	ReceiveDate datetime NULL,
	DueDate datetime NULL,
	SubAccountPK int NULL,
	SubAccountID varchar(25) NULL,
	SubAccountName varchar(50) NULL,
	Bin varchar(100) NULL,
 CONSTRAINT PK_PurchaseOrderDetail PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderDocument(
	PK int NOT NULL AUTO_INCREMENT,
	POPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_PurchaseOrderDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderInvoice    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderInvoice(
	InvoicePK int NOT NULL AUTO_INCREMENT,
	POPK int NOT NULL,
	InvoiceDate datetime NULL,
	InvoiceNumber varchar(30) NULL,
	Subtotal DECIMAL(13,2) NOT NULL,
	FreightCharge DECIMAL(13,2) NOT NULL,
	TaxAmount decimal(19, 6) NOT NULL,
	Total DECIMAL(13,2) NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ReceivedByPK int NULL,
	ReceivedByID varchar(25) NULL,
	ReceivedByName varchar(50) NULL,
	ReceiptNo varchar(25) NULL,
	ReceiptDate datetime NULL,
	ApprovedByPK int NULL,
	ApprovedByID varchar(25) NULL,
	ApprovedByName varchar(50) NULL,
	ReceiptNoInternal smallint NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	RcptTrans bit NULL,
	InvTrans bit NULL,
	CanNotDelete bit NULL,
	InvoiceDateNonTransactional datetime NULL,
	FuelSurcharge DECIMAL(13,2) NULL,
	ExchangeRateSurcharge DECIMAL(13,2) NULL,
	OtherCosts DECIMAL(13,2) NULL,
 CONSTRAINT PK_PurchaseOrderInvoice PRIMARY KEY NONCLUSTERED 
(
	InvoicePK ASC
)
);
/****** Object:  Table PurchaseOrderInvoiceDetail    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderInvoiceDetail(
	PK int NOT NULL AUTO_INCREMENT,
	InvoicePK int NOT NULL,
	PurchaseOrderDetailPK int NOT NULL,
	OrderUnitQtyReceived int NOT NULL,
	OrderUnitQtyBackOrdered int NOT NULL,
	OrderUnitQtyCanceled int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ReceiveDate datetime NULL,
	Bin varchar(100) NULL,
 CONSTRAINT PK_PurchaseOrderInvoiceDetail PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderNote(
	PK int NOT NULL AUTO_INCREMENT,
	POPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PONote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderPending    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderPending(
	PK int NOT NULL AUTO_INCREMENT,
	OrderPart bit NOT NULL,
	VendorPK int NULL,
	VendorID varchar(25) NULL,
	VendorName varchar(50) NULL,
	PartPK int NULL,
	PartID varchar(50) NULL,
	PartName varchar(500) NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	DirectIssue bit NOT NULL,
	VendorPartNumber varchar(50) NULL,
	WOPK int NULL,
	WOID varchar(50) NULL,
	WOPartPK int NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	LaborPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(50) NULL,
	OrderUnits varchar(25) NULL,
	OrderUnitsDesc varchar(50) NULL,
	ConversionToIssueUnits decimal(19, 6) NOT NULL,
	IssueUnits varchar(25) NULL,
	IssueUnitsDesc varchar(50) NULL,
	OrderUnitPrice DECIMAL(13,2) NOT NULL,
	OrderUnitQty int NOT NULL,
	Discount float NOT NULL,
	Subtotal DECIMAL(13,2) NOT NULL,
	IsTax bit NOT NULL,
	TaxRate decimal(19, 6) NOT NULL,
	TaxAmount decimal(19, 6) NOT NULL,
	LineItemTotal DECIMAL(13,2) NOT NULL,
	Comments varchar(2000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	Bin varchar(100) NULL,
 CONSTRAINT PK_PurchaseOrderPending PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderReceive    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderReceive(
	PK int NOT NULL AUTO_INCREMENT,
	POPK int NULL,
	PurchaseOrderDetailPK int NULL,
	ReceiveDate datetime NOT NULL,
	ReceivedByPK int NULL,
	ReceivedByInitials varchar(5) NULL,
	PackingSlipNo varchar(50) NULL,
	PartPK int NULL,
	LocationPK int NULL,
	OrderUnitPrice DECIMAL(13,2) NOT NULL,
	OrderUnitQtyReceived int NOT NULL,
	OrderUnitQtyIssued int NOT NULL,
	OrderUnitQtyAvailable int NOT NULL,
	RowVersionIPAddress varchar(50) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	OrderUnitQtyReserved int NULL,
	IssueUnitQtyReceived int NULL,
	IssueUnitQtyIssued int NULL,
	IssueUnitQtyAvailable int NULL,
	IssueUnitPrice DECIMAL(13,2) NULL,
	Bin varchar(100) NULL,
 CONSTRAINT PK_PurchaseOrderReceive PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderRMA    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderRMA(
	RMAPK int NOT NULL AUTO_INCREMENT,
	POPK int NULL,
	RMANo varchar(25) NULL,
	RMADate datetime NULL,
	RMAContactName varchar(50) NULL,
	RMAContactPhone varchar(20) NULL,
	ShipDate datetime NULL,
	ShippingMethod varchar(25) NULL,
	ShippingMethodDesc varchar(50) NULL,
	TrackingNo varchar(50) NULL,
	ApprovedByPK int NULL,
	ApprovedByID varchar(25) NULL,
	ApprovedByName varchar(50) NULL,
	RMANoInternal smallint NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PurchaseOrderRMA PRIMARY KEY NONCLUSTERED 
(
	RMAPK ASC
)
);
/****** Object:  Table PurchaseOrderRMADetail    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderRMADetail(
	PK int NOT NULL AUTO_INCREMENT,
	RMAPK int NOT NULL,
	PurchaseOrderDetailPK int NOT NULL,
	InvoicePK int NULL,
	OrderUnitQtyReturned int NOT NULL,
	OrderUnitQtyCanceled int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_PurchaseOrderRMADetail PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table PurchaseOrderStatusHistory    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE PurchaseOrderStatusHistory(
	PK int NOT NULL AUTO_INCREMENT,
	POPK int NOT NULL,
	IsAuthStatus bit NOT NULL,
	StatusDate datetime NOT NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ApprovalCountCompleted bit NULL,
 CONSTRAINT PK_PurchaseOrderStatusHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Recent    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Recent(
	pkRecent int NOT NULL AUTO_INCREMENT,
	user_guid varchar(50) NOT NULL,
	tablename varchar(50) NOT NULL,
	rowpk int NOT NULL,
	recent_timestamp datetime NOT NULL,
 CONSTRAINT PK_Recent PRIMARY KEY CLUSTERED 
(
	pkRecent ASC
)
);
/****** Object:  Table RepairCenter    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RepairCenter(
	RepairCenterPK int NOT NULL AUTO_INCREMENT,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	Active bit NOT NULL,
	DefaultRepairCenter bit NOT NULL,
	RequesterCanJoin bit NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	StockRoomPK int NULL,
	StockRoomID varchar(25) NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	Budget DECIMAL(13,2) NULL,
	BuyerCompanyPK int NULL,
	BuyerCompanyID varchar(25) NULL,
	BuyerCompanyName varchar(50) NULL,
	BuyerPK int NULL,
	BuyerID varchar(25) NULL,
	BuyerName varchar(50) NULL,
	ShipToPK int NULL,
	ShipToID varchar(25) NULL,
	ShipToName varchar(50) NULL,
	ShipToAttention varchar(50) NULL,
	ShipToAddress1 varchar(80) NULL,
	ShipToAddress2 varchar(80) NULL,
	ShipToAddress3 varchar(80) NULL,
	BillToPK int NULL,
	BillToID varchar(25) NULL,
	BillToName varchar(50) NULL,
	BillToAttention varchar(50) NULL,
	BillToAddress1 varchar(80) NULL,
	BillToAddress2 varchar(80) NULL,
	BillToAddress3 varchar(80) NULL,
	Address varchar(80) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(15) NULL,
	Country varchar(50) NULL,
	Phone varchar(30) NULL,
	Police varchar(50) NULL,
	Security varchar(50) NULL,
	Fire varchar(50) NULL,
	Fax varchar(30) NULL,
	Email varchar(50) NULL,
	EmailNotify varchar(25) NULL,
	EmailNotifyDesc varchar(50) NULL,
	URL varchar(200) NULL,
	Comments varchar(4000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	PMCycleStartDate datetime NULL,
	RiskLevel smallint NULL,
	PMCounter smallint NULL,
	Currency varchar(25) NULL,
	ApprovalType int NOT NULL,
	POApprovalType int NOT NULL,
	SMSEmail varchar(50) NULL,
	FormName varchar(50) NULL,
 CONSTRAINT PK_RepairCenter PRIMARY KEY NONCLUSTERED 
(
	RepairCenterPK ASC
),
 CONSTRAINT IX_RepairCenter UNIQUE CLUSTERED 
(
	RepairCenterID ASC
)
);
/****** Object:  Table RepairCenterApproval    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RepairCenterApproval(
	ApprovalPK int NOT NULL AUTO_INCREMENT,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	ModuleID varchar(5) NOT NULL,
	ApprovalType int NOT NULL,
	ApprovalLevel int NOT NULL,
	AccessGroupPK int NULL,
	AccessGroupID varchar(25) NULL,
	AccessGroupName varchar(50) NULL,
	AmountA decimal(18, 2) NOT NULL,
	AmountB decimal(18, 2) NOT NULL,
	Condition varchar(2) NOT NULL,
	RequiredApprovals int NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowversionDate datetime NOT NULL,
 CONSTRAINT PK__RepairCenterAppr__0290FADB PRIMARY KEY CLUSTERED 
(
	ApprovalPK ASC
)
);
/****** Object:  Table RepairCenterDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RepairCenterDocument(
	PK int NOT NULL AUTO_INCREMENT,
	RepairCenterPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_RepairCenterDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table RepairCenterNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RepairCenterNote(
	PK int NOT NULL AUTO_INCREMENT,
	RepairCenterPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_RepairCenterNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table RepairCenterPreference    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RepairCenterPreference(
	PreferenceName varchar(50) NOT NULL,
	RepairCenterPK int NOT NULL,
	PreferenceValue varchar(1000) NOT NULL,
	PreferenceValueDesc varchar(150) NULL,
	PreferenceValuePK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_RepairCenterPreference PRIMARY KEY CLUSTERED 
(
	PreferenceName ASC,
	RepairCenterPK ASC
)
);
/****** Object:  Table Report_ReportGroup    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Report_ReportGroup(
	PK int NOT NULL AUTO_INCREMENT,
	ReportPK int NOT NULL,
	ReportGroupPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NULL,
 CONSTRAINT PK_Report_ReportGroup PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ReportAccessGroup    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportAccessGroup(
	PK int NOT NULL AUTO_INCREMENT,
	ReportPK int NOT NULL,
	AccessGroupPK int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ReportAccessGroup PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ReportCharting    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportCharting(
	ChartPK int NOT NULL AUTO_INCREMENT,
	ChartID varchar(25) NOT NULL,
	ChartAreaID varchar(25) NOT NULL,
	ReportPK int NOT NULL,
	GroupField varchar(500) NOT NULL,
	DataFields varchar(2500) NOT NULL,
	DataAggregate varchar(10) NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowversionDate datetime NOT NULL,
 CONSTRAINT PK__ReportCharting__762B23F6 PRIMARY KEY CLUSTERED 
(
	ChartPK ASC
)
);
/****** Object:  Table ReportChartingData    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportChartingData(
	DataPK int NOT NULL AUTO_INCREMENT,
	ChartPK int NOT NULL,
	ChartAreaID varchar(50) NOT NULL,
	ReportPK int NOT NULL,
	DataAlias varchar(150) NOT NULL,
	DataField varchar(150) NOT NULL,
	DataAggregate varchar(10) NOT NULL,
	DataColor int NOT NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowversionDate datetime NOT NULL,
 CONSTRAINT PK__ReportChartingDa__0FEAF5F9 PRIMARY KEY CLUSTERED 
(
	DataPK ASC
)
);
/****** Object:  Table ReportCriteria    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportCriteria(
	ReportCriteriaPK int NOT NULL AUTO_INCREMENT,
	ReportPK int NOT NULL,
	DisplayTable varchar(50) NOT NULL,
	DisplayField varchar(50) NOT NULL,
	SQLWhereTable varchar(50) NOT NULL,
	SQLWhereField varchar(50) NOT NULL,
	DefaultCritValue varchar(2000) NULL,
	CritName varchar(2000) NULL,
	Operator varchar(50) NULL,
	isMulti bit NOT NULL,
	AskLater bit NOT NULL,
	LabelOverride varchar(255) NULL,
	DisplayOrder int NULL,
	FK_LookupOverride varchar(25) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	CriteriaHTML varchar(max) NOT NULL,
 CONSTRAINT PK_ReportCriteria PRIMARY KEY NONCLUSTERED 
(
	ReportCriteriaPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table ReportFields    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportFields(
	ReportFieldPK int NOT NULL AUTO_INCREMENT,
	ReportPK int NOT NULL,
	DataDictPK int NULL,
	AGFunction varchar(50) NULL,
	RFTable varchar(50) NOT NULL,
	RFField varchar(50) NOT NULL,
	Alias varchar(50) NULL,
	DisplayOrder int NOT NULL,
	Display bit NOT NULL,
	NotUserSelectable bit NOT NULL,
	LabelOverride varchar(255) NULL,
	TotalIfSelected bit NOT NULL,
	BlankLineIfSelected bit NOT NULL,
	UseCustomExpression bit NOT NULL,
	CustomExpression varchar(2000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	SLAction char(2) NULL,
	SLModuleID char(2) NULL,
	SLPKField varchar(100) NULL,
	SLReportID varchar(50) NULL,
	SLCustomAction varchar(2000) NULL,
	SLTooltip varchar(100) NULL,
	Alignment char(1) NULL,
	AdditionalWidth smallint NULL,
	PivotSetup char(2) NULL,
	AddPivotColumnsWithNoDataFrom varchar(200) NULL,
	AddPivotColumnsWithNoDataFromCustom varchar(1000) NULL,
	Data_Type_Override varchar(256) NULL,
	ColumnFormat smallint NULL,
	ColumnCS varchar(2000) NULL,
	GroupByCustomExpression bit NOT NULL,
 CONSTRAINT PK_ReportFields PRIMARY KEY NONCLUSTERED 
(
	ReportFieldPK ASC
)
);
/****** Object:  Table ReportGroup    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportGroup(
	ReportGroupPK int NOT NULL AUTO_INCREMENT,
	ReportGroupID varchar(25) NOT NULL,
	ReportGroupName varchar(255) NULL,
	ModuleID char(2) NULL,
	Sequence smallint NULL,
	Icon varchar(200) NULL,
	RepairCenterPK int NULL,
	IsUserGroup bit NOT NULL,
	IsBatchGroup bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	PrivateGroup bit NULL,
	FavoriteGroup bit NULL,
	LaborPK int NULL,
 CONSTRAINT PK_ReportGroup PRIMARY KEY NONCLUSTERED 
(
	ReportGroupPK ASC
),
 CONSTRAINT IX_ReportGroup UNIQUE CLUSTERED 
(
	ReportGroupID ASC
)
);
/****** Object:  Table ReportGroupHeader    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportGroupHeader(
	GroupHeaderPK int NOT NULL AUTO_INCREMENT,
	GroupHeaderName varchar(100) NULL,
	ModuleID char(2) NULL,
	GroupHeader varchar(4000) NULL,
 CONSTRAINT PK_ReportGroupHeader PRIMARY KEY CLUSTERED 
(
	GroupHeaderPK ASC
)
);
/****** Object:  Table Reports    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Reports(
	ReportPK int NOT NULL AUTO_INCREMENT,
	ReportIDPriorToCopy varchar(50) NULL,
	ReportID varchar(50) NULL,
	ReportName varchar(255) NOT NULL,
	ReportDesc varchar(500) NULL,
	RepairCenterPK int NULL,
	Sort1 varchar(255) NULL,
	Sort2 varchar(255) NULL,
	Sort3 varchar(255) NULL,
	Sort4 varchar(255) NULL,
	Sort5 varchar(255) NULL,
	Sort1DESC bit NOT NULL,
	Sort2DESC bit NOT NULL,
	Sort3DESC bit NOT NULL,
	Sort4DESC bit NOT NULL,
	Sort5DESC bit NOT NULL,
	Group1 bit NOT NULL,
	Group2 bit NOT NULL,
	Group3 bit NOT NULL,
	Group4 bit NOT NULL,
	Group5 bit NOT NULL,
	Header1 bit NOT NULL,
	Header2 bit NOT NULL,
	Header3 bit NOT NULL,
	Header4 bit NOT NULL,
	Header5 bit NOT NULL,
	GroupHeader1 int NULL,
	GroupHeader2 int NULL,
	GroupHeader3 int NULL,
	GroupHeader4 int NULL,
	GroupHeader5 int NULL,
	Total1 bit NOT NULL,
	Total2 bit NOT NULL,
	Total3 bit NOT NULL,
	Total4 bit NOT NULL,
	Total5 bit NOT NULL,
	Chart varchar(20) NULL,
	ChartName varchar(255) NULL,
	ChartField varchar(255) NULL,
	ChartSize char(1) NULL,
	ReportFile varchar(255) NOT NULL,
	FromSQL varchar(6000) NULL,
	JoinSQL varchar(1000) NULL,
	WhereSQL varchar(1000) NULL,
	GroupBy bit NOT NULL,
	hits int NOT NULL,
	Sequence int NOT NULL,
	Layout varchar(4) NOT NULL,
	VertCols smallint NOT NULL,
	PageBreakEachRecord bit NOT NULL,
	Custom bit NOT NULL,
	ReportCopy bit NOT NULL,
	MCRegistrationDB bit NOT NULL,
	PrintCriteria bit NOT NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	UsedFor varchar(25) NULL,
	ChartFunction char(1) NULL,
	ChartFunctionField varchar(255) NULL,
	NoDetail bit NULL,
	PB1 bit NULL,
	PB2 bit NULL,
	PB3 bit NULL,
	PB4 bit NULL,
	PB5 bit NULL,
	HavingSQL varchar(1000) NULL,
	SLDefault bit NULL,
	SLType char(1) NULL,
	SLAction char(2) NULL,
	SLModuleID char(2) NULL,
	SLPKField varchar(100) NULL,
	SLReportID varchar(50) NULL,
	SLCustomAction varchar(2000) NULL,
	SLTooltip varchar(100) NULL,
	SDDisplay char(5) NULL,
	SDModuleID char(2) NULL,
	SDPKField varchar(100) NULL,
	SmartEmail bit NULL,
	ReportSchedulePK int NULL,
	ChartPosition char(1) NULL,
	ChartFormat char(1) NULL,
	ChartSQL varchar(2000) NULL,
	Chart2 varchar(20) NULL,
	ChartName2 varchar(255) NULL,
	ChartField2 varchar(255) NULL,
	ChartSize2 char(1) NULL,
	ChartFormat2 char(1) NULL,
	ChartFunction2 char(1) NULL,
	ChartFunctionField2 varchar(255) NULL,
	ChartPosition2 char(1) NULL,
	ChartSQL2 varchar(2000) NULL,
	Chart3 varchar(20) NULL,
	ChartName3 varchar(255) NULL,
	ChartField3 varchar(255) NULL,
	ChartSize3 char(1) NULL,
	ChartFormat3 char(1) NULL,
	ChartFunction3 char(1) NULL,
	ChartFunctionField3 varchar(255) NULL,
	ChartPosition3 char(1) NULL,
	ChartSQL3 varchar(2000) NULL,
	ChartOnly bit NULL,
	NoHeader bit NULL,
	SRID1 varchar(50) NULL,
	SRPKField1 varchar(100) NULL,
	SRID2 varchar(50) NULL,
	SRPKField2 varchar(100) NULL,
	SRID3 varchar(50) NULL,
	SRPKField3 varchar(100) NULL,
	SRID4 varchar(50) NULL,
	SRPKField4 varchar(100) NULL,
	SRID5 varchar(50) NULL,
	SRPKField5 varchar(100) NULL,
	ReportPageSize varchar(10) NULL,
	ReportWidth varchar(10) NULL,
	PhotoCriteria bit NULL,
	ReportScheduleFilePK int NULL,
	ReportStyleName varchar(50) NULL,
	SmartEmailLaborPK int NULL,
	LockReport bit NULL,
	SCDefault char(1) NULL,
	SCField1 varchar(100) NULL,
	SCField2 varchar(100) NULL,
	SCField3 varchar(100) NULL,
	ReportStyleFontSize char(5) NULL,
	ReportStyleFontColor char(7) NULL,
	ReportStyleFontFamily varchar(100) NULL,
	OutputFormat char(1) NULL,
	KPIGroupPK int NULL,
	KPIGroupCriteria char(1) NULL,
	SmartEmailAutoLogin smallint NULL,
	DisplayPivotBar bit NULL,
	DisplayColumnLines bit NULL,
	DisplayTitleonPageBreak bit NULL,
	DisplayFormatCriteria bit NULL,
	R1T char(1) NULL,
	R1O char(15) NULL,
	R1V1 varchar(500) NULL,
	R1V2 varchar(100) NULL,
	R1A smallint NULL,
	R1L smallint NULL,
	R1F smallint NULL,
	R1CS varchar(2000) NULL,
	R1AF char(1) NULL,
	R2T char(1) NULL,
	R2O char(15) NULL,
	R2V1 varchar(500) NULL,
	R2V2 varchar(100) NULL,
	R2A smallint NULL,
	R2L smallint NULL,
	R2F smallint NULL,
	R2CS varchar(2000) NULL,
	R2AF char(1) NULL,
	R3T char(1) NULL,
	R3O char(15) NULL,
	R3V1 varchar(500) NULL,
	R3V2 varchar(100) NULL,
	R3A smallint NULL,
	R3L smallint NULL,
	R3F smallint NULL,
	R3CS varchar(2000) NULL,
	R3AF char(1) NULL,
	DisplayDescription bit NULL,
	ReportShareID varchar(50) NOT NULL,
	ShareRemoteID varchar(50) NOT NULL,
	ReportTemplateName varchar(50) NULL,
	ReportPageSizeTemplate varchar(250) NULL,
	VertColsAlign varchar(25) NULL,
	SubReportGen char(2) NULL,
	SRCaption1 varchar(100) NULL,
	SRCaption2 varchar(100) NULL,
	SRCaption3 varchar(100) NULL,
	SRCaption4 varchar(100) NULL,
	SRCaption5 varchar(100) NULL,
	LastRun datetime NULL,
	AutomationRCPK int NULL,
	RefreshInterval smallint NULL,
 CONSTRAINT PK_Reports PRIMARY KEY NONCLUSTERED 
(
	ReportPK ASC
)
);
/****** Object:  Table ReportSchedule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportSchedule(
	ReportSchedulePK int NOT NULL AUTO_INCREMENT,
	ReportScheduleName varchar(200) NULL,
	LaborPK int NULL,
	IsActive bit NOT NULL,
	RepairCenterPK int NULL,
	EmailFromName varchar(50) NULL,
	EmailFromAddress varchar(100) NULL,
	EmailToAlways varchar(max) NULL,
	EmailSubject varchar(500) NULL,
	EmailPriority smallint NULL,
	IntervalType char(1) NULL,
	IntervalPeriod int NULL,
	StartDate datetime NULL,
	NextScheduleDate datetime NULL,
	OutputFormat char(1) NULL,
	OutputType char(1) NULL,
	OutputFilePath varchar(2000) NULL,
	AddDateToFileName bit NULL,
	OutputURLPath varchar(2000) NULL,
	EmailToAlwaysEGPK int NULL,
 CONSTRAINT PK_ReportSchedule_ReportSchedulePK PRIMARY KEY CLUSTERED 
(
	ReportSchedulePK ASC
)
);
/****** Object:  Table ReportScheduleReport    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportScheduleReport(
	PK int NOT NULL AUTO_INCREMENT,
	ReportSchedulePK int NULL,
	ReportPK int NULL,
	IsActive bit NOT NULL,
	OutputFormat char(1) NULL,
	ReportFileName varchar(50) NULL,
	DoNotSendIfNoResults bit NULL,
	LaborPK int NULL,
	Exclude_MO bit NULL,
	Exclude_TU bit NULL,
	Exclude_WE bit NULL,
	Exclude_TH bit NULL,
	Exclude_FR bit NULL,
	Exclude_SA bit NULL,
	Exclude_SU bit NULL,
 CONSTRAINT PK_ReportScheduleReport PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ReportStyle    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportStyle(
	ReportStyleName varchar(50) NOT NULL,
	ReportStyleDesc varchar(500) NULL,
	ReportStyleCSS varchar(max) NULL,
	IsDefault bit NULL,
	IsBase bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ReportStyle PRIMARY KEY CLUSTERED 
(
	ReportStyleName ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table ReportTables    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportTables(
	PK int NOT NULL AUTO_INCREMENT,
	ReportPK int NOT NULL,
	RFTable varchar(50) NOT NULL,
	Alias varchar(50) NULL,
	DisplayOrder int NOT NULL,
	LabelOverride varchar(255) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ReportTables PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ReportTemplate    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReportTemplate(
	ReportTemplateName varchar(50) NOT NULL,
	ReportTemplateDesc varchar(500) NULL,
	ReportTemplateHTML varchar(max) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ReportTemplate PRIMARY KEY CLUSTERED 
(
	ReportTemplateName ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table RequesterAppOptions    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RequesterAppOptions(
	RAOPK int NOT NULL AUTO_INCREMENT,
	RepairCenterPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	WelcomeText_Display bit NOT NULL,
	CustomerService_Display bit NOT NULL,
	FAQText_Display bit NOT NULL,
	HelpText_Display bit NOT NULL,
	WelcomeText varchar(max) NULL,
	CustomerServiceText varchar(max) NULL,
	FAQText varchar(max) NULL,
	HelpText varchar(max) NULL,
	ServiceRequestStatus_Display bit NULL,
	FeedbackSurvey_Display bit NULL,
	RepairProcedures_Display bit NULL,
	Profile_Display bit NULL,
	ChangePassword_Display bit NULL,
	Reports_Display bit NULL,
	UseRequesterInfo bit NULL,
	Name bit NULL,
	Email bit NULL,
	Phone bit NULL,
	Type bit NULL,
	Priority bit NULL,
	TargetDate bit NULL,
	Asset bit NULL,
	Problem bit NULL,
	Task bit NULL,
	Reason bit NULL,
	DuplicateCheck bit NULL,
	Color_Title varchar(25) NULL,
	Color_SubTitle varchar(25) NULL,
	Color_Line varchar(25) NULL,
	Color_Header varchar(25) NULL,
	Color_Link varchar(25) NULL,
	Color_Text varchar(25) NULL,
	Color_Button varchar(25) NULL,
	Color_Background varchar(25) NULL,
	Color_AltLogo varchar(200) NULL,
	Color_BackgroundImage varchar(200) NULL,
	WelcomeText_Link varchar(200) NULL,
	ServiceRequest_Link varchar(200) NULL,
	ServiceRequestStatus_Link varchar(200) NULL,
	CustomerService_Link varchar(200) NULL,
	FAQText_Link varchar(200) NULL,
	HelpText_Link varchar(200) NULL,
	FeedbackSurvey_Link varchar(200) NULL,
	RepairProcedures_Link varchar(200) NULL,
	Profile_Link varchar(200) NULL,
	ChangePassword_Link varchar(200) NULL,
	Reports_Link varchar(200) NULL,
	HTML_AfterSave varchar(max) NULL,
	Approvals_Display bit NULL,
	Finalize_Display bit NULL,
	ShowAllRequests_Display bit NULL,
	Approvals_Link varchar(200) NULL,
	Finalize_Link varchar(200) NULL,
	ShowAllRequests_Link varchar(200) NULL,
	Shop bit NULL,
	RepairCenter bit NULL,
	ShopAll bit NULL,
	Category bit NULL,
	Department bit NULL,
	LaborRequest bit NULL,
	MaterialRequest bit NULL,
	OtherCostRequest bit NULL,
	ProcedureLibrary bit NULL,
	ProcedureLibrary_Labor bit NULL,
	ProcedureLibrary_Material bit NULL,
	Assignments bit NULL,
	Supervisor bit NULL,
	Approvals bit NULL,
	ApprovalRouting bit NULL,
	Customer bit NULL,
	Project bit NULL,
	Account bit NULL,
	Chargeable bit NULL,
	ShutdownRequired bit NULL,
	LockoutTagOut bit NULL,
	FollowupWork bit NULL,
	SpecialInstructions bit NULL,
	Attachments bit NULL,
	ImageUpload bit NULL,
	EmailControl bit NULL,
	EmailControlRequested bit NULL,
	EmailControlIssued bit NULL,
	EmailControlRespond bit NULL,
	EmailControlDenied bit NULL,
	EmailControlCanceled bit NULL,
	EmailControlOnHold bit NULL,
	EmailControlCompleted bit NULL,
	EmailControlClosed bit NULL,
	EmailControlAssigned bit NULL,
	EmailControlPickReport bit NULL,
	EmailControlOtherAddresses bit NULL,
	EmailControlCustom1 bit NULL,
	EmailControlCustom2 bit NULL,
	EmailControlPOPartsReceived bit NULL,
	EmailControlPOIssued bit NULL,
	EmailControlPOClosed bit NULL,
	Custom1 bit NULL,
	Custom2 bit NULL,
	Custom3 bit NULL,
	Custom4 bit NULL,
	Custom5 bit NULL,
	ShowDocuments bit NULL,
	ShowImages bit NULL,
	ShowMiscFiles bit NULL,
	ShowAccount bit NULL,
	HeaderText varchar(255) NULL,
	SubmitServiceRequest_Display bit NOT NULL,
	SubmitServiceRequestAsset_Display bit NOT NULL,
	Help_Display bit NOT NULL,
	SubmitServiceRequestText varchar(max) NULL,
	SubmitServiceRequestAssetText varchar(max) NULL,
	ServiceRequestStatusText varchar(max) NULL,
	FeedbackSurveyText varchar(max) NULL,
	nameorder tinyint NULL,
	emailorder tinyint NULL,
	phoneorder tinyint NULL,
	assetorder tinyint NULL,
	shoporder tinyint NULL,
	accountorder tinyint NULL,
	typeorder tinyint NULL,
	priorityorder tinyint NULL,
	categoryorder tinyint NULL,
	neededbyorder tinyint NULL,
	departmentorder tinyint NULL,
	problemorder tinyint NULL,
	taskorder tinyint NULL,
	reasonorder tinyint NULL,
	documentorder tinyint NULL,
	imageorder tinyint NULL,
	miscfileorder tinyint NULL,
	AssetFilter bit NOT NULL,
	ShopFilter bit NOT NULL,
	AccountFilter bit NOT NULL,
	TypeFilter bit NOT NULL,
	PriorityFilter bit NOT NULL,
	CategoryFilter bit NOT NULL,
	DepartmentFilter bit NOT NULL,
	ProblemFilter bit NOT NULL,
	DetailedStatus bit NOT NULL,
 CONSTRAINT PK_RequesterAppOptions PRIMARY KEY NONCLUSTERED 
(
	RAOPK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table Reservation    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Reservation(
	ReservationPK int NOT NULL AUTO_INCREMENT,
	StartTime smallint NOT NULL,
	EndTime smallint NOT NULL,
	StartDate datetime NOT NULL,
	EndDate datetime NOT NULL,
	AssetPk int NULL,
	PrepTime smallint NULL,
	CleanupTime smallint NULL,
	ReservationTitle varchar(250) NULL,
	ReservationType varchar(100) NULL,
	HostBy varchar(50) NULL,
	UserPK int NULL,
	UserID varchar(25) NULL,
	UserName varchar(50) NULL,
	MaxAttending int NULL,
	DisplayColor varchar(20) NULL,
	IsActive bit NOT NULL,
	IsRecurrence bit NULL,
	IsAllDayEvent bit NULL,
	RecurStartDate datetime NULL,
	RecurEndDate datetime NULL,
	isNoRecurEndDate bit NULL,
	RecurType varchar(10) NULL,
	RecurFreq int NULL,
	recurOn int NULL,
	LModified datetime NULL,
	RowVersionDate datetime NULL
);
/****** Object:  Table ReservationEquipment    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReservationEquipment(
	ReservationEquipmentPK int NOT NULL AUTO_INCREMENT,
	ReservationPK int NULL,
	AssetPK int NULL,
	isActive bit NOT NULL,
	Lmodified datetime NULL,
	RowVersionDate datetime NULL
);
/****** Object:  Table ReservationException    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReservationException(
	PK int NOT NULL AUTO_INCREMENT,
	ReservationPK int NULL,
	ExceptionDate datetime NULL,
	IsActive bit NULL,
	RowVersionDate datetime NULL
);
/****** Object:  Table ReservationLabor    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ReservationLabor(
	ReservationLaborPK int NOT NULL AUTO_INCREMENT,
	ReservationPK int NULL,
	LaborPK int NULL,
	isActive bit NOT NULL,
	LModified datetime NULL,
	RowVersionDate datetime NULL
);
/****** Object:  Table Route    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Route(
	RoutePK int NOT NULL AUTO_INCREMENT,
	RouteID varchar(25) NOT NULL,
	RouteName varchar(100) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Route PRIMARY KEY NONCLUSTERED 
(
	RoutePK ASC
)
);
/****** Object:  Table RouteList    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE RouteList(
	PK int NOT NULL AUTO_INCREMENT,
	RoutePK int NOT NULL,
	RouteNo int NULL,
	ProcedurePK int NULL,
	AssetPK int NULL,
	PreReqRouteNos varchar(1000) NULL,
	PreReqWarningOnly bit NOT NULL,
	GenerateType varchar(25) NULL,
	GenerateTypeDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_RouteList PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Shift    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Shift(
	ShiftPK int NOT NULL AUTO_INCREMENT,
	ShiftID varchar(25) NOT NULL,
	ShiftName varchar(50) NULL,
	ShiftColor varchar(10) NULL,
	ShiftStart datetime NULL,
	ShiftEnd datetime NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	Phone varchar(30) NULL,
	Pager varchar(50) NULL,
	PagerService varchar(50) NULL,
	PagerPIN varchar(50) NULL,
	PagerNumeric bit NOT NULL,
	Fax varchar(30) NULL,
	Email varchar(50) NULL,
	PagerEmail varchar(50) NULL,
	EmailNotify varchar(25) NULL,
	EmailNotifyDesc varchar(50) NULL,
	PrinterName varchar(50) NULL,
	URL varchar(200) NULL,
	Comments varchar(4000) NULL,
	Active bit NOT NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Shift PRIMARY KEY NONCLUSTERED 
(
	ShiftPK ASC
),
 CONSTRAINT IX_Shift UNIQUE CLUSTERED 
(
	ShiftID ASC
)
);
/****** Object:  Table ShiftTimeOff    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ShiftTimeOff(
	PK int NOT NULL AUTO_INCREMENT,
	ShiftPK int NOT NULL,
	WorkDate datetime NOT NULL,
	WorkDate2 datetime NULL,
	Hours decimal(19, 6) NOT NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ShiftWorkExceptions PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ShiftWorkSchedule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ShiftWorkSchedule(
	PK int NOT NULL AUTO_INCREMENT,
	ShiftPK int NOT NULL,
	BeginDate datetime NOT NULL,
	EndDate datetime NOT NULL,
	Sunday decimal(19, 6) NOT NULL,
	Monday decimal(19, 6) NOT NULL,
	Tuesday decimal(19, 6) NOT NULL,
	Wednesday decimal(19, 6) NOT NULL,
	Thursday decimal(19, 6) NOT NULL,
	Friday decimal(19, 6) NOT NULL,
	Saturday decimal(19, 6) NOT NULL,
	DemoShiftPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ShiftWorkHours PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Shop    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Shop(
	ShopPK int NOT NULL AUTO_INCREMENT,
	ShopID varchar(25) NOT NULL,
	ShopName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	Budget DECIMAL(13,2) NULL,
	Address varchar(80) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(15) NULL,
	Country varchar(50) NULL,
	Phone varchar(30) NULL,
	Pager varchar(50) NULL,
	PagerService varchar(50) NULL,
	PagerPIN varchar(50) NULL,
	PagerNumeric bit NOT NULL,
	Fax varchar(30) NULL,
	Email varchar(50) NULL,
	PagerEmail varchar(50) NULL,
	EmailNotify varchar(25) NULL,
	EmailNotifyDesc varchar(50) NULL,
	PrinterName varchar(50) NULL,
	URL varchar(200) NULL,
	Comments varchar(4000) NULL,
	Active bit NOT NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	PMCycleStartDate datetime NULL,
	RiskLevel smallint NULL,
	PMCounter smallint NULL,
	SMSEmail varchar(50) NULL,
	FormName varchar(50) NULL,
 CONSTRAINT PK_Shop PRIMARY KEY NONCLUSTERED 
(
	ShopPK ASC
),
 CONSTRAINT IX_Shop UNIQUE CLUSTERED 
(
	ShopID ASC
)
);
/****** Object:  Table ShopDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ShopDocument(
	PK int NOT NULL AUTO_INCREMENT,
	ShopPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_ShopDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ShopNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ShopNote(
	PK int NOT NULL AUTO_INCREMENT,
	ShopPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ShopNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Specification    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Specification(
	SpecificationPK int NOT NULL AUTO_INCREMENT,
	SpecificationName varchar(50) NOT NULL,
	Description varchar(150) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	TrackHistory bit NOT NULL,
	Active bit NOT NULL,
	Comments varchar(4000) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	UseLookupTable bit NULL,
	LookupTable varchar(25) NULL,
	UseWOLookupTable bit NULL,
	WOLookupTable varchar(25) NULL,
	UseWOLookupTable2 bit NULL,
	WOLookupTable2 varchar(25) NULL,
 CONSTRAINT PK_Specification PRIMARY KEY NONCLUSTERED 
(
	SpecificationPK ASC
),
 CONSTRAINT IX_Specification UNIQUE CLUSTERED 
(
	SpecificationName ASC
)
);
/****** Object:  Table SpecificationHistory    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SpecificationHistory(
	PK int NOT NULL AUTO_INCREMENT,
	TableName varchar(50) NULL,
	TablePK int NOT NULL,
	ChangeDate datetime NOT NULL,
	ChangeReason varchar(50) NULL,
	ValueNumeric decimal(19, 6) NULL,
	ValueText varchar(50) NULL,
	ValueDate datetime NULL,
	WOPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_SpecificationHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table SQLSchedule    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SQLSchedule(
	SQLSchedulePK int NOT NULL AUTO_INCREMENT,
	SQLScheduleName varchar(200) NULL,
	LaborPK int NULL,
	RepairCenterPK int NULL,
	IntervalType char(1) NOT NULL,
	IntervalPeriod int NOT NULL,
	StartDate datetime NOT NULL,
	NextScheduleDate datetime NOT NULL,
	SQLtoProcess varchar(max) NOT NULL,
	SQLLogDate datetime NULL,
	SQLLog varchar(max) NULL,
	ProcessCounterStopAt int NOT NULL,
	ProcessCounter int NOT NULL,
	IsActive bit NOT NULL,
	RulePK int NULL,
 CONSTRAINT PK_SQLSchedule_SQLSchedulePK PRIMARY KEY CLUSTERED 
(
	SQLSchedulePK ASC
)
);
/****** Object:  Table SQLScheduleLog    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SQLScheduleLog(
	PK int NOT NULL AUTO_INCREMENT,
	SQLSchedulePK int NOT NULL,
	SQLDate datetime NOT NULL,
	SQLLog varchar(max) NULL,
 CONSTRAINT PK_SQLScheduleLog PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table SUR_ID_Generation    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_ID_Generation(
	NEXT_AVAILABLE_ID int NOT NULL,
	NAME varchar(40) NOT NULL
);
/****** Object:  Table SUR_Item    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Item(
	ITEM_ID int NOT NULL,
	ITEM_TYPE_ID int NOT NULL,
	ITEM_TEXT varchar(max) NULL,
	ITEM_SUB_TEXT varchar(255) NULL,
	REQUIRED_YN varchar(1) NULL,
	OTHER_YN varchar(1) NULL,
	OTHER_TEXT varchar(max) NULL,
	MINIMUM_VALUE varchar(10) NULL,
	MAXIMUM_VALUE varchar(10) NULL,
	MINIMUM_NUMBER_RESPONSES int NULL,
	MAXIMUM_NUMBER_RESPONSES int NULL,
	DISPLAY_FORMAT varchar(30) NULL,
	DEFAULT_VALUE varchar(max) NULL,
	IMAGE_PATH varchar(50) NULL,
	IMAGE_WIDTH varchar(4) NULL,
	IMAGE_HEIGHT varchar(4) NULL,
	IMAGE_ALIGNMENT varchar(6) NULL,
 CONSTRAINT PK_SUR_ITEM PRIMARY KEY CLUSTERED 
(
	ITEM_ID ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table SUR_Item_Answer    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Item_Answer(
	ANSWER_ID int NOT NULL,
	ITEM_ID int NOT NULL,
	ANSWER_TEXT varchar(75) NULL,
	ORDER_NUMBER int NULL,
	DEFAULT_YN varchar(1) NOT NULL,
 CONSTRAINT PK_SUR_ITEM_ANSWER PRIMARY KEY CLUSTERED 
(
	ANSWER_ID ASC
)
);
/****** Object:  Table SUR_Item_Type    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Item_Type(
	ITEM_TYPE_ID int NOT NULL,
	ITEM_TYPE_NAME varchar(50) NOT NULL,
	ORDER_NUMBER int NOT NULL,
	QUESTION_YN varchar(1) NOT NULL,
 CONSTRAINT PK_SUR_ITEM_TYPE PRIMARY KEY CLUSTERED 
(
	ITEM_TYPE_ID ASC
)
);
/****** Object:  Table SUR_Page_Condition    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Page_Condition(
	PAGE_CONDITION_ID int NOT NULL,
	SURVEY_ID int NOT NULL,
	PAGE_NUMBER int NOT NULL,
	DEPENDENT_ITEM_ID int NOT NULL,
	GROUP_NUMBER int NOT NULL,
	OPERATOR_ID int NOT NULL,
	ANSWER_ID int NULL,
	ANSWER_TEXT varchar(255) NULL,
 CONSTRAINT PK_SUR_PAGE_CONDITION PRIMARY KEY CLUSTERED 
(
	PAGE_CONDITION_ID ASC
)
);
/****** Object:  Table SUR_Page_Condition_Operator    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Page_Condition_Operator(
	OPERATOR_ID int NOT NULL,
	OPERATOR_TEXT_SHORT varchar(20) NOT NULL,
	OPERATOR_TEXT_LONG varchar(75) NOT NULL,
	ORDER_NUMBER int NOT NULL,
 CONSTRAINT PK_SUR_PAGE_CONDITION_OPERATOR PRIMARY KEY CLUSTERED 
(
	OPERATOR_ID ASC
)
);
/****** Object:  Table SUR_Response    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Response(
	RESPONSE_ID int NOT NULL,
	SURVEY_ID int NOT NULL,
	WOPK int NULL,
	ANONYMOUS_ID int NULL,
	USERNAME varchar(20) NOT NULL,
	RESPONSE_START_DATE datetime NOT NULL,
	RESPONSE_END_DATE datetime NULL,
	IP_ADDRESS varchar(15) NOT NULL,
	COMPLETED_YN varchar(1) NOT NULL,
 CONSTRAINT PK_SUR_RESPONSE PRIMARY KEY CLUSTERED 
(
	RESPONSE_ID ASC
)
);
/****** Object:  Table SUR_Response_Answer    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Response_Answer(
	RESPONSE_ID int NOT NULL,
	ITEM_ID int NOT NULL,
	ANSWER_ID int NULL,
	ANSWER_TEXT varchar(max) NULL,
	OTHER_TEXT varchar(max) NULL
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table SUR_Survey    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Survey(
	SURVEY_ID int NOT NULL,
	REPAIRCENTERPK int NULL,
	TITLE varchar(50) NOT NULL,
	INTRODUCTION varchar(max) NULL,
	CLOSED_DATE datetime NULL,
	CREATED_DATE datetime NOT NULL,
	LAUNCHED_DATE datetime NULL,
	START_DATE datetime NULL,
	END_DATE datetime NULL,
	MAXIMUM_RESPONSES_FOR_SURVEY int NULL,
	RESPONSE_COUNT int NOT NULL,
	COMPLETION_ACTION varchar(50) NOT NULL,
	SURVEY_COMPLETE_MESSAGE varchar(max) NULL,
	SURVEY_COMPLETE_REDIRECT_URL varchar(255) NULL,
	STATUS varchar(10) NOT NULL,
	REPORT_SECURITY varchar(25) NOT NULL,
	OWNERS varchar(max) NULL,
	BORDER_COLOR varchar(20) NOT NULL,
	BORDER_WIDTH int NOT NULL,
	BACKGROUND_COLOR varchar(20) NOT NULL,
	TAKERS varchar(max) NULL,
	ADMIN_EMAIL_ADDRESSES varchar(max) NULL,
	MAXIMUM_RESPONSES_PER_USER int NULL,
	MODIFY_SURVEY_WITHIN_DAYS int NULL,
	RESPONDENT_ACCESS_LEVEL int NOT NULL,
	NUMBER_PAGES_YN varchar(1) NOT NULL,
	QUESTION_NUMBERING_FORMAT varchar(40) NOT NULL,
 CONSTRAINT PK_SUR_SURVEY PRIMARY KEY CLUSTERED 
(
	SURVEY_ID ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table SUR_Survey_To_Item_Mapping    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE SUR_Survey_To_Item_Mapping(
	SURVEY_ID int NOT NULL,
	ITEM_ID int NOT NULL,
	ORDER_NUMBER int NOT NULL,
	PAGE_NUMBER int NULL
);
/****** Object:  Table System    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE System(
	System char(5) NOT NULL,
	SystemName varchar(150) NOT NULL,
	Type varchar(50) NOT NULL,
	TypeDesc varchar(50) NOT NULL,
	Comments varchar(4000) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(50) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(50) NULL,
	RowVersionAction varchar(50) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_tblSystem PRIMARY KEY CLUSTERED 
(
	System ASC
)
);
/****** Object:  Table Task    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Task(
	TaskPK int NOT NULL AUTO_INCREMENT,
	TaskAction varchar(7000) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	TaskGroup varchar(25) NULL,
	TaskGroupDesc varchar(50) NULL,
	DisplayOrder smallint NULL,
	AvailableToRequester bit NOT NULL,
	TrackTask bit NOT NULL,
	Active bit NOT NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	UseLookupTable bit NULL,
	LookupTable varchar(25) NULL,
	UseLookupTable2 bit NULL,
	LookupTable2 varchar(25) NULL,
 CONSTRAINT PK_Task PRIMARY KEY CLUSTERED 
(
	TaskPK ASC
)
);
/****** Object:  Table tblReservations    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE tblReservations(
	RID int NOT NULL AUTO_INCREMENT,
	Resource varchar(255) NULL,
	Start smalldatetime NULL,
	End smalldatetime NULL,
	Person varchar(255) NULL,
	Cost DECIMAL(13,2) NULL,
	CCode varchar(255) NULL,
	Description varchar(max) NULL,
	Capacity int NULL,
	Count int NULL,
	Details varchar(max) NULL,
	Color varchar(50) NULL,
	Password varchar(50) NULL,
	SubscriberPassword varchar(50) NULL,
	CloneID int NULL,
	RepeatType int NULL,
	RepeatDate varchar(50) NULL,
	RepeatCount int NULL,
	Planner varchar(255) NULL,
	LastUpdate datetime NULL,
 CONSTRAINT PK_tblReservations PRIMARY KEY CLUSTERED 
(
	RID ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table tblResources    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE tblResources(
	RID int NOT NULL AUTO_INCREMENT,
	Resource nvarchar(255) NULL,
	Category nvarchar(255) NULL,
	Description nvarchar(max) NULL,
	Cost DECIMAL(13,2) NULL,
	Password nvarchar(50) NULL,
	Color nvarchar(50) NULL,
	Capacity int NULL,
	Icon nvarchar(255) NULL,
	Planner nvarchar(255) NULL,
 CONSTRAINT PK_tblResources PRIMARY KEY CLUSTERED 
(
	RID ASC
)
) ON PRIMARY TEXTIMAGE;
/****** Object:  Table tblSetup    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE tblSetup(
	PID int NOT NULL AUTO_INCREMENT,
	Planner nvarchar(255) NULL,
	GeneralPassword nvarchar(50) NULL,
	WritePassword nvarchar(50) NULL,
	AdminPassword nvarchar(50) NULL,
	DailyStart varchar(50) NULL,
	DailyEnd varchar(50) NULL,
	MinuteIncrements int NULL,
	MinColWidth int NULL,
	MaxPickerResources int NULL,
	ShowBlockedTimeInPicker varchar(50) NULL,
	PlannerColor varchar(50) NULL,
	BlockOutColor varchar(50) NULL,
	AutoRefresh int NULL,
	PrivacySettingsWriter int NULL,
	PrivacySettingsReader int NULL,
 CONSTRAINT PK_tblSetup PRIMARY KEY CLUSTERED 
(
	PID ASC
)
);
/****** Object:  Table Tenant    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Tenant(
	TenantPK int NOT NULL AUTO_INCREMENT,
	TenantID varchar(25) NOT NULL,
	TenantName varchar(100) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	Active bit NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	URL varchar(200) NULL,
	CustomerNum varchar(50) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	Attention varchar(50) NULL,
	Address varchar(50) NULL,
	Address2 varchar(50) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(50) NULL,
	Phone varchar(20) NULL,
	Fax varchar(20) NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	ChargePercentage decimal(19, 6) NULL,
	ChargeRate DECIMAL(13,2) NULL,
	ChargePercentageParts decimal(19, 6) NULL,
	ChargePercentageOtherCosts decimal(19, 6) NULL,
	Budget DECIMAL(13,2) NULL,
	PMCycleStartDate datetime NULL,
	Currency varchar(25) NULL,
 CONSTRAINT PK_Tenant PRIMARY KEY NONCLUSTERED 
(
	TenantPK ASC
),
 CONSTRAINT IX_Tenant UNIQUE CLUSTERED 
(
	TenantID ASC
)
);
/****** Object:  Table TenantDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE TenantDocument(
	PK int NOT NULL AUTO_INCREMENT,
	TenantPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_TenantDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table TenantLabor    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE TenantLabor(
	PK int NOT NULL AUTO_INCREMENT,
	TenantPK int NOT NULL,
	LaborPK int NOT NULL,
	PrimaryContact bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_TenantLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table TenantNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE TenantNote(
	PK int NOT NULL AUTO_INCREMENT,
	TenantPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_TenantNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Tool    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Tool(
	ToolPK int NOT NULL AUTO_INCREMENT,
	ToolID varchar(25) NOT NULL,
	ToolName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	Active bit NOT NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	ReferenceStandard varchar(25) NULL,
	ReferenceStandardDesc varchar(50) NULL,
	IsMaintainable bit NULL,
 CONSTRAINT PK_Tool PRIMARY KEY NONCLUSTERED 
(
	ToolPK ASC
),
 CONSTRAINT IX_Tool UNIQUE CLUSTERED 
(
	ToolID ASC
)
);
/****** Object:  Table ToolDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ToolDocument(
	PK int NOT NULL AUTO_INCREMENT,
	ToolPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_ToolDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ToolLocation    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ToolLocation(
	PK int NOT NULL AUTO_INCREMENT,
	ToolPK int NOT NULL,
	LocationPK int NOT NULL,
	PhysicalLast datetime NULL,
	Available decimal(19, 6) NOT NULL,
	Reserved decimal(19, 6) NOT NULL,
	OnHand decimal(19, 6) NOT NULL,
	Comments varchar(4000) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ToolLocation PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table ToolNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE ToolNote(
	PK int NOT NULL AUTO_INCREMENT,
	ToolPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_ToolNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Training    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Training(
	TrainingPK int NOT NULL AUTO_INCREMENT,
	TrainingID varchar(25) NOT NULL,
	TrainingName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	RequiredBy varchar(50) NULL,
	Active bit NOT NULL,
	Comments varchar(2000) NULL,
	Photo varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_Training PRIMARY KEY NONCLUSTERED 
(
	TrainingPK ASC
),
 CONSTRAINT IX_Training UNIQUE CLUSTERED 
(
	TrainingID ASC
)
);
/****** Object:  Table TrainingDocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE TrainingDocument(
	PK int NOT NULL AUTO_INCREMENT,
	TrainingPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoTrainingPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_TrainingDocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table TrainingNote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE TrainingNote(
	PK int NOT NULL AUTO_INCREMENT,
	TrainingPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_TrainingNote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Translation    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Translation(
	TranslationPK int NOT NULL AUTO_INCREMENT,
	English varchar(4000) NOT NULL,
	LanguageCode varchar(10) NOT NULL,
	TranslationOut nvarchar(4000) NOT NULL,
 CONSTRAINT PK_Translation PRIMARY KEY CLUSTERED 
(
	TranslationPK ASC
)
);
/****** Object:  Table WO    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WO(
	WOPK int NOT NULL AUTO_INCREMENT,
	WOID varchar(50) NULL,
	WOGroupType char(1) NULL,
	WOGroupPK int NULL,
	FollowupFromWOPK int NULL,
	RouteOrder smallint NOT NULL,
	Reason varchar(2000) NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	SubStatus varchar(50) NULL,
	AuthStatus varchar(15) NOT NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NOT NULL,
	StatusDate datetime NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	StockRoomPK int NULL,
	StockRoomID varchar(25) NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetDate datetime NOT NULL,
	TargetHours decimal(19, 6) NULL,
	ActualHours decimal(19, 6) NULL,
	ResponseHours decimal(19, 6) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NOT NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	WarrantyBox bit NOT NULL,
	ShutdownBox bit NOT NULL,
	LockoutTagoutBox bit NOT NULL,
	AttachmentsBox bit NOT NULL,
	SurveyBox bit NOT NULL,
	Survey_ID int NULL,
	PrintedBox bit NOT NULL,
	Instructions varchar(2000) NULL,
	LaborReport varchar(6000) NULL,
	Chargeable bit NOT NULL,
	FollowupWork bit NOT NULL,
	ProblemPK int NULL,
	ProblemID varchar(25) NULL,
	ProblemName varchar(50) NULL,
	FailurePK int NULL,
	FailureID varchar(25) NULL,
	FailureName varchar(50) NULL,
	SolutionPK int NULL,
	SolutionID varchar(25) NULL,
	SolutionName varchar(50) NULL,
	Component varchar(25) NULL,
	ComponentDesc varchar(50) NULL,
	FaultLocation varchar(25) NULL,
	FaultLocationDesc varchar(50) NULL,
	FailedWO bit NOT NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	CompletePercent decimal(19, 6) NULL,
	Complete datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	CostLaborActual DECIMAL(13,2) NOT NULL,
	CostEmployeeActual DECIMAL(13,2) NOT NULL,
	CostContractorActual DECIMAL(13,2) NOT NULL,
	CostPartActual DECIMAL(13,2) NOT NULL,
	CostMiscActual DECIMAL(13,2) NOT NULL,
	CostTotalActual DECIMAL(13,2) NOT NULL,
	CostLaborEstimated DECIMAL(13,2) NOT NULL,
	CostEmployeeEstimated DECIMAL(13,2) NOT NULL,
	CostContractorEstimated DECIMAL(13,2) NOT NULL,
	CostPartEstimated DECIMAL(13,2) NOT NULL,
	CostMiscEstimated DECIMAL(13,2) NOT NULL,
	CostTotalEstimated DECIMAL(13,2) NOT NULL,
	ChargeLaborActual DECIMAL(13,2) NOT NULL,
	ChargeEmployeeActual DECIMAL(13,2) NOT NULL,
	ChargeContractorActual DECIMAL(13,2) NOT NULL,
	ChargePartActual DECIMAL(13,2) NOT NULL,
	ChargeMiscActual DECIMAL(13,2) NOT NULL,
	ChargeTotalActual DECIMAL(13,2) NOT NULL,
	ChargeLaborEstimated DECIMAL(13,2) NOT NULL,
	ChargeEmployeeEstimated DECIMAL(13,2) NOT NULL,
	ChargeContractorEstimated DECIMAL(13,2) NOT NULL,
	ChargePartEstimated DECIMAL(13,2) NOT NULL,
	ChargeMiscEstimated DECIMAL(13,2) NOT NULL,
	ChargeTotalEstimated DECIMAL(13,2) NOT NULL,
	AssetStatusHistoryPK int NULL,
	IsOpen bit NOT NULL,
	IsApproved bit NOT NULL,
	IsAssigned bit NOT NULL,
	IsGenerated bit NOT NULL,
	IsPredictive bit NOT NULL,
	IsPartsReserved bit NOT NULL,
	Meter1Reading decimal(19, 6) NULL,
	Meter2Reading decimal(19, 6) NULL,
	PDAID varchar(25) NULL,
	PDAWO bit NOT NULL,
	PMPK int NULL,
	PMID varchar(25) NULL,
	PMName varchar(100) NULL,
	PMUndoPK int NULL,
	PredictivePK int NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	IsAssignmentsCompleted bit NULL,
	Responded datetime NULL,
	Finalized datetime NULL,
	IssuedInitials varchar(5) NULL,
	RespondedInitials varchar(5) NULL,
	CompletedInitials varchar(5) NULL,
	FinalizedInitials varchar(5) NULL,
	ClosedInitials varchar(5) NULL,
	TakenByInitials varchar(5) NULL,
	TakenByPK int NULL,
	TakenByID varchar(25) NULL,
	TakenByName varchar(50) NULL,
	ArchiveBit1 bit NULL,
	ArchiveBit2 bit NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	SubStatusDesc varchar(50) NULL,
	Currency varchar(25) NULL,
	CurrencySymbol varchar(10) NULL,
	FormName varchar(50) NULL,
	ContractPK int NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
	TargetDays decimal(19, 6) NULL,
 CONSTRAINT PK_WO_WOPK PRIMARY KEY CLUSTERED 
(
	WOPK ASC
),
 CONSTRAINT IX_WO_WOID UNIQUE NONCLUSTERED 
(
	WOID ASC
)
);
/****** Object:  Table WOAccount    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOAccount(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	AccountPK int NOT NULL,
	AccountID varchar(25) NOT NULL,
	AccountName varchar(50) NULL,
	ChargeMemo varchar(1000) NULL,
	ChargePercentage decimal(19, 6) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOAccount PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOAssign    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOAssign(
	PK int NOT NULL AUTO_INCREMENT,
	IsAssigned bit NOT NULL,
	WOPK int NOT NULL,
	WOlaborPK int NULL,
	LaborPK int NULL,
	AssignedHours decimal(19, 6) NOT NULL,
	AssignedDate datetime NULL,
	CompletePercent decimal(19, 6) NULL,
	CompletedDate datetime NULL,
	AssignedLead bit NOT NULL,
	AssignedPDA bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	Active bit NULL,
	LaborID varchar(25) NULL,
	LaborNAme varchar(50) NULL,
	Anytime bit NULL,
	LaborAcceptance varchar(50) NULL,
	LaborAcceptanceDesc varchar(100) NULL,
	Comments varchar(2000) NULL,
 CONSTRAINT PK_WOAssign_PK PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOAssignStatus    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOAssignStatus(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	LaborPK int NOT NULL,
	IsAssigned bit NOT NULL,
	Completed bit NOT NULL,
	AssignedDate datetime NULL,
	CompletedDate datetime NULL,
	RowVersionInitials varchar(5) NULL,
	CompletePercent decimal(19, 6) NULL,
CONSTRAINT PK_WOAssignStatus PRIMARY KEY NONCLUSTERED 
(
	PK ASC
),
 CONSTRAINT IX_WOAssignStatus UNIQUE CLUSTERED 
(
	WOPK ASC,
	LaborPK ASC
)
);
/****** Object:  Table WOAttachment    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOAttachment(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	Filename varchar(500) NULL,
	Active bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOAttachment PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WODependent    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WODependent(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	WOPKDependent int NOT NULL,
	DependencyRestrictionEvent varchar(25) NULL,
	DependencyAutoIssueEvent varchar(25) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WODependent PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WODependentTemp    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WODependentTemp(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	WOPKDependent int NOT NULL,
	DependencyRestrictionEvent varchar(25) NULL,
	DependencyAutoIssueEvent varchar(25) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NULL,
	PMUndoPK int NOT NULL,
 CONSTRAINT PK_WODependentTemp_1 PRIMARY KEY CLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WODocument    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WODocument(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	DocumentPK int NOT NULL,
	ModuleID char(2) NOT NULL,
	PrintWithWO bit NOT NULL,
	SendWithEmail bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	DisplayLink bit NULL,
 CONSTRAINT PK_WODocument PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOGroup    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOGroup(
	WOGroupPK int NOT NULL AUTO_INCREMENT,
	WOGroupName varchar(100) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOGroup PRIMARY KEY NONCLUSTERED 
(
	WOGroupPK ASC
)
);
/****** Object:  Table WOGroupProjection    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOGroupProjection(
	WOGroupPK int NOT NULL AUTO_INCREMENT,
	WOGroupName varchar(100) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOGroupProjection PRIMARY KEY NONCLUSTERED 
(
	WOGroupPK ASC
)
);
/****** Object:  Table WOLabor    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOLabor(
	PK int NOT NULL AUTO_INCREMENT,
	RecordType smallint NOT NULL,
	WOPK int NOT NULL,
	LaborPK int NOT NULL,
	LaborID varchar(25) NOT NULL,
	LaborName varchar(50) NULL,
	LaborType varchar(25) NULL,
	LaborTypeDesc varchar(50) NULL,
	EstimatedHours decimal(19, 6) NOT NULL,
	RegularHours decimal(19, 6) NOT NULL,
	OvertimeHours decimal(19, 6) NOT NULL,
	OtherHours decimal(19, 6) NOT NULL,
	TotalHours decimal(19, 6) NOT NULL,
	WorkDate datetime NULL,
	TimeIn datetime NULL,
	TimeOut datetime NULL,
	RegularCost DECIMAL(13,2) NOT NULL,
	OvertimeCost DECIMAL(13,2) NOT NULL,
	OtherCost DECIMAL(13,2) NOT NULL,
	TotalCost DECIMAL(13,2) NOT NULL,
	CostRegular DECIMAL(13,2) NOT NULL,
	CostOvertime DECIMAL(13,2) NOT NULL,
	CostOther DECIMAL(13,2) NOT NULL,
	ChargeRate DECIMAL(13,2) NOT NULL,
	ChargePercentage decimal(19, 6) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	EstimatePK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	Comments varchar(7000) NULL,
	AutoCalcCost bit NULL,
	AutoCalcCharge bit NULL,
 CONSTRAINT PK_WOLabor PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOMetrics    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOMetrics(
	MetricPK int NOT NULL AUTO_INCREMENT,
	MetricDate datetime NOT NULL,
	EntityDB varchar(50) NULL,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NOT NULL,
	IsGenerated bit NOT NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(100) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(100) NULL,
	Location1PK int NULL,
	Location1ID varchar(100) NULL,
	Location1Name varchar(150) NULL,
	Location1Class varchar(150) NULL,
	Location2PK int NULL,
	Location2ID varchar(100) NULL,
	Location2Name varchar(150) NULL,
	Location2Class varchar(150) NULL,
	Location3PK int NULL,
	Location3ID varchar(100) NULL,
	Location3Name varchar(150) NULL,
	Location3Class varchar(150) NULL,
	Location4PK int NULL,
	Location4ID varchar(100) NULL,
	Location4Name varchar(150) NULL,
	Location4Class varchar(150) NULL,
	Location5PK int NULL,
	Location5ID varchar(100) NULL,
	Location5Name varchar(150) NULL,
	Location5Class varchar(150) NULL,
	Location6PK int NULL,
	Location6ID varchar(100) NULL,
	Location6Name varchar(150) NULL,
	Location6Class varchar(150) NULL,
	Location7PK int NULL,
	Location7ID varchar(100) NULL,
	Location7Name varchar(150) NULL,
	Location7Class varchar(150) NULL,
	Location8PK int NULL,
	Location8ID varchar(100) NULL,
	Location8Name varchar(150) NULL,
	Location8Class varchar(150) NULL,
	ASClassificationPK int NULL,
	ASClassificationID varchar(100) NULL,
	ASClassificationName varchar(150) NULL,
	ASClassIndustry varchar(25) NULL,
	ASClassIndustryDesc varchar(50) NULL,
	TargetMonth char(2) NOT NULL,
	TargetYear char(4) NOT NULL,
	MetricMonth datetime NOT NULL,
	Total_Targeted int NOT NULL,
	Total_Completed int NOT NULL,
	Total_Issued int NOT NULL,
	Total_OnTime int NOT NULL,
	Total_IssueDays int NOT NULL,
	Total_CompleteDays int NOT NULL,
	Total_IssueHours int NOT NULL,
	Total_CompleteHours int NOT NULL
);
/****** Object:  Table WOMiscCost    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOMiscCost(
	PK int NOT NULL AUTO_INCREMENT,
	RecordType smallint NOT NULL,
	WOPK int NOT NULL,
	MiscCostPK int NULL,
	MiscCostID varchar(25) NULL,
	MiscCostName varchar(50) NOT NULL,
	MiscCostDesc varchar(1000) NULL,
	InvoiceNumber varchar(50) NULL,
	MiscCostDate datetime NOT NULL,
	QuantityEstimated decimal(19, 6) NOT NULL,
	QuantityActual decimal(19, 6) NOT NULL,
	EstimatedCost DECIMAL(13,2) NOT NULL,
	ActualCost DECIMAL(13,2) NOT NULL,
	ChargePercentage decimal(19, 6) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	EstimatePK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	CompanyPK int NULL,
	CompanyID varchar(25) NULL,
	CompanyName varchar(100) NULL,
	LaborPK int NULL,
	LaborID varchar(25) NULL,
	LaborName varchar(50) NULL,
	Comments varchar(7000) NULL,
	Other1 varchar(500) NULL,
	Other2 varchar(500) NULL,
	AutoCalcCost bit NULL,
	AutoCalcCharge bit NULL,
 CONSTRAINT PK_WOMiscCost PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WONote    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WONote(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	LaborPK int NULL,
	Name varchar(50) NULL,
	Initials varchar(5) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Reason varchar(25) NULL,
	ReasonDesc varchar(50) NULL,
	NoteDate datetime NOT NULL,
	Note varchar(4000) NULL,
	Custom1 bit NOT NULL,
	Custom2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WONote PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOPart    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOPart(
	PK int NOT NULL AUTO_INCREMENT,
	RecordType smallint NOT NULL,
	WOPK int NOT NULL,
	PartPK int NOT NULL,
	PartID varchar(50) NOT NULL,
	PartName varchar(500) NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	DirectIssue bit NOT NULL,
	OutOfPocket bit NOT NULL,
	POPK int NULL,
	PODetailPK int NULL,
	QuantityEstimated decimal(19, 6) NOT NULL,
	QuantityActual decimal(19, 6) NOT NULL,
	IssueUnitCost DECIMAL(13,2) NOT NULL,
	IssueUnitChargePrice DECIMAL(13,2) NOT NULL,
	IssueUnitChargePercentage decimal(19, 6) NOT NULL,
	OtherCost DECIMAL(13,2) NOT NULL,
	TotalCost DECIMAL(13,2) NOT NULL,
	TotalCharge DECIMAL(13,2) NOT NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	EstimatePK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	POReceivedPK int NULL,
	QuantityLinked int NULL,
	Comments varchar(7000) NULL,
	AutoCalcCost bit NULL,
	AutoCalcCharge bit NULL,
	QuantityEstimatedUsed decimal(19, 6) NULL,
	Serial varchar(50) NULL,
	SerialReplaceExisting char(1) NULL,
	SerialReplaceToLocationID varchar(100) NULL,
	SerialReplaced varchar(50) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
	SerialReplacedOutOfService bit NULL,
	PartDate datetime NULL,
	Bin varchar(100) NULL,
	LaborToPK int NULL,
 CONSTRAINT PK_WOPart PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOPhoto    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOPhoto(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	PhotoDate datetime NOT NULL,
	Photo varchar(200) NULL,
	Active bit NOT NULL,
	Followup bit NOT NULL,
	ReportOrder smallint NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOPhoto PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOPreReq    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOPreReq(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	WOPKPreReq int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOPreReq PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOProjection    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOProjection(
	WOPK int NOT NULL AUTO_INCREMENT,
	WOID varchar(50) NULL,
	WOGroupType char(1) NULL,
	WOGroupPK int NULL,
	FollowupFromWOPK int NULL,
	RouteOrder smallint NOT NULL,
	Reason varchar(1000) NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	SubStatus varchar(50) NULL,
	AuthStatus varchar(15) NOT NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NOT NULL,
	StatusDate datetime NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	StockRoomPK int NULL,
	StockRoomID varchar(25) NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetDate datetime NOT NULL,
	TargetHours decimal(19, 6) NULL,
	ActualHours decimal(19, 6) NULL,
	ResponseHours decimal(19, 6) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NOT NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NOT NULL,
	RepairCenterID varchar(25) NOT NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	WarrantyBox bit NOT NULL,
	ShutdownBox bit NOT NULL,
	LockoutTagoutBox bit NOT NULL,
	AttachmentsBox bit NOT NULL,
	SurveyBox bit NOT NULL,
	Survey_ID int NULL,
	PrintedBox bit NOT NULL,
	Instructions varchar(2000) NULL,
	LaborReport varchar(3000) NULL,
	Chargeable bit NOT NULL,
	FollowupWork bit NOT NULL,
	ProblemPK int NULL,
	ProblemID varchar(25) NULL,
	ProblemName varchar(50) NULL,
	FailurePK int NULL,
	FailureID varchar(25) NULL,
	FailureName varchar(50) NULL,
	SolutionPK int NULL,
	SolutionID varchar(25) NULL,
	SolutionName varchar(50) NULL,
	FailedWO bit NOT NULL,
	TakenByPK int NULL,
	TakenByInitials varchar(25) NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	Complete datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	CostLaborActual DECIMAL(13,2) NOT NULL,
	CostEmployeeActual DECIMAL(13,2) NOT NULL,
	CostContractorActual DECIMAL(13,2) NOT NULL,
	CostPartActual DECIMAL(13,2) NOT NULL,
	CostMiscActual DECIMAL(13,2) NOT NULL,
	CostTotalActual DECIMAL(13,2) NOT NULL,
	CostLaborEstimated DECIMAL(13,2) NOT NULL,
	CostEmployeeEstimated DECIMAL(13,2) NOT NULL,
	CostContractorEstimated DECIMAL(13,2) NOT NULL,
	CostPartEstimated DECIMAL(13,2) NOT NULL,
	CostMiscEstimated DECIMAL(13,2) NOT NULL,
	CostTotalEstimated DECIMAL(13,2) NOT NULL,
	ChargeLaborActual DECIMAL(13,2) NOT NULL,
	ChargeEmployeeActual DECIMAL(13,2) NOT NULL,
	ChargeContractorActual DECIMAL(13,2) NOT NULL,
	ChargePartActual DECIMAL(13,2) NOT NULL,
	ChargeMiscActual DECIMAL(13,2) NOT NULL,
	ChargeTotalActual DECIMAL(13,2) NOT NULL,
	ChargeLaborEstimated DECIMAL(13,2) NOT NULL,
	ChargeEmployeeEstimated DECIMAL(13,2) NOT NULL,
	ChargeContractorEstimated DECIMAL(13,2) NOT NULL,
	ChargePartEstimated DECIMAL(13,2) NOT NULL,
	ChargeMiscEstimated DECIMAL(13,2) NOT NULL,
	ChargeTotalEstimated DECIMAL(13,2) NOT NULL,
	AssetStatusHistoryPK int NULL,
	IsOpen bit NOT NULL,
	IsApproved bit NOT NULL,
	IsAssigned bit NOT NULL,
	IsGenerated bit NOT NULL,
	IsPartsReserved bit NOT NULL,
	Meter1Reading decimal(19, 6) NULL,
	Meter2Reading decimal(19, 6) NULL,
	PDAID varchar(25) NULL,
	PDAWO bit NOT NULL,
	PMPK int NULL,
	PMID varchar(25) NULL,
	PMName varchar(100) NULL,
	PMUndoPK int NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	IsAssignmentsCompleted bit NULL,
	Responded datetime NULL,
	Finalized datetime NULL,
	IssuedInitials varchar(5) NULL,
	RespondedInitials varchar(5) NULL,
	CompletedInitials varchar(5) NULL,
	FinalizedInitials varchar(5) NULL,
	ClosedInitials varchar(5) NULL,
	TakenByInitials1 varchar(5) NULL,
	TakenByPK1 int NULL,
	TakenByID varchar(25) NULL,
	TakenByName varchar(50) NULL,
	ArchiveBit1 bit NULL,
	ArchiveBit2 bit NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	SubStatusDesc varchar(50) NULL,
	Currency varchar(25) NULL,
	CurrencySymbol varchar(10) NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
 CONSTRAINT PK_WOProjection PRIMARY KEY NONCLUSTERED 
(
	WOPK ASC
)
);
/****** Object:  Table WORapidEntry    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WORapidEntry(
	WOPK int NOT NULL AUTO_INCREMENT,
	WOID varchar(50) NULL,
	WOGroupType char(1) NULL,
	WOGroupPK int NULL,
	FollowupFromWOPK int NULL,
	RouteOrder smallint NOT NULL,
	Reason varchar(2000) NULL,
	Status varchar(15) NULL,
	StatusDesc varchar(50) NULL,
	SubStatus varchar(50) NULL,
	AuthStatus varchar(15) NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NOT NULL,
	StatusDate datetime NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	StockRoomPK int NULL,
	StockRoomID varchar(25) NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetDate datetime NOT NULL,
	TargetHours decimal(19, 6) NULL,
	ActualHours decimal(19, 6) NULL,
	ResponseHours decimal(19, 6) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NOT NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	WarrantyBox bit NOT NULL,
	ShutdownBox bit NOT NULL,
	LockoutTagoutBox bit NOT NULL,
	AttachmentsBox bit NOT NULL,
	SurveyBox bit NOT NULL,
	Survey_ID int NULL,
	PrintedBox bit NOT NULL,
	Instructions varchar(2000) NULL,
	LaborReport varchar(6000) NULL,
	Chargeable bit NOT NULL,
	FollowupWork bit NOT NULL,
	ProblemPK int NULL,
	ProblemID varchar(25) NULL,
	ProblemName varchar(50) NULL,
	FailurePK int NULL,
	FailureID varchar(25) NULL,
	FailureName varchar(50) NULL,
	SolutionPK int NULL,
	SolutionID varchar(25) NULL,
	SolutionName varchar(50) NULL,
	Component varchar(25) NULL,
	ComponentDesc varchar(50) NULL,
	FaultLocation varchar(25) NULL,
	FaultLocationDesc varchar(50) NULL,
	FailedWO bit NOT NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	CompletePercent decimal(19, 6) NULL,
	Complete datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	CostLaborActual DECIMAL(13,2) NOT NULL,
	CostEmployeeActual DECIMAL(13,2) NOT NULL,
	CostContractorActual DECIMAL(13,2) NOT NULL,
	CostPartActual DECIMAL(13,2) NOT NULL,
	CostMiscActual DECIMAL(13,2) NOT NULL,
	CostTotalActual DECIMAL(13,2) NOT NULL,
	CostLaborEstimated DECIMAL(13,2) NOT NULL,
	CostEmployeeEstimated DECIMAL(13,2) NOT NULL,
	CostContractorEstimated DECIMAL(13,2) NOT NULL,
	CostPartEstimated DECIMAL(13,2) NOT NULL,
	CostMiscEstimated DECIMAL(13,2) NOT NULL,
	CostTotalEstimated DECIMAL(13,2) NOT NULL,
	ChargeLaborActual DECIMAL(13,2) NOT NULL,
	ChargeEmployeeActual DECIMAL(13,2) NOT NULL,
	ChargeContractorActual DECIMAL(13,2) NOT NULL,
	ChargePartActual DECIMAL(13,2) NOT NULL,
	ChargeMiscActual DECIMAL(13,2) NOT NULL,
	ChargeTotalActual DECIMAL(13,2) NOT NULL,
	ChargeLaborEstimated DECIMAL(13,2) NOT NULL,
	ChargeEmployeeEstimated DECIMAL(13,2) NOT NULL,
	ChargeContractorEstimated DECIMAL(13,2) NOT NULL,
	ChargePartEstimated DECIMAL(13,2) NOT NULL,
	ChargeMiscEstimated DECIMAL(13,2) NOT NULL,
	ChargeTotalEstimated DECIMAL(13,2) NOT NULL,
	AssetStatusHistoryPK int NULL,
	IsOpen bit NOT NULL,
	IsApproved bit NOT NULL,
	IsAssigned bit NOT NULL,
	IsGenerated bit NOT NULL,
	IsPredictive bit NOT NULL,
	IsPartsReserved bit NOT NULL,
	Meter1Reading decimal(19, 6) NULL,
	Meter2Reading decimal(19, 6) NULL,
	PDAID varchar(25) NULL,
	PDAWO bit NOT NULL,
	PMPK int NULL,
	PMID varchar(25) NULL,
	PMName varchar(100) NULL,
	PMUndoPK int NULL,
	PredictivePK int NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	IsAssignmentsCompleted bit NULL,
	Responded datetime NULL,
	Finalized datetime NULL,
	IssuedInitials varchar(5) NULL,
	RespondedInitials varchar(5) NULL,
	CompletedInitials varchar(5) NULL,
	FinalizedInitials varchar(5) NULL,
	ClosedInitials varchar(5) NULL,
	TakenByInitials varchar(5) NULL,
	TakenByPK int NULL,
	TakenByID varchar(25) NULL,
	TakenByName varchar(50) NULL,
	ArchiveBit1 bit NULL,
	ArchiveBit2 bit NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	LaborID1 varchar(25) NULL,
	LaborID2 varchar(25) NULL,
	LaborID3 varchar(25) NULL,
	LaborHours1 decimal(19, 6) NULL,
	LaborHours2 decimal(19, 6) NULL,
	LaborHours3 decimal(19, 6) NULL,
	RapidEntryStatus char(1) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	SubStatusDesc varchar(50) NULL,
	PartID1 varchar(50) NULL,
	PartID2 varchar(50) NULL,
	PartID3 varchar(50) NULL,
	PartID4 varchar(50) NULL,
	PartID5 varchar(50) NULL,
	PartID6 varchar(50) NULL,
	PartID7 varchar(50) NULL,
	PartID8 varchar(50) NULL,
	PartID9 varchar(50) NULL,
	PartID10 varchar(50) NULL,
	PartLocationID1 varchar(25) NULL,
	PartLocationID2 varchar(25) NULL,
	PartLocationID3 varchar(25) NULL,
	PartLocationID4 varchar(25) NULL,
	PartLocationID5 varchar(25) NULL,
	PartLocationID6 varchar(25) NULL,
	PartLocationID7 varchar(25) NULL,
	PartLocationID8 varchar(25) NULL,
	PartLocationID9 varchar(25) NULL,
	PartLocationID10 varchar(25) NULL,
	PartQty1 decimal(19, 6) NULL,
	PartQty2 decimal(19, 6) NULL,
	PartQty3 decimal(19, 6) NULL,
	PartQty4 decimal(19, 6) NULL,
	PartQty5 decimal(19, 6) NULL,
	PartQty6 decimal(19, 6) NULL,
	PartQty7 decimal(19, 6) NULL,
	PartQty8 decimal(19, 6) NULL,
	PartQty9 decimal(19, 6) NULL,
	PartQty10 decimal(19, 6) NULL,
	PartBin1 varchar(100) NULL,
	PartBin2 varchar(100) NULL,
	PartBin3 varchar(100) NULL,
	PartBin4 varchar(100) NULL,
	PartBin5 varchar(100) NULL,
	PartBin6 varchar(100) NULL,
	PartBin7 varchar(100) NULL,
	PartBin8 varchar(100) NULL,
	PartBin9 varchar(100) NULL,
	PartBin10 varchar(100) NULL,
 CONSTRAINT PK_WORapidEntry PRIMARY KEY CLUSTERED 
(
	WOPK ASC
)
);
/****** Object:  Table WORequester    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WORequester(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	LaborPK int NULL,
	IsPrimary bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WORequester PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOStartStop    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOStartStop(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	LaborPK int NULL,
	WOLaborPK int NULL,
	Started datetime NULL,
	Stopped datetime NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOStartStop PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOStatusHistory    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOStatusHistory(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	IsAuthStatus bit NOT NULL,
	StatusDate datetime NOT NULL,
	Status varchar(15) NOT NULL,
	StatusDesc varchar(50) NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	ApprovalCountCompleted bit NULL,
 CONSTRAINT PK_WOStatusHistory PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOSurveyTrack    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOSurveyTrack(
	RepairCenterPK int NULL
);
/****** Object:  Table WOTask    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOTask(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	TaskNo decimal(19, 6) NULL,
	TaskAction varchar(7000) NULL,
	Complete bit NOT NULL,
	Fail bit NOT NULL,
	Rate int NULL,
	Measurement varchar(50) NULL,
	Initials varchar(5) NULL,
	HoursEstimated decimal(19, 6) NULL,
	HoursActual decimal(19, 6) NULL,
	CraftPK int NULL,
	Spec bit NOT NULL,
	Header bit NOT NULL,
	LineStyle varchar(25) NULL,
	LineStyleDesc varchar(50) NULL,
	AssetSpecificationPK bigint NULL,
	SpecificationPK int NULL,
	Meter1 bit NOT NULL,
	Meter2 bit NOT NULL,
	TaskPK int NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
	AssetPK int NULL,
	ClassificationPK int NULL,
	MeasurementInitial varchar(50) NULL,
	ToolPK int NULL,
	ClassificationCount smallint NULL,
	ClassificationCountOrig smallint NULL,
	ClassificationPKTaskPKMaster int NULL,
	Comments varchar(7000) NULL,
	InternalSort1 smallint NULL,
	InternalSort2 smallint NULL,
	FollowupWOPK int NULL,
	NotApplicable bit NOT NULL,
	Photo1 varchar(200) NULL,
	Photo2 varchar(200) NULL,
 CONSTRAINT PK_WOTask PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table WOTemp    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOTemp(
	WOPK int NOT NULL AUTO_INCREMENT,
	WOID varchar(50) NULL,
	WOGroupType char(1) NULL,
	WOGroupPK int NULL,
	FollowupFromWOPK int NULL,
	RouteOrder smallint NULL,
	Reason varchar(2000) NULL,
	Status varchar(15) NULL,
	StatusDesc varchar(50) NULL,
	SubStatus varchar(50) NULL,
	AuthStatus varchar(15) NULL,
	AuthStatusDesc varchar(50) NULL,
	AuthLevelsRequired smallint NULL,
	StatusDate datetime NULL,
	RequesterPK int NULL,
	RequesterID varchar(25) NULL,
	RequesterName varchar(50) NULL,
	RequesterPhone varchar(30) NULL,
	RequesterEmail varchar(50) NULL,
	AssetPK int NULL,
	AssetID varchar(100) NULL,
	AssetName varchar(150) NULL,
	AccountPK int NULL,
	AccountID varchar(25) NULL,
	AccountName varchar(50) NULL,
	ProcedurePK int NULL,
	ProcedureID varchar(25) NULL,
	ProcedureName varchar(100) NULL,
	StockRoomPK int NULL,
	StockRoomID varchar(25) NULL,
	StockRoomName varchar(50) NULL,
	ToolRoomPK int NULL,
	ToolRoomID varchar(25) NULL,
	ToolRoomName varchar(50) NULL,
	Reference varchar(25) NULL,
	ReferenceDesc varchar(50) NULL,
	TargetDate datetime NULL,
	TargetHours decimal(19, 6) NULL,
	ActualHours decimal(19, 6) NULL,
	ResponseHours decimal(19, 6) NULL,
	Type varchar(25) NULL,
	TypeDesc varchar(50) NULL,
	Priority varchar(25) NULL,
	PriorityDesc varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	ProjectPK int NULL,
	ProjectID varchar(25) NULL,
	ProjectName varchar(50) NULL,
	ShopPK int NULL,
	ShopID varchar(25) NULL,
	ShopName varchar(50) NULL,
	DepartmentPK int NULL,
	DepartmentID varchar(25) NULL,
	DepartmentName varchar(100) NULL,
	TenantPK int NULL,
	TenantID varchar(25) NULL,
	TenantName varchar(100) NULL,
	WarrantyBox bit NULL,
	ShutdownBox bit NULL,
	LockoutTagoutBox bit NULL,
	AttachmentsBox bit NULL,
	SurveyBox bit NULL,
	Survey_ID int NULL,
	PrintedBox bit NULL,
	Instructions varchar(2000) NULL,
	LaborReport varchar(6000) NULL,
	Chargeable bit NULL,
	FollowupWork bit NULL,
	ProblemPK int NULL,
	ProblemID varchar(25) NULL,
	ProblemName varchar(50) NULL,
	FailurePK int NULL,
	FailureID varchar(25) NULL,
	FailureName varchar(50) NULL,
	SolutionPK int NULL,
	SolutionID varchar(25) NULL,
	SolutionName varchar(50) NULL,
	Component varchar(25) NULL,
	ComponentDesc varchar(50) NULL,
	FaultLocation varchar(25) NULL,
	FaultLocationDesc varchar(50) NULL,
	FailedWO bit NULL,
	AuthStatusUserPK int NULL,
	AuthStatusUserInitials varchar(5) NULL,
	AuthStatusDate datetime NULL,
	Requested datetime NULL,
	Issued datetime NULL,
	OnHold datetime NULL,
	CompletePercent decimal(19, 6) NULL,
	Complete datetime NULL,
	Closed datetime NULL,
	Canceled datetime NULL,
	Denied datetime NULL,
	CostLaborActual DECIMAL(13,2) NULL,
	CostEmployeeActual DECIMAL(13,2) NULL,
	CostContractorActual DECIMAL(13,2) NULL,
	CostPartActual DECIMAL(13,2) NULL,
	CostMiscActual DECIMAL(13,2) NULL,
	CostTotalActual DECIMAL(13,2) NULL,
	CostLaborEstimated DECIMAL(13,2) NULL,
	CostEmployeeEstimated DECIMAL(13,2) NULL,
	CostContractorEstimated DECIMAL(13,2) NULL,
	CostPartEstimated DECIMAL(13,2) NULL,
	CostMiscEstimated DECIMAL(13,2) NULL,
	CostTotalEstimated DECIMAL(13,2) NULL,
	ChargeLaborActual DECIMAL(13,2) NULL,
	ChargeEmployeeActual DECIMAL(13,2) NULL,
	ChargeContractorActual DECIMAL(13,2) NULL,
	ChargePartActual DECIMAL(13,2) NULL,
	ChargeMiscActual DECIMAL(13,2) NULL,
	ChargeTotalActual DECIMAL(13,2) NULL,
	ChargeLaborEstimated DECIMAL(13,2) NULL,
	ChargeEmployeeEstimated DECIMAL(13,2) NULL,
	ChargeContractorEstimated DECIMAL(13,2) NULL,
	ChargePartEstimated DECIMAL(13,2) NULL,
	ChargeMiscEstimated DECIMAL(13,2) NULL,
	ChargeTotalEstimated DECIMAL(13,2) NULL,
	AssetStatusHistoryPK int NULL,
	IsOpen bit NULL,
	IsApproved bit NULL,
	IsAssigned bit NULL,
	IsGenerated bit NULL,
	IsPredictive bit NULL,
	IsPartsReserved bit NULL,
	Meter1Reading decimal(19, 6) NULL,
	Meter2Reading decimal(19, 6) NULL,
	PDAID varchar(25) NULL,
	PDAWO bit NULL,
	PMPK int NULL,
	PMID varchar(25) NULL,
	PMName varchar(100) NULL,
	PMUndoPK int NOT NULL,
	PredictivePK int NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFChar6 varchar(50) NULL,
	UDFChar7 varchar(50) NULL,
	UDFChar8 varchar(50) NULL,
	UDFChar9 varchar(50) NULL,
	UDFChar10 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NULL,
	UDFBit2 bit NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	IsAssignmentsCompleted bit NULL,
	Responded datetime NULL,
	Finalized datetime NULL,
	IssuedInitials varchar(5) NULL,
	RespondedInitials varchar(5) NULL,
	CompletedInitials varchar(5) NULL,
	FinalizedInitials varchar(5) NULL,
	ClosedInitials varchar(5) NULL,
	TakenByInitials varchar(5) NULL,
	TakenByPK int NULL,
	TakenByID varchar(25) NULL,
	TakenByName varchar(50) NULL,
	ArchiveBit1 bit NULL,
	ArchiveBit2 bit NULL,
	ZonePK int NULL,
	ZoneID varchar(25) NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	UDFDate3 datetime NULL,
	UDFDate4 datetime NULL,
	UDFDate5 datetime NULL,
	UDFBit3 bit NULL,
	UDFBit4 bit NULL,
	UDFBit5 bit NULL,
	SubStatusDesc varchar(50) NULL,
	Currency varchar(25) NULL,
	CurrencySymbol varchar(10) NULL,
	FormName varchar(50) NULL,
	ContractPK int NULL,
	ShiftPK int NULL,
	ShiftID varchar(25) NULL,
	ShiftName varchar(50) NULL,
 CONSTRAINT PK_WOTemp PRIMARY KEY CLUSTERED 
(
	WOPK ASC
)
);
/****** Object:  Table WOTool    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE WOTool(
	PK int NOT NULL AUTO_INCREMENT,
	WOPK int NOT NULL,
	RecordType smallint NOT NULL,
	ToolPK int NOT NULL,
	ToolID varchar(25) NOT NULL,
	ToolName varchar(50) NULL,
	LocationPK int NULL,
	LocationID varchar(25) NULL,
	LocationName varchar(50) NULL,
	QuantityEstimated int NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionDate datetime NOT NULL,
 CONSTRAINT PK_WOTool PRIMARY KEY NONCLUSTERED 
(
	PK ASC
)
);
/****** Object:  Table Zone    Script Date: 9/4/2019 1:29:08 PM ******/

SET SESSION sql_mode = 'ANSI_QUOTES';
CREATE TABLE Zone(
	ZonePK int NOT NULL AUTO_INCREMENT,
	ZoneID varchar(25) NOT NULL,
	ZoneName varchar(50) NULL,
	ZoneColor varchar(10) NULL,
	CategoryPK int NULL,
	CategoryID varchar(25) NULL,
	CategoryName varchar(50) NULL,
	RepairCenterPK int NULL,
	RepairCenterID varchar(25) NULL,
	RepairCenterName varchar(50) NULL,
	SupervisorPK int NULL,
	SupervisorID varchar(25) NULL,
	SupervisorName varchar(50) NULL,
	Budget DECIMAL(13,2) NULL,
	Address varchar(80) NULL,
	City varchar(50) NULL,
	State varchar(50) NULL,
	Zip varchar(15) NULL,
	Country varchar(50) NULL,
	Phone varchar(30) NULL,
	Pager varchar(50) NULL,
	PagerService varchar(50) NULL,
	PagerPIN varchar(50) NULL,
	PagerNumeric bit NOT NULL,
	Fax varchar(30) NULL,
	Email varchar(50) NULL,
	PagerEmail varchar(50) NULL,
	EmailNotify varchar(25) NULL,
	EmailNotifyDesc varchar(50) NULL,
	PrinterName varchar(50) NULL,
	URL varchar(200) NULL,
	Comments varchar(4000) NULL,
	Active bit NOT NULL,
	Photo varchar(200) NULL,
	Icon varchar(200) NULL,
	UDFChar1 varchar(50) NULL,
	UDFChar2 varchar(50) NULL,
	UDFChar3 varchar(50) NULL,
	UDFChar4 varchar(50) NULL,
	UDFChar5 varchar(50) NULL,
	UDFDate1 datetime NULL,
	UDFDate2 datetime NULL,
	UDFBit1 bit NOT NULL,
	UDFBit2 bit NOT NULL,
	DemoLaborPK int NULL,
	RowVersionIPAddress varchar(25) NULL,
	RowVersionUserPK int NULL,
	RowVersionInitials varchar(5) NULL,
	RowVersionAction varchar(25) NULL,
	RowVersionDate datetime NOT NULL,
	PMCycleStartDate datetime NULL,
	RiskLevel smallint NULL,
	PMCounter smallint NULL,
 CONSTRAINT PK_Zone PRIMARY KEY NONCLUSTERED 
(
	ZonePK ASC
),
 CONSTRAINT IX_Zone UNIQUE CLUSTERED 
(
	ZoneID ASC
)
);
ALTER TABLE AccessGroup ADD  CONSTRAINT DF_AccessGroup_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE AccessGroup ADD  CONSTRAINT DF_AccessGroup_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE AccessGroup ADD  CONSTRAINT DF_AccessGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupActions ADD  CONSTRAINT DF_AccessGroupActions_MinAmount  DEFAULT (0) FOR MinAmount;
ALTER TABLE AccessGroupActions ADD  CONSTRAINT DF_AccessGroupActions_MaxAmount  DEFAULT (0) FOR MaxAmount;
ALTER TABLE AccessGroupActions ADD  CONSTRAINT DF_AccessGroupActions_UseAmount  DEFAULT (0) FOR UseAmount;
ALTER TABLE AccessGroupActions ADD  CONSTRAINT DF_AccessGroupActions_Enabled  DEFAULT (0) FOR Enabled;
ALTER TABLE AccessGroupActions ADD  CONSTRAINT DF_AccessGroupActions_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupActions ADD  CONSTRAINT DF_AccessGroupActions_UseValue  DEFAULT ((0)) FOR UseValue;
ALTER TABLE AccessGroupAsset ADD  CONSTRAINT DF_AccessGroupAsset_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupAssetMgr ADD  CONSTRAINT DF_AccessGroupAssetMgr_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupDocument ADD  CONSTRAINT DF_AccessGroupDocument_PrintWithWO  DEFAULT ((0)) FOR PrintWithWO;
ALTER TABLE AccessGroupDocument ADD  CONSTRAINT DF_AccessGroupDocument_SendWithEmail  DEFAULT ((0)) FOR SendWithEmail;
ALTER TABLE AccessGroupDocument ADD  CONSTRAINT DF_AccessGroupDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupDocument ADD  CONSTRAINT DF_AccessGroupDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE AccessGroupForm ADD  CONSTRAINT DF_AccessGroupForm_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupRepairCenter ADD  CONSTRAINT DF_AccessGroupRepairCenter_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AccessGroupReportGroup ADD  CONSTRAINT DF_AccessGroupReportGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Account ADD  CONSTRAINT DF_Account_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE Account ADD  CONSTRAINT DF_Account_Active  DEFAULT (1) FOR Active;
ALTER TABLE Account ADD  CONSTRAINT DF_Account_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Account ADD  CONSTRAINT DF_Account_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Account ADD  CONSTRAINT DF_Account_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Account ADD  CONSTRAINT DF_Account_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_Active  DEFAULT (1) FOR Active;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_MinAmount  DEFAULT (0) FOR MinAmount;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_MaxAmount  DEFAULT (0) FOR MaxAmount;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_UseAmount  DEFAULT (0) FOR UseAmount;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_Enabled  DEFAULT (0) FOR Enabled;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Actions ADD  CONSTRAINT DF_Actions_UseValue  DEFAULT ((0)) FOR UseValue;
ALTER TABLE AppCache ADD  DEFAULT ((0)) FOR Custom;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_IsLocation  DEFAULT (0) FOR IsLocation;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_IsClone  DEFAULT (0) FOR IsClone;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_RequesterCanView  DEFAULT (1) FOR RequesterCanView;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_IsUp  DEFAULT (1) FOR IsUp;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_DisplayMapOnWO  DEFAULT (0) FOR DisplayMapOnWO;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_PurchaseCost  DEFAULT (0) FOR PurchaseCost;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_CurrentValue  DEFAULT (0) FOR CurrentValue;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_ReplacementCost  DEFAULT (0) FOR ReplacementCost;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_InstructionsToWO  DEFAULT (0) FOR InstructionsToWO;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_IsPredictive  DEFAULT (0) FOR IsPredictive;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_IsMeter  DEFAULT (0) FOR IsMeter;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_Meter1Reading  DEFAULT (0) FOR Meter1Reading;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_Meter2Reading  DEFAULT (0) FOR Meter2Reading;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_DrawingUpdatesNeeded  DEFAULT (0) FOR DrawingUpdatesNeeded;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_UDFBit3  DEFAULT (0) FOR UDFBit3;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_UDFBit4  DEFAULT (0) FOR UDFBit4;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_UDFBit5  DEFAULT (0) FOR UDFBit5;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_ReceiveDate  DEFAULT (getdate()) FOR ReceiveDate;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_RiskLevelAutoCalc  DEFAULT ((1)) FOR RiskLevelAutoCalc;
ALTER TABLE Asset ADD  CONSTRAINT DF_Asset_PMRequiredAutoCalc  DEFAULT ((1)) FOR PMRequiredAutoCalc;
ALTER TABLE AssetComponent ADD  CONSTRAINT DF_AssetComponent_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetComponentSpecification ADD  CONSTRAINT DF_AssetComponentSpecification_ValueHi  DEFAULT (0) FOR ValueHi;
ALTER TABLE AssetComponentSpecification ADD  CONSTRAINT DF_AssetComponentSpecification_ValueLow  DEFAULT (0) FOR ValueLow;
ALTER TABLE AssetComponentSpecification ADD  CONSTRAINT DF_AssetComponentSpecification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE AssetComponentSpecification ADD  CONSTRAINT DF_AssetComponentSpecification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetContract ADD  CONSTRAINT DF_AssetContract_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostLaborActual  DEFAULT (0) FOR CostLaborActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostEmployeeActual  DEFAULT (0) FOR CostEmployeeActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostContractorActual  DEFAULT (0) FOR CostContractorActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostPartActual  DEFAULT (0) FOR CostPartActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostMiscActual  DEFAULT (0) FOR CostMiscActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostTotalActual  DEFAULT (0) FOR CostTotalActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeLaborActual  DEFAULT (0) FOR ChargeLaborActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeEmployeeActual  DEFAULT (0) FOR ChargeEmployeeActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeContractorActual  DEFAULT (0) FOR ChargeContractorActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargePartActual  DEFAULT (0) FOR ChargePartActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeMiscActual  DEFAULT (0) FOR ChargeMiscActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeTotalActual  DEFAULT (0) FOR ChargeTotalActual;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostLaborActual1  DEFAULT (0) FOR CostLaborActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostEmployeeActual1  DEFAULT (0) FOR CostEmployeeActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostContractorActual1  DEFAULT (0) FOR CostContractorActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostPartActual1  DEFAULT (0) FOR CostPartActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostMiscActual1  DEFAULT (0) FOR CostMiscActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_CostTotalActual1  DEFAULT (0) FOR CostTotalActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeLaborActual1  DEFAULT (0) FOR ChargeLaborActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeEmployeeActual1  DEFAULT (0) FOR ChargeEmployeeActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeContractorActual1  DEFAULT (0) FOR ChargeContractorActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargePartActual1  DEFAULT (0) FOR ChargePartActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeMiscActual1  DEFAULT (0) FOR ChargeMiscActual_AO;
ALTER TABLE AssetCost ADD  CONSTRAINT DF_AssetCost_ChargeTotalActual1  DEFAULT (0) FOR ChargeTotalActual_AO;
ALTER TABLE AssetDocument ADD  CONSTRAINT DF_AssetDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE AssetDocument ADD  CONSTRAINT DF_AssetDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE AssetDocument ADD  CONSTRAINT DF_AssetDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetDocument ADD  CONSTRAINT DF_AssetDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE AssetGroup ADD  CONSTRAINT DF_AssetGroup_Active  DEFAULT (1) FOR Active;
ALTER TABLE AssetGroup ADD  CONSTRAINT DF_AssetGroup_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE AssetGroup ADD  CONSTRAINT DF_AssetGroup_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE AssetGroup ADD  CONSTRAINT DF_AssetGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetGroupAsset ADD  CONSTRAINT DF_AssetGroupAsset_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetLabor ADD  CONSTRAINT DF_AssetLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetLabor ADD  CONSTRAINT DF_AssetLabor_AutoAssign  DEFAULT (0) FOR AutoAssign;
ALTER TABLE AssetLabor ADD  CONSTRAINT DF_AssetLabor_BackupResource  DEFAULT (0) FOR BackupResource;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_LeaseID  DEFAULT ('') FOR LeaseID;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_LeaseName  DEFAULT ('') FOR LeaseName;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alAssetPK  DEFAULT (0) FOR AssetPK;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alAssetID  DEFAULT ('') FOR AssetID;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alAssetName  DEFAULT ('') FOR AssetName;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alLeaseNumber  DEFAULT ('') FOR LeaseNumber;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alStatus  DEFAULT ('') FOR Status;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alStartDate  DEFAULT (getdate()) FOR StartDate;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_Table_1_alCanExpire  DEFAULT (0) FOR ExpireSet;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alExpiration  DEFAULT ('1/1/1900') FOR Expiration;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alSpaceType  DEFAULT ('') FOR SpaceType;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alTotalSF  DEFAULT (0) FOR TotalSF;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alRenewOption  DEFAULT (0) FOR RenewOption;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alRenewValue  DEFAULT (0) FOR RenewValue;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alRenewType  DEFAULT ('') FOR RenewType;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_alDaysBeforeNotice  DEFAULT (30) FOR DaysBeforeNotice;
ALTER TABLE AssetLease ADD  CONSTRAINT DF_AssetLease_RowVersionDate_1  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetLease ADD  CONSTRAINT DF__AssetLeas__SFTyp__0C847969  DEFAULT ('SF') FOR SFType;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_LeasePK  DEFAULT (0) FOR LeasePK;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_alpLeaseID  DEFAULT ('') FOR LeaseID;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_LeaseName  DEFAULT ('') FOR LeaseName;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_alpAmount  DEFAULT (0) FOR Amount;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_alpDate  DEFAULT (getdate()) FOR PaymentDate;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_alpPaidBy  DEFAULT (0) FOR ContactPK;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_alpPaidByID  DEFAULT ('') FOR ContactID;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_LaborName  DEFAULT ('') FOR ContactName;
ALTER TABLE AssetLeasePayments ADD  CONSTRAINT DF_AssetLeasePayments_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetLeaseResponse ADD  CONSTRAINT DF_AssetLeaseResponse_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE AssetLeaseResponse ADD  CONSTRAINT DF_AssetLeaseResponse_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE AssetLeaseResponse ADD  CONSTRAINT DF_AssetLeaseResponse_LeasePK  DEFAULT (0) FOR LeasePK;
ALTER TABLE AssetLeaseResponse ADD  CONSTRAINT DF_AssetLeaseResponse_alrLeaseID  DEFAULT ('') FOR LeaseID;
ALTER TABLE AssetLeaseResponse ADD  CONSTRAINT DF_AssetLeaseResponse_LeaseName  DEFAULT ('') FOR LeaseName;
ALTER TABLE AssetLeaseResponse ADD  CONSTRAINT DF_AssetLeaseResponse_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_sfLeaseID  DEFAULT (0) FOR LeasePK;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_LeaseID  DEFAULT ('') FOR LeaseID;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_LeaseName  DEFAULT ('') FOR LeaseName;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_sfName  DEFAULT ('') FOR Name;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_sfFeetValue  DEFAULT (0) FOR FeetValue;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_sfValue  DEFAULT (0) FOR Value;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_sfType  DEFAULT (0) FOR Type;
ALTER TABLE AssetLeaseSF ADD  CONSTRAINT DF_AssetLeaseSF_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetMeterHistory ADD  CONSTRAINT DF_AssetMeterHistory_ReadingDate  DEFAULT (getdate()) FOR ReadingDate;
ALTER TABLE AssetMeterHistory ADD  CONSTRAINT DF_AssetMeterHistory_Meter1Reading  DEFAULT (0) FOR Meter1Reading;
ALTER TABLE AssetMeterHistory ADD  CONSTRAINT DF_AssetMeterHistory_Meter2Reading  DEFAULT (0) FOR Meter2Reading;
ALTER TABLE AssetMeterHistory ADD  CONSTRAINT DF_AssetMeterHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetMove ADD  CONSTRAINT DF_AssetMove_AssetPK  DEFAULT (0) FOR AssetPK;
ALTER TABLE AssetMove ADD  CONSTRAINT DF_AssetMove_MoveDate  DEFAULT (getdate()) FOR MoveDate;
ALTER TABLE AssetMove ADD  CONSTRAINT DF_AssetMove_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetNote ADD  CONSTRAINT DF_AssetNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE AssetNote ADD  CONSTRAINT DF_AssetNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE AssetNote ADD  CONSTRAINT DF_AssetNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE AssetNote ADD  CONSTRAINT DF_AssetNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetPart ADD  CONSTRAINT DF_AssetPart_Qty  DEFAULT (1) FOR Qty;
ALTER TABLE AssetPart ADD  CONSTRAINT DF_AssetPart_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetPhoto ADD  CONSTRAINT DF_AssetPhoto_PhotoDate  DEFAULT (getdate()) FOR PhotoDate;
ALTER TABLE AssetPhoto ADD  CONSTRAINT DF_AssetPhoto_Active  DEFAULT (0) FOR Active;
ALTER TABLE AssetPhoto ADD  CONSTRAINT DF_AssetPhoto_Followup  DEFAULT (0) FOR Followup;
ALTER TABLE AssetPhoto ADD  CONSTRAINT DF_AssetPhoto_ReportOrder  DEFAULT (0) FOR ReportOrder;
ALTER TABLE AssetPhoto ADD  CONSTRAINT DF_AssetPhoto_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetRequester ADD  CONSTRAINT DF_AssetRequester_IsLease  DEFAULT (0) FOR IsLease;
ALTER TABLE AssetRequester ADD  CONSTRAINT DF_AssetRequester_IsPrimary  DEFAULT (0) FOR IsPrimary;
ALTER TABLE AssetRequester ADD  CONSTRAINT DF_AssetRequester_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__RiskI__2B0B30C4  DEFAULT (left(lower(replace(newid(),'-','')),(20))) FOR RiskID;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__RiskG__2BFF54FD  DEFAULT ('') FOR RiskGroup;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__RiskG__2CF37936  DEFAULT ((1000)) FOR RiskGroupOrder;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__RiskC__2DE79D6F  DEFAULT ('') FOR RiskCaption;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__RiskS__2EDBC1A8  DEFAULT ((0)) FOR RiskScore;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__RiskO__2FCFE5E1  DEFAULT ((1000)) FOR RiskOrder;
ALTER TABLE AssetRiskValues ADD  CONSTRAINT DF__AssetRisk__Rowve__30C40A1A  DEFAULT (getdate()) FOR RowversionDate;
ALTER TABLE AssetSpecification ADD  CONSTRAINT DF_AssetSpecification_ValueHi  DEFAULT (0) FOR ValueHi;
ALTER TABLE AssetSpecification ADD  CONSTRAINT DF_AssetSpecification_ValueLow  DEFAULT (0) FOR ValueLow;
ALTER TABLE AssetSpecification ADD  CONSTRAINT DF_AssetSpecification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE AssetSpecification ADD  CONSTRAINT DF_AssetSpecification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetSpecification ADD  CONSTRAINT DF_AssetSpecification_GenIfOutOfRange  DEFAULT (0) FOR ValueOutOfRangeWO;
ALTER TABLE AssetSpecification ADD  CONSTRAINT DF_AssetSpecification_WOGenerated  DEFAULT (0) FOR WOGenerated;
ALTER TABLE AssetStatusHistory ADD  CONSTRAINT DF_AssetStatusHistory_DownTime  DEFAULT (0) FOR DownTime;
ALTER TABLE AssetStatusHistory ADD  CONSTRAINT DF_AssetStatusHistory_DownTimeUnits  DEFAULT ('Hours') FOR DownTimeUnits;
ALTER TABLE AssetStatusHistory ADD  CONSTRAINT DF_AssetStatusHistory_DownHours  DEFAULT (0) FOR DownHours;
ALTER TABLE AssetStatusHistory ADD  CONSTRAINT DF_AssetStatusHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetTask ADD  CONSTRAINT DF_AssetTask_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE AssetValueHistory ADD  CONSTRAINT DF_AssetValueHistory_ValuationDate  DEFAULT (getdate()) FOR ValuationDate;
ALTER TABLE AssetValueHistory ADD  CONSTRAINT DF_AssetValueHistory_CurrentValue  DEFAULT (0) FOR CurrentValue;
ALTER TABLE AssetValueHistory ADD  CONSTRAINT DF_AssetValueHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE BatchReportList ADD  CONSTRAINT DF_BatchReportList_DisplayOrder  DEFAULT (1) FOR DisplayOrder;
ALTER TABLE BatchReportList ADD  CONSTRAINT DF_BatchReportList_Datetime  DEFAULT (getdate()) FOR Datetime;
ALTER TABLE Bulletin ADD  CONSTRAINT DF_Bulletin_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Bulletin ADD  CONSTRAINT DF_Bulletin_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Bulletin ADD  CONSTRAINT DF_Bulletin_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Cache ADD  CONSTRAINT DF_Cache_CacheID  DEFAULT ('') FOR CacheID;
ALTER TABLE Cache ADD  CONSTRAINT DF_Cache_cRPK  DEFAULT (0) FOR ParentPK;
ALTER TABLE Cache ADD  CONSTRAINT DF_Cache_cType  DEFAULT ('') FOR Type;
ALTER TABLE Cache ADD  CONSTRAINT DF_Cache_cTimestamp  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Category ADD  CONSTRAINT DF_Category_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE Category ADD  CONSTRAINT DF_Category_Active  DEFAULT (1) FOR Active;
ALTER TABLE Category ADD  CONSTRAINT DF_Category_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Category ADD  CONSTRAINT DF_Category_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Category ADD  CONSTRAINT DF_Category_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ChatFolders ADD  CONSTRAINT DF_ChatFolders_UserPK  DEFAULT ((0)) FOR UserPK;
ALTER TABLE ChatFolders ADD  CONSTRAINT DF_ChatFolders_FolderName  DEFAULT ('') FOR FolderName;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_ChatLogID  DEFAULT ('clid'+lower(replace(newid(),'-',''))) FOR ChatLogID;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_UserPK  DEFAULT ((0)) FOR UserPK;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_MyName  DEFAULT ('') FOR MyName;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_MyPhoto  DEFAULT ('') FOR MyPhoto;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_FriendPK  DEFAULT ((0)) FOR FriendPK;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_FriendName  DEFAULT ('') FOR FriendName;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_FriendPhoto  DEFAULT ('') FOR FriendPhoto;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_Message  DEFAULT ('') FOR Message;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_MessageTime  DEFAULT (getdate()) FOR MessageTime;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_MessageViewed  DEFAULT ((0)) FOR MessageViewed;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_IsOfflineMsg  DEFAULT ((0)) FOR IsOfflineMsg;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_WhoSentPK  DEFAULT ((0)) FOR WhoSentPK;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_ExecuteScript  DEFAULT ('') FOR ExecuteScript;
ALTER TABLE ChatLog ADD  CONSTRAINT DF_ChatLog_ChatActive  DEFAULT ((1)) FOR ChatActive;
ALTER TABLE ChatLogINI ADD  CONSTRAINT DF_ChatLogINI_ChatLogID  DEFAULT ('') FOR ChatLogID;
ALTER TABLE ChatLogINI ADD  CONSTRAINT DF_ChatLogINI_FriendPK  DEFAULT ((0)) FOR FriendPK;
ALTER TABLE ChatLogINI ADD  CONSTRAINT DF_ChatLogINI_INIKey  DEFAULT ('') FOR INIKey;
ALTER TABLE ChatLogINI ADD  CONSTRAINT DF_ChatLogINI_INIValue  DEFAULT ('') FOR INIValue;
ALTER TABLE ChatLogINI ADD  CONSTRAINT DF_ChatLogINI_INICaption  DEFAULT ('') FOR INICaption;
ALTER TABLE ChatTo ADD  CONSTRAINT DF_ChatTo_ChatMsgPK  DEFAULT ((0)) FOR ChatMsgPK;
ALTER TABLE ChatTo ADD  CONSTRAINT DF_ChatTo_LaborPK  DEFAULT ((0)) FOR LaborPK;
ALTER TABLE ChatTo ADD  CONSTRAINT DF_ChatTo_LaborName  DEFAULT ('') FOR LaborName;
ALTER TABLE ChatTo ADD  CONSTRAINT DF_ChatTo_ToType  DEFAULT ((1)) FOR ToType;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_IsLocation  DEFAULT (0) FOR IsLocation;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_AssetCount  DEFAULT (0) FOR AssetCount;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_Active  DEFAULT (1) FOR Active;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_IsMeter_1  DEFAULT (0) FOR IsMeter;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_Meter1TrackHistory_1  DEFAULT (0) FOR Meter1TrackHistory;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_Meter2TrackHistory_1  DEFAULT (0) FOR Meter2TrackHistory;
ALTER TABLE Classification ADD  CONSTRAINT DF_Classification_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE ClassificationCause ADD  CONSTRAINT DF_ClassificationCause_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationComponent ADD  CONSTRAINT DF_ClassificationComponent_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationComponentSpecification ADD  CONSTRAINT DF_ClassificationComponentSpecification_ValueHi  DEFAULT (0) FOR ValueHi;
ALTER TABLE ClassificationComponentSpecification ADD  CONSTRAINT DF_ClassificationComponentSpecification_ValueLow  DEFAULT (0) FOR ValueLow;
ALTER TABLE ClassificationComponentSpecification ADD  CONSTRAINT DF_ClassificationComponentSpecification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE ClassificationComponentSpecification ADD  CONSTRAINT DF_ClassificationComponentSpecification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationContract ADD  CONSTRAINT DF_ClassificationContract_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationDocument ADD  CONSTRAINT DF_ClassificationDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE ClassificationDocument ADD  CONSTRAINT DF_ClassificationDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE ClassificationDocument ADD  CONSTRAINT DF_ClassificationDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationDocument ADD  CONSTRAINT DF_ClassificationDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE ClassificationLabor ADD  CONSTRAINT DF_ClassificationLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationLabor ADD  CONSTRAINT DF_ClassificationLabor_AutoAssign  DEFAULT (0) FOR AutoAssign;
ALTER TABLE ClassificationLabor ADD  CONSTRAINT DF_ClassificationLabor_BackupResource  DEFAULT (0) FOR BackupResource;
ALTER TABLE ClassificationPart ADD  CONSTRAINT DF_ClassificationPart_Qty  DEFAULT (1) FOR Qty;
ALTER TABLE ClassificationPart ADD  CONSTRAINT DF_ClassificationPart_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationPM ADD  CONSTRAINT DF_ClassificationPM_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationProblem ADD  CONSTRAINT DF_ClassificationProblem_AvailableToRequester  DEFAULT (0) FOR AvailableToRequester;
ALTER TABLE ClassificationProblem ADD  CONSTRAINT DF_ClassificationProblem_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationProcedure ADD  CONSTRAINT DF_ClassificationProcedure_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationRemedy ADD  CONSTRAINT DF_ClassificationRemedy_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationRemedy ADD  CONSTRAINT DF_ClassificationRemedy_ProblemPK  DEFAULT ((-1)) FOR ProblemPK;
ALTER TABLE ClassificationSpecification ADD  CONSTRAINT DF_ClassificationSpecification_ValueHi  DEFAULT (0) FOR ValueHi;
ALTER TABLE ClassificationSpecification ADD  CONSTRAINT DF_ClassificationSpecification_ValueLow  DEFAULT (0) FOR ValueLow;
ALTER TABLE ClassificationSpecification ADD  CONSTRAINT DF_ClassificationSpecification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE ClassificationSpecification ADD  CONSTRAINT DF_ClassificationSpecification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationSpecification ADD  CONSTRAINT DF_ClassificationSpecification_ValueOutOfRangeWO  DEFAULT (0) FOR ValueOutOfRangeWO;
ALTER TABLE ClassificationTask ADD  CONSTRAINT DF_ClassificationTask_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ClassificationTreeStatus ADD  CONSTRAINT DF_ClassificationTreeStatus_IsOrdered  DEFAULT (0) FOR IsOrdered;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_Active  DEFAULT (1) FOR Active;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_PayTaxToVendor  DEFAULT (0) FOR PayTaxToVendor;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_TaxRate  DEFAULT (0) FOR TaxRate;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_ECommerceEnabled  DEFAULT (0) FOR ECommerceEnabled;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_PaymentOnReceipt  DEFAULT (0) FOR PaymentOnReceipt;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_UseAsLaborResource  DEFAULT (0) FOR UseAsLaborResource;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_UDFBit3  DEFAULT (0) FOR UDFBit3;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_UDFBit4  DEFAULT (0) FOR UDFBit4;
ALTER TABLE Company ADD  CONSTRAINT DF_Company_UDFBit5  DEFAULT (0) FOR UDFBit5;
ALTER TABLE CompanyDocument ADD  CONSTRAINT DF_CompanyDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE CompanyDocument ADD  CONSTRAINT DF_CompanyDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE CompanyDocument ADD  CONSTRAINT DF_CompanyDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE CompanyDocument ADD  CONSTRAINT DF_CompanyDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE CompanyLabor ADD  CONSTRAINT DF_CompanyLabor_PrimaryContact  DEFAULT (0) FOR PrimaryContact;
ALTER TABLE CompanyLabor ADD  CONSTRAINT DF_CompanyLabor_POContact  DEFAULT (0) FOR POContact;
ALTER TABLE CompanyLabor ADD  CONSTRAINT DF_CompanyLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE CompanyNote ADD  CONSTRAINT DF_CompanyNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE CompanyNote ADD  CONSTRAINT DF_CompanyNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE CompanyNote ADD  CONSTRAINT DF_CompanyNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE CompanyNote ADD  CONSTRAINT DF_CompanyNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Component ADD  CONSTRAINT DF_Component_Active  DEFAULT (1) FOR Active;
ALTER TABLE Component ADD  CONSTRAINT DF_Component_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Component ADD  CONSTRAINT DF_Component_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Component ADD  CONSTRAINT DF_Component_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ComponentSpecification ADD  CONSTRAINT DF_ComponentSpecification_ValueHi  DEFAULT (0) FOR ValueHi;
ALTER TABLE ComponentSpecification ADD  CONSTRAINT DF_ComponentSpecification_ValueLow  DEFAULT (0) FOR ValueLow;
ALTER TABLE ComponentSpecification ADD  CONSTRAINT DF_ComponentSpecification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE ComponentSpecification ADD  CONSTRAINT DF_ComponentSpecification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Contract ADD  CONSTRAINT DF_Contract_PeriodStart  DEFAULT ('1/1/1900') FOR PeriodStart;
ALTER TABLE Contract ADD  CONSTRAINT DF_Contract_PeriodEnd  DEFAULT ('1/1/1900') FOR PeriodEnd;
ALTER TABLE Contract ADD  CONSTRAINT DF_Contract_Active  DEFAULT (1) FOR Active;
ALTER TABLE Contract ADD  CONSTRAINT DF_ServiceContract_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Contract ADD  CONSTRAINT DF_ServiceContract_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Contract ADD  CONSTRAINT DF_ServiceContract_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Contract ADD  CONSTRAINT DF_Contract_Expired  DEFAULT (0) FOR Expired;
ALTER TABLE Contract ADD  CONSTRAINT DF_ServiceContract_UDFBit3  DEFAULT ((0)) FOR UDFBit3;
ALTER TABLE Contract ADD  CONSTRAINT DF_ServiceContract_UDFBit4  DEFAULT ((0)) FOR UDFBit4;
ALTER TABLE Contract ADD  CONSTRAINT DF_ServiceContract_UDFBit5  DEFAULT ((0)) FOR UDFBit5;
ALTER TABLE ContractDocument ADD  CONSTRAINT DF_ServiceContractDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE ContractDocument ADD  CONSTRAINT DF_ServiceContractDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE ContractDocument ADD  CONSTRAINT DF_ServiceContractDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ContractDocument ADD  CONSTRAINT DF_ContractDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE ContractNote ADD  CONSTRAINT DF_ContractNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE ContractNote ADD  CONSTRAINT DF_ContractNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE ContractNote ADD  CONSTRAINT DF_ContractNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE ContractNote ADD  CONSTRAINT DF_ContractNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE CriteriaGroup ADD  CONSTRAINT DF_CriteriaGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_TEMPLATE_DISPLAY  DEFAULT (1) FOR TEMPLATE_DISPLAY;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_TEMPLATE_ALIGN  DEFAULT ('left') FOR TEMPLATE_ALIGN;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_REQUIRED  DEFAULT (0) FOR REQUIRED;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_LOOKUP_TABLE  DEFAULT (0) FOR LOOKUP_TABLE;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_CUSTOM  DEFAULT (0) FOR CUSTOM;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_AUDIT  DEFAULT (0) FOR AUDIT;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_USE_FOR_AUDIT_NAME  DEFAULT (0) FOR USE_FOR_AUDIT_NAME;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_USE_TIME_IF_DATE  DEFAULT (0) FOR USE_TIME_IF_DATE;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_TotalIfSelected  DEFAULT (0) FOR TOTALIFSELECTED;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_FORM_FIELD_ID  DEFAULT ('') FOR FORM_FIELD_ID;
ALTER TABLE DataDict ADD  CONSTRAINT DF_DataDict_FORM_FIELD_VALIDATE  DEFAULT (0) FOR FORM_FIELD_VALIDATE;
ALTER TABLE DataDict ADD  DEFAULT ((0)) FOR FORM_HIDDEN2;
ALTER TABLE DebugSQL ADD  CONSTRAINT DF_DebugSQL_TransactionDate  DEFAULT (getdate()) FOR TransactionDate;
ALTER TABLE DebugSQL ADD  CONSTRAINT DF_DebugSQL_DB_DOK  DEFAULT ((1)) FOR DB_DOK;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_Active  DEFAULT (1) FOR Active;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_CostRegular  DEFAULT (0) FOR CostRegular;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_CostOvertime  DEFAULT (0) FOR CostOvertime;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_CostOther  DEFAULT (0) FOR CostOther;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_ChargeRate  DEFAULT (0) FOR ChargeRate;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE Department ADD  CONSTRAINT DF_Department_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE DepartmentDocument ADD  CONSTRAINT DF_DepartmentDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE DepartmentDocument ADD  CONSTRAINT DF_DepartmentDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE DepartmentDocument ADD  CONSTRAINT DF_DepartmentDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE DepartmentDocument ADD  CONSTRAINT DF_DepartmentDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE DepartmentLabor ADD  CONSTRAINT DF_DepartmentLabor_PrimaryContact  DEFAULT (0) FOR PrimaryContact;
ALTER TABLE DepartmentLabor ADD  CONSTRAINT DF_DepartmentLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE DepartmentNote ADD  CONSTRAINT DF_DepartmentNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE DepartmentNote ADD  CONSTRAINT DF_DepartmentNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE DepartmentNote ADD  CONSTRAINT DF_DepartmentNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE DepartmentNote ADD  CONSTRAINT DF_DepartmentNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_LocationType  DEFAULT (N'LIBRARY') FOR LocationType;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_SentWithEmail  DEFAULT (0) FOR SentWithEmail;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_Active  DEFAULT (1) FOR Active;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Document ADD  CONSTRAINT DF_Document_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE EmailGroup ADD  CONSTRAINT DF_EmailGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Escalation ADD  CONSTRAINT DF_Escalation_NextRepeat  DEFAULT (getdate()) FOR NextRepeat;
ALTER TABLE Event ADD  CONSTRAINT DF_Event_Quantity  DEFAULT (0) FOR Quantity;
ALTER TABLE Event ADD  CONSTRAINT DF_Event_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Event ADD  CONSTRAINT DF_Event_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Event ADD  CONSTRAINT DF_Event_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Failure ADD  CONSTRAINT DF_Failure_AvailableToRequester  DEFAULT (0) FOR AvailableToRequester;
ALTER TABLE Failure ADD  CONSTRAINT DF_Failure_Active  DEFAULT (0) FOR Active;
ALTER TABLE Failure ADD  CONSTRAINT DF_Failure_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Failure ADD  CONSTRAINT DF_Failure_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Failure ADD  CONSTRAINT DF_Failure_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE FavoriteGroup ADD  CONSTRAINT DF_FavoriteGroup_GlobalGroup  DEFAULT (0) FOR GlobalGroupView;
ALTER TABLE FavoriteGroup ADD  CONSTRAINT DF_FavoriteGroup_GlobalGroupUpdate  DEFAULT (0) FOR GlobalGroupUpdate;
ALTER TABLE FavoriteGroup ADD  CONSTRAINT DF_FavoriteGroup_DefaultGroup  DEFAULT (0) FOR DefaultGroup;
ALTER TABLE FavoriteGroup ADD  CONSTRAINT DF_FavoriteGroup_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE FavoriteGroup ADD  CONSTRAINT DF_FavoriteGroup_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE FavoriteGroup ADD  CONSTRAINT DF_FavoriteGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE FavoriteGroupKeys ADD  CONSTRAINT DF_FavoriteGroupKeys_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE FavoriteGroupKeys ADD  CONSTRAINT DF_FavoriteGroupKeys_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE FavoriteGroupKeys ADD  CONSTRAINT DF_FavoriteGroupKeys_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_ParentPK  DEFAULT (0) FOR ParentPK;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_ParentID  DEFAULT ('') FOR ParentID;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_Tablename  DEFAULT ('') FOR Tablename;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsOFilename  DEFAULT ('') FOR OriginalFilename;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_FilenameOnDisk  DEFAULT ('') FOR FilenameOnDisk;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsType  DEFAULT (0) FOR Type;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsVersion  DEFAULT (0.001) FOR Version;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsCategory  DEFAULT ('') FOR Category;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsContactID  DEFAULT (0) FOR ContactPK;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_ContactID  DEFAULT ('') FOR ContactID;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_ContactName  DEFAULT ('') FOR ContactName;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsTitle  DEFAULT ('') FOR Title;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsDescription  DEFAULT ('') FOR Description;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_fsRelatedID  DEFAULT (0) FOR RelatedPK;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_RelatedID  DEFAULT ('') FOR RelatedID;
ALTER TABLE Filestore ADD  CONSTRAINT DF_Filestore_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_GISLayerInitialStateOn  DEFAULT (1) FOR GISLayerInitialStateOn;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_GISLayerShowLabel  DEFAULT (0) FOR GISLayerShowLabel;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_GISLayerLabelFontBold  DEFAULT (0) FOR GISLayerLabelFontBold;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_GISLayerLabelShowOutline  DEFAULT (0) FOR GISLayerLabelShowOutline;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_Active  DEFAULT (1) FOR Active;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE GISLayer ADD  CONSTRAINT DF_GISLayer_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GISLayerGroup ADD  CONSTRAINT DF_GISLayerGroup_Icon  DEFAULT (1) FOR Icon;
ALTER TABLE GISLayerGroup ADD  CONSTRAINT DF_GISLayerGroup_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE GISLayerGroup ADD  CONSTRAINT DF_GISLayerGroup_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE GISLayerGroup ADD  CONSTRAINT DF_GISLayerGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GISViewer ADD  CONSTRAINT DF_GISViewer_ExternalViewer  DEFAULT (1) FOR ExternalViewer;
ALTER TABLE GISViewer ADD  CONSTRAINT DF_GISViewer_Active  DEFAULT (1) FOR Active;
ALTER TABLE GISViewer ADD  CONSTRAINT DF_GISViewer_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE GISViewer ADD  CONSTRAINT DF_GISViewer_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE GISViewer ADD  CONSTRAINT DF_GISViewer_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_GlobalAddressID  DEFAULT ('') FOR GlobalID;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_GlobalAddressName  DEFAULT ('') FOR GlobalName;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaParentID  DEFAULT (0) FOR ParentPK;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_ParentID  DEFAULT ('') FOR ParentID;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_Tablename  DEFAULT ('') FOR Tablename;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaAddress1  DEFAULT ('') FOR Address1;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaAddress2  DEFAULT ('') FOR Address2;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaCity  DEFAULT ('') FOR City;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaState  DEFAULT ('') FOR State;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaZipcode  DEFAULT ('') FOR Zipcode;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_gaCountry  DEFAULT ('') FOR Country;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_AddressType  DEFAULT ('') FOR AddressType;
ALTER TABLE GlobalAddress ADD  CONSTRAINT DF_GlobalAddress_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_GlobalContactsID  DEFAULT ('') FOR GlobalID;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_Tablename  DEFAULT ('') FOR Tablename;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcFirstname  DEFAULT ('') FOR Firstname;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcMiddlename  DEFAULT ('') FOR Middlename;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcLastname  DEFAULT ('') FOR Lastname;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcEmail  DEFAULT ('') FOR Email;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcWebsite  DEFAULT ('') FOR Website;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcCategory  DEFAULT ('') FOR Category;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_gcIsDefault  DEFAULT (0) FOR IsDefault;
ALTER TABLE GlobalContacts ADD  CONSTRAINT DF_GlobalContacts_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_gnParentID  DEFAULT (0) FOR ParentPK;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_ParentID  DEFAULT ('') FOR ParentID;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_Tablename  DEFAULT ('') FOR Tablename;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_gnSubject  DEFAULT ('') FOR Subject;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_gnNote  DEFAULT ('') FOR Note;
ALTER TABLE GlobalNotes ADD  CONSTRAINT DF_GlobalNotes_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_gpParentID  DEFAULT (0) FOR ParentPK;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_ParentID  DEFAULT ('') FOR ParentID;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_Tablename  DEFAULT ('') FOR Tablename;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_gpPhone  DEFAULT ('') FOR Phone;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_gpExt  DEFAULT ('') FOR Ext;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_PhoneType  DEFAULT ('') FOR PhoneType;
ALTER TABLE GlobalPhones ADD  CONSTRAINT DF_GlobalPhones_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_GlobalID  DEFAULT ('') FOR GlobalID;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_GlobalName  DEFAULT ('') FOR GlobalName;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_ParentPK  DEFAULT (0) FOR ParentPK;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_ParentID  DEFAULT ('') FOR ParentID;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_gsTableName  DEFAULT ('') FOR TableName;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_ChildPK  DEFAULT (0) FOR ChildPK;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_gsChildID  DEFAULT ('') FOR ChildID;
ALTER TABLE GlobalShare ADD  CONSTRAINT DF_GlobalShare_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE History ADD  CONSTRAINT DF_History_hUID  DEFAULT (0) FOR hUID;
ALTER TABLE History ADD  CONSTRAINT DF_History_hPID  DEFAULT (0) FOR hPID;
ALTER TABLE History ADD  CONSTRAINT DF_History_hPIDChar  DEFAULT ('') FOR hPIDChar;
ALTER TABLE History ADD  CONSTRAINT DF_History_hType  DEFAULT ('WO') FOR hType;
ALTER TABLE History ADD  CONSTRAINT DF_History_hAction  DEFAULT ('') FOR hAction;
ALTER TABLE History ADD  CONSTRAINT DF_History_hGlobalID  DEFAULT ('hid' + lower(replace(newid(),'-',''))) FOR hGlobalID;
ALTER TABLE History ADD  CONSTRAINT DF_History_hModified  DEFAULT (getdate()) FOR hModified;
ALTER TABLE History ADD  CONSTRAINT DF_History_hActive  DEFAULT (1) FOR hActive;
ALTER TABLE History ADD  CONSTRAINT DF_History_hTimestamp  DEFAULT (getdate()) FOR hTimestamp;
ALTER TABLE Holiday ADD  CONSTRAINT DF_Holiday_IsFixedDate  DEFAULT (1) FOR IsFixedDate;
ALTER TABLE Holiday ADD  CONSTRAINT DF_Holiday_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Holiday ADD  CONSTRAINT DF_Holiday_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Holiday ADD  CONSTRAINT DF_Holiday_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE InterfaceUser ADD  CONSTRAINT DF_InterfaceUser_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE KeyAssetLocation ADD  CONSTRAINT DF_KeyAssetLocation_LModifiedDate  DEFAULT (getdate()) FOR LModifiedDate;
ALTER TABLE KeyAssetLocation ADD  CONSTRAINT DF_KeyAssetLocation_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE KeyData ADD  CONSTRAINT DF_KeyData_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Keys ADD  CONSTRAINT DF_Keys_IsActive  DEFAULT (1) FOR IsActive;
ALTER TABLE Keys ADD  CONSTRAINT DF_Keys_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE KeyTransaction ADD  CONSTRAINT DF_KeyTransaction_TransactionDate  DEFAULT (getdate()) FOR TransactionDate;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_Decimals  DEFAULT (0) FOR Decimals;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_Format  DEFAULT ('G') FOR Format;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_DisplayOrder  DEFAULT (1000) FOR DisplayOrder;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_IsPercent  DEFAULT (1) FOR IsPercent;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_LowRange  DEFAULT (0) FOR LowRange;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_HighRange  DEFAULT (100) FOR HighRange;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_CriticalValue  DEFAULT (50) FOR CriticalValue;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_AlertValue  DEFAULT (75) FOR AlertValue;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_ShowOptimalDial  DEFAULT (0) FOR ShowOptimalDial;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_LastValue  DEFAULT (0) FOR LastValue;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_LastVariance  DEFAULT (0) FOR LastVariance;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_Active  DEFAULT (1) FOR Active;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_AlertValue1  DEFAULT (75) FOR UnacceptableValue;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_ShareID  DEFAULT ('') FOR ShareID;
ALTER TABLE KPI ADD  CONSTRAINT DF_KPI_RemoteID  DEFAULT ('') FOR ShareRemoteID;
ALTER TABLE KPICustomImages ADD  CONSTRAINT DF_KPICustomImages_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE KPICustomImages ADD  CONSTRAINT DF_KPICustomImages_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE KPICustomImages ADD  CONSTRAINT DF_KPICustomImages_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE KPIGroup ADD  CONSTRAINT DF_KPIGroup_DisplayOrder  DEFAULT (1000) FOR DisplayOrder;
ALTER TABLE KPIGroup ADD  CONSTRAINT DF_KPIGroup_Active  DEFAULT (1) FOR Active;
ALTER TABLE KPIGroup ADD  CONSTRAINT DF_KPIGroup_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE KPIGroup ADD  CONSTRAINT DF_KPIGroup_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE KPIGroup ADD  CONSTRAINT DF_KPIGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE KPIHistory ADD  CONSTRAINT DF__KPIHistor__NValu__0D789DA2  DEFAULT (0) FOR NValue;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_Active  DEFAULT (1) FOR Active;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_InterfaceUser  DEFAULT (0) FOR SystemUser;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_Access  DEFAULT (0) FOR Access;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_PagerNumeric  DEFAULT (0) FOR PagerNumeric;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_CostRegular  DEFAULT (0) FOR CostRegular;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_CostOvertime  DEFAULT (0) FOR CostOvertime;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_CostOther  DEFAULT (0) FOR CostOther;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_ChargeRate  DEFAULT (0) FOR ChargeRate;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_AvailForAutoAssign  DEFAULT (1) FOR AvailForAutoAssign;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_OnCall  DEFAULT (0) FOR OnCall;
ALTER TABLE Labor ADD  DEFAULT ((0)) FOR RecordViews;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_UseScheduleFromShift  DEFAULT ((0)) FOR UseScheduleFromShift;
ALTER TABLE Labor ADD  CONSTRAINT DF_Labor_AppCache_Clear  DEFAULT ((0)) FOR AppCache_Clear;
ALTER TABLE LaborAddressBook ADD  CONSTRAINT DF_LaborAddressBook_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborAsset ADD  CONSTRAINT DF_LaborAsset_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborAssetFilter ADD  CONSTRAINT DF_LaborAssetFilter_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborAssetMgr ADD  CONSTRAINT DF_LaborAssetMgr_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborDocument ADD  CONSTRAINT DF_LaborDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE LaborDocument ADD  CONSTRAINT DF_LaborDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE LaborDocument ADD  CONSTRAINT DF_LaborDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborDocument ADD  CONSTRAINT DF_LaborDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE LaborNote ADD  CONSTRAINT DF_LaborNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE LaborNote ADD  CONSTRAINT DF_LaborNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE LaborNote ADD  CONSTRAINT DF_LaborNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE LaborNote ADD  CONSTRAINT DF_LaborNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborPreference ADD  CONSTRAINT DF_LaborPreference_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborReportGroup ADD  CONSTRAINT DF_LaborReportGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborTimeOff ADD  CONSTRAINT DF_LaborTimeOff_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborTimeOff ADD  CONSTRAINT DF_LaborTimeOff_FromShift  DEFAULT ((0)) FOR FromShift;
ALTER TABLE LaborTraining ADD  CONSTRAINT DF_LaborTraining_Hours  DEFAULT (0) FOR Hours;
ALTER TABLE LaborTraining ADD  CONSTRAINT DF_LaborTraining_TravelCost  DEFAULT (0) FOR TravelCost;
ALTER TABLE LaborTraining ADD  CONSTRAINT DF_LaborTraining_TuitionCost  DEFAULT (0) FOR TuitionCost;
ALTER TABLE LaborTraining ADD  CONSTRAINT DF_LaborTraining_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborWorkHours ADD  CONSTRAINT DF_LaborWorkHours_RowVersionDate_1  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Sunday  DEFAULT (0) FOR Sunday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Monday  DEFAULT (0) FOR Monday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Tuesday  DEFAULT (0) FOR Tuesday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Wednesday  DEFAULT (0) FOR Wednesday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Thursday  DEFAULT (0) FOR Thursday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Friday  DEFAULT (0) FOR Friday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_Saturday  DEFAULT (0) FOR Saturday;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LaborWorkSchedule ADD  CONSTRAINT DF_LaborWorkSchedule_FromShift  DEFAULT ((0)) FOR FromShift;
ALTER TABLE Languages ADD  DEFAULT ((1)) FOR Active;
ALTER TABLE Location ADD  CONSTRAINT DF_Location_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE Location ADD  CONSTRAINT DF_Location_Active  DEFAULT (1) FOR Active;
ALTER TABLE Location ADD  CONSTRAINT DF_Location_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Location ADD  CONSTRAINT DF_Location_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Location ADD  CONSTRAINT DF_Location_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LocationDocument ADD  CONSTRAINT DF_LocationDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE LocationDocument ADD  CONSTRAINT DF_LocationDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE LocationDocument ADD  CONSTRAINT DF_LocationDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LocationDocument ADD  CONSTRAINT DF_LocationDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE Log ADD  CONSTRAINT DF_Log_Date  DEFAULT (getdate()) FOR Date;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_Internal  DEFAULT (0) FOR Internal;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_Enabled  DEFAULT (0) FOR Enabled;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_SkipValidation  DEFAULT (0) FOR SkipValidation;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_CanModify  DEFAULT (0) FOR CanModify;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_NoCascadeUpdate  DEFAULT (0) FOR NoCascadeUpdate;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_CodeValueEnabled  DEFAULT (0) FOR CodeValueEnabled;
ALTER TABLE LookupTable ADD  CONSTRAINT DF_LookupTable_NotViewableInLookupTableMgr  DEFAULT (0) FOR NotViewableInLookupTableMgr;
ALTER TABLE LookupTableValues ADD  CONSTRAINT DF_LookupTableValues_CodeValue  DEFAULT (0) FOR CodeValue;
ALTER TABLE LookupTableValues ADD  CONSTRAINT DF_LookupTableValues_SystemCode  DEFAULT (0) FOR SystemCode;
ALTER TABLE LookupTableValues ADD  CONSTRAINT DF_LookupTableValues_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE LookupTableValues ADD  CONSTRAINT DF_LookupTableValues_AvailableToRequester  DEFAULT ((0)) FOR AvailableToRequester;
ALTER TABLE MC_ExceptionLog ADD  CONSTRAINT DF_MC_ExceptionLog_TransactionDate  DEFAULT (getdate()) FOR TransactionDate;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_Table_1_mcrfTableName  DEFAULT ('') FOR TableName;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_Table_1_mcrfFieldName  DEFAULT ('') FOR FieldName;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_Table_1_mcrfRequired  DEFAULT (0) FOR Required;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_MC_FieldOptions_foOptions  DEFAULT ('') FOR Options;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_MC_FieldOptions_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_Table_1_mcrfDisplayonSR  DEFAULT ((0)) FOR DisplayonSR;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_MC_FieldOptions_Filtered  DEFAULT ((0)) FOR Filtered;
ALTER TABLE MC_FieldOptions ADD  CONSTRAINT DF_MC_FieldOptions_CanbeFiltered  DEFAULT ((0)) FOR CanbeFiltered;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Level__4E546D01  DEFAULT ((0)) FOR Levels;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApproval__POPK__4F48913A  DEFAULT ((0)) FOR POPK;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApproval__WOPK__503CB573  DEFAULT ((0)) FOR WOPK;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Modul__5130D9AC  DEFAULT ('') FOR ModuleID;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Appro__5224FDE5  DEFAULT ((0)) FOR ApprovalLevel;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Appro__5319221E  DEFAULT ((0)) FOR Approved;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Appro__540D4657  DEFAULT ((0)) FOR ApprovedByPK;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Appro__55016A90  DEFAULT ('') FOR ApprovedByInitials;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__RowVe__55F58EC9  DEFAULT ((0)) FOR RowVersionUserPK;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__RowVe__56E9B302  DEFAULT ('') FOR RowVersionInitials;
ALTER TABLE MCApprovals ADD  CONSTRAINT DF__MCApprova__Rowve__57DDD73B  DEFAULT (getdate()) FOR RowversionDate;
ALTER TABLE MCAudit ADD  CONSTRAINT DF_MCAudit_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCAutoEmail ADD  CONSTRAINT DF_MCAutoEmail_TransactionDate  DEFAULT (getdate()) FOR TransactionDate;
ALTER TABLE MCAutoEmail ADD  CONSTRAINT DF_MCAutoEmail_Processed  DEFAULT ('N') FOR Processed;
ALTER TABLE MCAutoMessage ADD  CONSTRAINT DF_MCAutoAlert_TransactionDate  DEFAULT (getdate()) FOR TransactionDate;
ALTER TABLE MCAutoMessage ADD  CONSTRAINT DF_MCAutoAlert_Processed  DEFAULT ('N') FOR Processed;
ALTER TABLE MCEdition ADD  CONSTRAINT DF_MCEdition_CurrentEdition  DEFAULT (0) FOR CurrentEdition;
ALTER TABLE MCEdition ADD  CONSTRAINT DF_MCEdition_BaseEdition  DEFAULT (0) FOR BaseEdition;
ALTER TABLE MCEdition ADD  CONSTRAINT DF_MCEdition_CustomerSpecific  DEFAULT (0) FOR CustomerSpecific;
ALTER TABLE MCEmailLog ADD  CONSTRAINT DF_MCEmailLog_EmailDate  DEFAULT (getdate()) FOR EmailDate;
ALTER TABLE MCEvent ADD  CONSTRAINT DF_MCEvent_EventOrder  DEFAULT (0) FOR EventOrder;
ALTER TABLE MCEvent ADD  CONSTRAINT DF_MCEvent_Active  DEFAULT (1) FOR Active;
ALTER TABLE MCEvent ADD  CONSTRAINT DF_MCEvent_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCExport ADD  CONSTRAINT DF_MCExport_ExportDate  DEFAULT (getdate()) FOR ExportDate;
ALTER TABLE MCForm ADD  CONSTRAINT DF_MCForm_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCICalendar ADD  CONSTRAINT DF_MCICalendar_Active  DEFAULT ((1)) FOR Active;
ALTER TABLE MCICalendar ADD  CONSTRAINT DF_MCICalendar_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCINI ADD  CONSTRAINT DF__MCINI__INI_KEY__03F163A3  DEFAULT ('TUFJTlRFTkFOQ0U=') FOR INI_KEY;
ALTER TABLE MCINI ADD  CONSTRAINT DF__MCINI__INI_VALUE__04E587DC  DEFAULT ('Q09OTkVDVElPTg==') FOR INI_VALUE;
ALTER TABLE MCLanguage ADD  CONSTRAINT DF_MCLanguage_Active  DEFAULT ((1)) FOR Active;
ALTER TABLE MCLanguage ADD  CONSTRAINT DF_Language_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE MCLanguage ADD  CONSTRAINT DF_Language_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE MCLanguage ADD  CONSTRAINT DF_Language_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCMenu ADD  CONSTRAINT DF_MCMenu_IsActive  DEFAULT (1) FOR IsActive;
ALTER TABLE MCMenuXP ADD  DEFAULT ('') FOR Module;
ALTER TABLE MCMenuXP ADD  DEFAULT ('') FOR PageID;
ALTER TABLE MCMenuXP ADD  DEFAULT ('') FOR MenuName;
ALTER TABLE MCMenuXP ADD  DEFAULT ((1000)) FOR DisplayOrder;
ALTER TABLE MCMenuXP ADD  DEFAULT ('') FOR MenuHTML;
ALTER TABLE MCModule ADD  CONSTRAINT DF_MCModule_CreateModObjOnStartup  DEFAULT (1) FOR CreateModObjOnStartup;
ALTER TABLE MCModule ADD  CONSTRAINT DF_MCModule_UsesFindAndReplace  DEFAULT (0) FOR UsesFindAndReplace;
ALTER TABLE MCModule ADD  CONSTRAINT DF_MCModule_IsPhoto  DEFAULT (0) FOR IsPhoto;
ALTER TABLE MCModule ADD  CONSTRAINT DF_MCModule_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_DisplayOrder  DEFAULT (0) FOR DisplayOrder;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_UseActionLoader  DEFAULT (0) FOR UseActionLoader;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_ActionPageHeight  DEFAULT (500) FOR ActionPageHeight;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_ActionPageWidth  DEFAULT (500) FOR ActionPageWidth;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_IsCustomForClient  DEFAULT (0) FOR IsCustomForClient;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_IsEnabled  DEFAULT (1) FOR IsEnabled;
ALTER TABLE MCModuleActions ADD  CONSTRAINT DF_MCModuleActions_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCModuleActionsModule ADD  CONSTRAINT DF_MCModuleActionsModule_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCModuleLookupCodes ADD  CONSTRAINT DF_MCModuleLookupCodes_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__RowVe__0F63164F  DEFAULT ((0)) FOR RowVersionUserPK;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__Rowve__114B5EC1  DEFAULT (getdate()) FOR RowversionDate;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__BodyM__0C86A9A4  DEFAULT ('') FOR BodyMessage;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__Notif__0A9E6132  DEFAULT ('') FOR NotificationType;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__Capti__0B92856B  DEFAULT ('MC Notification') FOR Caption;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__Click__0E6EF216  DEFAULT ('') FOR ClickAction;
ALTER TABLE MCNotifications ADD  CONSTRAINT DF__MCNotific__Showe__0D7ACDDD  DEFAULT ((0)) FOR ShowedMessage;
ALTER TABLE MCRule ADD  CONSTRAINT DF_AssignmentRule_Active  DEFAULT (1) FOR Active;
ALTER TABLE MCRule ADD  CONSTRAINT DF_AssignmentRule_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCRule ADD  CONSTRAINT DF_MCRule_DeleteAfterRuleInvoked  DEFAULT (0) FOR DeleteAfterRuleInvoked;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_EmailReport  DEFAULT (0) FOR EmailReport;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_RepairCenter  DEFAULT (0) FOR RepairCenter;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_RepairCenterSupervisor  DEFAULT (0) FOR RepairCenterSupervisor;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Shop  DEFAULT (0) FOR Shop;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_ShopSupervisor  DEFAULT (0) FOR ShopSupervisor;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Supervisor  DEFAULT (0) FOR Supervisor;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Assigned  DEFAULT (0) FOR Assigned;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Requester  DEFAULT (0) FOR Requester;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_StockRoom  DEFAULT (0) FOR StockRoom;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_StockRoomSupervisor  DEFAULT (0) FOR StockRoomSupervisor;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_ToolRoom  DEFAULT (0) FOR ToolRoom;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_ToolRoomSupervisor  DEFAULT (0) FOR ToolRoomSupervisor;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Contact  DEFAULT (0) FOR Contact;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Operator  DEFAULT (0) FOR Operator;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Occupant  DEFAULT (0) FOR Occupant;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Department  DEFAULT (0) FOR Department;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Tenant  DEFAULT (0) FOR Tenant;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Vendor  DEFAULT (0) FOR Vendor;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Buyer  DEFAULT (0) FOR Buyer;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_ShipTo  DEFAULT (0) FOR ShipTo;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_BillTo  DEFAULT (0) FOR BillTo;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom3  DEFAULT (0) FOR Custom3;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom4  DEFAULT (0) FOR Custom4;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom5  DEFAULT (0) FOR Custom5;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom6  DEFAULT (0) FOR Custom6;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom7  DEFAULT (0) FOR Custom7;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_Custom8  DEFAULT (0) FOR Custom8;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_ReportInBody  DEFAULT (0) FOR ReportInBody;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF_MCRuleEmail_TakenBy  DEFAULT (0) FOR TakenBy;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF__MCRuleEma__Taken__78487692  DEFAULT (0) FOR Taken;
ALTER TABLE MCRuleEmail ADD  CONSTRAINT DF__MCRuleEma__Pager__793C9ACB  DEFAULT (0) FOR Pager;
ALTER TABLE MCRuleEmailReport ADD  CONSTRAINT DF_MCRuleEmailReport_UseReportCriteria  DEFAULT (0) FOR UseReportCriteria;
ALTER TABLE MCRuleEmailReport ADD  CONSTRAINT DF_MCRuleEmailReport_RPTCriteriaUseParent  DEFAULT (0) FOR RPTCriteriaUseParent;
ALTER TABLE MCRuleEmailReport ADD  CONSTRAINT DF_MCRuleEmailReport_IsActive  DEFAULT (1) FOR IsActive;
ALTER TABLE MCRuleLabor ADD  CONSTRAINT DF_AssignmentRuleLabor_OnCall  DEFAULT (0) FOR OnCall;
ALTER TABLE MCRuleLabor ADD  CONSTRAINT DF_AssignmentRuleLabor_BackupResource  DEFAULT (0) FOR BackupResource;
ALTER TABLE MCRuleLabor ADD  CONSTRAINT DF_AssignmentRuleLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCRuleLaborAlert ADD  CONSTRAINT DF_MCRuleLaborAlert_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_Port  DEFAULT (110) FOR PortNumber;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_APOP  DEFAULT (0) FOR AuthMethod;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_ImageCommands  DEFAULT (1) FOR ImageUploadCommands;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_ReportCommands  DEFAULT (1) FOR ReportCommands;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_ReportCommandsEmailMustExistinLA  DEFAULT (0) FOR ReportCommandsEmailMustExistinLA;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_SaveAttachmentsToImageServer  DEFAULT (0) FOR SaveAttachmentsToImageServer;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_Active  DEFAULT (1) FOR Active;
ALTER TABLE MCRulePOP3 ADD  CONSTRAINT DF_MCRulePOP3_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCRuleWO ADD  CONSTRAINT DF_MCRuleWO_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCScriptAction ADD  CONSTRAINT DF_MCScriptAction_Enabled  DEFAULT ((1)) FOR Enabled;
ALTER TABLE MCScriptAction ADD  CONSTRAINT DF_MCScriptAction_UserDefined  DEFAULT ((0)) FOR UserDefined;
ALTER TABLE MCTab ADD  CONSTRAINT DF_MCTab_Tab_Dynamic  DEFAULT (0) FOR Tab_Label_JSDynamic;
ALTER TABLE MCTab ADD  CONSTRAINT DF_MCTab_CreateModObjOnStartup  DEFAULT (0) FOR Tab_CreateOnStartup;
ALTER TABLE MCTab ADD  CONSTRAINT DF_MCTab_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE MCTab ADD  CONSTRAINT DF__mctab__IsSystem__0E4CC64A  DEFAULT (1) FOR IsSystem;
ALTER TABLE MCTable ADD  CONSTRAINT DF_MCTable_UseTable_DescOnAudit  DEFAULT (0) FOR UseTable_DescOnAudit;
ALTER TABLE MCTable ADD  CONSTRAINT DF_MCTable_AuditInserts  DEFAULT (0) FOR AuditInserts;
ALTER TABLE MCTable ADD  CONSTRAINT DF_MCTable_AuditUpdates  DEFAULT (0) FOR AuditUpdates;
ALTER TABLE MCTable ADD  CONSTRAINT DF_MCTable_AuditDeletes  DEFAULT (0) FOR AuditDeletes;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_ChatOfflineMsgID  DEFAULT ('comid'+lower(replace(newid(),'-',''))) FOR ChatOfflineMsgID;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_UserPK  DEFAULT ((0)) FOR UserPK;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_FriendPK  DEFAULT ((0)) FOR FriendPK;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_FriendName  DEFAULT ('') FOR FriendName;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_FriendPhoto  DEFAULT ('') FOR FriendPhoto;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_Subject  DEFAULT ('') FOR Subject;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_Message  DEFAULT ('') FOR Message;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_MsgRead  DEFAULT ((0)) FOR MsgRead;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_MessageTime  DEFAULT (getdate()) FOR MessageTime;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_MsgStatus  DEFAULT ((1)) FOR MsgStatus;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_MsgActive  DEFAULT ((1)) FOR MsgActive;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_MsgFiles  DEFAULT ((0)) FOR MsgFiles;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_MsgUrgent  DEFAULT ((0)) FOR MsgUrgent;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_WOPK  DEFAULT ((0)) FOR WOPK;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_WOID  DEFAULT ('') FOR WOID;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_FolderID  DEFAULT ('inbox') FOR FolderID;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_ChatOfflineMsg_IsEvent  DEFAULT ((0)) FOR IsGenerated;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_MessageCenter_HasReports  DEFAULT ((0)) FOR HasReports;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_MessageCenter_ReportID1  DEFAULT ('') FOR ReportID1;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_MessageCenter_ReportID2  DEFAULT ('') FOR ReportID2;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_MessageCenter_ReportID3  DEFAULT ('') FOR ReportID3;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_MessageCenter_ReportID4  DEFAULT ('') FOR ReportID4;
ALTER TABLE MessageCenter ADD  CONSTRAINT DF_MessageCenter_ReportID5  DEFAULT ('') FOR ReportID5;
ALTER TABLE MeterEntry ADD  CONSTRAINT DF_MeterEntry_TransactionDate  DEFAULT (getdate()) FOR TransactionDate;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_Active  DEFAULT (1) FOR Active;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_Hazardous  DEFAULT (0) FOR Hazardous;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_ShelfLifeDays  DEFAULT (0) FOR ShelfLifeDays;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_ShippingWeight  DEFAULT (0) FOR ShippingWeight;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_IssueUnitCost  DEFAULT (0) FOR IssueUnitCost;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_IssueUnitChargePrice  DEFAULT (0) FOR IssueUnitChargePrice;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_IssueUnitChargePercentage  DEFAULT (0) FOR IssueUnitChargePercentage;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_AverageOrderUnitPrice  DEFAULT (0) FOR AverageOrderUnitPrice;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_LastOrderUnitPrice  DEFAULT (0) FOR LastOrderUnitPrice;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_UpdateStockRooms  DEFAULT (0) FOR UpdateStockRooms;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_UpdateStockRooms1  DEFAULT (0) FOR UpdateStockRooms2;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_Stock  DEFAULT (1) FOR DirectIssue;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_AvailableToRequester  DEFAULT (1) FOR AvailableToRequester;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_RotatingPart  DEFAULT (0) FOR RotatingPart;
ALTER TABLE Part ADD  CONSTRAINT DF_Part_ConversionToIssueUnits  DEFAULT (1) FOR ConversionToIssueUnits;
ALTER TABLE Part ADD  CONSTRAINT DF__PartO__UDFBi__39444182  DEFAULT ((0)) FOR UDFBit3;
ALTER TABLE Part ADD  CONSTRAINT DF__PartO__UDFBi__3A3865BB  DEFAULT ((0)) FOR UDFBit4;
ALTER TABLE Part ADD  CONSTRAINT DF__PartO__UDFBi__3B2C89F4  DEFAULT ((0)) FOR UDFBit5;
ALTER TABLE PartAlternate ADD  CONSTRAINT DF_PartAlternate_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PartDocument ADD  CONSTRAINT DF_PartDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE PartDocument ADD  CONSTRAINT DF_PartDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE PartDocument ADD  CONSTRAINT DF_PartDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PartDocument ADD  CONSTRAINT DF_PartDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_Stock  DEFAULT (1) FOR Stock;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_MinIssueUnitQty  DEFAULT (0) FOR MinIssueUnitQty;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_MaxIssueUnitQty  DEFAULT (0) FOR MaxIssueUnitQty;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_ReorderOrderUnitQty  DEFAULT (0) FOR ReorderOrderUnitQty;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_ConversionToIssueUnits  DEFAULT (1) FOR ConversionToIssueUnits;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_Available  DEFAULT (0) FOR Available;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_Reserved  DEFAULT (0) FOR Reserved;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_OnHand  DEFAULT (0) FOR OnHand;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_OnOrder  DEFAULT (0) FOR OnOrder;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_OnOrderOpenPO  DEFAULT (0) FOR OnOrderOpenPO;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_AverageOrderUnitPrice  DEFAULT (0) FOR AverageOrderUnitPrice;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_LastOrderUnitPrice  DEFAULT (0) FOR LastOrderUnitPrice;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_IssueUnitCost  DEFAULT (0) FOR IssueUnitCost;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_IssueUnitChargePrice  DEFAULT (0) FOR IssueUnitChargePrice;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_IssueUnitChargePercentage  DEFAULT (0) FOR IssueUnitChargePercentage;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PartLocation ADD  CONSTRAINT DF_PartLocation_AverageOrderUnitPriceStatic  DEFAULT (0) FOR AverageOrderUnitPriceStatic;
ALTER TABLE PartNote ADD  CONSTRAINT DF_PartNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE PartNote ADD  CONSTRAINT DF_PartNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE PartNote ADD  CONSTRAINT DF_PartNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE PartNote ADD  CONSTRAINT DF_PartNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PartPendingRecalc ADD  CONSTRAINT DF_PartPendingRecalc_ReCalcOnOrder  DEFAULT (0) FOR ReCalcOnOrder;
ALTER TABLE PartPendingRecalc ADD  CONSTRAINT DF_PartPendingRecalc_ReCalcOnOrderOpenPO  DEFAULT (0) FOR ReCalcOnOrderOpenPO;
ALTER TABLE PartPendingRecalc ADD  CONSTRAINT DF_PartPendingRecalc_ReCalcReserved  DEFAULT (0) FOR ReCalcReserved;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_ValueNumeric  DEFAULT (0) FOR ValueNumeric;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_ValueHi  DEFAULT (0) FOR ValueHi;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_ValueLow  DEFAULT (0) FOR ValueLow;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_ValueOutOfRangeWO  DEFAULT (0) FOR ValueOutOfRangeWO;
ALTER TABLE PartSpecification ADD  CONSTRAINT DF_PartSpecification_WOGenerated  DEFAULT (0) FOR WOGenerated;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_Qty  DEFAULT (0) FOR Qty;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_UDFBit21  DEFAULT (0) FOR UDFBit3;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_ConversionToIssueUnits  DEFAULT (1) FOR ConversionToIssueUnits;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_OrderUnitQty  DEFAULT (0) FOR OrderUnitQty;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_OrderUnitPrice  DEFAULT (0) FOR OrderUnitPrice;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_OrderUnitQtyIssued  DEFAULT (0) FOR QtyIssuedFIFOLIFO;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_OrderUnitQtyAvailable  DEFAULT (0) FOR QtyAvailableFIFOLIFO;
ALTER TABLE PartTransaction ADD  CONSTRAINT DF_PartTransaction_OrderUnitQtyReserved  DEFAULT (0) FOR QtyReservedFIFOLIFO;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_Priority  DEFAULT (1) FOR Priority;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_ConversionToIssueUnits  DEFAULT (1) FOR ConversionToIssueUnits;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_OrderUnitPrice  DEFAULT (0) FOR OrderUnitPrice;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_Discount  DEFAULT (0) FOR Discount;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_IsTax  DEFAULT (0) FOR IsTax;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_TaxRate  DEFAULT (0) FOR TaxRate;
ALTER TABLE PartVendor ADD  CONSTRAINT DF_PartVendor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PhoneGroup ADD  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_AverageHours  DEFAULT (0) FOR AverageHours;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_TimesDone  DEFAULT (0) FOR TimesDone;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_TimesCounter  DEFAULT (0) FOR TimesCounter;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_ShutdownBox  DEFAULT (0) FOR ShutdownBox;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_LockoutTagoutBox  DEFAULT (0) FOR LockoutTagoutBox;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_AttachmentsBox  DEFAULT (0) FOR AttachmentsBox;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_Chargeable  DEFAULT (0) FOR Chargeable;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_RecalcSchedule  DEFAULT (0) FOR RecalcSchedule;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_IsScheduled  DEFAULT (0) FOR IsScheduled;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_MustBeInService  DEFAULT (0) FOR AssetStatusRequirement;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_NumberInterval  DEFAULT (1) FOR NumberInterval;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_DayOfWeek  DEFAULT (1) FOR DayOfWeek;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_DayOfMonth  DEFAULT (1) FOR DayOfMonth;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_WeekOfMonth  DEFAULT (1) FOR WeekOfMonth;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_MonthOfYear  DEFAULT (1) FOR MonthOfYear;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_MonthlyChoice  DEFAULT (0) FOR MonthlyChoice;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_YearlyChoice  DEFAULT (0) FOR YearlyChoice;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnSunday  DEFAULT (0) FOR OnSunday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnMonday  DEFAULT (0) FOR OnMonday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnTuesday  DEFAULT (0) FOR OnTuesday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnWednesday  DEFAULT (0) FOR OnWednesday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnThursday  DEFAULT (0) FOR OnThursday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnFriday  DEFAULT (0) FOR OnFriday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnSaturday  DEFAULT (0) FOR OnSaturday;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_OnAny  DEFAULT (0) FOR OnAny;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseLastCompleted  DEFAULT (0) FOR UseLastCompleted;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_StartAfter  DEFAULT (1) FOR StartAfter;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_DateRangeType  DEFAULT (0) FOR DateRangeType;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_EndOccurrences  DEFAULT (0) FOR EndOccurrences;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_NotifyOnEnd  DEFAULT (0) FOR NotifyOnEnd;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_ScheduleDisabled  DEFAULT (0) FOR ScheduleDisabled;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_Meter1Interval  DEFAULT (0) FOR Meter1Interval;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_Meter2Interval  DEFAULT (0) FOR Meter2Interval;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_AutomationMethod  DEFAULT (0) FOR AutomationMethod;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_AutomationDaysPrior  DEFAULT (0) FOR AutomationDaysPrior;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_ASAutomationMethod  DEFAULT (0) FOR ASAutomationMethod;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_Autofilter_RecordType  DEFAULT (0) FOR Autofilter_RecordType;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_WOGroup  DEFAULT (0) FOR WOGroup;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_WOGroupRC  DEFAULT (0) FOR WOGroupRC;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_WOGroupManageAsSingle  DEFAULT (0) FOR WOGroupManageAsSingle;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_CostTotalEstimated  DEFAULT (0) FOR CostTotalEstimated;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_CostPartEstimated  DEFAULT (0) FOR CostPartEstimated;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_CostMiscEstimated  DEFAULT (0) FOR CostMiscEstimated;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_CostLaborEstimated  DEFAULT (0) FOR CostLaborEstimated;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_CostLaborEmployeeEstimated  DEFAULT (0) FOR CostLaborEmployeeEstimated;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_CostLaborContractorEstimated  DEFAULT (0) FOR CostLaborContractorEstimated;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_Active  DEFAULT (1) FOR Active;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_ScheduleType  DEFAULT (0) FOR ScheduleType;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseLastCompletedMeter  DEFAULT (0) FOR UseLastCompleted2;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaults  DEFAULT (0) FOR UseAssetDefaults;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaultsRC  DEFAULT (0) FOR UseAssetDefaultsRC;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaultsSH  DEFAULT (0) FOR UseAssetDefaultsSH;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaultsSH1  DEFAULT (0) FOR UseAssetDefaultsSU;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaultsAC  DEFAULT (0) FOR UseAssetDefaultsAC;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaultsDP  DEFAULT (0) FOR UseAssetDefaultsDP;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_UseAssetDefaultsTN  DEFAULT (0) FOR UseAssetDefaultsTN;
ALTER TABLE PM ADD  CONSTRAINT DF_PM_AutomationDaysPriorFromProcedure  DEFAULT ((0)) FOR AutomationDaysPriorFromProcedure;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_RouteOrder  DEFAULT (0) FOR RouteOrder;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_TimesCounter  DEFAULT (0) FOR TimesCounter;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_PMEnded  DEFAULT (0) FOR PMEnded;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_TimesDone  DEFAULT (0) FOR TimesDone;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_Meter1ReadingLastInterval  DEFAULT (0) FOR Meter1ReadingLastInterval;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_Meter1NextInterval  DEFAULT (0) FOR Meter1NextInterval;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_Meter2ReadingLastInterval  DEFAULT (0) FOR Meter2ReadingLastInterval;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_Meter2NextInterval  DEFAULT (0) FOR Meter2NextInterval;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_ScheduleDisabled  DEFAULT (0) FOR ScheduleDisabled;
ALTER TABLE PMAsset ADD  CONSTRAINT DF_PMAsset_AutomationDaysPrior  DEFAULT ((0)) FOR AutomationDaysPrior;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_RouteOrder  DEFAULT ((0)) FOR RouteOrder;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_PMCounter  DEFAULT ((1)) FOR PMCounter;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_TimesCounter  DEFAULT ((0)) FOR TimesCounter;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_PMEnded  DEFAULT ((0)) FOR PMEnded;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_TimesDone  DEFAULT ((0)) FOR TimesDone;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_Meter1ReadingLastInterval  DEFAULT ((0)) FOR Meter1ReadingLastInterval;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_Meter1NextInterval  DEFAULT ((0)) FOR Meter1NextInterval;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_Meter2ReadingLastInterval  DEFAULT ((0)) FOR Meter2ReadingLastInterval;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_Meter2NextInterval  DEFAULT ((0)) FOR Meter2NextInterval;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_ScheduleDisabled  DEFAULT ((0)) FOR ScheduleDisabled;
ALTER TABLE PMAssetTemp ADD  CONSTRAINT DF_PMAssetTemp_AutomationDaysPrior  DEFAULT ((0)) FOR AutomationDaysPrior;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_TimesCounter  DEFAULT (0) FOR TimesCounter;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_PMEnded  DEFAULT (0) FOR PMEnded;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_TimesDone  DEFAULT (0) FOR TimesDone;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_Meter1ReadingLastInterval  DEFAULT (0) FOR Meter1ReadingLastInterval;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_Meter1NextInterval  DEFAULT (0) FOR Meter1NextInterval;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_Meter2ReadingLastInterval  DEFAULT (0) FOR Meter2ReadingLastInterval;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_Meter2NextInterval  DEFAULT (0) FOR Meter2NextInterval;
ALTER TABLE PMAssetUndo ADD  CONSTRAINT DF_PMAssetUndo_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PMDocument ADD  CONSTRAINT DF_PMDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE PMDocument ADD  CONSTRAINT DF_PMDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE PMDocument ADD  CONSTRAINT DF_PMDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PMDocument ADD  CONSTRAINT DF_PMDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE PMNote ADD  CONSTRAINT DF_PMNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE PMNote ADD  CONSTRAINT DF_PMNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE PMNote ADD  CONSTRAINT DF_PMNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE PMNote ADD  CONSTRAINT DF_PMNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_PMType  DEFAULT (N'RECUR') FOR PMType;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_PMTypeDesc  DEFAULT (N'Recurring Procedures') FOR PMTypeDesc;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_OneTimeDateGenerated  DEFAULT (0) FOR OneTimeDateGenerated;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_Meter1  DEFAULT (0) FOR Meter1;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_Meter1Generated  DEFAULT (0) FOR Meter1Generated;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_Meter2  DEFAULT (0) FOR Meter2;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_Meter2Generated  DEFAULT (0) FOR Meter2Generated;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PMProcedure ADD  CONSTRAINT DF_PMProcedure_AutomationDaysPrior  DEFAULT ((0)) FOR AutomationDaysPrior;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_GenerateDate  DEFAULT (getdate()) FOR GenerateDate;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_GenDateOption  DEFAULT (0) FOR GenDateOption;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_AutoCreate  DEFAULT (0) FOR AutoCreate;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_Undo  DEFAULT (0) FOR Undo;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_Processing  DEFAULT (0) FOR Processing;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_WOCount  DEFAULT (0) FOR WOCount;
ALTER TABLE PMUndo ADD  CONSTRAINT DF_PMUndo_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_AuthLevelsRequired  DEFAULT (0) FOR AuthLevelsRequired;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_TargetDate  DEFAULT (getdate()) FOR TargetDate;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_TargetHours  DEFAULT (1) FOR TargetHours;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_LastSurveyDate  DEFAULT (getdate()) FOR LastSurveyDate;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_WarrantyBox  DEFAULT (0) FOR WarrantyBox;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_ShutdownBox  DEFAULT (0) FOR ShutdownBox;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_LockoutTagoutBox  DEFAULT (0) FOR LockoutTagoutBox;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_AttachmentsBox  DEFAULT (0) FOR AttachmentsBox;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_PrintedBox  DEFAULT (0) FOR PrintedBox;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_FailedPredictive  DEFAULT (0) FOR FailedPredictive;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_Requested  DEFAULT (getdate()) FOR Requested;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_IsOpen  DEFAULT (1) FOR IsOpen;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_IsApproved  DEFAULT (0) FOR IsApproved;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_PDAWO  DEFAULT (0) FOR PDAPredictive;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Predictive ADD  CONSTRAINT DF_Predictive_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PredictiveNote ADD  CONSTRAINT DF_PredictiveNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE PredictiveNote ADD  CONSTRAINT DF_PredictiveNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PredictiveNote ADD  CONSTRAINT DF_PredictiveNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE PredictiveNote ADD  CONSTRAINT DF_PredictiveNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE PredictivePhoto ADD  CONSTRAINT DF_PredictivePhoto_PhotoDate  DEFAULT (getdate()) FOR PhotoDate;
ALTER TABLE PredictivePhoto ADD  CONSTRAINT DF_PredictivePhoto_Active  DEFAULT (0) FOR Active;
ALTER TABLE PredictivePhoto ADD  CONSTRAINT DF_PredictivePhoto_Followup  DEFAULT (0) FOR Followup;
ALTER TABLE PredictivePhoto ADD  CONSTRAINT DF_PredictivePhoto_ReportOrder  DEFAULT (0) FOR ReportOrder;
ALTER TABLE PredictivePhoto ADD  CONSTRAINT DF_PredictivePhoto_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PredictiveStatusHistory ADD  CONSTRAINT DF_PredictiveStatusHistory_IsAuthStatus  DEFAULT (0) FOR IsAuthStatus;
ALTER TABLE PredictiveStatusHistory ADD  CONSTRAINT DF_PredictiveStatusHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_Complete  DEFAULT (0) FOR Complete;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_Fail  DEFAULT (0) FOR Fail;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_Spec  DEFAULT (0) FOR Spec;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_Header  DEFAULT (0) FOR Header;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_Meter1  DEFAULT (0) FOR Meter1;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_Meter2  DEFAULT (0) FOR Meter2;
ALTER TABLE PredictiveTask ADD  CONSTRAINT DF_PredictiveTask_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Preference ADD  CONSTRAINT DF_Preference_IsCustomForClient  DEFAULT (0) FOR IsCustomForClient;
ALTER TABLE Preference ADD  CONSTRAINT DF_Preference_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PreferenceCategory ADD  CONSTRAINT DF_PreferenceCategory_DisplayOrder  DEFAULT (0) FOR DisplayOrder;
ALTER TABLE PreferenceCategory ADD  CONSTRAINT DF_PreferenceCategory_IsCustomForClient  DEFAULT (0) FOR IsCustomForClient;
ALTER TABLE PreferenceCategory ADD  CONSTRAINT DF_PreferenceCategory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PreferenceModule ADD  CONSTRAINT DF_PreferenceModule_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureDocument ADD  CONSTRAINT DF_ProcedureDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE ProcedureDocument ADD  CONSTRAINT DF_ProcedureDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE ProcedureDocument ADD  CONSTRAINT DF_ProcedureDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureDocument ADD  CONSTRAINT DF_ProcedureDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_EstimatedHours  DEFAULT (0) FOR EstimatedHours;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_RegularHours  DEFAULT (0) FOR RegularHours;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_OvertimeHours  DEFAULT (0) FOR OvertimeHours;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_OtherHours  DEFAULT (0) FOR OtherHours;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_TotalHours  DEFAULT (0) FOR TotalHours;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_WorkDate  DEFAULT (convert(datetime,convert(varchar,getdate(),1))) FOR WorkDate;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_RegularCost  DEFAULT (0) FOR RegularCost;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_OvertimeCost  DEFAULT (0) FOR OvertimeCost;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_OtherCost  DEFAULT (0) FOR OtherCost;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_TotalCost  DEFAULT (0) FOR TotalCost;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_CostRegular  DEFAULT (0) FOR CostRegular;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_CostOvertime  DEFAULT (0) FOR CostOvertime;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_CostOther  DEFAULT (0) FOR CostOther;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_ChargeRate  DEFAULT (0) FOR ChargeRate;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE ProcedureLabor ADD  CONSTRAINT DF_ProcedureLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ShutdownBox  DEFAULT (0) FOR ShutdownBox;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_LockoutTagoutBox  DEFAULT (0) FOR LockoutTagoutBox;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_AttachmentsBox  DEFAULT (0) FOR AttachmentsBox;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_SurveyBox  DEFAULT (0) FOR SurveyBox;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_Chargeable  DEFAULT (0) FOR Chargeable;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_CostTotalEstimated  DEFAULT (0) FOR CostTotalEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_CostPartEstimated  DEFAULT (0) FOR CostPartEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_CostMiscEstimated  DEFAULT (0) FOR CostMiscEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_CostLaborEstimated  DEFAULT (0) FOR CostLaborEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_CostEmployeeEstimated  DEFAULT (0) FOR CostEmployeeEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_CostContractorEstimated  DEFAULT (0) FOR CostContractorEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ChargeLaborEstimated  DEFAULT (0) FOR ChargeLaborEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ChargeEmployeeEstimated  DEFAULT (0) FOR ChargeEmployeeEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ChargeContractorEstimated  DEFAULT (0) FOR ChargeContractorEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ChargePartEstimated  DEFAULT (0) FOR ChargePartEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ChargeMiscEstimated  DEFAULT (0) FOR ChargeMiscEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ChargeTotalEstimated  DEFAULT (0) FOR ChargeTotalEstimated;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_Active  DEFAULT (1) FOR Active;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_AvailableToRequester  DEFAULT (0) FOR AvailableToRequester;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_AvailableToRequester1  DEFAULT (0) FOR AvailableToRequester1;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_ShareID  DEFAULT ('') FOR ShareID;
ALTER TABLE ProcedureLibrary ADD  CONSTRAINT DF_ProcedureLibrary_RemoteID  DEFAULT ('') FOR ShareRemoteID;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_MiscCostDate  DEFAULT (convert(datetime,convert(varchar,getdate(),1))) FOR MiscCostDate;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_EstimatedCost  DEFAULT (0) FOR EstimatedCost;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_ActualCost  DEFAULT (0) FOR ActualCost;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE ProcedureMiscCost ADD  CONSTRAINT DF_ProcedureMiscCost_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureNote ADD  CONSTRAINT DF_ProcedureNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE ProcedureNote ADD  CONSTRAINT DF_ProcedureNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE ProcedureNote ADD  CONSTRAINT DF_ProcedureNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE ProcedureNote ADD  CONSTRAINT DF_ProcedureNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_DirectIssue  DEFAULT (0) FOR DirectIssue;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_OutOfPocket  DEFAULT (0) FOR OutOfPocket;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_QuantityEstimated  DEFAULT (0) FOR QuantityEstimated;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_QuantityActual  DEFAULT (0) FOR QuantityActual;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_IssueUnitCost  DEFAULT (0) FOR IssueUnitCost;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_IssueUnitChargePrice  DEFAULT (0) FOR IssueUnitChargePrice;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_IssueUnitChargePercentage  DEFAULT (0) FOR IssueUnitChargePercentage;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_OtherCost  DEFAULT (0) FOR OtherCost;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_TotalCost  DEFAULT (0) FOR TotalCost;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE ProcedurePart ADD  CONSTRAINT DF_ProcedurePart_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureProcedure ADD  CONSTRAINT DF_ProcedureProcedure_ProcedureOrder  DEFAULT (1) FOR ProcedureSequence;
ALTER TABLE ProcedureProcedure ADD  CONSTRAINT DF_ProcedureProcedure_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureTask ADD  CONSTRAINT DF_ProcedureTask_Spec  DEFAULT (0) FOR Spec;
ALTER TABLE ProcedureTask ADD  CONSTRAINT DF_ProcedureTask_Header  DEFAULT (0) FOR Header;
ALTER TABLE ProcedureTask ADD  CONSTRAINT DF_ProcedureTask_Meter1  DEFAULT (0) FOR Meter1;
ALTER TABLE ProcedureTask ADD  CONSTRAINT DF_ProcedureTask_Meter2  DEFAULT (0) FOR Meter2;
ALTER TABLE ProcedureTask ADD  CONSTRAINT DF_ProcedureTask_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProcedureTool ADD  CONSTRAINT DF_ProcedureTool_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE ProcedureTool ADD  CONSTRAINT DF_ProcedureTool_QuantityEstimated  DEFAULT (0) FOR QuantityEstimated;
ALTER TABLE ProcedureTool ADD  CONSTRAINT DF_ProcedureTool_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_AuthLevelsRequired  DEFAULT (0) FOR AuthLevelsRequired;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_TargetDate  DEFAULT (getdate()) FOR TargetDate;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_Requested  DEFAULT (getdate()) FOR Requested;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_IsOpen  DEFAULT (1) FOR IsOpen;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_IsApproved  DEFAULT (0) FOR IsApproved;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_CostRegular  DEFAULT (0) FOR CostRegular;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_CostOvertime  DEFAULT (0) FOR CostOvertime;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_CostOther  DEFAULT (0) FOR CostOther;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_ChargeRate  DEFAULT (0) FOR ChargeRate;
ALTER TABLE Project ADD  CONSTRAINT DF_Project_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE ProjectAsset ADD  CONSTRAINT DF_ProjectAsset_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProjectDocument ADD  CONSTRAINT DF_ProjectDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE ProjectDocument ADD  CONSTRAINT DF_ProjectDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE ProjectDocument ADD  CONSTRAINT DF_ProjectDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProjectDocument ADD  CONSTRAINT DF_ProjectDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE Projection ADD  CONSTRAINT DF_Projection_GenerateDate  DEFAULT (getdate()) FOR GenerateDate;
ALTER TABLE Projection ADD  CONSTRAINT DF_Projection_GenDateOption  DEFAULT (0) FOR GenDateOption;
ALTER TABLE Projection ADD  CONSTRAINT DF_Projection_WOCount  DEFAULT (0) FOR WOCount;
ALTER TABLE Projection ADD  CONSTRAINT DF_Projection_IsPreview  DEFAULT (0) FOR IsPreview;
ALTER TABLE Projection ADD  CONSTRAINT DF_Projection_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProjectNote ADD  CONSTRAINT DF_ProjectNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE ProjectNote ADD  CONSTRAINT DF_ProjectNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE ProjectNote ADD  CONSTRAINT DF_ProjectNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE ProjectNote ADD  CONSTRAINT DF_ProjectNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ProjectStatusHistory ADD  CONSTRAINT DF_ProjectStatusHistory_IsAuthStatus  DEFAULT (0) FOR IsAuthStatus;
ALTER TABLE ProjectStatusHistory ADD  CONSTRAINT DF_ProjectStatusHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_AuthLevelsRequired  DEFAULT (0) FOR AuthLevelsRequired;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_PODate  DEFAULT (getdate()) FOR PODate;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_Subtotal  DEFAULT (0) FOR Subtotal;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_FreightCharge  DEFAULT (0) FOR FreightCharge;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_TaxAmount  DEFAULT (0) FOR TaxAmount;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_Total  DEFAULT (0) FOR Total;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_Requested  DEFAULT (getdate()) FOR Requested;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_IsOpen  DEFAULT (1) FOR IsOpen;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_IsApproved  DEFAULT (0) FOR IsApproved;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_IsPartsOrdered  DEFAULT (0) FOR IsPartsOrdered;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_ChargePercentage  DEFAULT (0) FOR DiscountPercentage;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_ChargeRate  DEFAULT (0) FOR Discount;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_SubTotalPreDiscount  DEFAULT (0) FOR SubTotalPreDiscount;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_SubTotalPreDiscount1  DEFAULT (0) FOR SubTotalPostDiscount;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_PrintedBox  DEFAULT (0) FOR PrintedBox;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF_PurchaseOrder_IsAveraged  DEFAULT (0) FOR IsAveraged;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF__PurchaseO__UDFBi__39444182  DEFAULT ((0)) FOR UDFBit3;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF__PurchaseO__UDFBi__3A3865BB  DEFAULT ((0)) FOR UDFBit4;
ALTER TABLE PurchaseOrder ADD  CONSTRAINT DF__PurchaseO__UDFBi__3B2C89F4  DEFAULT ((0)) FOR UDFBit5;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_LineItemNo  DEFAULT (0) FOR LineItemNo;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_DirectIssue  DEFAULT (0) FOR DirectIssue;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_ConversionToIssueUnits  DEFAULT (1) FOR ConversionToIssueUnits;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_OrderUnitPrice  DEFAULT (0) FOR OrderUnitPrice;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_OrderUnitQty  DEFAULT (0) FOR OrderUnitQty;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_OrderUnitQtyBackOrdered  DEFAULT (0) FOR OrderUnitQtyBackOrdered;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_OrderUnitQtyReceived  DEFAULT (0) FOR OrderUnitQtyReceived;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_Discount  DEFAULT (0) FOR Discount;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_Subtotal  DEFAULT (0) FOR Subtotal;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_IsTax  DEFAULT (0) FOR IsTax;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_TaxRate  DEFAULT (0) FOR TaxRate;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_TaxAmount  DEFAULT (0) FOR TaxAmount;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_LineItemTotal  DEFAULT (0) FOR LineItemTotal;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_FreightCharge  DEFAULT (0) FOR FreightCharge;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_OrderUnitQtyCanceled  DEFAULT (0) FOR OrderUnitQtyCanceled;
ALTER TABLE PurchaseOrderDetail ADD  CONSTRAINT DF_PurchaseOrderDetail_ReceiveDate  DEFAULT (getdate()) FOR ReceiveDate;
ALTER TABLE PurchaseOrderDocument ADD  CONSTRAINT DF_PurchaseOrderDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE PurchaseOrderDocument ADD  CONSTRAINT DF_PurchaseOrderDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE PurchaseOrderDocument ADD  CONSTRAINT DF_PurchaseOrderDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderDocument ADD  CONSTRAINT DF_PurchaseOrderDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_Subtotal  DEFAULT (0) FOR Subtotal;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_FreightCharge  DEFAULT (0) FOR FreightCharge;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_TaxAmount  DEFAULT (0) FOR TaxAmount;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_Total  DEFAULT (0) FOR Total;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_RcptTrans  DEFAULT (0) FOR RcptTrans;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_InvTrans  DEFAULT (0) FOR InvTrans;
ALTER TABLE PurchaseOrderInvoice ADD  CONSTRAINT DF_PurchaseOrderInvoice_CanNotDelete  DEFAULT (0) FOR CanNotDelete;
ALTER TABLE PurchaseOrderInvoiceDetail ADD  CONSTRAINT DF_PurchaseOrderInvoiceDetail_OrderUnitQtyReceived  DEFAULT (0) FOR OrderUnitQtyReceived;
ALTER TABLE PurchaseOrderInvoiceDetail ADD  CONSTRAINT DF_PurchaseOrderInvoiceDetail_OrderUnitQtyBackOrdered  DEFAULT (0) FOR OrderUnitQtyBackOrdered;
ALTER TABLE PurchaseOrderInvoiceDetail ADD  CONSTRAINT DF_PurchaseOrderInvoiceDetail_OrderUnitQtyCanceled  DEFAULT (0) FOR OrderUnitQtyCanceled;
ALTER TABLE PurchaseOrderInvoiceDetail ADD  CONSTRAINT DF_PurchaseOrderInvoiceDetail_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderInvoiceDetail ADD  CONSTRAINT DF_PurchaseOrderInvoiceDetail_ReceiveDate  DEFAULT (getdate()) FOR ReceiveDate;
ALTER TABLE PurchaseOrderNote ADD  CONSTRAINT DF_PONote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE PurchaseOrderNote ADD  CONSTRAINT DF_PONote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE PurchaseOrderNote ADD  CONSTRAINT DF_PONote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE PurchaseOrderNote ADD  CONSTRAINT DF_PONote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_OrderPart  DEFAULT (0) FOR OrderPart;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_DirectIssue  DEFAULT (0) FOR DirectIssue;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_ConversionToIssueUnits  DEFAULT (1) FOR ConversionToIssueUnits;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_OrderUnitPrice  DEFAULT (0) FOR OrderUnitPrice;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_OrderUnitQty  DEFAULT (0) FOR OrderUnitQty;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_Discount  DEFAULT (0) FOR Discount;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_Subtotal  DEFAULT (0) FOR Subtotal;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_IsTax  DEFAULT (0) FOR IsTax;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_TaxRate  DEFAULT (0) FOR TaxRate;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_TaxAmount  DEFAULT (0) FOR TaxAmount;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_LineItemTotal  DEFAULT (0) FOR LineItemTotal;
ALTER TABLE PurchaseOrderPending ADD  CONSTRAINT DF_PurchaseOrderPending_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_ReceiveDate  DEFAULT (getdate()) FOR ReceiveDate;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_OrderUnitPrice  DEFAULT (0) FOR OrderUnitPrice;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_OrderUnitQtyReceived  DEFAULT (0) FOR OrderUnitQtyReceived;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_OrderUnitQtyIssued  DEFAULT (0) FOR OrderUnitQtyIssued;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_OrderUnitQtyAvailable  DEFAULT (0) FOR OrderUnitQtyAvailable;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderReceive ADD  CONSTRAINT DF_PurchaseOrderReceive_OrderUnitQtyReserved  DEFAULT (0) FOR OrderUnitQtyReserved;
ALTER TABLE PurchaseOrderRMA ADD  CONSTRAINT DF_PurchaseOrderRMA_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderRMADetail ADD  CONSTRAINT DF_PurchaseOrderRMADetail_OrderUnitQtyReturned  DEFAULT (0) FOR OrderUnitQtyReturned;
ALTER TABLE PurchaseOrderRMADetail ADD  CONSTRAINT DF_PurchaseOrderRMADetail_OrderUnitQtyCanceled  DEFAULT (0) FOR OrderUnitQtyCanceled;
ALTER TABLE PurchaseOrderRMADetail ADD  CONSTRAINT DF_PurchaseOrderRMADetail_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderStatusHistory ADD  CONSTRAINT DF_PurchaseOrderStatusHistory_IsAuthStatus  DEFAULT (0) FOR IsAuthStatus;
ALTER TABLE PurchaseOrderStatusHistory ADD  CONSTRAINT DF_PurchaseOrderStatusHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE PurchaseOrderStatusHistory ADD  CONSTRAINT DF_PurchaseOrderStatusHistory_ApprovalCountCompleted  DEFAULT (0) FOR ApprovalCountCompleted;
ALTER TABLE Recent ADD  CONSTRAINT DF_Recent_user_guid  DEFAULT ('') FOR user_guid;
ALTER TABLE Recent ADD  CONSTRAINT DF_Recent_tablename  DEFAULT ('') FOR tablename;
ALTER TABLE Recent ADD  CONSTRAINT DF_Recent_rowpk  DEFAULT ((0)) FOR rowpk;
ALTER TABLE Recent ADD  CONSTRAINT DF_Recent_recent_timestamp  DEFAULT (getdate()) FOR recent_timestamp;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_Active  DEFAULT (1) FOR Active;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_DefaultRepairCenter  DEFAULT (0) FOR DefaultRepairCenter;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_RequesterCanJoin  DEFAULT (0) FOR RequesterCanJoin;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF_RepairCenter_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF__RepairCen__Appro__5AB813AB  DEFAULT ((0)) FOR ApprovalType;
ALTER TABLE RepairCenter ADD  CONSTRAINT DF__RepairCen__POApp__3CF2A69A  DEFAULT ((0)) FOR POApprovalType;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Repai__34949AFE  DEFAULT ((0)) FOR RepairCenterPK;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Repai__3588BF37  DEFAULT ('') FOR RepairCenterID;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Modul__367CE370  DEFAULT ('') FOR ModuleID;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Appro__377107A9  DEFAULT ((0)) FOR ApprovalType;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Appro__38652BE2  DEFAULT ((0)) FOR ApprovalLevel;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Amoun__3959501B  DEFAULT ((0)) FOR AmountA;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Amoun__3A4D7454  DEFAULT ((0)) FOR AmountB;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Condi__3B41988D  DEFAULT ('=') FOR Condition;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Requi__3C35BCC6  DEFAULT ((1)) FOR RequiredApprovals;
ALTER TABLE RepairCenterApproval ADD  CONSTRAINT DF__RepairCen__Rowve__3D29E0FF  DEFAULT (getdate()) FOR RowversionDate;
ALTER TABLE RepairCenterDocument ADD  CONSTRAINT DF_RepairCenterDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE RepairCenterDocument ADD  CONSTRAINT DF_RepairCenterDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE RepairCenterDocument ADD  CONSTRAINT DF_RepairCenterDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE RepairCenterDocument ADD  CONSTRAINT DF_RepairCenterDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE RepairCenterNote ADD  CONSTRAINT DF_RepairCenterNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE RepairCenterNote ADD  CONSTRAINT DF_RepairCenterNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE RepairCenterNote ADD  CONSTRAINT DF_RepairCenterNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE RepairCenterNote ADD  CONSTRAINT DF_RepairCenterNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE RepairCenterPreference ADD  CONSTRAINT DF_RepairCenterPreference_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Report_ReportGroup ADD  CONSTRAINT DF_Report_ReportGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportAccessGroup ADD  CONSTRAINT DF_ReportAccessGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__Chart__2181C68A  DEFAULT (left(lower(replace(newid(),'-','')),(20))) FOR ChartID;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__Chart__2275EAC3  DEFAULT ('Chart1') FOR ChartAreaID;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__Repor__236A0EFC  DEFAULT ((0)) FOR ReportPK;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__Group__245E3335  DEFAULT ('') FOR GroupField;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__DataF__2552576E  DEFAULT ('') FOR DataFields;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__DataA__26467BA7  DEFAULT ('SUM') FOR DataAggregate;
ALTER TABLE ReportCharting ADD  CONSTRAINT DF__ReportCha__Rowve__273A9FE0  DEFAULT (getdate()) FOR RowversionDate;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__Chart__17F85C50  DEFAULT ((0)) FOR ChartPK;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__Chart__18EC8089  DEFAULT ('chart1') FOR ChartAreaID;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__Repor__19E0A4C2  DEFAULT ((0)) FOR ReportPK;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__DataA__1AD4C8FB  DEFAULT ('') FOR DataAlias;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__DataF__1BC8ED34  DEFAULT ('') FOR DataField;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__DataA__1CBD116D  DEFAULT ('SUM') FOR DataAggregate;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__DataC__1DB135A6  DEFAULT ((0)) FOR DataColor;
ALTER TABLE ReportChartingData ADD  CONSTRAINT DF__ReportCha__Rowve__1EA559DF  DEFAULT (getdate()) FOR RowversionDate;
ALTER TABLE ReportCriteria ADD  CONSTRAINT DF_ReportCriteria_isMulti  DEFAULT (0) FOR isMulti;
ALTER TABLE ReportCriteria ADD  CONSTRAINT DF_ReportCriteria_AskLater  DEFAULT (1) FOR AskLater;
ALTER TABLE ReportCriteria ADD  CONSTRAINT DF_ReportCriteria_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportCriteria ADD  CONSTRAINT DF__ReportCri__Crite__58CFCB39  DEFAULT ('') FOR CriteriaHTML;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_DisplayOrder  DEFAULT (0) FOR DisplayOrder;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_Display  DEFAULT (1) FOR Display;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_NotUserSelectable  DEFAULT (0) FOR NotUserSelectable;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_TotalIfSelected  DEFAULT (0) FOR TotalIfSelected;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_BlankLineIfSelected  DEFAULT (0) FOR BlankLineIfSelected;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_UseCustomExpression  DEFAULT (0) FOR UseCustomExpression;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportFields ADD  CONSTRAINT DF_ReportFields_GroupByCustomExpression  DEFAULT (1) FOR GroupByCustomExpression;
ALTER TABLE ReportGroup ADD  CONSTRAINT DF_ReportGroup_IsUserGroup  DEFAULT (0) FOR IsUserGroup;
ALTER TABLE ReportGroup ADD  CONSTRAINT DF_ReportGroup_IsBatchGroup  DEFAULT (0) FOR IsBatchGroup;
ALTER TABLE ReportGroup ADD  CONSTRAINT DF_ReportGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportGroup ADD  CONSTRAINT DF_ReportGroup_PrivateGroup  DEFAULT ((0)) FOR PrivateGroup;
ALTER TABLE ReportGroup ADD  CONSTRAINT DF_ReportGroup_FavoriteGroup  DEFAULT ((0)) FOR FavoriteGroup;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Sort1DESC  DEFAULT (0) FOR Sort1DESC;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Sort2DESC  DEFAULT (0) FOR Sort2DESC;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Sort3DESC  DEFAULT (0) FOR Sort3DESC;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Sort4DESC  DEFAULT (0) FOR Sort4DESC;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Sort5DESC  DEFAULT (0) FOR Sort5DESC;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Group1  DEFAULT (0) FOR Group1;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Group2  DEFAULT (0) FOR Group2;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Group3  DEFAULT (0) FOR Group3;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Group4  DEFAULT (0) FOR Group4;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Group5  DEFAULT (0) FOR Group5;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Header1  DEFAULT (0) FOR Header1;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Header2  DEFAULT (0) FOR Header2;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Header3  DEFAULT (0) FOR Header3;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Header4  DEFAULT (0) FOR Header4;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Header5  DEFAULT (0) FOR Header5;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total1  DEFAULT (0) FOR Total1;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total2  DEFAULT (0) FOR Total2;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total3  DEFAULT (0) FOR Total3;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total4  DEFAULT (0) FOR Total4;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total5  DEFAULT (0) FOR Total5;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_ReportFile  DEFAULT ('rpt_generic1.asp') FOR ReportFile;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_GroupBy  DEFAULT (0) FOR GroupBy;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_hits  DEFAULT (0) FOR hits;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Sequence  DEFAULT (0) FOR Sequence;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Layout  DEFAULT ('hor') FOR Layout;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_VertCols  DEFAULT (1) FOR VertCols;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_PageBreakEachRecord  DEFAULT (0) FOR PageBreakEachRecord;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Custom  DEFAULT (0) FOR Custom;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_ReportCopy  DEFAULT (0) FOR ReportCopy;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_MCRegistrationDB  DEFAULT (0) FOR MCRegistrationDB;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_PrintCriteria  DEFAULT (0) FOR PrintCriteria;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Active  DEFAULT (1) FOR Active;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_UsedFor  DEFAULT ('Reports') FOR UsedFor;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total11  DEFAULT (0) FOR PB1;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total21  DEFAULT (0) FOR PB2;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total31  DEFAULT (0) FOR PB3;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total41  DEFAULT (0) FOR PB4;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_Total51  DEFAULT (0) FOR PB5;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_PhotoCriteria  DEFAULT (1) FOR PhotoCriteria;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_ShowPivotBar  DEFAULT (0) FOR DisplayPivotBar;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_ReportShareID  DEFAULT ('') FOR ReportShareID;
ALTER TABLE Reports ADD  CONSTRAINT DF_Reports_ReportRemoteID  DEFAULT ('') FOR ShareRemoteID;
ALTER TABLE ReportSchedule ADD  CONSTRAINT DF_ReportSchedules_IsActive  DEFAULT (1) FOR IsActive;
ALTER TABLE ReportSchedule ADD  CONSTRAINT DF_ReportSchedule_AddDateToFilePath  DEFAULT (0) FOR AddDateToFileName;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_IsActive  DEFAULT (1) FOR IsActive;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_MO  DEFAULT ((0)) FOR Exclude_MO;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_TU  DEFAULT ((0)) FOR Exclude_TU;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_WE  DEFAULT ((0)) FOR Exclude_WE;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_TH  DEFAULT ((0)) FOR Exclude_TH;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_FR  DEFAULT ((0)) FOR Exclude_FR;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_SA  DEFAULT ((0)) FOR Exclude_SA;
ALTER TABLE ReportScheduleReport ADD  CONSTRAINT DF_ReportScheduleReport_Exclude_SU  DEFAULT ((0)) FOR Exclude_SU;
ALTER TABLE ReportStyle ADD  CONSTRAINT DF_ReportStyle_IsDefault  DEFAULT (0) FOR IsDefault;
ALTER TABLE ReportStyle ADD  CONSTRAINT DF_ReportStyle_IsBase  DEFAULT (0) FOR IsBase;
ALTER TABLE ReportStyle ADD  CONSTRAINT DF_ReportStyle_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportTables ADD  CONSTRAINT DF_ReportTables_DisplayOrder  DEFAULT (0) FOR DisplayOrder;
ALTER TABLE ReportTables ADD  CONSTRAINT DF_ReportTables_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ReportTemplate ADD  CONSTRAINT DF_ReportTemplate_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_WelcomeText_Display  DEFAULT (1) FOR WelcomeText_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_CustomerService_Display  DEFAULT (1) FOR CustomerService_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_FAQText_Display  DEFAULT (1) FOR FAQText_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_HelpText_Display  DEFAULT (1) FOR HelpText_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ServiceRequestStatus_Display  DEFAULT (1) FOR ServiceRequestStatus_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_FeedbackSurvey_Display  DEFAULT (1) FOR FeedbackSurvey_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_RepairProcedures_Display  DEFAULT (0) FOR RepairProcedures_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Profile_Display  DEFAULT (1) FOR Profile_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ChangePassword_Display  DEFAULT (1) FOR ChangePassword_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Reports_Display  DEFAULT (1) FOR Reports_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_UseRequesterInfo  DEFAULT (1) FOR UseRequesterInfo;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Name  DEFAULT (0) FOR Name;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_EmailAddress  DEFAULT (0) FOR Email;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Phone  DEFAULT (0) FOR Phone;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Type  DEFAULT (1) FOR Type;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Priority  DEFAULT (1) FOR Priority;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_TargetDate  DEFAULT (1) FOR TargetDate;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Asset  DEFAULT (1) FOR Asset;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Problem  DEFAULT (1) FOR Problem;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_TasksNeeded  DEFAULT (1) FOR Task;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Reason  DEFAULT (1) FOR Reason;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_DuplicateCheck  DEFAULT (0) FOR DuplicateCheck;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_WelcomeText_Display1  DEFAULT (1) FOR WelcomeText_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ServiceRequestStatus_Display1  DEFAULT (1) FOR ServiceRequestStatus_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_CustomerService_Display1  DEFAULT (1) FOR CustomerService_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_FAQText_Display1  DEFAULT (1) FOR FAQText_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_HelpText_Display1  DEFAULT (1) FOR HelpText_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_FeedbackSurvey_Display1  DEFAULT (1) FOR FeedbackSurvey_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_RepairProcedures_Display1  DEFAULT (0) FOR RepairProcedures_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Profile_Display1  DEFAULT (1) FOR Profile_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ChangePassword_Display1  DEFAULT (1) FOR ChangePassword_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Reports_Display1  DEFAULT (1) FOR Reports_Link;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Reports_Display1_1  DEFAULT (1) FOR Approvals_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_Reports_Display1_2  DEFAULT (1) FOR Finalize_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ShowDocuments  DEFAULT (0) FOR ShowDocuments;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ShowImages  DEFAULT (0) FOR ShowImages;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF_RequesterAppOptions_ShowMiscFiles  DEFAULT (0) FOR ShowMiscFiles;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Submi__27259147  DEFAULT ((1)) FOR SubmitServiceRequest_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Submi__2819B580  DEFAULT ((1)) FOR SubmitServiceRequestAsset_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Help___290DD9B9  DEFAULT ((1)) FOR Help_Display;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Asset__2A01FDF2  DEFAULT ((0)) FOR AssetFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__ShopF__2AF6222B  DEFAULT ((0)) FOR ShopFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Accou__2BEA4664  DEFAULT ((0)) FOR AccountFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__TypeF__2CDE6A9D  DEFAULT ((0)) FOR TypeFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Prior__2DD28ED6  DEFAULT ((0)) FOR PriorityFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Categ__2EC6B30F  DEFAULT ((0)) FOR CategoryFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Depar__2FBAD748  DEFAULT ((0)) FOR DepartmentFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Probl__30AEFB81  DEFAULT ((0)) FOR ProblemFilter;
ALTER TABLE RequesterAppOptions ADD  CONSTRAINT DF__Requester__Detai__31A31FBA  DEFAULT ((0)) FOR DetailedStatus;
ALTER TABLE Route ADD  CONSTRAINT DF_Route_Active  DEFAULT (1) FOR Active;
ALTER TABLE Route ADD  CONSTRAINT DF_Route_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Route ADD  CONSTRAINT DF_Route_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Route ADD  CONSTRAINT DF_Route_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE RouteList ADD  CONSTRAINT DF_RouteList_PreReqWarningOnly  DEFAULT (0) FOR PreReqWarningOnly;
ALTER TABLE RouteList ADD  CONSTRAINT DF_RouteList_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Shift ADD  CONSTRAINT DF_Shift_PagerNumeric  DEFAULT ((0)) FOR PagerNumeric;
ALTER TABLE Shift ADD  CONSTRAINT DF_Shift_Active  DEFAULT ((1)) FOR Active;
ALTER TABLE Shift ADD  CONSTRAINT DF_Shift_UDFBit1  DEFAULT ((0)) FOR UDFBit1;
ALTER TABLE Shift ADD  CONSTRAINT DF_Shift_UDFBit2  DEFAULT ((0)) FOR UDFBit2;
ALTER TABLE Shift ADD  CONSTRAINT DF_Shift_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ShiftTimeOff ADD  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Sunday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Monday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Tuesday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Wednesday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Thursday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Friday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT ((0)) FOR Saturday;
ALTER TABLE ShiftWorkSchedule ADD  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_PagerNumeric  DEFAULT (0) FOR PagerNumeric;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_Active  DEFAULT (1) FOR Active;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Shop ADD  CONSTRAINT DF_Shop_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE ShopDocument ADD  CONSTRAINT DF_ShopDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE ShopDocument ADD  CONSTRAINT DF_ShopDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE ShopDocument ADD  CONSTRAINT DF_ShopDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ShopDocument ADD  CONSTRAINT DF_ShopDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE ShopNote ADD  CONSTRAINT DF_ShopNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE ShopNote ADD  CONSTRAINT DF_ShopNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE ShopNote ADD  CONSTRAINT DF_ShopNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE ShopNote ADD  CONSTRAINT DF_ShopNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Specification ADD  CONSTRAINT DF_Specification_TrackHistory  DEFAULT (0) FOR TrackHistory;
ALTER TABLE Specification ADD  CONSTRAINT DF_Specification_Active  DEFAULT (1) FOR Active;
ALTER TABLE Specification ADD  CONSTRAINT DF_Specification_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Specification ADD  CONSTRAINT DF_Specification_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Specification ADD  CONSTRAINT DF_Specification_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE SpecificationHistory ADD  CONSTRAINT DF_SpecificationHistory_ChangeDate  DEFAULT (getdate()) FOR ChangeDate;
ALTER TABLE SpecificationHistory ADD  CONSTRAINT DF_SpecificationHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE SQLSchedule ADD  CONSTRAINT DF_SQLSchedule_RunOnce  DEFAULT ((-1)) FOR ProcessCounterStopAt;
ALTER TABLE SQLSchedule ADD  CONSTRAINT DF_SQLSchedule_ProcessCounter  DEFAULT (0) FOR ProcessCounter;
ALTER TABLE SQLSchedule ADD  CONSTRAINT DF_SQLSchedule_IsActive  DEFAULT (1) FOR IsActive;
ALTER TABLE SQLScheduleLog ADD  CONSTRAINT DF_SQLScheduleLog_SQLDate  DEFAULT (getdate()) FOR SQLDate;
ALTER TABLE System ADD  CONSTRAINT DF_System_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Task ADD  CONSTRAINT DF_Task_AvailableToRequester  DEFAULT (1) FOR AvailableToRequester;
ALTER TABLE Task ADD  CONSTRAINT DF_Task_TrackTask  DEFAULT (0) FOR TrackTask;
ALTER TABLE Task ADD  CONSTRAINT DF_Task_Active  DEFAULT (1) FOR Active;
ALTER TABLE Task ADD  CONSTRAINT DF_Task_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Task ADD  CONSTRAINT DF_Task_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Task ADD  CONSTRAINT DF_Task_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_Active  DEFAULT (1) FOR Active;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_ChargeRate  DEFAULT (0) FOR ChargeRate;
ALTER TABLE Tenant ADD  CONSTRAINT DF_Tenant_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE TenantDocument ADD  CONSTRAINT DF_TenantDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE TenantDocument ADD  CONSTRAINT DF_TenantDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE TenantDocument ADD  CONSTRAINT DF_TenantDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE TenantDocument ADD  CONSTRAINT DF_TenantDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE TenantLabor ADD  CONSTRAINT DF_TenantLabor_PrimaryContact  DEFAULT (0) FOR PrimaryContact;
ALTER TABLE TenantLabor ADD  CONSTRAINT DF_TenantLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE TenantNote ADD  CONSTRAINT DF_TenantNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE TenantNote ADD  CONSTRAINT DF_TenantNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE TenantNote ADD  CONSTRAINT DF_TenantNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE TenantNote ADD  CONSTRAINT DF_TenantNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Tool ADD  CONSTRAINT DF_Tool_Active  DEFAULT (1) FOR Active;
ALTER TABLE Tool ADD  CONSTRAINT DF_Tool_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Tool ADD  CONSTRAINT DF_Tool_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Tool ADD  CONSTRAINT DF_Tool_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Tool ADD  CONSTRAINT DF_Tool_IsMaintainable  DEFAULT ((0)) FOR IsMaintainable;
ALTER TABLE ToolDocument ADD  CONSTRAINT DF_ToolDocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE ToolDocument ADD  CONSTRAINT DF_ToolDocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE ToolDocument ADD  CONSTRAINT DF_ToolDocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ToolDocument ADD  CONSTRAINT DF_ToolDocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE ToolLocation ADD  CONSTRAINT DF_ToolLocation_Available  DEFAULT (0) FOR Available;
ALTER TABLE ToolLocation ADD  CONSTRAINT DF_ToolLocation_Reserved  DEFAULT (0) FOR Reserved;
ALTER TABLE ToolLocation ADD  CONSTRAINT DF_ToolLocation_OnHand  DEFAULT (0) FOR OnHand;
ALTER TABLE ToolLocation ADD  CONSTRAINT DF_ToolLocation_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE ToolNote ADD  CONSTRAINT DF_ToolNote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE ToolNote ADD  CONSTRAINT DF_ToolNote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE ToolNote ADD  CONSTRAINT DF_ToolNote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE ToolNote ADD  CONSTRAINT DF_ToolNote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Training ADD  CONSTRAINT DF_Training_Active  DEFAULT (1) FOR Active;
ALTER TABLE Training ADD  CONSTRAINT DF_Training_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Training ADD  CONSTRAINT DF_Training_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Training ADD  CONSTRAINT DF_Training_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE TrainingDocument ADD  DEFAULT ((0)) FOR PrintWithWO;
ALTER TABLE TrainingDocument ADD  DEFAULT ((0)) FOR SendWithEmail;
ALTER TABLE TrainingDocument ADD  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE TrainingDocument ADD  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE TrainingNote ADD  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE TrainingNote ADD  DEFAULT ((0)) FOR Custom1;
ALTER TABLE TrainingNote ADD  DEFAULT ((0)) FOR Custom2;
ALTER TABLE TrainingNote ADD  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Translation ADD  CONSTRAINT DF_Translation_English  DEFAULT ('') FOR English;
ALTER TABLE Translation ADD  CONSTRAINT DF_Translation_LanguageCode  DEFAULT ('en') FOR LanguageCode;
ALTER TABLE Translation ADD  CONSTRAINT DF_Translation_TranslationOut  DEFAULT ('') FOR TranslationOut;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_WOGroupPK  DEFAULT ((-1)) FOR WOGroupPK;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_RouteOrder  DEFAULT (0) FOR RouteOrder;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_AuthLevelsRequired  DEFAULT (0) FOR AuthLevelsRequired;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_TargetDate  DEFAULT (getdate()) FOR TargetDate;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_TargetHours  DEFAULT (1) FOR TargetHours;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ActualHours  DEFAULT (0) FOR ActualHours;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_WarrantyBox  DEFAULT (0) FOR WarrantyBox;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ShutdownBox  DEFAULT (0) FOR ShutdownBox;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_LockoutTagoutBox  DEFAULT (0) FOR LockoutTagoutBox;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_AttachmentsBox  DEFAULT (0) FOR AttachmentsBox;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_SurveyBox  DEFAULT (0) FOR SurveyBox;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_PrintedBox  DEFAULT (0) FOR PrintedBox;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_Chargeable  DEFAULT (0) FOR Chargeable;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_FollowupWork  DEFAULT (0) FOR FollowupWork;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_FailedWO  DEFAULT (0) FOR FailedWO;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_Requested  DEFAULT (getdate()) FOR Requested;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostLaborActual  DEFAULT (0) FOR CostLaborActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostEmployeeActual  DEFAULT (0) FOR CostEmployeeActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostContractorActual  DEFAULT (0) FOR CostContractorActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostPartActual  DEFAULT (0) FOR CostPartActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostMiscActual  DEFAULT (0) FOR CostMiscActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostTotalActual  DEFAULT (0) FOR CostTotalActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostLaborEstimated  DEFAULT (0) FOR CostLaborEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostEmployeeEstimated  DEFAULT (0) FOR CostEmployeeEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostContractorEstimated  DEFAULT (0) FOR CostContractorEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostPartEstimated  DEFAULT (0) FOR CostPartEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostMiscEstimated  DEFAULT (0) FOR CostMiscEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_CostTotalEstimated  DEFAULT (0) FOR CostTotalEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeLaborActual  DEFAULT (0) FOR ChargeLaborActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeEmployeeActual  DEFAULT (0) FOR ChargeEmployeeActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeContractorActual  DEFAULT (0) FOR ChargeContractorActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargePartActual  DEFAULT (0) FOR ChargePartActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeMiscActual  DEFAULT (0) FOR ChargeMiscActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeTotalActual  DEFAULT (0) FOR ChargeTotalActual;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeLaborEstimated  DEFAULT (0) FOR ChargeLaborEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeEmployeeEstimated  DEFAULT (0) FOR ChargeEmployeeEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeContractorEstimated  DEFAULT (0) FOR ChargeContractorEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargePartEstimated  DEFAULT (0) FOR ChargePartEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeMiscEstimated  DEFAULT (0) FOR ChargeMiscEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_ChargeTotalEstimated  DEFAULT (0) FOR ChargeTotalEstimated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsOpen  DEFAULT (1) FOR IsOpen;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsApproved  DEFAULT (0) FOR IsApproved;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsAssigned  DEFAULT (0) FOR IsAssigned;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsGenerated  DEFAULT (0) FOR IsGenerated;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsPredictive  DEFAULT (0) FOR IsPredictive;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsPartsReserved  DEFAULT (0) FOR IsPartsReserved;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_PDAWO  DEFAULT (0) FOR PDAWO;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_IsAssignmentsCompleted  DEFAULT (1) FOR IsAssignmentsCompleted;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_UDFBit21  DEFAULT (0) FOR UDFBit3;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_UDFBit21_1  DEFAULT (0) FOR UDFBit4;
ALTER TABLE WO ADD  CONSTRAINT DF_WO_UDFBit21_2  DEFAULT (0) FOR UDFBit5;
ALTER TABLE WOAccount ADD  CONSTRAINT DF_WOAccount_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE WOAccount ADD  CONSTRAINT DF_WOAccount_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE WOAccount ADD  CONSTRAINT DF_WOAccount_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_IsAssigned  DEFAULT (0) FOR IsAssigned;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_AssignedHours  DEFAULT (0) FOR AssignedHours;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_AssignedLead  DEFAULT (0) FOR AssignedLead;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_AssignedPDA  DEFAULT (0) FOR AssignedPDA;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_Anytime  DEFAULT ((1)) FOR Anytime;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_LaborAcceptance  DEFAULT ('PENDING') FOR LaborAcceptance;
ALTER TABLE WOAssign ADD  CONSTRAINT DF_WOAssign_LaborAcceptanceDesc  DEFAULT ('Pending') FOR LaborAcceptanceDesc;
ALTER TABLE WOAssignStatus ADD  CONSTRAINT DF_WOAssignStatus_IsAssigned  DEFAULT (0) FOR IsAssigned;
ALTER TABLE WOAssignStatus ADD  CONSTRAINT DF_WOAssignStatus_Completed  DEFAULT (0) FOR Completed;
ALTER TABLE WOAssignStatus ADD  CONSTRAINT DF_WOAssignStatus_AssignedDate_1  DEFAULT (getdate()) FOR AssignedDate;
ALTER TABLE WOAssignStatus ADD  CONSTRAINT DF_WOAssignStatus_CompletedDate_1  DEFAULT (getdate()) FOR CompletedDate;
ALTER TABLE WOAttachment ADD  CONSTRAINT DF_WOAttachment_Active  DEFAULT (1) FOR Active;
ALTER TABLE WOAttachment ADD  CONSTRAINT DF_WOAttachment_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WODependent ADD  CONSTRAINT DF__WODepende__RowVe__151BEFA5  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WODocument ADD  CONSTRAINT DF_WODocument_PrintWithWO  DEFAULT (0) FOR PrintWithWO;
ALTER TABLE WODocument ADD  CONSTRAINT DF_WODocument_SendWithEmail  DEFAULT (0) FOR SendWithEmail;
ALTER TABLE WODocument ADD  CONSTRAINT DF_WODocument_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WODocument ADD  CONSTRAINT DF_WODocument_DisplayLink  DEFAULT ((1)) FOR DisplayLink;
ALTER TABLE WOGroup ADD  CONSTRAINT DF_WOGroup_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOGroupProjection ADD  CONSTRAINT DF_WOGroupProjection_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_EstimatedHours  DEFAULT (0) FOR EstimatedHours;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_RegularHours  DEFAULT (0) FOR RegularHours;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_OvertimeHours  DEFAULT (0) FOR OvertimeHours;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_OtherHours  DEFAULT (0) FOR OtherHours;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_TotalHours  DEFAULT (0) FOR TotalHours;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_WorkDate  DEFAULT (convert(datetime,convert(varchar,getdate(),1))) FOR WorkDate;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_RegularCost  DEFAULT (0) FOR RegularCost;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_OvertimeCost  DEFAULT (0) FOR OvertimeCost;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_OtherCost  DEFAULT (0) FOR OtherCost;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_TotalCost  DEFAULT (0) FOR TotalCost;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_CostRegular  DEFAULT (0) FOR CostRegular;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_CostOvertime  DEFAULT (0) FOR CostOvertime;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_CostOther  DEFAULT (0) FOR CostOther;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_ChargeRate  DEFAULT (0) FOR ChargeRate;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE WOLabor ADD  CONSTRAINT DF_WOLabor_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOMetrics ADD  CONSTRAINT DF__WOMetrics__Metri__416F732F  DEFAULT (getdate()) FOR MetricDate;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_MiscCostDate  DEFAULT (convert(datetime,convert(varchar,getdate(),1))) FOR MiscCostDate;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_QuantityEstimated  DEFAULT (0) FOR QuantityEstimated;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_QuantityActual  DEFAULT (0) FOR QuantityActual;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_EstimatedCost  DEFAULT (0) FOR EstimatedCost;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_ActualCost  DEFAULT (0) FOR ActualCost;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_ChargePercentage  DEFAULT (0) FOR ChargePercentage;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE WOMiscCost ADD  CONSTRAINT DF_WOMiscCost_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WONote ADD  CONSTRAINT DF_WONote_NoteDate  DEFAULT (getdate()) FOR NoteDate;
ALTER TABLE WONote ADD  CONSTRAINT DF_WONote_Custom1  DEFAULT (0) FOR Custom1;
ALTER TABLE WONote ADD  CONSTRAINT DF_WONote_Custom2  DEFAULT (0) FOR Custom2;
ALTER TABLE WONote ADD  CONSTRAINT DF_WONote_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_DirectIssue  DEFAULT (0) FOR DirectIssue;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_OutOfPocket  DEFAULT (0) FOR OutOfPocket;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_QuantityEstimated  DEFAULT (0) FOR QuantityEstimated;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_QuantityActual  DEFAULT (0) FOR QuantityActual;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_IssueUnitCost  DEFAULT (0) FOR IssueUnitCost;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_IssueUnitChargePrice  DEFAULT (0) FOR IssueUnitChargePrice;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_IssueUnitChargePercentage  DEFAULT (0) FOR IssueUnitChargePercentage;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_OtherCost  DEFAULT (0) FOR OtherCost;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_TotalCost  DEFAULT (0) FOR TotalCost;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_TotalCharge  DEFAULT (0) FOR TotalCharge;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_QuantityEstimatedUsed  DEFAULT (0) FOR QuantityEstimatedUsed;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE WOPart ADD  CONSTRAINT DF_WOPart_PartDate  DEFAULT (getdate()) FOR PartDate;
ALTER TABLE WOPhoto ADD  CONSTRAINT DF_WOPhoto_PhotoDate  DEFAULT (getdate()) FOR PhotoDate;
ALTER TABLE WOPhoto ADD  CONSTRAINT DF_WOPhoto_Active  DEFAULT (0) FOR Active;
ALTER TABLE WOPhoto ADD  CONSTRAINT DF_WOPhoto_Followup  DEFAULT (0) FOR Followup;
ALTER TABLE WOPhoto ADD  CONSTRAINT DF_WOPhoto_ReportOrder  DEFAULT (0) FOR ReportOrder;
ALTER TABLE WOPhoto ADD  CONSTRAINT DF_WOPhoto_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOPreReq ADD  CONSTRAINT DF_WOPreReq_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_WOGroupPK  DEFAULT ((-1)) FOR WOGroupPK;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_RouteOrder  DEFAULT (0) FOR RouteOrder;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_AuthLevelsRequired  DEFAULT (0) FOR AuthLevelsRequired;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_TargetDate  DEFAULT (getdate()) FOR TargetDate;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_TargetHours  DEFAULT (1) FOR TargetHours;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ActualHours  DEFAULT (0) FOR ActualHours;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_WarrantyBox  DEFAULT (0) FOR WarrantyBox;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ShutdownBox  DEFAULT (0) FOR ShutdownBox;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_LockoutTagoutBox  DEFAULT (0) FOR LockoutTagoutBox;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_AttachmentsBox  DEFAULT (0) FOR AttachmentsBox;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_SurveyBox  DEFAULT (0) FOR SurveyBox;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_PrintedBox  DEFAULT (0) FOR PrintedBox;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_Chargeable  DEFAULT (0) FOR Chargeable;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_FollowupWork  DEFAULT (0) FOR FollowupWork;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_FailedWO  DEFAULT (0) FOR FailedWO;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_Requested  DEFAULT (getdate()) FOR Requested;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostLaborActual  DEFAULT (0) FOR CostLaborActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostEmployeeActual  DEFAULT (0) FOR CostEmployeeActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostContractorActual  DEFAULT (0) FOR CostContractorActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostPartActual  DEFAULT (0) FOR CostPartActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostMiscActual  DEFAULT (0) FOR CostMiscActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostTotalActual  DEFAULT (0) FOR CostTotalActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostLaborEstimated  DEFAULT (0) FOR CostLaborEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostEmployeeEstimated  DEFAULT (0) FOR CostEmployeeEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostContractorEstimated  DEFAULT (0) FOR CostContractorEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostPartEstimated  DEFAULT (0) FOR CostPartEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostMiscEstimated  DEFAULT (0) FOR CostMiscEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_CostTotalEstimated  DEFAULT (0) FOR CostTotalEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeLaborActual  DEFAULT (0) FOR ChargeLaborActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeEmployeeActual  DEFAULT (0) FOR ChargeEmployeeActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeContractorActual  DEFAULT (0) FOR ChargeContractorActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargePartActual  DEFAULT (0) FOR ChargePartActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeMiscActual  DEFAULT (0) FOR ChargeMiscActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeTotalActual  DEFAULT (0) FOR ChargeTotalActual;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeLaborEstimated  DEFAULT (0) FOR ChargeLaborEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeEmployeeEstimated  DEFAULT (0) FOR ChargeEmployeeEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeContractorEstimated  DEFAULT (0) FOR ChargeContractorEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargePartEstimated  DEFAULT (0) FOR ChargePartEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeMiscEstimated  DEFAULT (0) FOR ChargeMiscEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_ChargeTotalEstimated  DEFAULT (0) FOR ChargeTotalEstimated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_IsOpen  DEFAULT (1) FOR IsOpen;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_IsApproved  DEFAULT (0) FOR IsApproved;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_IsAssigned  DEFAULT (0) FOR IsAssigned;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_IsGenerated  DEFAULT (0) FOR IsGenerated;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_IsPartsReserved  DEFAULT (0) FOR IsPartsReserved;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_PDAWO  DEFAULT (0) FOR PDAWO;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_IsAssignmentsCompleted  DEFAULT (1) FOR IsAssignmentsCompleted;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_UDFBit3  DEFAULT (0) FOR UDFBit3;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_UDFBit4  DEFAULT (0) FOR UDFBit4;
ALTER TABLE WOProjection ADD  CONSTRAINT DF_WOProjection_UDFBit5  DEFAULT (0) FOR UDFBit5;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_WOGroupPK  DEFAULT ((-1)) FOR WOGroupPK;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_RouteOrder  DEFAULT (0) FOR RouteOrder;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_AuthLevelsRequired  DEFAULT (0) FOR AuthLevelsRequired;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_TargetDate  DEFAULT (getdate()) FOR TargetDate;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_TargetHours  DEFAULT (1) FOR TargetHours;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ActualHours  DEFAULT (0) FOR ActualHours;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_Priority  DEFAULT (2) FOR Priority;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_WarrantyBox  DEFAULT (0) FOR WarrantyBox;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ShutdownBox  DEFAULT (0) FOR ShutdownBox;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_LockoutTagoutBox  DEFAULT (0) FOR LockoutTagoutBox;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_AttachmentsBox  DEFAULT (0) FOR AttachmentsBox;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_SurveyBox  DEFAULT (0) FOR SurveyBox;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_PrintedBox  DEFAULT (0) FOR PrintedBox;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_Chargeable  DEFAULT (0) FOR Chargeable;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_FollowupWork  DEFAULT (0) FOR FollowupWork;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_FailedWO  DEFAULT (0) FOR FailedWO;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_Requested  DEFAULT (getdate()) FOR Requested;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostLaborActual  DEFAULT (0) FOR CostLaborActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostEmployeeActual  DEFAULT (0) FOR CostEmployeeActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostContractorActual  DEFAULT (0) FOR CostContractorActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostPartActual  DEFAULT (0) FOR CostPartActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostMiscActual  DEFAULT (0) FOR CostMiscActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostTotalActual  DEFAULT (0) FOR CostTotalActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostLaborEstimated  DEFAULT (0) FOR CostLaborEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostEmployeeEstimated  DEFAULT (0) FOR CostEmployeeEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostContractorEstimated  DEFAULT (0) FOR CostContractorEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostPartEstimated  DEFAULT (0) FOR CostPartEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostMiscEstimated  DEFAULT (0) FOR CostMiscEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_CostTotalEstimated  DEFAULT (0) FOR CostTotalEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeLaborActual  DEFAULT (0) FOR ChargeLaborActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeEmployeeActual  DEFAULT (0) FOR ChargeEmployeeActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeContractorActual  DEFAULT (0) FOR ChargeContractorActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargePartActual  DEFAULT (0) FOR ChargePartActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeMiscActual  DEFAULT (0) FOR ChargeMiscActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeTotalActual  DEFAULT (0) FOR ChargeTotalActual;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeLaborEstimated  DEFAULT (0) FOR ChargeLaborEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeEmployeeEstimated  DEFAULT (0) FOR ChargeEmployeeEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeContractorEstimated  DEFAULT (0) FOR ChargeContractorEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargePartEstimated  DEFAULT (0) FOR ChargePartEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeMiscEstimated  DEFAULT (0) FOR ChargeMiscEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_ChargeTotalEstimated  DEFAULT (0) FOR ChargeTotalEstimated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsOpen  DEFAULT (1) FOR IsOpen;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsApproved  DEFAULT (0) FOR IsApproved;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsAssigned  DEFAULT (0) FOR IsAssigned;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsGenerated  DEFAULT (0) FOR IsGenerated;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsPredictive  DEFAULT (0) FOR IsPredictive;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsPartsReserved  DEFAULT (0) FOR IsPartsReserved;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_PDAWO  DEFAULT (0) FOR PDAWO;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_IsAssignmentsCompleted  DEFAULT (1) FOR IsAssignmentsCompleted;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_RapidEntryStatus  DEFAULT ('N') FOR RapidEntryStatus;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_UDFBit3  DEFAULT (0) FOR UDFBit3;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_UDFBit4  DEFAULT (0) FOR UDFBit4;
ALTER TABLE WORapidEntry ADD  CONSTRAINT DF_WORapidEntry_UDFBit5  DEFAULT (0) FOR UDFBit5;
ALTER TABLE WORequester ADD  CONSTRAINT DF_WORequester_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOStatusHistory ADD  CONSTRAINT DF_WOStatusHistory_IsAuthStatus  DEFAULT (0) FOR IsAuthStatus;
ALTER TABLE WOStatusHistory ADD  CONSTRAINT DF_WOStatusHistory_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOStatusHistory ADD  CONSTRAINT DF_WOStatusHistory_ApprovalCountCompleted  DEFAULT (0) FOR ApprovalCountCompleted;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_Complete  DEFAULT (0) FOR Complete;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_Fail  DEFAULT (0) FOR Fail;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_Spec  DEFAULT (0) FOR Spec;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_Header  DEFAULT (0) FOR Header;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_Meter1  DEFAULT (0) FOR Meter1;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_Meter2  DEFAULT (0) FOR Meter2;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE WOTask ADD  CONSTRAINT DF_WOTask_NA  DEFAULT ((0)) FOR NotApplicable;
ALTER TABLE WOTool ADD  CONSTRAINT DF_WOTool_RecordType  DEFAULT (1) FOR RecordType;
ALTER TABLE WOTool ADD  CONSTRAINT DF_WOTool_QuantityEstimated  DEFAULT (0) FOR QuantityEstimated;
ALTER TABLE WOTool ADD  CONSTRAINT DF_WOTool_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_Budget  DEFAULT (0) FOR Budget;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_PagerNumeric  DEFAULT (0) FOR PagerNumeric;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_Active  DEFAULT (1) FOR Active;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_UDFBit1  DEFAULT (0) FOR UDFBit1;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_UDFBit2  DEFAULT (0) FOR UDFBit2;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_RowVersionDate  DEFAULT (getdate()) FOR RowVersionDate;
ALTER TABLE Zone ADD  CONSTRAINT DF_Zone_PMCounter  DEFAULT (1) FOR PMCounter;
ALTER TABLE AccessGroup  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroup_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE AccessGroup CHECK CONSTRAINT FK_AccessGroup_RepairCenter;
ALTER TABLE AccessGroupActions  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupActions_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE;
ALTER TABLE AccessGroupActions CHECK CONSTRAINT FK_AccessGroupActions_AccessGroup;
ALTER TABLE AccessGroupActions  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupActions_Actions FOREIGN KEY(ActionPK)
REFERENCES Actions (ActionPK)
ON DELETE CASCADE;
ALTER TABLE AccessGroupActions CHECK CONSTRAINT FK_AccessGroupActions_Actions;
ALTER TABLE AccessGroupAsset  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupAsset_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE;
ALTER TABLE AccessGroupAsset CHECK CONSTRAINT FK_AccessGroupAsset_AccessGroup;
ALTER TABLE AccessGroupAsset  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupAsset_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AccessGroupAsset CHECK CONSTRAINT FK_AccessGroupAsset_Asset;
ALTER TABLE AccessGroupAssetMgr  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupAssetMgr_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AccessGroupAssetMgr CHECK CONSTRAINT FK_AccessGroupAssetMgr_AccessGroup;
ALTER TABLE AccessGroupAssetMgr  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupAssetMgr_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AccessGroupAssetMgr CHECK CONSTRAINT FK_AccessGroupAssetMgr_Asset;
ALTER TABLE AccessGroupDocument  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupDocument_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AccessGroupDocument CHECK CONSTRAINT FK_AccessGroupDocument_AccessGroup;
ALTER TABLE AccessGroupDocument  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AccessGroupDocument CHECK CONSTRAINT FK_AccessGroupDocument_Document;
ALTER TABLE AccessGroupForm  WITH CHECK ADD  CONSTRAINT FK_AccessGroupForm_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE AccessGroupForm CHECK CONSTRAINT FK_AccessGroupForm_AccessGroup;
ALTER TABLE AccessGroupRepairCenter  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupRepairCenter_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE;
ALTER TABLE AccessGroupRepairCenter CHECK CONSTRAINT FK_AccessGroupRepairCenter_AccessGroup;
ALTER TABLE AccessGroupRepairCenter  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupRepairCenter_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE;
ALTER TABLE AccessGroupRepairCenter CHECK CONSTRAINT FK_AccessGroupRepairCenter_RepairCenter;
ALTER TABLE AccessGroupReportGroup  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupReportGroup_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AccessGroupReportGroup CHECK CONSTRAINT FK_AccessGroupReportGroup_AccessGroup;
ALTER TABLE AccessGroupReportGroup  WITH NOCHECK ADD  CONSTRAINT FK_AccessGroupReportGroup_ReportGroup FOREIGN KEY(ReportGroupPK)
REFERENCES ReportGroup (ReportGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AccessGroupReportGroup CHECK CONSTRAINT FK_AccessGroupReportGroup_ReportGroup;
ALTER TABLE Account  WITH NOCHECK ADD  CONSTRAINT FK_Account_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
NOT FOR REPLICATION ;
ALTER TABLE Account CHECK CONSTRAINT FK_Account_Department;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK);
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Classification;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
NOT FOR REPLICATION ;
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Department;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Manufacturer FOREIGN KEY(ManufacturerPK)
REFERENCES Company (CompanyPK);
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Manufacturer;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_RepairCenter;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Shop;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Tenant FOREIGN KEY(TenantPK)
REFERENCES Tenant (TenantPK)
NOT FOR REPLICATION ;
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Tenant;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Vendor FOREIGN KEY(VendorPK)
REFERENCES Company (CompanyPK);
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Vendor;
ALTER TABLE Asset  WITH NOCHECK ADD  CONSTRAINT FK_Asset_Zone FOREIGN KEY(ZonePK)
REFERENCES Zone (ZonePK)
NOT FOR REPLICATION ;
ALTER TABLE Asset CHECK CONSTRAINT FK_Asset_Zone;
ALTER TABLE AssetAncestor  WITH NOCHECK ADD  CONSTRAINT FK_AssetAncestor_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetAncestor CHECK CONSTRAINT FK_AssetAncestor_Asset;
ALTER TABLE AssetComponent  WITH NOCHECK ADD  CONSTRAINT FK_AssetComponent_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetComponent CHECK CONSTRAINT FK_AssetComponent_Asset;
ALTER TABLE AssetComponent  WITH NOCHECK ADD  CONSTRAINT FK_AssetComponent_Component FOREIGN KEY(ComponentPK)
REFERENCES Component (ComponentPK);
ALTER TABLE AssetComponent CHECK CONSTRAINT FK_AssetComponent_Component;
ALTER TABLE AssetComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_AssetComponentSpecification_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetComponentSpecification CHECK CONSTRAINT FK_AssetComponentSpecification_Asset;
ALTER TABLE AssetComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_AssetComponentSpecification_Component FOREIGN KEY(ComponentPK)
REFERENCES Component (ComponentPK);
ALTER TABLE AssetComponentSpecification CHECK CONSTRAINT FK_AssetComponentSpecification_Component;
ALTER TABLE AssetComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_AssetComponentSpecification_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK);
ALTER TABLE AssetComponentSpecification CHECK CONSTRAINT FK_AssetComponentSpecification_Specification;
ALTER TABLE AssetContract  WITH NOCHECK ADD  CONSTRAINT FK_AssetContract_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE AssetContract CHECK CONSTRAINT FK_AssetContract_Asset;
ALTER TABLE AssetContract  WITH NOCHECK ADD  CONSTRAINT FK_AssetContract_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE AssetContract CHECK CONSTRAINT FK_AssetContract_Company;
ALTER TABLE AssetCost  WITH NOCHECK ADD  CONSTRAINT FK_AssetCost_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetCost CHECK CONSTRAINT FK_AssetCost_Asset;
ALTER TABLE AssetDocument  WITH NOCHECK ADD  CONSTRAINT FK_AssetDocument_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetDocument CHECK CONSTRAINT FK_AssetDocument_Asset;
ALTER TABLE AssetDocument  WITH NOCHECK ADD  CONSTRAINT FK_AssetDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AssetDocument CHECK CONSTRAINT FK_AssetDocument_Document;
ALTER TABLE AssetGroup  WITH NOCHECK ADD  CONSTRAINT FK_AssetGroup_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE AssetGroup CHECK CONSTRAINT FK_AssetGroup_RepairCenter;
ALTER TABLE AssetGroupAsset  WITH NOCHECK ADD  CONSTRAINT FK_AssetGroupAsset_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetGroupAsset CHECK CONSTRAINT FK_AssetGroupAsset_Asset;
ALTER TABLE AssetGroupAsset  WITH NOCHECK ADD  CONSTRAINT FK_AssetGroupAsset_AssetGroup FOREIGN KEY(AssetGroupPK)
REFERENCES AssetGroup (AssetGroupPK)
ON DELETE CASCADE;
ALTER TABLE AssetGroupAsset CHECK CONSTRAINT FK_AssetGroupAsset_AssetGroup;
ALTER TABLE AssetHierarchy  WITH NOCHECK ADD  CONSTRAINT FK_AssetHierarchy_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetHierarchy CHECK CONSTRAINT FK_AssetHierarchy_Asset;
ALTER TABLE AssetLabor  WITH NOCHECK ADD  CONSTRAINT FK_AssetLabor_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetLabor CHECK CONSTRAINT FK_AssetLabor_Asset;
ALTER TABLE AssetLabor  WITH NOCHECK ADD  CONSTRAINT FK_AssetLabor_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE AssetLabor CHECK CONSTRAINT FK_AssetLabor_Labor;
ALTER TABLE AssetMeterHistory  WITH NOCHECK ADD  CONSTRAINT FK_AssetMeterHistory_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetMeterHistory CHECK CONSTRAINT FK_AssetMeterHistory_Asset;
ALTER TABLE AssetMove  WITH NOCHECK ADD  CONSTRAINT FK_AssetMove_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetMove CHECK CONSTRAINT FK_AssetMove_Asset;
ALTER TABLE AssetNote  WITH NOCHECK ADD  CONSTRAINT FK_AssetNote_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AssetNote CHECK CONSTRAINT FK_AssetNote_Asset;
ALTER TABLE AssetPart  WITH NOCHECK ADD  CONSTRAINT FK_AssetPart_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetPart CHECK CONSTRAINT FK_AssetPart_Asset;
ALTER TABLE AssetPart  WITH NOCHECK ADD  CONSTRAINT FK_AssetPart_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE;
ALTER TABLE AssetPart CHECK CONSTRAINT FK_AssetPart_Part;
ALTER TABLE AssetPhoto  WITH NOCHECK ADD  CONSTRAINT FK_AssetPhoto_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE AssetPhoto CHECK CONSTRAINT FK_AssetPhoto_Asset;
ALTER TABLE AssetRequester  WITH NOCHECK ADD  CONSTRAINT FK_AssetRequester_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetRequester CHECK CONSTRAINT FK_AssetRequester_Asset;
ALTER TABLE AssetRequester  WITH NOCHECK ADD  CONSTRAINT FK_AssetRequester_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE AssetRequester CHECK CONSTRAINT FK_AssetRequester_Labor;
ALTER TABLE AssetSpecification  WITH NOCHECK ADD  CONSTRAINT FK_AssetSpecification_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetSpecification CHECK CONSTRAINT FK_AssetSpecification_Asset;
ALTER TABLE AssetSpecification  WITH NOCHECK ADD  CONSTRAINT FK_AssetSpecification_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK);
ALTER TABLE AssetSpecification CHECK CONSTRAINT FK_AssetSpecification_Specification;
ALTER TABLE AssetStatusHistory  WITH NOCHECK ADD  CONSTRAINT FK_AssetStatusHistory_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetStatusHistory CHECK CONSTRAINT FK_AssetStatusHistory_Asset;
ALTER TABLE AssetStructure  WITH NOCHECK ADD  CONSTRAINT FK_AssetStructure_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE;
ALTER TABLE AssetStructure CHECK CONSTRAINT FK_AssetStructure_Classification;
ALTER TABLE AssetTask  WITH NOCHECK ADD  CONSTRAINT FK_AssetTask_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetTask CHECK CONSTRAINT FK_AssetTask_Asset;
ALTER TABLE AssetTask  WITH NOCHECK ADD  CONSTRAINT FK_AssetTask_Task FOREIGN KEY(TaskPK)
REFERENCES Task (TaskPK)
NOT FOR REPLICATION ;
ALTER TABLE AssetTask CHECK CONSTRAINT FK_AssetTask_Task;
ALTER TABLE AssetValueHistory  WITH NOCHECK ADD  CONSTRAINT FK_AssetValueHistory_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE AssetValueHistory CHECK CONSTRAINT FK_AssetValueHistory_Asset;
ALTER TABLE Bulletin  WITH NOCHECK ADD  CONSTRAINT FK_Bulletin_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK)
ON DELETE CASCADE;
ALTER TABLE Bulletin CHECK CONSTRAINT FK_Bulletin_Shop;
ALTER TABLE ChatTo  WITH CHECK ADD  CONSTRAINT FK_ChatTo_MessageCenter FOREIGN KEY(ChatMsgPK)
REFERENCES MessageCenter (pkChatOfflineMsg)
ON DELETE CASCADE;
ALTER TABLE ChatTo CHECK CONSTRAINT FK_ChatTo_MessageCenter;
ALTER TABLE ClassificationAncestor  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationAncestor_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationAncestor CHECK CONSTRAINT FK_ClassificationAncestor_Classification;
ALTER TABLE ClassificationCause  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationCause_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationCause CHECK CONSTRAINT FK_ClassificationCause_Classification;
ALTER TABLE ClassificationCause  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationCause_Failure FOREIGN KEY(FailurePK)
REFERENCES Failure (FailurePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationCause CHECK CONSTRAINT FK_ClassificationCause_Failure;
ALTER TABLE ClassificationComponent  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationComponent_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationComponent CHECK CONSTRAINT FK_ClassificationComponent_Classification;
ALTER TABLE ClassificationComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationComponentSpecification_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationComponentSpecification CHECK CONSTRAINT FK_ClassificationComponentSpecification_Classification;
ALTER TABLE ClassificationComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationComponentSpecification_Component FOREIGN KEY(ComponentPK)
REFERENCES Component (ComponentPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationComponentSpecification CHECK CONSTRAINT FK_ClassificationComponentSpecification_Component;
ALTER TABLE ClassificationComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationComponentSpecification_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationComponentSpecification CHECK CONSTRAINT FK_ClassificationComponentSpecification_Specification;
ALTER TABLE ClassificationContract  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationContract_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationContract CHECK CONSTRAINT FK_ClassificationContract_Classification;
ALTER TABLE ClassificationDocument  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationDocument_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationDocument CHECK CONSTRAINT FK_ClassificationDocument_Classification;
ALTER TABLE ClassificationDocument  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationDocument CHECK CONSTRAINT FK_ClassificationDocument_Document;
ALTER TABLE ClassificationHierarchy  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationHierarchy_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationHierarchy CHECK CONSTRAINT FK_ClassificationHierarchy_Classification;
ALTER TABLE ClassificationLabor  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationLabor_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationLabor CHECK CONSTRAINT FK_ClassificationLabor_Classification;
ALTER TABLE ClassificationLabor  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationLabor_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationLabor CHECK CONSTRAINT FK_ClassificationLabor_Labor;
ALTER TABLE ClassificationPart  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationPart_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationPart CHECK CONSTRAINT FK_ClassificationPart_Classification;
ALTER TABLE ClassificationPart  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationPart_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationPart CHECK CONSTRAINT FK_ClassificationPart_Part;
ALTER TABLE ClassificationPM  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationPM_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationPM CHECK CONSTRAINT FK_ClassificationPM_Classification;
ALTER TABLE ClassificationPM  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationPM_PM FOREIGN KEY(PMPK)
REFERENCES PM (PMPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationPM CHECK CONSTRAINT FK_ClassificationPM_PM;
ALTER TABLE ClassificationProblem  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationProblem_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationProblem CHECK CONSTRAINT FK_ClassificationProblem_Classification;
ALTER TABLE ClassificationProblem  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationProblem_Failure FOREIGN KEY(FailurePK)
REFERENCES Failure (FailurePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationProblem CHECK CONSTRAINT FK_ClassificationProblem_Failure;
ALTER TABLE ClassificationProblem  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationProblem_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
NOT FOR REPLICATION ;
ALTER TABLE ClassificationProblem CHECK CONSTRAINT FK_ClassificationProblem_ProcedureLibrary;
ALTER TABLE ClassificationProcedure  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationProcedure_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationProcedure CHECK CONSTRAINT FK_ClassificationProcedure_Classification;
ALTER TABLE ClassificationRemedy  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationRemedy_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationRemedy CHECK CONSTRAINT FK_ClassificationRemedy_Classification;
ALTER TABLE ClassificationRemedy  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationRemedy_Failure FOREIGN KEY(FailurePK)
REFERENCES Failure (FailurePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationRemedy CHECK CONSTRAINT FK_ClassificationRemedy_Failure;
ALTER TABLE ClassificationSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationSpecification_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationSpecification CHECK CONSTRAINT FK_ClassificationSpecification_Classification;
ALTER TABLE ClassificationSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationSpecification_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK)
ON DELETE CASCADE;
ALTER TABLE ClassificationSpecification CHECK CONSTRAINT FK_ClassificationSpecification_Specification;
ALTER TABLE ClassificationTask  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationTask_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ClassificationTask CHECK CONSTRAINT FK_ClassificationTask_Classification;
ALTER TABLE ClassificationTask  WITH NOCHECK ADD  CONSTRAINT FK_ClassificationTask_Task FOREIGN KEY(TaskPK)
REFERENCES Task (TaskPK)
NOT FOR REPLICATION ;
ALTER TABLE ClassificationTask CHECK CONSTRAINT FK_ClassificationTask_Task;
ALTER TABLE CompanyDocument  WITH NOCHECK ADD  CONSTRAINT FK_CompanyDocument_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK)
ON DELETE CASCADE;
ALTER TABLE CompanyDocument CHECK CONSTRAINT FK_CompanyDocument_Company;
ALTER TABLE CompanyDocument  WITH NOCHECK ADD  CONSTRAINT FK_CompanyDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE CompanyDocument CHECK CONSTRAINT FK_CompanyDocument_Document;
ALTER TABLE CompanyLabor  WITH NOCHECK ADD  CONSTRAINT FK_CompanyLabor_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE CompanyLabor CHECK CONSTRAINT FK_CompanyLabor_Company;
ALTER TABLE CompanyLabor  WITH NOCHECK ADD  CONSTRAINT FK_CompanyLabor_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE CompanyLabor CHECK CONSTRAINT FK_CompanyLabor_Labor;
ALTER TABLE CompanyNote  WITH NOCHECK ADD  CONSTRAINT FK_CompanyNote_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE CompanyNote CHECK CONSTRAINT FK_CompanyNote_Company;
ALTER TABLE ComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ComponentSpecification_Component FOREIGN KEY(ComponentPK)
REFERENCES Component (ComponentPK)
ON DELETE CASCADE;
ALTER TABLE ComponentSpecification CHECK CONSTRAINT FK_ComponentSpecification_Component;
ALTER TABLE ComponentSpecification  WITH NOCHECK ADD  CONSTRAINT FK_ComponentSpecification_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK)
ON DELETE CASCADE;
ALTER TABLE ComponentSpecification CHECK CONSTRAINT FK_ComponentSpecification_Specification;
ALTER TABLE Contract  WITH NOCHECK ADD  CONSTRAINT FK_Contract_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK);
ALTER TABLE Contract CHECK CONSTRAINT FK_Contract_Company;
ALTER TABLE Contract  WITH CHECK ADD  CONSTRAINT FK_Contract_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE Contract CHECK CONSTRAINT FK_Contract_Department;
ALTER TABLE Contract  WITH CHECK ADD  CONSTRAINT FK_Contract_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE Contract CHECK CONSTRAINT FK_Contract_Labor;
ALTER TABLE Contract  WITH NOCHECK ADD  CONSTRAINT FK_Contract_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Contract CHECK CONSTRAINT FK_Contract_RepairCenter;
ALTER TABLE ContractDocument  WITH NOCHECK ADD  CONSTRAINT FK_ContractDocument_Contract FOREIGN KEY(ContractPK)
REFERENCES Contract (ContractPK)
ON DELETE CASCADE;
ALTER TABLE ContractDocument CHECK CONSTRAINT FK_ContractDocument_Contract;
ALTER TABLE ContractNote  WITH NOCHECK ADD  CONSTRAINT FK_ContractNote_Contract FOREIGN KEY(ContractPK)
REFERENCES Contract (ContractPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ContractNote CHECK CONSTRAINT FK_ContractNote_Contract;
ALTER TABLE Department  WITH NOCHECK ADD  CONSTRAINT FK_Department_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Department CHECK CONSTRAINT FK_Department_RepairCenter;
ALTER TABLE DepartmentDocument  WITH NOCHECK ADD  CONSTRAINT FK_DepartmentDocument_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
ON DELETE CASCADE;
ALTER TABLE DepartmentDocument CHECK CONSTRAINT FK_DepartmentDocument_Department;
ALTER TABLE DepartmentDocument  WITH NOCHECK ADD  CONSTRAINT FK_DepartmentDocument_Document FOREIGN KEY(DepartmentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE DepartmentDocument CHECK CONSTRAINT FK_DepartmentDocument_Document;
ALTER TABLE DepartmentLabor  WITH NOCHECK ADD  CONSTRAINT FK_DepartmentLabor_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
ON DELETE CASCADE;
ALTER TABLE DepartmentLabor CHECK CONSTRAINT FK_DepartmentLabor_Department;
ALTER TABLE DepartmentLabor  WITH NOCHECK ADD  CONSTRAINT FK_DepartmentLabor_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE DepartmentLabor CHECK CONSTRAINT FK_DepartmentLabor_Labor;
ALTER TABLE DepartmentNote  WITH NOCHECK ADD  CONSTRAINT FK_DepartmentNote_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE DepartmentNote CHECK CONSTRAINT FK_DepartmentNote_Department;
ALTER TABLE Document  WITH NOCHECK ADD  CONSTRAINT FK_Document_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK);
ALTER TABLE Document CHECK CONSTRAINT FK_Document_Labor;
ALTER TABLE Document  WITH NOCHECK ADD  CONSTRAINT FK_Document_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Document CHECK CONSTRAINT FK_Document_RepairCenter;
ALTER TABLE Document  WITH NOCHECK ADD  CONSTRAINT FK_Document_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE Document CHECK CONSTRAINT FK_Document_Shop;
ALTER TABLE Failure  WITH NOCHECK ADD  CONSTRAINT FK_Failure_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK);
ALTER TABLE Failure CHECK CONSTRAINT FK_Failure_Asset;
ALTER TABLE Failure  WITH NOCHECK ADD  CONSTRAINT FK_Failure_Failure FOREIGN KEY(ParentPK)
REFERENCES Failure (FailurePK);
ALTER TABLE Failure CHECK CONSTRAINT FK_Failure_Failure;
ALTER TABLE Failure  WITH NOCHECK ADD  CONSTRAINT FK_Failure_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK);
ALTER TABLE Failure CHECK CONSTRAINT FK_Failure_ProcedureLibrary;
ALTER TABLE Failure  WITH NOCHECK ADD  CONSTRAINT FK_Failure_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK)
NOT FOR REPLICATION ;
ALTER TABLE Failure CHECK CONSTRAINT FK_Failure_Shop;
ALTER TABLE FavoriteGroup  WITH NOCHECK ADD  CONSTRAINT FK_FavoriteGroup_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE FavoriteGroup CHECK CONSTRAINT FK_FavoriteGroup_Labor;
ALTER TABLE FavoriteGroupKeys  WITH NOCHECK ADD  CONSTRAINT FK_FavoriteGroupKeys_FavoriteGroup FOREIGN KEY(FavoriteGroupPK)
REFERENCES FavoriteGroup (FavoriteGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE FavoriteGroupKeys CHECK CONSTRAINT FK_FavoriteGroupKeys_FavoriteGroup;
ALTER TABLE FRCriteria  WITH NOCHECK ADD  CONSTRAINT FK_FRCriteria_FRMain FOREIGN KEY(FRPK)
REFERENCES FRMain (FRPK)
ON DELETE CASCADE;
ALTER TABLE FRCriteria CHECK CONSTRAINT FK_FRCriteria_FRMain;
ALTER TABLE FRFields  WITH NOCHECK ADD  CONSTRAINT FK_FRFields_FRMain FOREIGN KEY(FRPK)
REFERENCES FRMain (FRPK)
ON DELETE CASCADE;
ALTER TABLE FRFields CHECK CONSTRAINT FK_FRFields_FRMain;
ALTER TABLE GISLayer  WITH NOCHECK ADD  CONSTRAINT FK_GISLayer_GISLayerGroup1 FOREIGN KEY(GISViewerName, GISLayerGroupName)
REFERENCES GISLayerGroup (GISViewerName, GISLayerGroupName)
ON UPDATE CASCADE
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE GISLayer CHECK CONSTRAINT FK_GISLayer_GISLayerGroup1;
ALTER TABLE GISLayer  WITH NOCHECK ADD  CONSTRAINT FK_GISLayer_GISViewer FOREIGN KEY(GISViewerName)
REFERENCES GISViewer (GISViewerName)
ON UPDATE CASCADE
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE GISLayer CHECK CONSTRAINT FK_GISLayer_GISViewer;
ALTER TABLE InterfaceUser  WITH NOCHECK ADD  CONSTRAINT FK_InterfaceUser_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK);
ALTER TABLE InterfaceUser CHECK CONSTRAINT FK_InterfaceUser_Labor;
ALTER TABLE KeyAssetLocation  WITH NOCHECK ADD  CONSTRAINT FK_KeyAssetLocation_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
NOT FOR REPLICATION ;
ALTER TABLE KeyAssetLocation CHECK CONSTRAINT FK_KeyAssetLocation_Asset;
ALTER TABLE Keys  WITH NOCHECK ADD  CONSTRAINT FK_Keys_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK)
NOT FOR REPLICATION ;
ALTER TABLE Keys CHECK CONSTRAINT FK_Keys_Company;
ALTER TABLE KPI  WITH NOCHECK ADD  CONSTRAINT FK_KPI_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE KPI NOCHECK CONSTRAINT FK_KPI_RepairCenter;
ALTER TABLE KPIGroupKPI  WITH NOCHECK ADD  CONSTRAINT FK_KPIGroupKPI_KPI FOREIGN KEY(KPIPK)
REFERENCES KPI (KPIPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE KPIGroupKPI CHECK CONSTRAINT FK_KPIGroupKPI_KPI;
ALTER TABLE KPIGroupKPI  WITH NOCHECK ADD  CONSTRAINT FK_KPIGroupKPI_KPIGroup FOREIGN KEY(KPIGroupPK)
REFERENCES KPIGroup (KPIGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE KPIGroupKPI CHECK CONSTRAINT FK_KPIGroupKPI_KPIGroup;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK);
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_AccessGroup;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK);
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_Company;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK)
NOT FOR REPLICATION ;
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_Department;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_Labor FOREIGN KEY(CraftPK)
REFERENCES Labor (LaborPK);
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_Labor;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_Labor1 FOREIGN KEY(SupervisorPK)
REFERENCES Labor (LaborPK);
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_Labor1;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE;
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_RepairCenter;
ALTER TABLE Labor  WITH CHECK ADD  CONSTRAINT FK_Labor_Shift FOREIGN KEY(ShiftPK)
REFERENCES Shift (ShiftPK);
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_Shift;
ALTER TABLE Labor  WITH NOCHECK ADD  CONSTRAINT FK_Labor_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK)
NOT FOR REPLICATION ;
ALTER TABLE Labor CHECK CONSTRAINT FK_Labor_Shop;
ALTER TABLE LaborAddressBook  WITH NOCHECK ADD  CONSTRAINT FK_LaborAddressBook_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborAddressBook CHECK CONSTRAINT FK_LaborAddressBook_Labor;
ALTER TABLE LaborAsset  WITH NOCHECK ADD  CONSTRAINT FK_LaborAsset_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE LaborAsset CHECK CONSTRAINT FK_LaborAsset_Asset;
ALTER TABLE LaborAsset  WITH NOCHECK ADD  CONSTRAINT FK_LaborAsset_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborAsset CHECK CONSTRAINT FK_LaborAsset_Labor;
ALTER TABLE LaborAssetFilter  WITH NOCHECK ADD  CONSTRAINT FK_LaborAssetFilter_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborAssetFilter CHECK CONSTRAINT FK_LaborAssetFilter_Asset;
ALTER TABLE LaborAssetFilter  WITH NOCHECK ADD  CONSTRAINT FK_LaborAssetFilter_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborAssetFilter CHECK CONSTRAINT FK_LaborAssetFilter_Labor;
ALTER TABLE LaborAssetMgr  WITH NOCHECK ADD  CONSTRAINT FK_LaborAssetMgr_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborAssetMgr CHECK CONSTRAINT FK_LaborAssetMgr_Asset;
ALTER TABLE LaborAssetMgr  WITH NOCHECK ADD  CONSTRAINT FK_LaborAssetMgr_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborAssetMgr CHECK CONSTRAINT FK_LaborAssetMgr_Labor;
ALTER TABLE LaborDocument  WITH NOCHECK ADD  CONSTRAINT FK_LaborDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborDocument CHECK CONSTRAINT FK_LaborDocument_Document;
ALTER TABLE LaborDocument  WITH NOCHECK ADD  CONSTRAINT FK_LaborDocument_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborDocument CHECK CONSTRAINT FK_LaborDocument_Labor;
ALTER TABLE LaborNote  WITH NOCHECK ADD  CONSTRAINT FK_LaborNote_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborNote CHECK CONSTRAINT FK_LaborNote_Labor;
ALTER TABLE LaborPreference  WITH NOCHECK ADD  CONSTRAINT FK_LaborPreference_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborPreference CHECK CONSTRAINT FK_LaborPreference_Labor;
ALTER TABLE LaborPreference  WITH NOCHECK ADD  CONSTRAINT FK_LaborPreference_Preference FOREIGN KEY(PreferenceName)
REFERENCES Preference (PreferenceName)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE LaborPreference CHECK CONSTRAINT FK_LaborPreference_Preference;
ALTER TABLE LaborReportGroup  WITH NOCHECK ADD  CONSTRAINT FK_LaborReportGroup_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborReportGroup CHECK CONSTRAINT FK_LaborReportGroup_Labor;
ALTER TABLE LaborReportGroup  WITH NOCHECK ADD  CONSTRAINT FK_LaborReportGroup_ReportGroup FOREIGN KEY(ReportGroupPK)
REFERENCES ReportGroup (ReportGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LaborReportGroup CHECK CONSTRAINT FK_LaborReportGroup_ReportGroup;
ALTER TABLE LaborTimeOff  WITH NOCHECK ADD  CONSTRAINT FK_LaborWorkExceptions_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborTimeOff CHECK CONSTRAINT FK_LaborWorkExceptions_Labor;
ALTER TABLE LaborTraining  WITH NOCHECK ADD  CONSTRAINT FK_LaborTraining_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborTraining CHECK CONSTRAINT FK_LaborTraining_Labor;
ALTER TABLE LaborTraining  WITH NOCHECK ADD  CONSTRAINT FK_LaborTraining_Training FOREIGN KEY(TrainingPK)
REFERENCES Training (TrainingPK);
ALTER TABLE LaborTraining CHECK CONSTRAINT FK_LaborTraining_Training;
ALTER TABLE LaborWorkHours  WITH NOCHECK ADD  CONSTRAINT FK_LaborWorkHours_Labor1 FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborWorkHours CHECK CONSTRAINT FK_LaborWorkHours_Labor1;
ALTER TABLE LaborWorkSchedule  WITH NOCHECK ADD  CONSTRAINT FK_LaborWorkHours_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE LaborWorkSchedule CHECK CONSTRAINT FK_LaborWorkHours_Labor;
ALTER TABLE Location  WITH NOCHECK ADD  CONSTRAINT FK_Location_Labor FOREIGN KEY(SupervisorPK)
REFERENCES Labor (LaborPK);
ALTER TABLE Location CHECK CONSTRAINT FK_Location_Labor;
ALTER TABLE Location  WITH NOCHECK ADD  CONSTRAINT FK_Location_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Location CHECK CONSTRAINT FK_Location_RepairCenter;
ALTER TABLE LocationDocument  WITH NOCHECK ADD  CONSTRAINT FK_LocationDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE LocationDocument CHECK CONSTRAINT FK_LocationDocument_Document;
ALTER TABLE LocationDocument  WITH NOCHECK ADD  CONSTRAINT FK_LocationDocument_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK)
ON DELETE CASCADE;
ALTER TABLE LocationDocument CHECK CONSTRAINT FK_LocationDocument_Location;
ALTER TABLE LookupTableValues  WITH NOCHECK ADD  CONSTRAINT FK_LookupTableValues_LookupTable FOREIGN KEY(LookupTable)
REFERENCES LookupTable (LookupTable)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE LookupTableValues CHECK CONSTRAINT FK_LookupTableValues_LookupTable;
ALTER TABLE MCForm  WITH NOCHECK ADD  CONSTRAINT FK_MCForm_MCEdition FOREIGN KEY(EditionName)
REFERENCES MCEdition (EditionName)
ON UPDATE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCForm CHECK CONSTRAINT FK_MCForm_MCEdition;
ALTER TABLE MCForm  WITH NOCHECK ADD  CONSTRAINT FK_MCForm_MCModule FOREIGN KEY(ModuleID)
REFERENCES MCModule (ModuleID)
ON UPDATE CASCADE
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCForm CHECK CONSTRAINT FK_MCForm_MCModule;
ALTER TABLE MCModuleActionsModule  WITH NOCHECK ADD  CONSTRAINT FK_MCModuleActionsModule_MCModule FOREIGN KEY(ModuleID)
REFERENCES MCModule (ModuleID)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE MCModuleActionsModule CHECK CONSTRAINT FK_MCModuleActionsModule_MCModule;
ALTER TABLE MCModuleActionsModule  WITH CHECK ADD  CONSTRAINT FK_MCModuleActionsModule_MCModuleActions FOREIGN KEY(ActionPK)
REFERENCES MCModuleActions (ActionPK)
ON DELETE CASCADE;
ALTER TABLE MCModuleActionsModule CHECK CONSTRAINT FK_MCModuleActionsModule_MCModuleActions;
ALTER TABLE MCRule  WITH NOCHECK ADD  CONSTRAINT FK_MCRule_KPI FOREIGN KEY(KPIPK)
REFERENCES KPI (KPIPK)
NOT FOR REPLICATION ;
ALTER TABLE MCRule CHECK CONSTRAINT FK_MCRule_KPI;
ALTER TABLE MCRuleEmail  WITH NOCHECK ADD  CONSTRAINT FK_MCRuleEmail_MCRule FOREIGN KEY(RulePK)
REFERENCES MCRule (RulePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCRuleEmail CHECK CONSTRAINT FK_MCRuleEmail_MCRule;
ALTER TABLE MCRuleEmailReport  WITH NOCHECK ADD  CONSTRAINT FK_MCRuleEmailReport_MCRule FOREIGN KEY(RulePK)
REFERENCES MCRule (RulePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCRuleEmailReport CHECK CONSTRAINT FK_MCRuleEmailReport_MCRule;
ALTER TABLE MCRuleLabor  WITH NOCHECK ADD  CONSTRAINT FK_AssignmentRuleLabor_AssignmentRule FOREIGN KEY(RulePK)
REFERENCES MCRule (RulePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCRuleLabor CHECK CONSTRAINT FK_AssignmentRuleLabor_AssignmentRule;
ALTER TABLE MCRuleLaborAlert  WITH CHECK ADD  CONSTRAINT FK_MCRuleLaborAlert_MCRule FOREIGN KEY(RulePK)
REFERENCES MCRule (RulePK)
ON DELETE CASCADE;
ALTER TABLE MCRuleLaborAlert CHECK CONSTRAINT FK_MCRuleLaborAlert_MCRule;
ALTER TABLE MCRuleWO  WITH NOCHECK ADD  CONSTRAINT FK_MCRuleWO_MCRule FOREIGN KEY(RulePK)
REFERENCES MCRule (RulePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCRuleWO CHECK CONSTRAINT FK_MCRuleWO_MCRule;
ALTER TABLE MCTab  WITH NOCHECK ADD  CONSTRAINT FK_MCTab_MCModule FOREIGN KEY(ModuleID)
REFERENCES MCModule (ModuleID)
ON UPDATE CASCADE
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE MCTab CHECK CONSTRAINT FK_MCTab_MCModule;
ALTER TABLE MessageCenter  WITH CHECK ADD  CONSTRAINT FK_MessageCenter_MessageCenter FOREIGN KEY(pkChatOfflineMsg)
REFERENCES MessageCenter (pkChatOfflineMsg);
ALTER TABLE MessageCenter CHECK CONSTRAINT FK_MessageCenter_MessageCenter;
ALTER TABLE Part  WITH NOCHECK ADD  CONSTRAINT FK_Part_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK);
ALTER TABLE Part CHECK CONSTRAINT FK_Part_Classification;
ALTER TABLE Part  WITH NOCHECK ADD  CONSTRAINT FK_Part_Company FOREIGN KEY(ManufacturerPK)
REFERENCES Company (CompanyPK);
ALTER TABLE Part CHECK CONSTRAINT FK_Part_Company;
ALTER TABLE PartAlternate  WITH NOCHECK ADD  CONSTRAINT FK_PartAlternate_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE PartAlternate CHECK CONSTRAINT FK_PartAlternate_Part;
ALTER TABLE PartDocument  WITH NOCHECK ADD  CONSTRAINT FK_PartDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE PartDocument CHECK CONSTRAINT FK_PartDocument_Document;
ALTER TABLE PartDocument  WITH NOCHECK ADD  CONSTRAINT FK_PartDocument_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE;
ALTER TABLE PartDocument CHECK CONSTRAINT FK_PartDocument_Part;
ALTER TABLE PartLocation  WITH NOCHECK ADD  CONSTRAINT FK_PartLocation_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK);
ALTER TABLE PartLocation CHECK CONSTRAINT FK_PartLocation_Location;
ALTER TABLE PartLocation  WITH NOCHECK ADD  CONSTRAINT FK_PartLocation_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK);
ALTER TABLE PartLocation CHECK CONSTRAINT FK_PartLocation_Part;
ALTER TABLE PartNote  WITH NOCHECK ADD  CONSTRAINT FK_PartNote_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE PartNote CHECK CONSTRAINT FK_PartNote_Part;
ALTER TABLE PartSpecification  WITH NOCHECK ADD  CONSTRAINT FK_PartSpecification_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE;
ALTER TABLE PartSpecification CHECK CONSTRAINT FK_PartSpecification_Part;
ALTER TABLE PartSpecification  WITH NOCHECK ADD  CONSTRAINT FK_PartSpecification_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK);
ALTER TABLE PartSpecification CHECK CONSTRAINT FK_PartSpecification_Specification;
ALTER TABLE PartVendor  WITH NOCHECK ADD  CONSTRAINT FK_PartVendor_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK);
ALTER TABLE PartVendor CHECK CONSTRAINT FK_PartVendor_Company;
ALTER TABLE PartVendor  WITH NOCHECK ADD  CONSTRAINT FK_PartVendor_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK);
ALTER TABLE PartVendor CHECK CONSTRAINT FK_PartVendor_Location;
ALTER TABLE PartVendor  WITH NOCHECK ADD  CONSTRAINT FK_PartVendor_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK);
ALTER TABLE PartVendor CHECK CONSTRAINT FK_PartVendor_Part;
ALTER TABLE PM  WITH NOCHECK ADD  CONSTRAINT FK_PM_Labor FOREIGN KEY(SupervisorPK)
REFERENCES Labor (LaborPK);
ALTER TABLE PM CHECK CONSTRAINT FK_PM_Labor;
ALTER TABLE PM  WITH NOCHECK ADD  CONSTRAINT FK_PM_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK);
ALTER TABLE PM CHECK CONSTRAINT FK_PM_Project;
ALTER TABLE PM  WITH NOCHECK ADD  CONSTRAINT FK_PM_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE PM CHECK CONSTRAINT FK_PM_RepairCenter;
ALTER TABLE PM  WITH CHECK ADD  CONSTRAINT FK_PM_Shift FOREIGN KEY(ShiftPK)
REFERENCES Shift (ShiftPK);
ALTER TABLE PM CHECK CONSTRAINT FK_PM_Shift;
ALTER TABLE PM  WITH NOCHECK ADD  CONSTRAINT FK_PM_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE PM CHECK CONSTRAINT FK_PM_Shop;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Asset;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Department FOREIGN KEY(DepartmentPK)
REFERENCES Department (DepartmentPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Department;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Labor FOREIGN KEY(SupervisorPK)
REFERENCES Labor (LaborPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Labor;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Location FOREIGN KEY(StockRoomPK)
REFERENCES Location (LocationPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Location;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_PM FOREIGN KEY(PMPK)
REFERENCES PM (PMPK)
ON DELETE CASCADE;
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_PM;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Project;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_RepairCenter;
ALTER TABLE PMAsset  WITH CHECK ADD  CONSTRAINT FK_PMAsset_Shift FOREIGN KEY(ShiftPK)
REFERENCES Shift (ShiftPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Shift;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Shop;
ALTER TABLE PMAsset  WITH NOCHECK ADD  CONSTRAINT FK_PMAsset_Tenant FOREIGN KEY(TenantPK)
REFERENCES Tenant (TenantPK);
ALTER TABLE PMAsset CHECK CONSTRAINT FK_PMAsset_Tenant;
ALTER TABLE PMAssetUndo  WITH NOCHECK ADD  CONSTRAINT FK_PMAssetUndo_PMUndo FOREIGN KEY(PMUndoPK)
REFERENCES PMUndo (PMUndoPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE PMAssetUndo CHECK CONSTRAINT FK_PMAssetUndo_PMUndo;
ALTER TABLE PMDocument  WITH NOCHECK ADD  CONSTRAINT FK_PMDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE;
ALTER TABLE PMDocument CHECK CONSTRAINT FK_PMDocument_Document;
ALTER TABLE PMDocument  WITH NOCHECK ADD  CONSTRAINT FK_PMDocument_PM FOREIGN KEY(PMPK)
REFERENCES PM (PMPK)
ON DELETE CASCADE;
ALTER TABLE PMDocument CHECK CONSTRAINT FK_PMDocument_PM;
ALTER TABLE PMNote  WITH NOCHECK ADD  CONSTRAINT FK_PMNote_PM FOREIGN KEY(PMPK)
REFERENCES PM (PMPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE PMNote CHECK CONSTRAINT FK_PMNote_PM;
ALTER TABLE PMProcedure  WITH NOCHECK ADD  CONSTRAINT FK_PMProcedure_PM FOREIGN KEY(PMPK)
REFERENCES PM (PMPK)
ON DELETE CASCADE;
ALTER TABLE PMProcedure CHECK CONSTRAINT FK_PMProcedure_PM;
ALTER TABLE PMProcedure  WITH NOCHECK ADD  CONSTRAINT FK_PMProcedure_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE PMProcedure CHECK CONSTRAINT FK_PMProcedure_ProcedureLibrary;
ALTER TABLE PMUndoRepairCenter  WITH NOCHECK ADD  CONSTRAINT FK_PMUndoRepairCenter_PMUndo FOREIGN KEY(PMUndoPK)
REFERENCES PMUndo (PMUndoPK)
ON DELETE CASCADE;
ALTER TABLE PMUndoRepairCenter CHECK CONSTRAINT FK_PMUndoRepairCenter_PMUndo;
ALTER TABLE PredictiveNote  WITH NOCHECK ADD  CONSTRAINT FK_PredictiveNote_Predictive FOREIGN KEY(PredictivePK)
REFERENCES Predictive (PredictivePK)
ON DELETE CASCADE;
ALTER TABLE PredictiveNote CHECK CONSTRAINT FK_PredictiveNote_Predictive;
ALTER TABLE PredictivePhoto  WITH NOCHECK ADD  CONSTRAINT FK_PredictivePhoto_Predictive FOREIGN KEY(PredictivePK)
REFERENCES Predictive (PredictivePK)
ON DELETE CASCADE;
ALTER TABLE PredictivePhoto CHECK CONSTRAINT FK_PredictivePhoto_Predictive;
ALTER TABLE PredictiveStatusHistory  WITH NOCHECK ADD  CONSTRAINT FK_PredictiveStatusHistory_Predictive FOREIGN KEY(PredictivePK)
REFERENCES Predictive (PredictivePK)
ON DELETE CASCADE;
ALTER TABLE PredictiveStatusHistory CHECK CONSTRAINT FK_PredictiveStatusHistory_Predictive;
ALTER TABLE PredictiveTask  WITH NOCHECK ADD  CONSTRAINT FK_PredictiveTask_Predictive FOREIGN KEY(PredictivePK)
REFERENCES Predictive (PredictivePK)
ON DELETE CASCADE;
ALTER TABLE PredictiveTask CHECK CONSTRAINT FK_PredictiveTask_Predictive;
ALTER TABLE Preference  WITH NOCHECK ADD  CONSTRAINT FK_Preference_PreferenceCategory FOREIGN KEY(PreferenceCategory)
REFERENCES PreferenceCategory (PreferenceCategory)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE Preference CHECK CONSTRAINT FK_Preference_PreferenceCategory;
ALTER TABLE PreferenceModule  WITH NOCHECK ADD  CONSTRAINT FK_PreferenceModule_MCModule FOREIGN KEY(ModuleID)
REFERENCES MCModule (ModuleID)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE PreferenceModule CHECK CONSTRAINT FK_PreferenceModule_MCModule;
ALTER TABLE PreferenceModule  WITH NOCHECK ADD  CONSTRAINT FK_PreferenceModule_Preference FOREIGN KEY(PreferenceName)
REFERENCES Preference (PreferenceName)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE PreferenceModule CHECK CONSTRAINT FK_PreferenceModule_Preference;
ALTER TABLE ProcedureDocument  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureDocument_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureDocument CHECK CONSTRAINT FK_ProcedureDocument_Classification;
ALTER TABLE ProcedureDocument  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE;
ALTER TABLE ProcedureDocument CHECK CONSTRAINT FK_ProcedureDocument_Document;
ALTER TABLE ProcedureDocument  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureDocument_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE ProcedureDocument CHECK CONSTRAINT FK_ProcedureDocument_ProcedureLibrary;
ALTER TABLE ProcedureLabor  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLabor_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureLabor CHECK CONSTRAINT FK_ProcedureLabor_Classification;
ALTER TABLE ProcedureLabor  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLabor_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE ProcedureLabor CHECK CONSTRAINT FK_ProcedureLabor_Labor;
ALTER TABLE ProcedureLabor  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLabor_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE ProcedureLabor CHECK CONSTRAINT FK_ProcedureLabor_ProcedureLibrary;
ALTER TABLE ProcedureLibrary  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLibrary_Labor FOREIGN KEY(SupervisorPK)
REFERENCES Labor (LaborPK);
ALTER TABLE ProcedureLibrary CHECK CONSTRAINT FK_ProcedureLibrary_Labor;
ALTER TABLE ProcedureLibrary  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLibrary_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureLibrary CHECK CONSTRAINT FK_ProcedureLibrary_Project;
ALTER TABLE ProcedureLibrary  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLibrary_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE ProcedureLibrary CHECK CONSTRAINT FK_ProcedureLibrary_RepairCenter;
ALTER TABLE ProcedureLibrary  WITH CHECK ADD  CONSTRAINT FK_ProcedureLibrary_Shift FOREIGN KEY(ShiftPK)
REFERENCES Shift (ShiftPK);
ALTER TABLE ProcedureLibrary CHECK CONSTRAINT FK_ProcedureLibrary_Shift;
ALTER TABLE ProcedureLibrary  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureLibrary_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE ProcedureLibrary CHECK CONSTRAINT FK_ProcedureLibrary_Shop;
ALTER TABLE ProcedureMiscCost  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureMiscCost_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureMiscCost CHECK CONSTRAINT FK_ProcedureMiscCost_Classification;
ALTER TABLE ProcedureMiscCost  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureMiscCost_Company FOREIGN KEY(CompanyPK)
REFERENCES Company (CompanyPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureMiscCost CHECK CONSTRAINT FK_ProcedureMiscCost_Company;
ALTER TABLE ProcedureMiscCost  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureMiscCost_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE ProcedureMiscCost CHECK CONSTRAINT FK_ProcedureMiscCost_ProcedureLibrary;
ALTER TABLE ProcedureNote  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureNote_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ProcedureNote CHECK CONSTRAINT FK_ProcedureNote_ProcedureLibrary;
ALTER TABLE ProcedurePart  WITH NOCHECK ADD  CONSTRAINT FK_ProcedurePart_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedurePart CHECK CONSTRAINT FK_ProcedurePart_Classification;
ALTER TABLE ProcedurePart  WITH NOCHECK ADD  CONSTRAINT FK_ProcedurePart_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK)
ON DELETE CASCADE;
ALTER TABLE ProcedurePart CHECK CONSTRAINT FK_ProcedurePart_Location;
ALTER TABLE ProcedurePart  WITH NOCHECK ADD  CONSTRAINT FK_ProcedurePart_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE;
ALTER TABLE ProcedurePart CHECK CONSTRAINT FK_ProcedurePart_Part;
ALTER TABLE ProcedurePart  WITH NOCHECK ADD  CONSTRAINT FK_ProcedurePart_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE ProcedurePart CHECK CONSTRAINT FK_ProcedurePart_ProcedureLibrary;
ALTER TABLE ProcedureProcedure  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureProcedure_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ProcedureProcedure CHECK CONSTRAINT FK_ProcedureProcedure_Asset;
ALTER TABLE ProcedureProcedure  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureProcedure_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureProcedure CHECK CONSTRAINT FK_ProcedureProcedure_Classification;
ALTER TABLE ProcedureProcedure  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureProcedure_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ProcedureProcedure CHECK CONSTRAINT FK_ProcedureProcedure_ProcedureLibrary;
ALTER TABLE ProcedureTask  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTask_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ProcedureTask CHECK CONSTRAINT FK_ProcedureTask_Asset;
ALTER TABLE ProcedureTask  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTask_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureTask CHECK CONSTRAINT FK_ProcedureTask_Classification;
ALTER TABLE ProcedureTask  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTask_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE ProcedureTask CHECK CONSTRAINT FK_ProcedureTask_ProcedureLibrary;
ALTER TABLE ProcedureTask  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTask_Specification FOREIGN KEY(SpecificationPK)
REFERENCES Specification (SpecificationPK)
ON DELETE CASCADE;
ALTER TABLE ProcedureTask CHECK CONSTRAINT FK_ProcedureTask_Specification;
ALTER TABLE ProcedureTask  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTask_Task FOREIGN KEY(TaskPK)
REFERENCES Task (TaskPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureTask CHECK CONSTRAINT FK_ProcedureTask_Task;
ALTER TABLE ProcedureTool  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTool_Classification FOREIGN KEY(ClassificationPK)
REFERENCES Classification (ClassificationPK)
NOT FOR REPLICATION ;
ALTER TABLE ProcedureTool CHECK CONSTRAINT FK_ProcedureTool_Classification;
ALTER TABLE ProcedureTool  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTool_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK)
ON DELETE CASCADE;
ALTER TABLE ProcedureTool CHECK CONSTRAINT FK_ProcedureTool_Location;
ALTER TABLE ProcedureTool  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTool_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE ProcedureTool CHECK CONSTRAINT FK_ProcedureTool_ProcedureLibrary;
ALTER TABLE ProcedureTool  WITH NOCHECK ADD  CONSTRAINT FK_ProcedureTool_Tool FOREIGN KEY(ToolPK)
REFERENCES Tool (ToolPK)
ON DELETE CASCADE;
ALTER TABLE ProcedureTool CHECK CONSTRAINT FK_ProcedureTool_Tool;
ALTER TABLE Project  WITH NOCHECK ADD  CONSTRAINT FK_Project_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE Project CHECK CONSTRAINT FK_Project_Shop;
ALTER TABLE ProjectAsset  WITH NOCHECK ADD  CONSTRAINT FK_ProjectAsset_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK)
ON DELETE CASCADE;
ALTER TABLE ProjectAsset CHECK CONSTRAINT FK_ProjectAsset_Project;
ALTER TABLE ProjectDocument  WITH NOCHECK ADD  CONSTRAINT FK_ProjectDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ProjectDocument CHECK CONSTRAINT FK_ProjectDocument_Document;
ALTER TABLE ProjectDocument  WITH NOCHECK ADD  CONSTRAINT FK_ProjectDocument_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK)
ON DELETE CASCADE;
ALTER TABLE ProjectDocument CHECK CONSTRAINT FK_ProjectDocument_Project;
ALTER TABLE ProjectNote  WITH NOCHECK ADD  CONSTRAINT FK_ProjectNote_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ProjectNote CHECK CONSTRAINT FK_ProjectNote_Project;
ALTER TABLE ProjectStatusHistory  WITH NOCHECK ADD  CONSTRAINT FK_ProjectStatusHistory_Project FOREIGN KEY(ProjectPK)
REFERENCES Project (ProjectPK)
ON DELETE CASCADE;
ALTER TABLE ProjectStatusHistory CHECK CONSTRAINT FK_ProjectStatusHistory_Project;
ALTER TABLE PurchaseOrderDetail  WITH CHECK ADD  CONSTRAINT FK_PurchaseOrderDetail_PurchaseOrder FOREIGN KEY(POPK)
REFERENCES PurchaseOrder (POPK);
ALTER TABLE PurchaseOrderDetail CHECK CONSTRAINT FK_PurchaseOrderDetail_PurchaseOrder;
ALTER TABLE PurchaseOrderDocument  WITH CHECK ADD  CONSTRAINT FK_PurchaseOrderDocument_PurchaseOrder FOREIGN KEY(POPK)
REFERENCES PurchaseOrder (POPK)
ON DELETE CASCADE;
ALTER TABLE PurchaseOrderDocument CHECK CONSTRAINT FK_PurchaseOrderDocument_PurchaseOrder;
ALTER TABLE PurchaseOrderInvoice  WITH CHECK ADD  CONSTRAINT FK_PurchaseOrderInvoice_PurchaseOrder FOREIGN KEY(POPK)
REFERENCES PurchaseOrder (POPK);
ALTER TABLE PurchaseOrderInvoice CHECK CONSTRAINT FK_PurchaseOrderInvoice_PurchaseOrder;
ALTER TABLE PurchaseOrderInvoiceDetail  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderInvoiceDetail_PurchaseOrderDetail FOREIGN KEY(PurchaseOrderDetailPK)
REFERENCES PurchaseOrderDetail (PK);
ALTER TABLE PurchaseOrderInvoiceDetail CHECK CONSTRAINT FK_PurchaseOrderInvoiceDetail_PurchaseOrderDetail;
ALTER TABLE PurchaseOrderInvoiceDetail  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderInvoiceDetail_PurchaseOrderInvoice FOREIGN KEY(InvoicePK)
REFERENCES PurchaseOrderInvoice (InvoicePK);
ALTER TABLE PurchaseOrderInvoiceDetail CHECK CONSTRAINT FK_PurchaseOrderInvoiceDetail_PurchaseOrderInvoice;
ALTER TABLE PurchaseOrderNote  WITH NOCHECK ADD  CONSTRAINT FK_PONote_PurchaseOrder FOREIGN KEY(POPK)
REFERENCES PurchaseOrder (POPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE PurchaseOrderNote CHECK CONSTRAINT FK_PONote_PurchaseOrder;
ALTER TABLE PurchaseOrderPending  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderPending_Company FOREIGN KEY(VendorPK)
REFERENCES Company (CompanyPK)
ON DELETE CASCADE;
ALTER TABLE PurchaseOrderPending CHECK CONSTRAINT FK_PurchaseOrderPending_Company;
ALTER TABLE PurchaseOrderPending  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderPending_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK)
ON DELETE CASCADE;
ALTER TABLE PurchaseOrderPending CHECK CONSTRAINT FK_PurchaseOrderPending_Location;
ALTER TABLE PurchaseOrderPending  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderPending_Part FOREIGN KEY(PartPK)
REFERENCES Part (PartPK)
ON DELETE CASCADE;
ALTER TABLE PurchaseOrderPending CHECK CONSTRAINT FK_PurchaseOrderPending_Part;
ALTER TABLE PurchaseOrderPending  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderPending_WOPart FOREIGN KEY(WOPartPK)
REFERENCES WOPart (PK)
ON DELETE CASCADE;
ALTER TABLE PurchaseOrderPending CHECK CONSTRAINT FK_PurchaseOrderPending_WOPart;
ALTER TABLE PurchaseOrderReceive  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderReceive_PurchaseOrderDetail FOREIGN KEY(PurchaseOrderDetailPK)
REFERENCES PurchaseOrderDetail (PK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE PurchaseOrderReceive CHECK CONSTRAINT FK_PurchaseOrderReceive_PurchaseOrderDetail;
ALTER TABLE PurchaseOrderRMA  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderRMA_PurchaseOrder FOREIGN KEY(POPK)
REFERENCES PurchaseOrder (POPK)
NOT FOR REPLICATION ;
ALTER TABLE PurchaseOrderRMA CHECK CONSTRAINT FK_PurchaseOrderRMA_PurchaseOrder;
ALTER TABLE PurchaseOrderRMADetail  WITH NOCHECK ADD  CONSTRAINT FK_PurchaseOrderRMADetail_PurchaseOrderRMA FOREIGN KEY(RMAPK)
REFERENCES PurchaseOrderRMA (RMAPK)
NOT FOR REPLICATION ;
ALTER TABLE PurchaseOrderRMADetail CHECK CONSTRAINT FK_PurchaseOrderRMADetail_PurchaseOrderRMA;
ALTER TABLE PurchaseOrderStatusHistory  WITH CHECK ADD  CONSTRAINT FK_PurchaseOrderStatusHistory_PurchaseOrder FOREIGN KEY(POPK)
REFERENCES PurchaseOrder (POPK)
ON DELETE CASCADE;
ALTER TABLE PurchaseOrderStatusHistory CHECK CONSTRAINT FK_PurchaseOrderStatusHistory_PurchaseOrder;
ALTER TABLE RepairCenter  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenter_Labor FOREIGN KEY(SupervisorPK)
REFERENCES Labor (LaborPK);
ALTER TABLE RepairCenter CHECK CONSTRAINT FK_RepairCenter_Labor;
ALTER TABLE RepairCenter  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenter_Location FOREIGN KEY(StockRoomPK)
REFERENCES Location (LocationPK);
ALTER TABLE RepairCenter CHECK CONSTRAINT FK_RepairCenter_Location;
ALTER TABLE RepairCenter  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenter_Location1 FOREIGN KEY(ToolRoomPK)
REFERENCES Location (LocationPK);
ALTER TABLE RepairCenter CHECK CONSTRAINT FK_RepairCenter_Location1;
ALTER TABLE RepairCenterDocument  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenterDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE RepairCenterDocument CHECK CONSTRAINT FK_RepairCenterDocument_Document;
ALTER TABLE RepairCenterDocument  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenterDocument_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE;
ALTER TABLE RepairCenterDocument CHECK CONSTRAINT FK_RepairCenterDocument_RepairCenter;
ALTER TABLE RepairCenterNote  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenterNote_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE RepairCenterNote CHECK CONSTRAINT FK_RepairCenterNote_RepairCenter;
ALTER TABLE RepairCenterPreference  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenterPreference_Preference FOREIGN KEY(PreferenceName)
REFERENCES Preference (PreferenceName)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE RepairCenterPreference CHECK CONSTRAINT FK_RepairCenterPreference_Preference;
ALTER TABLE RepairCenterPreference  WITH NOCHECK ADD  CONSTRAINT FK_RepairCenterPreference_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE;
ALTER TABLE RepairCenterPreference CHECK CONSTRAINT FK_RepairCenterPreference_RepairCenter;
ALTER TABLE Report_ReportGroup  WITH NOCHECK ADD  CONSTRAINT FK_Report_ReportGroup_ReportGroup FOREIGN KEY(ReportGroupPK)
REFERENCES ReportGroup (ReportGroupPK)
ON DELETE CASCADE;
ALTER TABLE Report_ReportGroup CHECK CONSTRAINT FK_Report_ReportGroup_ReportGroup;
ALTER TABLE Report_ReportGroup  WITH NOCHECK ADD  CONSTRAINT FK_Report_ReportGroup_Reports FOREIGN KEY(ReportPK)
REFERENCES Reports (ReportPK)
ON DELETE CASCADE;
ALTER TABLE Report_ReportGroup CHECK CONSTRAINT FK_Report_ReportGroup_Reports;
ALTER TABLE ReportAccessGroup  WITH NOCHECK ADD  CONSTRAINT FK_ReportAccessGroup_AccessGroup FOREIGN KEY(AccessGroupPK)
REFERENCES AccessGroup (AccessGroupPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ReportAccessGroup CHECK CONSTRAINT FK_ReportAccessGroup_AccessGroup;
ALTER TABLE ReportAccessGroup  WITH NOCHECK ADD  CONSTRAINT FK_ReportAccessGroup_Reports FOREIGN KEY(ReportPK)
REFERENCES Reports (ReportPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ReportAccessGroup CHECK CONSTRAINT FK_ReportAccessGroup_Reports;
ALTER TABLE ReportCriteria  WITH NOCHECK ADD  CONSTRAINT FK_ReportCriteria_Reports FOREIGN KEY(ReportPK)
REFERENCES Reports (ReportPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE ReportCriteria CHECK CONSTRAINT FK_ReportCriteria_Reports;
ALTER TABLE ReportFields  WITH NOCHECK ADD  CONSTRAINT FK_ReportFields_Reports FOREIGN KEY(ReportPK)
REFERENCES Reports (ReportPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE ReportFields CHECK CONSTRAINT FK_ReportFields_Reports;
ALTER TABLE Reports  WITH NOCHECK ADD  CONSTRAINT FK_Reports_ReportStyle FOREIGN KEY(ReportStyleName)
REFERENCES ReportStyle (ReportStyleName)
ON UPDATE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE Reports CHECK CONSTRAINT FK_Reports_ReportStyle;
ALTER TABLE Reports  WITH CHECK ADD  CONSTRAINT FK_Reports_ReportTemplate FOREIGN KEY(ReportTemplateName)
REFERENCES ReportTemplate (ReportTemplateName)
ON UPDATE CASCADE;
ALTER TABLE Reports CHECK CONSTRAINT FK_Reports_ReportTemplate;
ALTER TABLE ReportScheduleReport  WITH NOCHECK ADD  CONSTRAINT FK_ReportScheduleReports_ReportSchedules FOREIGN KEY(ReportSchedulePK)
REFERENCES ReportSchedule (ReportSchedulePK)
ON DELETE CASCADE;
ALTER TABLE ReportScheduleReport CHECK CONSTRAINT FK_ReportScheduleReports_ReportSchedules;
ALTER TABLE ReportTables  WITH NOCHECK ADD  CONSTRAINT FK_ReportTables_Reports FOREIGN KEY(ReportPK)
REFERENCES Reports (ReportPK)
ON UPDATE CASCADE
ON DELETE CASCADE;
ALTER TABLE ReportTables CHECK CONSTRAINT FK_ReportTables_Reports;
ALTER TABLE ReportTemplate  WITH CHECK ADD  CONSTRAINT FK_ReportTemplate_ReportTemplate FOREIGN KEY(ReportTemplateName)
REFERENCES ReportTemplate (ReportTemplateName);
ALTER TABLE ReportTemplate CHECK CONSTRAINT FK_ReportTemplate_ReportTemplate;
ALTER TABLE Route  WITH NOCHECK ADD  CONSTRAINT FK_Route_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Route CHECK CONSTRAINT FK_Route_RepairCenter;
ALTER TABLE Route  WITH NOCHECK ADD  CONSTRAINT FK_Route_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK);
ALTER TABLE Route CHECK CONSTRAINT FK_Route_Shop;
ALTER TABLE RouteList  WITH NOCHECK ADD  CONSTRAINT FK_RouteList_Asset FOREIGN KEY(AssetPK)
REFERENCES Asset (AssetPK)
ON DELETE CASCADE;
ALTER TABLE RouteList CHECK CONSTRAINT FK_RouteList_Asset;
ALTER TABLE RouteList  WITH NOCHECK ADD  CONSTRAINT FK_RouteList_ProcedureLibrary FOREIGN KEY(ProcedurePK)
REFERENCES ProcedureLibrary (ProcedurePK)
ON DELETE CASCADE;
ALTER TABLE RouteList CHECK CONSTRAINT FK_RouteList_ProcedureLibrary;
ALTER TABLE RouteList  WITH NOCHECK ADD  CONSTRAINT FK_RouteList_Route FOREIGN KEY(RoutePK)
REFERENCES Route (RoutePK)
ON DELETE CASCADE;
ALTER TABLE RouteList CHECK CONSTRAINT FK_RouteList_Route;
ALTER TABLE ShiftTimeOff  WITH NOCHECK ADD  CONSTRAINT FK_ShiftWorkExceptions_Shift FOREIGN KEY(ShiftPK)
REFERENCES Shift (ShiftPK)
ON DELETE CASCADE;
ALTER TABLE ShiftTimeOff CHECK CONSTRAINT FK_ShiftWorkExceptions_Shift;
ALTER TABLE ShiftWorkSchedule  WITH NOCHECK ADD  CONSTRAINT FK_ShiftWorkHours_Shift FOREIGN KEY(ShiftPK)
REFERENCES Shift (ShiftPK)
ON DELETE CASCADE;
ALTER TABLE ShiftWorkSchedule CHECK CONSTRAINT FK_ShiftWorkHours_Shift;
ALTER TABLE Shop  WITH NOCHECK ADD  CONSTRAINT FK_Shop_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE;
ALTER TABLE Shop CHECK CONSTRAINT FK_Shop_RepairCenter;
ALTER TABLE ShopDocument  WITH NOCHECK ADD  CONSTRAINT FK_ShopDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ShopDocument CHECK CONSTRAINT FK_ShopDocument_Document;
ALTER TABLE ShopDocument  WITH NOCHECK ADD  CONSTRAINT FK_ShopDocument_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK)
ON DELETE CASCADE;
ALTER TABLE ShopDocument CHECK CONSTRAINT FK_ShopDocument_Shop;
ALTER TABLE ShopNote  WITH NOCHECK ADD  CONSTRAINT FK_ShopNote_Shop FOREIGN KEY(ShopPK)
REFERENCES Shop (ShopPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ShopNote CHECK CONSTRAINT FK_ShopNote_Shop;
ALTER TABLE SUR_Survey  WITH NOCHECK ADD  CONSTRAINT FK_SUR_Survey_RepairCenter FOREIGN KEY(REPAIRCENTERPK)
REFERENCES RepairCenter (RepairCenterPK)
ON DELETE CASCADE;
ALTER TABLE SUR_Survey CHECK CONSTRAINT FK_SUR_Survey_RepairCenter;
ALTER TABLE Tenant  WITH NOCHECK ADD  CONSTRAINT FK_Tenant_RepairCenter FOREIGN KEY(RepairCenterPK)
REFERENCES RepairCenter (RepairCenterPK);
ALTER TABLE Tenant CHECK CONSTRAINT FK_Tenant_RepairCenter;
ALTER TABLE TenantDocument  WITH NOCHECK ADD  CONSTRAINT FK_TenantDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE TenantDocument CHECK CONSTRAINT FK_TenantDocument_Document;
ALTER TABLE TenantDocument  WITH NOCHECK ADD  CONSTRAINT FK_TenantDocument_Tenant FOREIGN KEY(TenantPK)
REFERENCES Tenant (TenantPK)
ON DELETE CASCADE;
ALTER TABLE TenantDocument CHECK CONSTRAINT FK_TenantDocument_Tenant;
ALTER TABLE TenantLabor  WITH NOCHECK ADD  CONSTRAINT FK_TenantLabor_Labor FOREIGN KEY(LaborPK)
REFERENCES Labor (LaborPK)
ON DELETE CASCADE;
ALTER TABLE TenantLabor CHECK CONSTRAINT FK_TenantLabor_Labor;
ALTER TABLE TenantLabor  WITH NOCHECK ADD  CONSTRAINT FK_TenantLabor_Tenant FOREIGN KEY(TenantPK)
REFERENCES Tenant (TenantPK)
ON DELETE CASCADE;
ALTER TABLE TenantLabor CHECK CONSTRAINT FK_TenantLabor_Tenant;
ALTER TABLE TenantNote  WITH NOCHECK ADD  CONSTRAINT FK_TenantNote_Tenant FOREIGN KEY(TenantPK)
REFERENCES Tenant (TenantPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE TenantNote CHECK CONSTRAINT FK_TenantNote_Tenant;
ALTER TABLE ToolDocument  WITH NOCHECK ADD  CONSTRAINT FK_ToolDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ToolDocument CHECK CONSTRAINT FK_ToolDocument_Document;
ALTER TABLE ToolDocument  WITH NOCHECK ADD  CONSTRAINT FK_ToolDocument_Tool FOREIGN KEY(ToolPK)
REFERENCES Tool (ToolPK)
ON DELETE CASCADE;
ALTER TABLE ToolDocument CHECK CONSTRAINT FK_ToolDocument_Tool;
ALTER TABLE ToolLocation  WITH NOCHECK ADD  CONSTRAINT FK_ToolLocation_Location FOREIGN KEY(LocationPK)
REFERENCES Location (LocationPK);
ALTER TABLE ToolLocation CHECK CONSTRAINT FK_ToolLocation_Location;
ALTER TABLE ToolLocation  WITH NOCHECK ADD  CONSTRAINT FK_ToolLocation_Tool FOREIGN KEY(ToolPK)
REFERENCES Tool (ToolPK);
ALTER TABLE ToolLocation CHECK CONSTRAINT FK_ToolLocation_Tool;
ALTER TABLE ToolNote  WITH NOCHECK ADD  CONSTRAINT FK_ToolNote_Tool FOREIGN KEY(ToolPK)
REFERENCES Tool (ToolPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE ToolNote CHECK CONSTRAINT FK_ToolNote_Tool;
ALTER TABLE TrainingDocument  WITH NOCHECK ADD  CONSTRAINT FK_TrainingDocument_Document FOREIGN KEY(DocumentPK)
REFERENCES Document (DocumentPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE TrainingDocument CHECK CONSTRAINT FK_TrainingDocument_Document;
ALTER TABLE TrainingDocument  WITH NOCHECK ADD  CONSTRAINT FK_TrainingDocument_Training FOREIGN KEY(TrainingPK)
REFERENCES Training (TrainingPK)
ON DELETE CASCADE;
ALTER TABLE TrainingDocument CHECK CONSTRAINT FK_TrainingDocument_Training;
ALTER TABLE TrainingNote  WITH NOCHECK ADD  CONSTRAINT FK_TrainingNote_Training FOREIGN KEY(TrainingPK)
REFERENCES Training (TrainingPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE TrainingNote CHECK CONSTRAINT FK_TrainingNote_Training;
ALTER TABLE WOAccount  WITH NOCHECK ADD  CONSTRAINT FK_WOAccount_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOAccount CHECK CONSTRAINT FK_WOAccount_WO;
ALTER TABLE WOAssign  WITH NOCHECK ADD  CONSTRAINT FK_WOAssign_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOAssign CHECK CONSTRAINT FK_WOAssign_WO;
ALTER TABLE WOAssignStatus  WITH NOCHECK ADD  CONSTRAINT FK_WOAssignStatus_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE WOAssignStatus CHECK CONSTRAINT FK_WOAssignStatus_WO;
ALTER TABLE WOAttachment  WITH NOCHECK ADD  CONSTRAINT FK_WOAttachment_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOAttachment CHECK CONSTRAINT FK_WOAttachment_WO;
ALTER TABLE WODependent  WITH NOCHECK ADD  CONSTRAINT FK_WODependent_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE WODependent CHECK CONSTRAINT FK_WODependent_WO;
ALTER TABLE WODocument  WITH NOCHECK ADD  CONSTRAINT FK_WOdocument_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WODocument CHECK CONSTRAINT FK_WOdocument_WO;
ALTER TABLE WOLabor  WITH NOCHECK ADD  CONSTRAINT FK_WOLabor_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOLabor CHECK CONSTRAINT FK_WOLabor_WO;
ALTER TABLE WOMiscCost  WITH NOCHECK ADD  CONSTRAINT FK_WOmiscCost_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOMiscCost CHECK CONSTRAINT FK_WOmiscCost_WO;
ALTER TABLE WONote  WITH NOCHECK ADD  CONSTRAINT FK_WONote_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE WONote CHECK CONSTRAINT FK_WONote_WO;
ALTER TABLE WOPart  WITH NOCHECK ADD  CONSTRAINT FK_WOPart_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK);
ALTER TABLE WOPart CHECK CONSTRAINT FK_WOPart_WO;
ALTER TABLE WOPhoto  WITH NOCHECK ADD  CONSTRAINT FK_WOPhoto_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE WOPhoto CHECK CONSTRAINT FK_WOPhoto_WO;
ALTER TABLE WOProjection  WITH NOCHECK ADD  CONSTRAINT FK_WOProjection_Projection FOREIGN KEY(PMUndoPK)
REFERENCES Projection (ProjectionPK)
ON DELETE CASCADE;
ALTER TABLE WOProjection CHECK CONSTRAINT FK_WOProjection_Projection;
ALTER TABLE WORequester  WITH NOCHECK ADD  CONSTRAINT FK_WORequester_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE
NOT FOR REPLICATION ;
ALTER TABLE WORequester CHECK CONSTRAINT FK_WORequester_WO;
ALTER TABLE WOStartStop  WITH NOCHECK ADD  CONSTRAINT FK_WOStartStop_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOStartStop CHECK CONSTRAINT FK_WOStartStop_WO;
ALTER TABLE WOStatusHistory  WITH NOCHECK ADD  CONSTRAINT FK_WOStatusHistory_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOStatusHistory CHECK CONSTRAINT FK_WOStatusHistory_WO;
ALTER TABLE WOTask  WITH NOCHECK ADD  CONSTRAINT FK_WOTask_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOTask CHECK CONSTRAINT FK_WOTask_WO;
ALTER TABLE WOTool  WITH NOCHECK ADD  CONSTRAINT FK_WOTool_WO FOREIGN KEY(WOPK)
REFERENCES WO (WOPK)
ON DELETE CASCADE;
ALTER TABLE WOTool CHECK CONSTRAINT FK_WOTool_WO;
