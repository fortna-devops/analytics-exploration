import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.tsa.api import VAR, DynamicVAR
from statsmodels.tsa.base.datetools import dates_from_str

"""
Example from
http://www.statsmodels.org/devel/vector_ar.html
"""

mdata = sm.datasets.macrodata.load_pandas().data
dates = mdata[['year', 'quarter']].astype(int).astype(str)
quarterly = dates["year"] + "Q" + dates["quarter"]
quarterly = dates_from_str(quarterly)

mdata = mdata[['realgdp', 'realcons', 'realinv']]

mdata.index = pd.DatetimeIndex(quarterly)

data = np.log(mdata).diff().dropna()

# make a VAR model
model = VAR(data)

print(mdata)

print(data)

results = model.fit()

print(results.k_ar)

print(results.summary())

results.plot().show()

# print(results)
lag_order = results.k_ar
print(results.forecast(data.values[-10:], 5))

fig = results.plot_forecast(5)

fig.show()
# print(model.predict(data))
