#!/usr/bin/env python3
"""Summary

Attributes:
    client (TYPE): S3 client
    conveyors (list): conveyor name
    mse_dict (dict): Description
    params_dict (dict): Description
    S3_BUCKET_NAME (str): Description
"""
import s3fs
import pickle
import ipdb
import boto3
import scipy.stats
import glob
import pytest

from utils_model_performance.utils_comp_model_confidence import *


import ipdb

S3_BUCKET_NAME = 'mhspredict-site-fedex-louisville'
conveyor = 'SR1-10'
tag_list = ['rms_velocity_x', 'rms_velocity_z', 'temperature']


@pytest.fixture
def input_data_dict():
    conveyor_dict_test_data = {}
    for picklefile in glob.iglob('test_predictive_modeling_sklearn/test_data_comp_model_confidence/*.pickle'):
        with open(picklefile, 'rb') as handle:
            data = handle.read()
        model_and_metadata = pickle.loads(data)
        metadata_mse = model_and_metadata['metadata']['mse']
        file = picklefile.split('/')[-1].split('.')[0]
        conveyor_dict_test_data[file] = metadata_mse
    return conveyor_dict_test_data


def test_grab_metadata(input_data_dict):
    conveyor_dict = grab_metadata(S3_BUCKET_NAME, conveyor, 'mse')
    assert len(conveyor_dict) == len(input_data_dict)


def test_flat_conveyor_dict(input_data_dict):

    conveyor_flat_dict = flat_conveyor_dict(input_data_dict, tag_list)
    lengths = [len(v) for v in conveyor_flat_dict.values()]
    assert lengths[0] == lengths[1] == lengths[2]
    assert len(input_data_dict) == len(conveyor_flat_dict) * lengths[0]


def test_generate_params_dict(input_data_dict):
    conveyor_flat_dict = flat_conveyor_dict(input_data_dict, tag_list)
    tag_params_dict = generate_params_dict(conveyor_flat_dict)
    assert len(tag_params_dict) == len(tag_list)
    assert list(tag_params_dict.keys()) == tag_list


def test_compute_model_confidence(input_data_dict):
    conveyor_flat_dict = flat_conveyor_dict(input_data_dict, tag_list)
    tag_params_dict = generate_params_dict(conveyor_flat_dict)
    model_confindence_dict = compute_model_confidence(input_data_dict, tag_params_dict)
    assert len(model_confindence_dict) == len(input_data_dict)
    assert list(input_data_dict.keys()) == list(model_confindence_dict.keys())
