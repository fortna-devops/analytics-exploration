#!/usr/bin/env python3
'''
Reading metadata mse and calculate model confidence for every models when rasing alarms
..author: Coco Wu <jiewu@mhsinc.net>

Attributes:
    client (TYPE): S3 client
    conveyor_medadatatag (dict): final dictionary returns model confidence score for every models
    conveyors (string): conveyor name
    S3_BUCKET_NAME (str): s3 bucket name
    tag_list (list): model tag list
'''
import s3fs
import scipy.stats
import math
import boto3
import pickle


def grab_metadata(S3_BUCKET_NAME, conveyor, metadata_tag):
    """Summary
    grab mse metadata for a specific conveyor

    params:
        conveyor (TYPE): string
        metafdata_tag (TYPE): string

    Returns:
        TYPE: dictionary

    Args:
        conveyor (TYPE): conveyor name
        metadata_tag (TYPE): metadata tag
    """
    fs = s3fs.S3FileSystem()
    client = boto3.client('s3')
    resource = boto3.resource('s3')
    my_bucket = resource.Bucket(S3_BUCKET_NAME)
    files = list(my_bucket.objects.filter(
        Prefix='models/timeseries_predictions/{cname}/'.format(bname=S3_BUCKET_NAME, cname=conveyor.replace("-", "_"))))
    metadata_dict = {}
    for file in files:
        model_full_path = "s3://{bname}/{mpath}".format(bname=S3_BUCKET_NAME, mpath=file.key)
        with fs.open(model_full_path, 'rb') as fl:
            data = fl.read()
        model_and_metadata = pickle.loads(data)
        model = pickle.loads(model_and_metadata['model'])
        metadata = model_and_metadata['metadata']
        model_tag = file.key.split('/')[-1].split('.')[0]
        metadata_dict[model_tag] = metadata[metadata_tag]
    return metadata_dict


def flat_conveyor_dict(conveyor_dict, tag_list):
    """Summary
    group tags and flat conveyor dict
    Args:
        conveyor_dict (TYPE): dictionary
        tag_list (TYPE): list of tags

    Returns:
        TYPE: dictionary
    """
    conveyor_flat_dict = {}
    for tag in tag_list:
        mse = []
        for model_metadata, mse_value in conveyor_dict.items():
            if tag in model_metadata:
                mse.append(mse_value)
        conveyor_flat_dict[tag] = mse
    return conveyor_flat_dict


def get_params_from_distribution(mse_list):
    """Summary
    get mean and standard deviation from distribution

    Args:
        mse_value (TYPE): list

    Returns:
        TYPE: mean, stand deviation, shift
    """
    dist = getattr(scipy.stats, 'gamma')
    param = dist.fit(mse_list)
    return param[0], param[1], param[2]


def generate_params_dict(conveyor_flat_dict):
    """Summary
    generate mean and standard deviation dictionary for every conveyor
    Args:
        conveyor_flat_dict (TYPE): dictionary

    Returns:
        TYPE: dictionary
    """
    tag_params_dict = {}
    for tag, tag_mse_values in conveyor_flat_dict.items():
        mean, std, scale = get_params_from_distribution(tag_mse_values)
        tag_params_dict[tag] = (mean, std, scale)
    return tag_params_dict


def compute_model_confidence(conveyor_dict, tag_params_dict):
    """Summary
    compute model confidence for every conveyor
    Args:
        conveyor_dict (TYPE): dictionary
        tag_params_dict (TYPE): dictionary

    Returns:
        TYPE: dictionary
    """
    model_confindence_dict = {}
    for model_metadata, mse in conveyor_dict.items():
        # TODO: find a better way to find tag name
        last_letter = model_metadata.split('_')[-1]
        if last_letter == 'temperature':
            tag = 'temperature'
        else:
            tag = '_'.join(model_metadata.split('_')[-3:])
        mean, std, scale = tag_params_dict[tag]
        model_confidence = round(1 - scipy.stats.gamma(mean, std, scale).cdf(mse), 4)
        model_confindence_dict[model_metadata] = model_confidence
    return model_confindence_dict
