#!/usr/bin/env python3
'''
one-class SVM for novelty detection
'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager
from sklearn import svm
