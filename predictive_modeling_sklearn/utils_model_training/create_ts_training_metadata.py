#!/usr/bin/env python3
'''
Creating metadata for every conveyor and save it to s3 bucket /site/models/timeseries_prediction/*.pickle
..author: Coco Wu <jiewu@mhsinc.net>
'''

import io
import os
import boto3
import time
from datetime import datetime
import logging
import numpy as np

from analytics_toolbox.data_init import data_retrieval, data_handles
from predictive_modeling_sklearn import gbm_model, create_metadata, save_obj

# only accept following data range for the input data. used for cleaning purpose
datarange = {'rms_velocity_x': [0, 1], 'rms_velocity_z': [0, 1], 'temperature': [0, 250]}
TZOFFSET = -4
S3BUCKET_NAME = 'mhspredict-site-dhl-miami'
rely_on_vibrations = False

logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))


tags = [
    'rms_velocity_z',
    'temperature',
    'rms_velocity_x',
    'peak_acceleration_z',
    'peak_acceleration_x',
    'peak_frequency_z',
    'peak_frequency_x',
    'rms_acceleration_z',
    'rms_acceleration_x',
    'kurtosis_z',
    'kurtosis_x',
    'crest_acceleration_z',
    'crest_acceleration_x',
    'peak_velocity_z',
    'peak_velocity_x',
    'hf_rms_acceleration_z',
    'hf_rms_acceleration_x'
]

dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)


conveyors = dqe.get_conveyors(S3BUCKET_NAME)

# set datatime and pulling data for that timeframe using Athena
t1 = datetime(day=13, month=6, year=2019, hour=0)
t2 = datetime(day=15, month=6, year=2019, hour=23)
timetuple = (t1, t2)

logger.info(f"start training for {S3BUCKET_NAME} using tags {tags} from time {t1} to {t2}")

# #tt = data_retrieval.construct_relative_timestamp_tuple(time_back="60 minute",tzoffset=TZOFFSET)
# find how many unique equipment for each conveyor
conveyor_not_success = set()
for conveyor in conveyors:
    logger.info(f"start querying for conveyor {conveyor}")
    temp_data_dict = dqe.execute_timeseries_data_query(
        taglist=tags,
        timerange_tuple=timetuple,
        conveyor=conveyor,
        equip="all",
        return_as_df=True)

# creating more features
    for equipment, equipment_df in temp_data_dict.items():
        equipment_df[f'{equipment}.status'] = np.where(equipment_df[f'{equipment}.rms_acceleration_x'] > 0.02, 1, 0)
        equipment_df[f'{equipment}.velocity_magnitude'] = (np.power(equipment_df[f'{equipment}.rms_velocity_z'], 2) + np.power(equipment_df[f'{equipment}.rms_velocity_x'], 2))**(1 / 2)
        equipment_df[f'{equipment}.velocity_theta'] = np.arctan(equipment_df[f'{equipment}.rms_velocity_x'] / equipment_df[f'{equipment}.rms_velocity_z'])
        equipment_df[f'{equipment}.acceleration_magnitude'] = (np.power(equipment_df[f'{equipment}.rms_acceleration_x'], 2) + np.power(equipment_df[f'{equipment}.rms_acceleration_z'], 2)) ** (1 / 2)
        equipment_df[f'{equipment}.acceleration_theta'] = np.arctan(equipment_df[f'{equipment}.rms_acceleration_x'] / equipment_df[f'{equipment}.rms_acceleration_z'])
        equipment_df[f'{equipment}.crest_acceleration_magnitude'] = (np.power(equipment_df[f'{equipment}.crest_acceleration_z'], 2) + np.power(equipment_df[f'{equipment}.crest_acceleration_x'], 2)) ** (1 / 2)
        equipment_df[f'{equipment}.crest_acceleration_theta'] = np.arctan(equipment_df[f'{equipment}.crest_acceleration_x'] / equipment_df[f'{equipment}.crest_acceleration_z'])
        equipment_df[f'{equipment}.max_peak_acceleration'] = equipment_df[[f'{equipment}.peak_acceleration_z', f'{equipment}.peak_acceleration_x']].max(axis=1)
        equipment_df[f'{equipment}.max_peak_velocity'] = equipment_df[[f'{equipment}.peak_velocity_z', f'{equipment}.peak_velocity_x']].max(axis=1)
        equipment_df[f'{equipment}.max_peak_frequency'] = equipment_df[[f'{equipment}.peak_frequency_z', f'{equipment}.peak_frequency_x']].max(axis=1)

    aggregated_df = data_handles.merge_disparate_dfs(
        list(temp_data_dict.values()), merge_method='nearest', merge_tol='15s')
    # make sure that datetime is the index
    aggregated_df = data_handles.set_datetime_index(aggregated_df)

    locations = dqe.get_conveyor_sensors(conveyor=conveyor)
    # fill na based on the most recent rows. Next row preferred, if not exist, use the previous row
    aggregated_df.fillna(method='bfill', inplace=True)
    aggregated_df.fillna(method='ffill', inplace=True)

    # remove data points that are out of healthy range (similar with QA data range check)
    for column in aggregated_df.columns:
        for tag in datarange:
            if tag in column:
                aggregated_df = aggregated_df[aggregated_df[column] > min(datarange[tag])]
                aggregated_df = aggregated_df[aggregated_df[column] < max(datarange[tag])]
    if len(aggregated_df) == 0:
        logger.warning(f'conveyor{conveyor} does not have enough valid data')
        conveyor_not_success.add(conveyor)
        continue
    # save the original format of data pulling
    merged_data = aggregated_df.copy()
    # convert 'sensor*' to str
    for column in merged_data.columns:
        if 'sensor' in column:
            merged_data[column] = merged_data[column].astype('str')

    # Drop categorical variables
    for column in merged_data.columns:
        if column in merged_data.select_dtypes(include=['category', 'object']).columns:
            merged_data.drop(column, axis=1, inplace=True)

    # Create correlation matrix
    corr_matrix = merged_data.corr(method='spearman').abs()

    # Select upper triangle of correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

    # Find index of feature columns with correlation greater than 0.90
    to_drop = [column for column in upper.columns if any(upper[column] > 0.90)]
    # Target Variable
    target_list = ['rms_velocity_x', 'temperature']
    listnotremove = []
    for i in merged_data.columns:
        for j in target_list:
            if i.find(j) > 0:
                listnotremove.append(i)

    for i in listnotremove:
        if i in to_drop:
            to_drop.remove(i)

    logger.info(f"drop highly correlated columns {to_drop}")
    # Drop highly correlated variables (correlation larger than 0.95)
    merged_data = merged_data.drop(to_drop, axis=1, inplace=False)

    # # ## uploading metadata
    version = '0.0.1'
    model_type = 'gradient-boosting-regression'
    prefix = 'models/timeseries_predictions/{c}/'.format(c=conveyor.replace('-', '_'))
    # listnotremove is the list of all target columns
    for model_target in listnotremove:
        sensor_num = locations[locations['location'] == model_target.split('.')[0]]['sensor'].values[0]
        equipment_type = model_target.split('.')[0]
        # not train only one equipment using following code
        if equipment_type == 'None':
            continue
        try:
            t0 = time.time()
            model, model_input_data, training_time, mse = gbm_model(
                merged_data, model_target, rely_on_vibrations=True, performCV=True)
            logger.info(f'the training takes {time.time() - t0} seconds')
            import ipdb
            ipdb.set_trace()
            metadata = create_metadata(conveyor, tags, model, version, model_type, training_time, model_input_data,
                                       model_target=model_target, mse=mse, sensor_num=sensor_num, equipment_type=equipment_type)
            name = 'gbm_{c}_{mt}'.format(c=conveyor.replace('-', '_'), mt=model_target.replace('.', '_'))
            local_metadata_directory = os.path.dirname('../models_metadata/{b}/'.format(b=S3BUCKET_NAME))
            if not os.path.exists(local_metadata_directory):
                os.makedirs(local_metadata_directory)
            save_obj(metadata, '../models_metadata/{b}/'.format(b=S3BUCKET_NAME) + name)
            # boto3.Session().resource('s3').Bucket(S3BUCKET_NAME).Object(os.path.join(prefix, '{name}.pickle'.format(
            #     name=name))).upload_file(('../models_metadata/{b}/'.format(b=S3BUCKET_NAME) + name + '.pickle'))
        except Exception as e:
            logger.info(e)
            logger.info(f'conveyor {conveyor} does not run model creation for predicting {model_target} successfully')
            conveyor_not_success.add(conveyor)
if len(conveyor_not_success) > 0:
    logger.info(f'there are {len(conveyor_not_success)} number of conveyors are failed, which are {conveyor_not_success}')
