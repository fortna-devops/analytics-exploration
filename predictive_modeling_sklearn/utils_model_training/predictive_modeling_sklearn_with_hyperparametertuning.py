#!/usr/bin/env python3

import io
import boto3
import random
import pandas as pd
import numpy as np
import os
import pickle
from datetime import datetime

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn import ensemble
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import learning_curve
from sklearn.model_selection import train_test_split
from sklearn.model_selection import ShuffleSplit
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans


from analytics_toolbox.data_init import data_retrieval, data_handles
from analytics_toolbox.units_manager import Q_
from analytics_toolbox.machine_learning import data_preparation, modeling
from analytics_toolbox.preprocessing import data_cleaning

import ipdb
# only accept following data range for the input data. used for cleaning purpose
datarange = {'rms_velocity_x': [0, 1], 'rms_velocity_z': [0, 1], 'temperature': [0, 250]}


def gbm_model(input_data, target_column):
    '''
    model: gradient boosting machine
    input_data: pandas dataframe
    target_column: pandas series

    '''
    ydrop = [target_column]
    y = input_data[target_column]
    # not using gearbox to predict motor or the other way around since those two are connected
    # if 'M.' in target_column:
    #     if ''.join(['G.', target_column.split('.')[1]]) in input_data.columns:
    #         ydrop.append((target_column.replace('M.','G.')))
    # elif 'G.' in target_column:
    #     if ''.join(['M.', target_column.split('.')[1]]) in input_data.columns:
    #         ydrop.append((target_column.replace('G.','M.')))
    # else:
    #     pass

    X = input_data.drop(ydrop, axis=1, inplace=False)
    # Train, Test Split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=False)
    # Gradient Boosting Regression
    training_time = datetime.now()
    params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 2,
              'learning_rate': 0.01, 'loss': 'ls'}
    clf = ensemble.GradientBoostingRegressor(**params)
    clf.fit(X_train, y_train)
    y_test_pred = clf.predict(X_test)
    mse = mean_squared_error(y_test, y_test_pred)
    return (clf, X_train, training_time, mse)
#
#


def create_metadata(model, model_input_data, training_time, **kwargs):
    '''
    function for creating metadata, used for both gradient boosting machine and kmeans
    model: model output from modeling function
    model_input_data: the model input dataframe used in training phases
    training_time: the training time from the model
    *** kwargs: model_target, mse: those for regression model not in kmeans
    '''
    adict = {}
    bdict = {}
    cdict = {}
    bdict['conveyor'] = conveyor

    bdict['query_tags'] = tags
    bdict['model_version'] = version
    bdict['model_type'] = model_type
    bdict['training_time'] = training_time.strftime("%Y-%m-%d %H:%M:%S")
    bdict['training_datarange'] = [model_input_data.index.min(), model_input_data.index.max()]

    if "regression" in model_type:
        model_target = kwargs['model_target']
        mse = kwargs['mse']
        bdict['sensor_num'] = sensor_num
        bdict['equipment_type'] = equipment_type
        bdict['model_target'] = model_target
        bdict['mse'] = mse

    elif "clustering" in model_type:
        status_dic = kwargs['status_dic']
        bdict['model_target'] = status_dic
        bdict['mse'] = None
    adict['metadata'] = bdict
    adict['model'] = pickle.dumps(model)
    for i in model_input_data.columns:
        rList = []
        rList.append(model_input_data[i].min())
        rList.append(model_input_data[i].max())
        cdict[i] = rList
    adict['metadata']['model_input_datarange'] = cdict
    return adict
# #
# save pickle object


def save_obj(obj, name):
    with open(name + '.pickle', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

#############
TZOFFSET = -4
S3BUCKET_NAME = 'mhspredict-site-dhl-miami'


tags = [
    'rms_velocity_z',
    'temperature',
    'rms_velocity_x',
    'kurtosis_z',
    'kurtosis_x',
    'crest_acceleration_z',
    'crest_acceleration_x'
]


conveyors = ['PF-F1-1', 'HC-F1-3PT']
# ,'T2-F2-2PT','LF-F1-7PT','PF-F2-3PT','LF-F1-2','LF-F1-4'
#  'T1-F1-1','LF-F1-3PT','T1-F1-4PT','T1-F1-2PT','T2-F1-2PT',
#  'PF-F1-1','HC-F1-3PT',
# 'PF-F1-3PT','T2-F1-6PT','T2-F1-1',
#  'PF-F1-5PT','T2-F2-1','XR-F1-1','PF-F1-6','PF-F2-4',
#  'T2-F1-4PT','LF-F1-1','HC-F1-2PT','T2-F1-3','	T1-F1-5',
#  'XR-F1-2PT','PF-F1-2','LF-F1-5PT','PF-F1-7PT','PF-F1-4','T2-F1-7']

# set datatime and pulling data for that timeframe using Athena
t1 = datetime(day=1, month=11, year=2018, hour=0)
t2 = datetime(day=1, month=11, year=2018, hour=1)
timetuple = (t1, t2)
dqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)
# #tt = data_retrieval.construct_relative_timestamp_tuple(time_back="60 minute",tzoffset=TZOFFSET)
for conveyor in conveyors:
    temp_data_dict = dqe.execute_timeseries_data_query(
        taglist=tags,
        timerange_tuple=timetuple,
        conveyor=conveyor,
        equip="all",
        return_as_df=True)
    # if "STRUCT" in temp_data_dict.keys():
    #     temp_data_dict = temp_data_dict["STRUCT"]
    locations = dqe.get_conveyor_sensors(conveyor=conveyor)
    # aggregate dataframe for use in all of the models
    aggregated_df = data_handles.merge_disparate_dfs(
        list(temp_data_dict.values()), merge_method='nearest', merge_tol='15s')
    # make sure that datetime is the index
    aggregated_df = data_handles.set_datetime_index(aggregated_df)

    # fill na based on the most recent rows. Next row preferred, if not exist, use the previous row
    aggregated_df.fillna(method='bfill', inplace=True)
    aggregated_df.fillna(method='ffill', inplace=True)

    # remove data points that are out of healthy range (similar with QA data range check)
    for column in aggregated_df.columns:
        for tag in datarange:
            if tag in column:
                aggregated_df = aggregated_df[aggregated_df[column] > min(datarange[tag])]
                aggregated_df = aggregated_df[aggregated_df[column] < max(datarange[tag])]

    # save the original format of data pulling
    merged_data = aggregated_df.copy()
    # convert 'sensor*' to str
    for column in merged_data.columns:
        if 'sensor' in column:
            merged_data[column] = merged_data[column].astype('str')

    # Drop categorical variables
    for column in merged_data.columns:
        if column in merged_data.select_dtypes(include=['category', 'object']).columns:
            merged_data.drop(column, axis=1, inplace=True)

    # Create correlation matrix
    corr_matrix = merged_data.corr(method='spearman').abs()

    # Select upper triangle of correlation matrix
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

    # Find index of feature columns with correlation greater than 0.90
    to_drop = [column for column in upper.columns if any(upper[column] > 0.95)]

    # DO NOT Remove Target Variable
    target_list = ['temperature', 'rms_velocity_z', 'rms_velocity_x']
    listnotremove = []
    for i in merged_data.columns:
        for j in target_list:
            if i.find(j) > 0:
                listnotremove.append(i)

    for i in listnotremove:
        if i in to_drop:
            to_drop.remove(i)

    # Drop highly correlated variables (correlation larger than 0.95)
    merged_data = merged_data.drop(to_drop, axis=1, inplace=False)

    # # ## uploading metadata
    version = '0.0.1'
    model_type = 'gradient-boosting-regression'
    prefix = 'models/timeseries_predictions/{c}/'.format(c=conveyor.replace('-', '_'))
    # listnotremove is the list of all target columns
    for model_target in listnotremove:
        ipdb.set_trace()
        sensor_num = locations[locations['location'] == model_target.split('.')[0]]['sensor'].values[0]
        equipment_type = model_target.split('.')[0]
        # train only one equipment using following code
        # if equipment_type == 'STRUCT':
        #     pass
        # else:
        #     continue
        model, model_input_data, training_time, mse = gbm_model(merged_data, model_target)
        metadata = create_metadata(model, model_input_data, training_time, model_target=model_target, mse=mse)
        name = 'gbm_{c}_{mt}'.format(c=conveyor.replace('-', '_'), mt=model_target.replace('.', '_'))
        if not os.path.exists(os.path.dirname('./models_metadata/{b}/'.format(b=S3BUCKET_NAME))):
            os.makedirs(os.path.dirname('./models_metadata/{b}/'.format(b=S3BUCKET_NAME)))
        save_obj(metadata, './models_metadata/{b}/'.format(b=S3BUCKET_NAME) + name)
        boto3.Session().resource('s3').Bucket(S3BUCKET_NAME).Object(os.path.join(prefix, '{name}.pickle'.format(
            name=name))).upload_file(('./models_metadata/{b}/'.format(b=S3BUCKET_NAME) + name + '.pickle'))


# Kmeans - on/off threshold

# create a list to save all the model input tags
# kmeans_input_list = ['rms_velocity_z','rms_velocity_x']
# kmeans_input = []
# for i in merged_data.columns:
#     for j in kmeans_input_list:
#         if i.find(j) > 0:
#             kmeans_input.append(i)


# def kmeans_clustering(input_data):
#     '''
#     parameter:
#         input_data: pandas dataframe
#     returns:
#         kmeans: kmeans models
#         X: input data in the training
#         training_time: the training time from the model
#
#     '''
#     training_time = datetime.now()
#     X = input_data[kmeans_input]
#     resultdf = X.copy()
#     X_train, scaler = data_preparation.scale_data(X)
#     kmeans = KMeans(n_clusters=2, random_state=0).fit(X_train)
#     resultdf['pred'] = kmeans.labels_
#     aggdf = resultdf.groupby(['pred'])['M.rms_velocity_x'].mean().reset_index(name='mean')
#     aggdf['status'] = np.where(aggdf['mean'] == max(aggdf['mean']), 'on','off')
#     status_dic = {}
#     status_dic[0] = aggdf.loc[aggdf['pred'] == 0]['status'].reset_index(drop=True)[0]
#     status_dic[1] = aggdf.loc[aggdf['pred'] == 1]['status'].reset_index(drop=True)[0]
#
#     return (kmeans,X,training_time,status_dic)

# function calls for kmeans
# for conveyor in conveyors:
#     version = '0.0.1'
#     model_type = 'kmeans-clustering'
#     model, model_input_data, training_time, status_dic = kmeans_clustering(merged_data)
#     metadata = create_metadata(model, model_input_data, training_time,status_dic = status_dic)
#     prefix = 'models/on_off/{c}/'.format(c = conveyor.replace('-','_'))
#     name = 'kmeans_model_{c}_on_off'.format(c = conveyor.replace('-','_'))
#     save_obj(metadata,'./kmeans_metadata/'+name)
#     boto3.Session().resource('s3').Bucket(S3BUCKET_NAME).Object(os.path.join(prefix,'{name}.pickle'.format(name = name))).upload_file('./kmeans_metadata/'+ name + '.pickle')


# following for hyperparameter tunning... Work In Progress..
# def GradientBooster(param_grid, n_jobs):
#     """
#     Gerenate gredient boosting regression to auto run the cross validation
#
#     param_grid: dictionary with list inside for every hyperperimeter
#
#     n_jobs = number of jobs run in parallel
#
#     default using 5-fold cross validation.
#
#     """
#     estimator = ensemble.GradientBoostingRegressor()
#     cv = ShuffleSplit(n_splits = 10, test_size=0.2, random_state = 0)
#     classifier = GridSearchCV(estimator=estimator, cv=cv.get_n_splits(X_train.shape[0]),                               param_grid=param_grid, n_jobs=n_jobs)
#     classifier.fit(X_train, y_train)
#     print ("Best Estimator learned through GridSearch")
#     print (classifier.best_estimator_)
#     return cv, classifier.best_estimator_


# In[41]:
#
#
# def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None, n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
#     """
#     Generate a simple plot of the test and traning learning curve.
#     Parameters
#     ----------
#     estimator : object type that implements the "fit" and "predict" methods An object of that type which is cloned for each validation.
#
#     title : string Title for the chart.
#
#     X : array-like, shape (n_samples, n_features) Training vector,
#     where n_samples is the number of samples and n_features is the number of features.
#
#     y : array-like, shape (n_samples) or (n_samples, n_features)
#
#     ylim : tuple, shape (ymin, ymax), optional Defines minimum and maximum yvalues plotted.
#
#     cv : integer, cross-validation generator, optional If an integer is passed,
#     it is the number of folds (defaults to 3).
#     Specific cross-validation objects can be passed, see sklearn.cross_validation module for the list of possible objects
#
#     n_jobs : integer, optional Number of jobs to run in parallel (default 1).
#
#     train_sizes: array like; [0.1 , 0.325, 0.55 , 0.775, 1.0]
#
#     """
#
#     plt.figure()
#     plt.title(title)
#     if ylim is not None:
#         plt.ylim(*ylim)
#     plt.xlabel("Training examples")
#     plt.ylabel("Score")
#     train_sizes, train_scores, test_scores = learning_curve(estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
#     train_scores_mean = np.mean(train_scores, axis=1)
#     train_scores_std = np.std(train_scores, axis=1)
#     test_scores_mean = np.mean(test_scores, axis=1)
#     test_scores_std = np.std(test_scores, axis=1)
#     plt.grid()
#
#     plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.1, color="r")
#     plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")
#     plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score")
#     plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")
#     plt.legend(loc="best")
#     return plt

# param_grid={'n_estimators':[100],
#             'learning_rate': [0.1, 0.05], #, 0.02, 0.01]
#             'max_depth':[6,4],
#             'min_samples_leaf':[3,5],#,9,17],
#             'max_features':[1.0,0.3]#,0.1]
#            }
# n_jobs=4
# cv,best_est=GradientBooster(param_grid, n_jobs)


# print ("Best Estimator Parameters")
# print ("---------------------------")
# print ("n_estimators: %d" %best_est.n_estimators)
# print ("max_depth: %d" %best_est.max_depth)
# print ("Learning Rate: %.1f" %best_est.learning_rate)
# print ("min_samples_leaf: %d" %best_est.min_samples_leaf)
# print ("max_features: %.1f" %best_est.max_features)
# print ("Train R-squared: %.2f" %best_est.score(X_train,y_train))


# title = "Learning Curves (Gradient Boosted Regression Trees)"
# estimator = ensemble.GradientBoostingRegressor(n_estimators=best_est.n_estimators, \
#                                       max_depth=best_est.max_depth, \
#                                       learning_rate=best_est.learning_rate,\
#                                       min_samples_leaf=best_est.min_samples_leaf, \
#                                       max_features=best_est.max_features)

# plot_learning_curve(estimator, title, X_train, y_train, cv=cv, n_jobs=n_jobs)
# plt.show()


# testing
# X = merged_data.drop(['M.temperature','G.temperature'], axis=1, inplace=False)
# y = merged_data['G.temperature']
#
# X_train, X_test, y_train, y_test = train_test_split(X, y,test_size=0.2, shuffle=False)
#
# params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 2,
#           'learning_rate': 0.01, 'loss': 'ls'}
# clf = ensemble.GradientBoostingRegressor(**params)
# clf.fit(X_train, y_train)
# y_test_pred = clf.predict(X_test)
# mse = mean_squared_error(y_test, y_test_pred)
# print (mse)


# variable importance
# importances = pd.DataFrame({'feature':X_train.columns,'importance':np.round(clf.feature_importances_,3)})
# importances = importances.sort_values('importance',ascending=False).set_index('feature')
# print (importances)
# importances.plot.bar()
