#!/usr/bin/env python3

# create a list to save all the model input tags
kmeans_input_list = ['rms_velocity_z', 'rms_velocity_x']
kmeans_input = []
for i in merged_data.columns:
    for j in kmeans_input_list:
        if i.find(j) > 0:
            kmeans_input.append(i)

function calls for kmeans
for conveyor in conveyors:
    version = '0.0.1'
    model_type = 'kmeans-clustering'
    model, model_input_data, training_time, status_dic = kmeans_clustering(merged_data)
    metadata = create_metadata(model, model_input_data, training_time, status_dic=status_dic)
    prefix = 'models/on_off/{c}/'.format(c=conveyor.replace('-', '_'))
    name = 'kmeans_model_{c}_on_off'.format(c=conveyor.replace('-', '_'))
    save_obj(metadata, './kmeans_metadata/' + name)
    boto3.Session().resource('s3').Bucket(S3BUCKET_NAME).Object(os.path.join(
        prefix, '{name}.pickle'.format(name=name))).upload_file('./kmeans_metadata/' + name + '.pickle')
