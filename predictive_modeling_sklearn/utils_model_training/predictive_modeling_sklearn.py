#!/usr/bin/env python3

from datetime import datetime
import math
import time
import logging

import pandas as pd
import numpy as np
import pickle


from sklearn import ensemble
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.cluster import KMeans


logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))

# GBM Model - regressor


def gbm_model(input_data, target_column, rely_on_vibrations=False, performCV=True, cv_fold=5):
    """
    input_data: pandas dataframe
    target_column: pandas series
    rely_on_vibrations: whether we need vz to perdict vx or not
    preformCV: whether we want implement cross validation or not
    cv_fold: cross validation fold
    """

    ydrop = [target_column]
    y = input_data[target_column]
    # not using gearbox to predict motor or the other way around since those two are connected
    if 'M.' in target_column:
        if ''.join(['G.', target_column.split('.')[1]]) in input_data.columns:
            ydrop.append((target_column.replace('M.', 'G.')))
    elif 'G.' in target_column:
        if ''.join(['M.', target_column.split('.')[1]]) in input_data.columns:
            ydrop.append((target_column.replace('G.', 'M.')))
    else:
        pass

    # not using vibrations z if predicting vibrations x for every equipment
    # not using vibrations x if predicting vibrations z for every equipment
    if not rely_on_vibrations:
        if 'rms_velocity_x' in target_column:
            ydrop.append(f"{target_column.split('.')[0]}.rms_velocity_z")
        elif 'rms_velocity_z' in target_column:
            ydrop.append(f"{target_column.split('.')[0]}.rms_velocity_x")

    X = input_data.drop(ydrop, axis=1, inplace=False)
    # Train, Test Split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)
    # Gradient Boosting Regression
    training_time = datetime.now()
    # Tuning Job Start
    if performCV:
        # tune model related hyperparameters forst, define tree related hyperparameters fixed, will tune later.
        # set default learning rate and n_estimators
        tuning_model_hparam = {'learning_rate': [0.15, 0.1, 0.05, 0.01, 0.005, 0.001],
                               'n_estimators': [5750, 6000, 6250, 6500, 6750]}
        #[250, 500, 750, 1000, 1250, 1500, 1750]
        while True:
            logger.info('tunning model specific hyperparameters: learning_rate and n_estimators')
            t0 = time.time()
            tuning_model = GridSearchCV(estimator=ensemble.GradientBoostingRegressor(max_depth=4, min_samples_split=20, min_samples_leaf=10, subsample=0.8, max_features='sqrt', random_state=10),
                                        param_grid=tuning_model_hparam, scoring='neg_mean_squared_error', n_jobs=4, iid=False, cv=cv_fold)
            tuning_model.fit(X_train, y_train)
            best_model_hparam = tuning_model.best_params_
            best_learning_rate = best_model_hparam['learning_rate']
            best_n_estimators = best_model_hparam['n_estimators']

            # check if the best estimators falls into the start or the end of the list
            if (best_learning_rate != tuning_model_hparam['learning_rate'][-1]) & (best_n_estimators != tuning_model_hparam['n_estimators'][-1]):
                break
            else:
                logger.info(
                    'best parameters exist either in the start or the end of the list, recreate the list and run the cross validation again for model specific hyperparameters')
                logger.info(f'current best cross-validated socre is {tuning_model.best_score_}')
                if best_learning_rate == tuning_model_hparam['learning_rate'][-1]:
                    learning_rate_start = tuning_model_hparam['learning_rate'][-1]
                    learning_rate_end = tuning_model_hparam['learning_rate'][-1] / 10
                    learning_rate_step = (learning_rate_start - learning_rate_end) / 5 * (-1)
                    tuning_model_hparam['learning_rate'] = np.arange(
                        learning_rate_start, learning_rate_end, learning_rate_step)
                    logger.info(f"the new learning_rate list is {tuning_model_hparam['learning_rate']}")
                if best_n_estimators == tuning_model_hparam['n_estimators'][-1]:
                    n_estimators_start = tuning_model_hparam['n_estimators'][-1]
                    n_estimators_step = 250
                    n_estimators_end = n_estimators_start + 5 * n_estimators_step
                    tuning_model_hparam['n_estimators'] = list(
                        range(n_estimators_start, n_estimators_end, n_estimators_step))
                    logger.info(f"the new n_estimators list is {tuning_model_hparam['n_estimators']}")

        logger.info(f'It takes {time.time() - t0} seconds to tune model specific hyperparameters: learning_rate and n_estimators, best value for learning_rate is {best_learning_rate}, and best value for n_estimators is {best_n_estimators}')
        # tune tree specific hyperparameters (max_depth)
        logger.info('tunning tree specific hyperparameters: max_depth')
        tuning_tree_hparam = {'max_depth': [4, 6, 8, 10, 12, 14]}
        t0 = time.time()
        tuning_tree = GridSearchCV(estimator=ensemble.GradientBoostingRegressor(learning_rate=best_learning_rate, n_estimators=best_n_estimators, min_samples_split=20, min_samples_leaf=10, subsample=0.8, max_features='sqrt', random_state=10),
                                   param_grid=tuning_tree_hparam, scoring='neg_mean_squared_error', n_jobs=4, iid=False, cv=cv_fold)

        tuning_tree.fit(X_train, y_train)
        tuning_tree_hparam = tuning_tree.best_params_
        best_max_depth = tuning_tree_hparam['max_depth']
        # tune tree specific hyperparameters (min_samples_split, min_samples_leaf)
        logger.info(f'It takes {time.time() - t0} seconds to tune tree specific hyperparameters: max_depth, best value for max_depth is {best_max_depth}')
        while True:
            logger.info('tunning tree specific hyperparameters: min_samples_split and min_samples_leaf')
            min_samples_split_start = math.floor(input_data.shape[0] * 0.01)
            min_samples_split_end = min_samples_split_start * 10
            step = (min_samples_split_end - min_samples_split_start) // 5
            tuning_tree_hparam2 = {'min_samples_split': range(
                min_samples_split_start, min_samples_split_end, step), 'min_samples_leaf': [10, 20, 30, 40, 50]}
            t0 = time.time()
            tuning_tree_2 = GridSearchCV(estimator=ensemble.GradientBoostingRegressor(learning_rate=best_learning_rate, n_estimators=best_n_estimators, max_depth=best_max_depth, subsample=0.8, max_features='sqrt', random_state=10),
                                         param_grid=tuning_tree_hparam2, scoring='neg_mean_squared_error', n_jobs=4, iid=False, cv=cv_fold)
            tuning_tree_2.fit(X_train, y_train)
            tuning_tree_hparam2 = tuning_tree_2.best_params_
            best_min_samples_split = tuning_tree_hparam2['min_samples_split']
            best_min_samples_leaf = tuning_tree_hparam2['min_samples_leaf']

            if (best_min_samples_split != tuning_tree_hparam2['min_samples_split'][-1]) & (tuning_tree_hparam2['min_samples_leaf'][-1]):
                break
            else:
                logger.info(
                    "best parameters exist either in the start or the end of the list, recreate the list and run the cross validation again for tree specific hyperparameters")
                if best_min_samples_split == tuning_model_hparam['min_samples_split'][-1]:
                    min_samples_split_start = tuning_model_hparam['min_samples_split'][-1]
                    min_samples_split_end = tuning_model_hparam['min_samples_split'][-1] * 10
                    min_samples_split_step = (min_samples_split_end - min_samples_split_start) // 5
                    tuning_model_hparam['min_samples_split'] = list(range(
                        min_samples_split_start, min_samples_split_end, min_samples_split_step))
                    logger.info(f"the new min_samples_split list is {tuning_model_hparam['min_samples_split']}")
                if best_min_samples_leaf == tuning_model_hparam['min_samples_leaf'][-1]:
                    min_samples_leaf_start = tuning_model_hparam['min_samples_leaf'][-1]
                    min_samples_leaf_step = 10
                    min_samples_leaf_end = min_samples_leaf_start + 5 * n_estimators_step
                    tuning_model_hparam['min_samples_leaf'] = list(range(
                        min_samples_leaf_start, min_samples_leaf_end, min_samples_leaf_step))
                    logger.info(f"the new min_samples_split list is {tuning_model_hparam['min_samples_leaf']}")

        logger.info(f'It takes {time.time() - t0} seconds to tune tree specific hyperparameters: min_samples_split and min_samples_leaf, best value for min_samples_leaf is {best_min_samples_leaf} and best_min_samples_split is {best_min_samples_split}')
        final_model = ensemble.GradientBoostingRegressor(learning_rate=best_learning_rate, n_estimators=best_n_estimators,
                                                         max_depth=best_max_depth, min_samples_split=best_min_samples_split, min_samples_leaf=best_min_samples_leaf, subsample=0.8, max_features='sqrt', random_state=10)
        final_model.fit(X_train, y_train)
        import ipdb
        ipdb.set_trace()
        final_model.feature_importances_

    else:
        params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 20, 'min_samples_leaf': 10,
                  'learning_rate': 0.01, 'loss': 'ls'}
        final_model = ensemble.GradientBoostingRegressor(**params)
        final_model.fit(X_train, y_train)

    y_test_pred = final_model.predict(X_test)
    mse = mean_squared_error(y_test, y_test_pred)
    return (final_model, X_train, training_time, mse)

# XGB model - regressor


def create_metadata(conveyor, tags, model, version, model_type, training_time, model_input_data, **kwargs):
    '''
    function for creating metadata, used for both gradient boosting machine and kmeans
    conveyor: the conveyor for this metadata
    tags = the tags needed for make models
    model: model output from modeling function
    version: model version
    model_type: model type used
    training_time: the training time of the model
    model_input_data: the model input dataframe used in training phases
    training_time: the training time from the model
    *** kwargs: model_target, mse, sensor_num, equipment_type: those kwargs are for timeseries prediction model not in kmeans
    '''
    adict = {}
    bdict = {}
    cdict = {}
    bdict['conveyor'] = conveyor
    bdict['query_tags'] = tags
    bdict['model_version'] = version
    bdict['model_type'] = model_type
    bdict['training_time'] = training_time.strftime("%Y-%m-%d %H:%M:%S")
    bdict['training_datarange'] = [model_input_data.index.min(), model_input_data.index.max()]

    if "regression" in model_type:
        bdict['model_target'] = kwargs['model_target']
        bdict['mse'] = kwargs['mse']
        bdict['sensor_num'] = kwargs['sensor_num']
        bdict['equipment_type'] = kwargs['equipment_type']

    elif "clustering" in model_type:
        status_dic = kwargs['status_dic']
        bdict['model_target'] = status_dic
        bdict['mse'] = None
    adict['metadata'] = bdict
    adict['model'] = pickle.dumps(model)
    for i in model_input_data.columns:
        rList = []
        rList.append(model_input_data[i].min())
        rList.append(model_input_data[i].max())
        cdict[i] = rList
    adict['metadata']['model_input_datarange'] = cdict
    return adict

# save pickle object


def save_obj(obj, name):
    with open(name + '.pickle', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


# Kmeans - on/off threshold
def kmeans_clustering(input_data):
    '''
    parameter:
        input_data: pandas dataframe
    returns:
        kmeans: kmeans models
        X: input data in the training
        training_time: the training time from the model

    '''
    training_time = datetime.now()
    X = input_data[kmeans_input]
    resultdf = X.copy()
    X_train, scaler = data_preparation.scale_data(X)
    kmeans = KMeans(n_clusters=2, random_state=0).fit(X_train)
    resultdf['pred'] = kmeans.labels_
    aggdf = resultdf.groupby(['pred'])['M.rms_velocity_x'].mean().reset_index(name='mean')
    aggdf['status'] = np.where(aggdf['mean'] == max(aggdf['mean']), 'on', 'off')
    status_dic = {}
    status_dic[0] = aggdf.loc[aggdf['pred'] == 0]['status'].reset_index(drop=True)[0]
    status_dic[1] = aggdf.loc[aggdf['pred'] == 1]['status'].reset_index(drop=True)[0]

    return (kmeans, X, training_time, status_dic)
