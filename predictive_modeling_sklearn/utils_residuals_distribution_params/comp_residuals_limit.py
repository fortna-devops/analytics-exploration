#!/usr/bin/env python3
'''
extract residual data and compute confidence level of rasing an alarm
'''
import pandas as pd
import scipy
import scipy.stats
import numpy as np

from analytics_sdk_core.data_init import data_pull

import ipdb


sensitive_level = 0.80  # large number means less sensitive to raise an alarm
distribution_dict = {
    # mean and std for residual distribution of rms_velocity_x
    'rms_velocity_x': [-0.016780082124588445, 0.32916378448664824],
    # mean and std for residual distribution of rms_velocity_z
    'rms_velocity_z': [-0.015673157262345243, 0.3108498098616547],
    'temperature': [2.6689308594988814, 7.090382199858984]  # mean and std for residual distribution of temperature
}

# pull residual data

# dqe = data_pull.DataQueryEngineLite(s3_bucket_name='mhspredict-site-fedex-louisville')
# raw_query = \
# "SELECT * FROM timeseries_predictions \
#         WHERE date \
#             BETWEEN date '2019-1-17' \
#                 AND date '2019-1-17' \
#         AND timestamp \
#             BETWEEN timestamp '2019-1-17 00:00:00' \
#                 AND timestamp '2019-1-17 01:00:00'"
# query_data_results = dqe.execute_generic_query(raw_query, return_as_df = True)
# ipdb.set_trace()

tolerance_level_dict = {}
for tag in distribution_dict.keys():
    # calculate tolerance value associate with sensitive level
    residual_upper_level = np.round(scipy.stats.norm(
        distribution_dict[tag][0], distribution_dict[tag][1]).ppf(sensitive_level), 3)
    residual_lower_level = np.round(scipy.stats.norm(distribution_dict[tag][0], distribution_dict[
        tag][1]).ppf(1 - sensitive_level), 3)

    tolerance_level_dict[tag] = [residual_lower_level, residual_upper_level]
    residual_col = '{t}_residuals'.format(t=tag)
    normal_distribution = scipy.stats.norm(distribution_dict[tag][0], distribution_dict[tag][1])
    ipdb.set_trace()
    # data['confidence_level'] = data.where(~data[residual_col].between(residual_lower_level,residual_upper_level), other
    #data['confidence_level'] = data[residual_col].apply(lambda x: normal_distribution.cdf(x))


ipdb.set_trace()
# calculate confidence interval
# for tag in distribution_dict.keys():
#
# scipy.stats.norm(0.00014132, 0.0085674).cdf(residual)
