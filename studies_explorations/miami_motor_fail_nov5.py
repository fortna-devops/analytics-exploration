import pandas as pd
import numpy as np
import pdb

from analytics_toolbox.data_init import data_retrieval
from data_retrieval import DataQueryEngine


conveyors=[
"PF-F1-5PT",
"PF-F1-3PT",
"LF-F1-5PT",
"LF-F1-3PT"]

for c in conveyors:
	dqe = DataQueryEngine(s3_bucket_name="mhspredict-site-dhl-miami")
	tt = data_retrieval.construct_relative_timestamp_tuple("15 days",-4)
	df = dqe.execute_timeseries_data_query(taglist=['hf_rms_acceleration_x','hf_rms_acceleration_z','vfd_torque'],
									  conveyor=c,
									  equip='B',
									  timerange_tuple=tt)

	df.to_csv('{}_dhlmiami_bearing_study.csv'.format(c).replace('-','_'))






df=pd.read_csv('miami_motor_fail_nov5.csv')

df['sensor']

df_M = df[df['sensor']==32]


df_G = df[df['sensor']==29]

df_B = df[df['sensor']==88]

dfs = [df_M,df_G,df_B]


for equip_df in dfs:
	equip_df.index = pd.DatetimeIndex(equip_df.timestamp)
	equip_df.sort_index(inplace=True)
	grouped_df = equip_df.groupby(pd.Grouper(freq='24h'))
	for group,day_df in grouped_df:
		#pdb.set_trace()
		z_95 = np.percentile(day_df.rms_velocity_z,95)
		x_95 = np.percentile(day_df.rms_velocity_x,95)

		print("{d} 95th percentile rms_velocity_x for {s}: {v}".format(d=group,s=day_df.sensor[0],v=x_95))
		print("{d} 95th percentile rms_velocity_z for {s}: {v}".format(d=group,s=day_df.sensor[0],v=z_95))



