import pandas as pd
import numpy as np
import plotly.offline as pltly
import plotly.graph_objs as go
import plotly.figure_factory as ff
import pdb

from analytics_toolbox.data_init.data_retrieval import DataQueryEngine, construct_relative_timestamp_tuple
from analytics_toolbox.data_init.data_handles import merge_disparate_dfs



pwr_turn_conveyors=[
"PF-F1-5PT",
"PF-F1-3PT",
"LF-F1-5PT",
"LF-F1-3PT"]

# for c in pwr_turn_conveyors:
# 	dqe = DataQueryEngine(s3_bucket_name="mhspredict-site-dhl-miami")
# 	tt = construct_relative_timestamp_tuple("15 days",-4)
# 	df_dict = dqe.execute_timeseries_data_query(taglist=['hf_rms_acceleration_x','hf_rms_acceleration_z','torque'],
# 									  conveyor=c,
# 									  equip='all',
# 									  timerange_tuple=tt)
# 	pdb.set_trace()

# 	for k,v in df_dict.items():
# 		df = v
# 		df.to_csv('{c}_{eq}dhlmiami_bearing_study.csv'.format(c=c,eq=k).replace('-','_'))

# bearing types that we have pulled data for
bearing_types = ['BMF','BMN','BF','BN']
for c in pwr_turn_conveyors:
	plc_df_path = "{c}_PLC_{c}dhlmiami_bearing_study.csv".format(c=c).replace('-','_')
	plc_df = pd.read_csv(plc_df_path)
	for b_type in bearing_types:
		df = pd.read_csv('{c}_{e}dhlmiami_bearing_study.csv'.format(c=c,e=b_type).replace('-','_'))
		merged_df = merge_disparate_dfs([df,plc_df],merge_tol='25s',merge_method='round')
		corr_df = merged_df.corr()
		print("Bearing Type: {}".format(b_type))
		print("Correleation DF: {}".format(corr_df))


		fig_heatmap=ff.create_annotated_heatmap(z=merged_df.corr().values,
										x=list(merged_df.corr().columns),
										y=list(merged_df.corr().columns),
										colorscale='YlGnBu')
		#pltly.plot(fig_heatmap)
		data = []
		# pdb.set_trace()
		data.append(
			go.Scatter(
				y=merged_df["{}.hf_rms_acceleration_z".format(b_type)],
				x=merged_df["PLC-{}.torque".format(c)],
				mode='markers'
				)
			)
		layout = go.Layout(
			title="{} Bearings ({}) Tag Relations".format(c,b_type),
			yaxis=dict(title="HF RMS ACCELERATION Z"),
			xaxis=dict(title="TORQUE"))
		fig_scatter = go.Figure(data=data, layout=layout)
		pltly.plot(fig_scatter)


