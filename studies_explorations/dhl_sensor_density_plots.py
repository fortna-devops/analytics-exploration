
import pdb
import pandas as pd
from analytics_toolbox.data_init.data_retrieval import DataQueryEngine as DQE

from analytics_toolbox.data_init.data_retrieval import construct_relative_timestamp_tuple
import matplotlib.pyplot as plt

bucket = "mhspredict-site-dhl-miami"
dqe = DQE(bucket)


# location_query = """SELECT * FROM location"""

# lq_result = dqe.execute_generic_query(location_query,return_as_df=False)

# pw_turn_sensors = [x['sensor'] for x in lq_result if 'power turn' in x['description'].lower() and 'b' == x['equipment'].lower()]
# pulley_sensors = [x['sensor'] for x in lq_result if 'pull' in x['description'].lower() and 'b' == x['equipment'].lower()]
# print(pulley_sensors)
# print(pw_turn_sensors)
pulley_sensors = ['47', '61', '66', '84', '15', '25', '62', '95', '28']
pw_turn_sensors=['5', '74', '67', '55', '12', '2', '93', '69', '38', '58', '85', '24', '72', '77', '88', '8', '3', '81', '50', '80', '7', '78', '49', '36', '37', '56', '54']
def construct_generic_query(sens_list):


	timerange_tuple = construct_relative_timestamp_tuple('30 days',-4)

	joined_sensor_filter = " or ".join(["sensor='{}'".format(s) for s in sens_list])

	tag_str = "hf_rms_acceleration_x,hf_rms_acceleration_z"
	tname='sensor_minute'

	d1 = min(timerange_tuple).strftime("%Y-%m-%d")
	d2 = max(timerange_tuple).strftime("%Y-%m-%d")


	t1 = min(timerange_tuple).strftime("%Y-%m-%d %H:%M:00")
	t2 = max(timerange_tuple).strftime("%Y-%m-%d %H:%M:00")

	query_string = """SELECT {comma_sep_tags} FROM {table} 
	WHERE date between date '{d1}' AND date '{d2}'
	AND timestamp
	between timestamp '{t1}' AND timestamp '{t2}' 
	AND ({sensor_eq});""".format(
		comma_sep_tags=tag_str,
		table=tname,
		d1=d1,
		d2=d2,
		t1=t1,
		t2=t2,
		sensor_eq=joined_sensor_filter
		)
	return query_string


pw_query = construct_generic_query(pw_turn_sensors)
pulley_query = construct_generic_query(pulley_sensors)


# pw_df = dqe.execute_generic_query(raw_query_string=pw_query,return_as_df=True)
# pw_df.to_csv('power_turn_dhl_miami_bearings_30day_minute_downsample.csv')


# pulley_df = dqe.execute_generic_query(raw_query_string=pulley_query,return_as_df=True)
# pulley_df.to_csv('motorized_pulley_dhl_miami_bearings_30day_minute_downsample.csv')
# pdb.set_trace()

pulley_df = pd.read_csv('motorized_pulley_dhl_miami_bearings_30day_minute_downsample.csv')

# pw_df = pd.read_csv('power_turn_dhl_miami_bearings_30day_minute_downsample.csv')

# plt.figure(figsize=(10,10))
# plt.subplot(211)
# pw_df['hf_rms_acceleration_z'].plot(kind = 'density',legend = True, title = 'Power Turn Bearing Data Distribution' )
# pw_df['hf_rms_acceleration_x'].plot(kind = 'density',legend = True, title = 'Power Turn Bearing Data Distribution' )
# plt.show()
# plt.close()


# # plt.figure(figsize=(10,10))
# # plt.subplot(212)
pulley_df = pulley_df[pulley_df['hf_rms_acceleration_z']<12]
pulley_df = pulley_df[pulley_df['hf_rms_acceleration_x']<12]
pulley_df['hf_rms_acceleration_z'].plot(kind = 'density',legend = True, title = 'Motorized Pulley Bearing Data Distribution' )
pulley_df['hf_rms_acceleration_x'].plot(kind = 'density',legend = True, title = 'Motorized Pulley Bearing Data Distribution' )

plt.show()
plt.close()