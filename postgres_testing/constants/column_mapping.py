"""
Columns that need some extra attention when cleaning the data are placed here
in different categories. 

NOTE: If another key is added, logic needs to be added for that column in cleaning
script.

TODO: add dict for conveyor columns? dhl-cincinnati has differing units (feet and inches)
for width and length
"""

motor_columns = {
	'with_discrete_options' : 	['voltage', 
								'rpm', 
								'amperage', 
								'full_load_amps'],
	'with_units' : 				['length', 
								'ambient_temperature', 
								'efficiency_at_100_percent_load', 
								'high_voltage_full_load_amps', 
								'constant_torque_speed_range'],
	'with_fractions' : 			['length', 
								'shaft_size,'],
	'wrong_units' : 			['constant_torque_speed_range', 
								'variable_torque_speed_range',
								'nominal_efficiency',
								'efficiency_at_100_percent_load'],
	'not_in_range_format' : 	['constant_torque_speed_range', 
								'variable_torque_speed_range', 
								'ambient_temperature']
}

bearing_columns = {
	'with_fractions' : ['bore_diameter', 
						'base_to_centerline_height', 
						'distance_from_bearing_centerline_to_inner_ring_long_hub_face', 
						'distance_from_bearing_centerline_to_cap_face', 
						'bolt_center_to_center_length', 
						'mounting_pad_length', 
						'mounting_pad_width', 
						'mounting_pad_height', 
						'mounting_bolt_size', 
						'mouting_bolt_hole_slot_length', 
						'overall_length_or_diameter', 
						'overall_housing_height',
						'locking_collar_outside_diameter',
						'length_through_bore',
						'grease_fitting_size',
						'bearing_shaft_diameter'],
	'with_units' : ['operating_temperature_range'],
	'not_in_range_format' : ['operating_temperature_range'],
	'wrong_units' : ['maximum_temperature']
}

gearbox_columns = {
	'with_units' : ['weight']
}