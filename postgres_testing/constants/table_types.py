"""
specify data types for the table columns

IMPORTANT NOTE:
The PK 'id' column should NOT be here. It is generated automatically.
"""


MOTOR_SPECIFICATIONS_TYPES = {
	'sensor' : str, #used for reference purposes; will not be used in database
	'order_number' : str,
	'manufacturer' : str,
	'model' : str,
	'serial_number' : str,
	'frame' : str,
	'frame_material' : str,
	'hp' : float,
	'rpm' : float,
	'speed' : float,
	'speed_code' : str,
	'voltage' : float, # may change in the future
	'amperage' : float, # may change in the future
	'motor_size' : str,
	'class' : str,
	'full_load_amps' : float, # may change in the future
	'term_prot' : str,
	'weight' : float,
	'perf_data' : float,
	'dim_data' : float,
	'ph' : float,
	'hz' : float,
	'insulation_class' : str,
	'ratio' : float,
	'standards' : str,
	'length' : float,
	'ambient_temperature' : 'range',
	'service_factor' : float,
	'design_code' : str,
	'auxillary_box' : str,
	'drip_cover' : str,
	'blower' : str,
	'duty_rating' : str,
	'nominal_efficiency' : float,
	'efficiency_at_100_percent_load' : float,
	'electrically_isolated_bearing' : str,
	'front_face_code' : str,
	'feedback_device' : str,
	'high_voltage_full_load_amps' : float,
	'front_shaft_indicator' : str,
	'heater_indicator' : str,
	'inverter_code' : str,
	'kva_code' : str,
	'lifting_lungs' : str,
	'locked_bearing_indicator' : str,
	'lead_exit' : str,
	'lead_termination' : str,
	'lead_quantity_and_wire_size' : str,
	'type' : str,
	'mounting_arragement' : str,
	'number_of_poles' : int,
	'power_factor' : float,
	'product_family' : str,
	'pulley_end_bearing_type' : str,
	'pulley_end_face_code' : str,
	'pulley_shaft_indicator' : str,
	'rodent_screen' : str,
	'shaft_diameter' : float,
	'shaft_size' : float,
	'shaft_extension_location' : str,
	'shaft_ground_indicator' : str,
	'shaft_rotation' : str,
	'shaft_slinger_indicator' : str,
	'starting_method' : str,
	'constant_torque_speed_range' : 'range',
	'torque_rating' : float,
	'variable_torque_speed_range' : 'range',
	'series' : str,
	'thermal_class' : str,
	'ingress_protection' : int,
	'gear_unit_ratio' : float,
	'maximum_torque' : float,
	'oil_type' : str,
	'oil_fill_volume' : float,
	'enclosure_type' : str,
	'lubrication_type' : str,
	'gear_unit_type' : str,
	'gear_unit_option' : str,
	'gear_unit_size' : float,
	'install_date' : 'date',
	'last_replaced' : 'date'
}

BEARING_SPECIFICATIONS_TYPES = {
	'sensor' : str, #used for reference purposes; will not be used in database
	'order_number' : str,
	'manufacturer' : str,
	'model' : str,
	'bore_diameter' : float,
	'base_to_centerline_height' : float,
	'distance_from_bearing_centerline_to_inner_ring_long_hub_face' : float,
	'distance_from_bearing_centerline_to_cap_face' : float,
	'bolt_center_to_center_length' : float,
	'mounting_pad_length' : float,
	'mounting_pad_width' : float,
	'mounting_pad_height' : float,
	'mounting_bolt_size' : float,
	'mouting_bolt_hole_slot_length' : float,
	'overall_length_or_diameter' : float,
	'overall_housing_height' : float,
	'bore_type' : str,
	'duty_type' : str,
	'mounting_type' : str,
	'housing_material' : str,
	'locking_device' : str,
	'locking_collar_outside_diameter' : float,
	'expansion_type' : str,
	'dynamic_load_capacity' : float,
	'static_load_capacity' : float,
	'seal_type' : str,
	'length_through_bore' : float,
	'operating_temperature_range' : 'range',
	'maximum_temperature' : float,
	'finish_or_coating' : str,
	'maximum_rpm' : float,
	'vibration_frequency_inner_ring_defect' : float,
	'vibration_frequency_roller_spin' : float,
	'vibration_frequency_fundamental_train' : float,
	'vibration_frequency_outer_ring_defect' : float,
	'lubrication_type' : str,
	'grease_fitting_size' : str,
	'series' : str,
	'replacement_bearing' : str,
	'replacement_seal_kit' : str,
	'set_scerw_torque' : float,
	'shaft_diameter' : float,
	'install_date' : 'date',
	'last_replaced' : 'date'
}

GEARBOX_SPECIFICATIONS_TYPES = {
	'sensor' : str, #used for reference purposes; will not be used in database
	'order_number' : str,
	'manufacturer' : str,
	'model' : str,
	'serial_number' : str,
	'ratio' : float,
	'assembly' : str,
	'input_adaptor' : str,
	'hollow_output_shaft_bore_size' : float,
	'accessory_and_mounting' : str,
	'complete_nomenclature' : str,
	'weight' : float,
	'hp' : float,
	'rpm' : float,
	'sf' : float,
	'install_date' : 'date',
	'last_replaced' : 'date'
}

CONVEYOR_SPECIFICATIONS_TYPES = {
	'sensor' : str, #used for reference purposes; will not be used in database
	'manufacturer' : str,
	'description' : str,
	'width' : float,
	'length' : float
}

FACILITY_SPECIFICATIONS_TYPES = {
    'facility_name' : str,
	'organization' : str,
	'number_of_sensors' : int,
	'square_footage' : float,
	'country' : str,
	'region' : str,
	'biome' : str
}

WORK_ORDER_INFORMATION_TYPES = {
	'sensor_id' : 'uuid',
	'conveyor_id' : 'uuid',
	'reason' : str,
	'problem' : str,
	'requester_id' : str,
	'name' : str,
	'phone' : str,
	'email' : str,
	'asset_location' : str,
	'procedure' : str,
	'target_date' : 'date',
	'target_hours' : float,
	'type' : str,
	'category' : str,
	'account' : str,
	'priority' : int,
	'project' : str,
	'repair_center' : str,
	'shop' : str,
	'supervisor' : str,
	'shift' : str,
	'customer' : str,
	'status' : str,
	'sub_status' : str,
	'approval' : bool,
	'assignments' : str,
	'warranty' : bool,
	'attachments' : bool,
	'chargeable' : bool,
	'printed_or_e_mailed' : bool,
	'shutdown_required' : bool,
	'lockout_tagout' : bool,
	'follow_up_work' : bool
}

MASTER_SENSOR_TYPES = {
    'sensor' : str,
    'equipment_type' : str,
    'facility_id' : 'uuid',
    'conveyor_id' : 'uuid',
    'motor_id' : 'uuid',
    'bearing_id' : 'uuid',
    'gearbox_id' : 'uuid'
}

MASTER_CONVEYOR_TYPES = {
	'name' : str,
	'conveyor_id' : 'uuid',
	'facility_id' : 'uuid'
}