"""
To create fake data that makes more sense in demonstrations, here are some data
options for each of the columns.

"""

FAKE_WORK_ORDER_INFORMATION = {
	'reason' : ['something is wrong with the power', 'it is old', 'new replacement', 'missing parts'],
	'problem' : ['air issue', 'broken parts', 'worn out', 'electric issue'],
	'requester_id' : str,
	'name' : ['Bob Bobby', 'Lucy Sanders', 'Robert Smith', 'Natasha Panasyuk'],
	'phone' : ['888-2498-2480', '948-234-5820', '134-542-5982'],
	'email' : ['my-company@company.com', 'yes@me.net', 'hereitis@gmail.com'],
	'asset_location' : ['Atlanta, Georgia', 'Rome, Italy', 'New York City, New York', 'Asheville, North Carolina'],
	'procedure' : ['inspection', 'replacement', 'this action', 'monitor for 24 hours'],
	'target_date' : 'date',
	'target_hours' : float,
	'type' : str,
	'category' : str,
	'account' : str,
	'priority' : int,
	'project' : str,
	'repair_center' : ['MHS', 'competitor'],
	'shop' : ['main', 'sister shop', 'international branch'],
	'supervisor' : ['Daniel Smith', 'Andrey Lou', 'Jimin Kim'],
	'shift' : str,
	'customer' : str,
	'status' : ['requested', 'denied', 'complete', 'closed'],
	'sub_status' : ['in progress', 'halted'],
	'approval' : bool,
	'assignments' : str,
	'warranty' : bool,
	'attachments' : bool,
	'chargeable' : bool,
	'printed_or_e_mailed' : bool,
	'shutdown_required' : bool,
	'lockout_tagout' : bool,
	'follow_up_work' : bool
}

FAKE_MASTER_CONVEYOR = {
	'name' : ['BC-1', 'T4-H7', 'GP-5', 'GP-4', 'CONV3', 'CONV', 'AA-3', 'PF-4-03']
}

FAKE_CONVEYOR_SPECIFICATIONS = {
	'manufacturer' : ['Bastain', 'MHS', 'Best Belts', 'Conveyor Systems', 'We RCS']
}

FAKE_FACILITY_SPECIFICATIONS = {
	'country' : ['Italy', 'Germany', 'United States', 'Austrailia', 'Netherlands'],
	'region' : ['south', 'east', 'west', 'north', 'northeast', 'northwest', 'southeast', 'southwest', 'central'],
	'biome' : ['aquatic', 'forest', 'desert', 'tundra', 'grassland', 'chaparral']
}