"""
this script changes the column names in the following ways:
- verify that all of the column names are the same
  across all sites' sheets
- make sure that the length of the column names don't
  exceed the max length for columns on PostgreSQL (63 bytes)
- replace spaces in column names with underscores
- remove any special characters (there's an @ symbol in there
  and plenty of parenthesis)
- column names should be all lowercase
- there should be no equipment types in front of 
  specification type (ex: motor ratio) b/c it's redundant
- the unit specifications in parenthesis at the end needs
  to go. however, if this is done, where should we specify
  the units? ofc, i made copies, but is that good enough?


TODO: if there is a chance to improve this code i would reorganize \
it to minimize how many times i am opening the csv files. open it once, \
call the methods to do the things that need to be done inside, and close.
TODO: since i've written this code, i added more characters to the column \
names that will be ignored when handed off to postgres but are used for
reference purposes. so, to not trigger an error for a column name falsely \
being too long, write code to ignore that first word
"""

import os
import logging
import re
import csv
import unicodecsv
import string

PATH = "Documents/assets-info/postgres_testing/specs_as_csv_testing/"

def verify_sameness():
	"""
	verify that all of the sheets' columns are the same by comparing to a "master"
	sheet that only has columns and no data

	:returns: at the end of the code, if everything is successful (no errors are \
	raised) True is returned
	:rtype: bool
	"""
	compare_to_path = PATH + "only-columns.csv"
	with open(compare_to_path, 'r') as master_csvfl:
		reader = csv.reader(master_csvfl, delimiter=',')
		compare_to_columns = next(reader)

	for filename in os.listdir(PATH):
		if filename == "only-columns.csv" or not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		with open(file_path, 'r') as csvfl:
			reader = csv.reader(csvfl, delimiter=',')
			columns = next(reader)

		if len(columns) == len(compare_to_columns):
			for i in range(len(columns)):
				if columns[i] != compare_to_columns[i]:
					raise ValueError(f"Column names are not the same for {filename} at index {i} where names are '{columns[i]}' and '{compare_to_columns[i]}'")
		else:
			raise ValueError(f'Column lengths are not the same for {filename} where master is {len(compare_to_columns)} and {filename} is {len(columns)}')

	return True

def rid_of_units():
	"""
	many columns have the units in parenthesis at the end of the name, which
	is against the PostgreSQL column name restrictions, so this will get rid of
	those
	"""

	for filename in os.listdir(PATH):
		if not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		#gets the current column names
		with open(file_path, 'r') as csvfl:
			reader = csv.reader(csvfl, delimiter=',')
			original_csv = [row for row in reader]

		updated_csv = original_csv

		#updates the column names
		for index, columns in enumerate(updated_csv):
			if index == 0:
				for nested_index, column_name in enumerate(columns):
					current_name = column_name
					while '(' in current_name and ')' in current_name:
						current_name = re.sub(r'\([^()]*\)', '', current_name).strip()
						updated_csv[0][nested_index] = current_name
			else:
				break

		"""
		someone else's code: re.sub(r'\([^)]*\)', '', filename)
		my code: re.sub('\(.*?\)', '', column_name)

		the difference: to my knowledge, the other one takes care of
		nested parenthesis and doesn't care where the location of those
		parenthesis are. mine just takes care of the expected case which
		is just "(unit)" at the end of the column name

		and use str.strip() at the end!! :)
		"""

		with open(file_path, 'w') as csvfl:
			writer = csv.writer(csvfl, delimiter=',')
			writer.writerows(updated_csv)


def replace_spaces_with_underscores():
	"""
	to make column names postgres friendly, there cannot be spaces, but
	underscores are permitted. so, spaces will be replaces with underscores.

	this function also makes all letters lowercase
	"""
	for filename in os.listdir(PATH):
		if not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		#this way of reading gets rid of the BOM (byte order mark)
		with open(file_path, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			original_csv = [row for row in reader]

		updated_csv = original_csv

		for row, columns in enumerate(updated_csv):
			if row == 0:
				for index, value in enumerate(columns):
					updated_csv[row][index] = value.replace(' ', '_').lower()
			else:
				break

		with open(file_path, 'w') as csvfl:
			writer = csv.writer(csvfl, delimiter=',')
			writer.writerows(updated_csv)

def is_friendly(input_string):
	"""
	checks if a string has letters, numbers, and underscores only

	:param input_string: the string that needs to be checked
	:type input_string: str

	:returns: if it is friendly or not
	:rtype: bool
	"""

	string_options = string.ascii_lowercase + string.digits + '_'

	for letter in input_string:
		if letter not in string_options:
			return False

	return True

def check_for_special_characters():
	"""
	make sure that the column names do not have special characters, which are
	not permitted by postgres
	"""
	for filename in os.listdir(PATH):
		if not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		#this way of reading gets rid of the BOM (byte order mark)
		with open(file_path, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			original_csv = [row for row in reader]

		for row, columns in enumerate(original_csv):
			if row == 0:
				str_is_friendly = True
				for index, value in enumerate(columns):
					if not is_friendly(value):
						str_is_friendly = False
						raise ValueError(f'The column name {value} in {filename} is not alphanumeric.')
				if not str_is_friendly:
					break
			else:
				break

def manage_special_characters():
	"""
	periods will be deleted and @ symbols will be replaces with "at"
	"""
	for filename in os.listdir(PATH):
		if not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		#this way of reading gets rid of the BOM (byte order mark)
		with open(file_path, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			original_csv = [row for row in reader]

		updated_csv = original_csv

		for row, columns in enumerate(updated_csv):
			if row==0:
				for index, value in enumerate(columns):

					current_value = value
					#THIS WILL ONLY FIX ONE ISSUE IN THE NAME (WHICH IS A PROBLEM IF THERE ARE MULTIPLE)
					if not is_friendly(current_value):
						if '.' in current_value:
							current_value = current_value.replace('.','')
						if "@" in current_value:
							current_value = current_value.replace("@", "at")
						if '%' in current_value:
							current_value = current_value.replace('%', '_percent')
						if '/wire' in current_value:
							current_value = current_value.replace('/', '_and_')
						if '-' in current_value:
							current_value = current_value.replace('-', '_')
						if 'length/d' in current_value or 'finish/c' in current_value:
							current_value = current_value.replace('/', '_or_')
						if ':' in current_value:
							current_value = current_value.replace(':', '')
						updated_csv[row][index] = current_value
			else:
				break

		with open(file_path, 'w') as csvfl:
			writer = csv.writer(csvfl, delimiter=',')
			writer.writerows(updated_csv)

def check_column_lengths():
	"""
	checks the length of the column name to make sure the length is less than
	63 bytes/characters which is the maximum length for a column name on 
	PostgreSQL
	"""
	for filename in os.listdir(PATH):
		if not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		#this way of reading gets rid of the BOM (byte order mark)
		with open(file_path, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			original_csv = [row for row in reader]


		for row, columns in enumerate(original_csv):
			if row==0:
				for index, value in enumerate(columns):
					if len(value) > 63:
						raise ValueError(f'The column name {value} in {filename} is {len(value)-63} characters too long.')
			else:
				break

# def sync_columns():
# 	"""
# 	I wrote this code for a ONE TIME use, so it should not be used again. It
# 	will sync the columns of other tables with the columns found in 'only-columns.csv'
# 	file.

# 	"""

# 	sync_with_path = PATH + "only-columns.csv"
# 	with open(sync_with_path, 'r') as master_csvfl:
# 		reader = csv.reader(master_csvfl, delimiter=',')
# 		sync_with_columns = next(reader)

# 	for filename in os.listdir(PATH):
# 		if not filename.endswith('.csv'):
# 			continue
# 		file_path = PATH + filename

# 		#this way of reading gets rid of the BOM (byte order mark)
# 		with open(file_path, 'rb', ) as csvfl:
# 			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
# 			original_csv = [row for row in reader]

# 		updated_csv = original_csv

# 		for row, columns in enumerate(original_csv):
# 			if row==0:
# 				for index, value in enumerate(columns):
# 					updated_csv[row][index] = sync_with_columns[index]
# 			else:
# 				break

# 		with open(file_path, 'w') as csvfl:
# 			writer = csv.writer(csvfl, delimiter=',')
# 			writer.writerows(updated_csv)


# sync_columns()


verify_sameness()

rid_of_units()
replace_spaces_with_underscores()
manage_special_characters()

verify_sameness()
check_for_special_characters()
check_column_lengths()