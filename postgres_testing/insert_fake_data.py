"""
This script was used to assist in adding fake data to the PostgreSQL database
in order to test the database design throught example queries.

This is NOT a script that should be ran without review of the code, as much of it
is hardcoded and the data in test database is bound to change.

"""


import psycopg2
import string
import random

from constants.table_types import WORK_ORDER_INFORMATION_TYPES
from constants.table_schemas import WORK_ORDER_INFORMATION
from constants.fake_data import FAKE_WORK_ORDER_INFORMATION, FAKE_MASTER_CONVEYOR, FAKE_CONVEYOR_SPECIFICATIONS, FAKE_FACILITY_SPECIFICATIONS

ALL_CONVEYOR_SPECS_IDS = ["3d98b8de-8318-4e29-9c0e-6432a9e60a87",
"6ef57ad3-531a-4208-bfef-2758935dce28",
"3ce941c1-7388-4379-85f7-ca497b144ae5",
"61e0625e-914a-4880-b8ce-f090a26392f9",
"4fabff9f-c549-4009-aeed-20edfa847cce",
"a96f97a2-9ddd-46be-9c9c-03dee02c1a02",
"a6dc7fd9-fc3a-4a3c-a07f-3750f4df59a5",
"7a25f60d-1085-423b-bb94-4456f408d7ff",
"627c49e4-a7ae-4cb1-bc19-aa4bc87d7049",
"f4454e79-5d12-4e07-b6b6-9f1b65a0dd69",
"2dc61013-93cd-48d4-9c29-00fdee014c36",
"66d9ef0c-9ec6-4b01-ae3a-b61cda80ebf0",
"769dd217-fa52-413c-9f60-d0fdf3af0153",
"6dd661e3-e778-4bbd-a86a-126ee9c7de02",
"b9c53aa2-b36b-4f0e-8f79-bc82c475fbc0",
"67d993c2-5416-400d-bafd-ec61d810dcec",
"6d7ae6a6-8508-40d4-a75a-f3e736525e48",
"e67dea43-8056-4718-97d1-53518eb675db",
"038ac770-85d8-42d6-9fbc-a22a7943103a",
"99079388-84e2-4971-9859-f5cef45429bc"]

ALL_CONVEYOR_IDS = ["2977675f-68c7-4c77-96f7-4a1b304e67cb",
"e53e3264-0cc9-45b7-be8d-a2e726e7387f",
"fcfb1b2c-497c-4866-a9dc-a1305394e302",
"81ad16ce-8aeb-4e38-89b6-1168c104d00b",
"2a66231a-fbb4-4168-a4f4-0b27be834b12",
"8478224a-7f53-4082-8fe3-f77b16ebea78",
"6041aaa4-990c-4be0-b0d0-4606e080f1a1",
"3486aeee-53b2-4b7d-bc8f-3fbfd56c3253",
"e1514063-462d-4c3a-b397-2d4d1b2c163c",
"ba204ae2-853c-4fbf-8880-66a12e380cb0",
"79809ade-aaa9-402d-89d5-b50bc9ca4803",
"b2441791-f44d-4fe5-92ee-d50a1a11726b",
"131e3385-2e83-4144-b0fc-84c7e233ad1c",
"741fa66e-d2d8-4204-ad67-65961a3152d8",
"694dafc7-73aa-4995-8e08-0d28293d9110",
'38d0e28e-cb79-4e9f-b590-fd81051aff77',
'09baffde-a359-408e-8069-2106cd04e93e',
'ab9f59c2-548e-41b1-85e9-6c2671020a0f']

ALL_FACILITY_SPECS_IDS = ["bf757ebf-f4d3-43e3-a267-55f5ffd6d7eb",
"50ebb3a8-cdca-4dba-beb0-1559f32ed636",
"3d923777-e271-4820-8075-c8f2e84b1c17",
"115ac72f-d65c-4ae5-a96c-8328de11a3e8",
"5ae8391a-b46f-4a23-999c-b6ae8eb3c736",
"2055313b-dd0d-4e2e-9a3d-8c55df1fe282",
"e4afcf7e-085f-4839-b9a2-8090d8181c1e",
"f43118ca-bbad-4929-9779-b6d9cf5b716c",
"423e4507-0f36-4415-b845-eb98a4852042",
"87a19f92-73aa-447f-afc6-26e04922d408",
"83155f95-a09f-42ac-8c59-01043def5620",
"dbf5e1eb-2002-443c-ae30-3e9506a20658",
"e930099b-6d9f-4703-83cd-1dc16a652b45",
"c6599318-b7b0-4be5-bd95-a0e65c572e77",
"c88cf26e-3c8d-4d9f-a476-7854b8b16cb1",
"eb643aff-ab64-43d4-a4f6-9020adcd5c80"]

ALL_SENSOR_IDS = ["ee6151c4-6016-4d8a-a874-699e1a84b66f",
"2f7fc064-46db-43bc-8e39-efa437a54279",
"fbb18874-7423-4628-8e8d-7a709ecf28c2",
"988605e7-9a84-4983-accf-3a16df2609b2",
"26692fe3-716e-40cf-8ee2-97d120d82d26",
"1358d479-66a0-4b14-96e7-7b15f20d3f82",
"3b0fa07f-572d-41ac-b2d4-9bbb3b435334",
"86301256-d173-4e05-b8d9-79f76bd71b3b",
"a312403d-0063-4794-a17a-8dd1a2530288",
"20cd1e17-110a-4049-a550-cc463aca4670",
"9803a942-337e-4947-9221-f760e3b9da90",
"eff0960d-655a-4789-812b-14b93e45ece2",
"ec391a9b-cda7-4b8b-995c-1c53e82b35b4",
"b2625327-39d6-43b4-8100-772ea0b46688",
"a8252391-dca7-4cda-b3e9-8dbe80d9e985",
"6c4a84ad-92cf-404c-b79f-444dcf41f2ca",
"4c41241b-3524-4768-b76d-9dea64d9534e",
"ed44921e-9d89-4cf5-b577-d730c57b3e7d",
"d4533d29-e183-426e-8341-a8e625cfb099",
"887101b4-8771-4ff5-9286-7fec2b5d74bf",
"cc08216b-8fd2-4bb1-b72f-e4aae196ce49",
"2b9f68be-35fa-4b9d-8f1f-3f64e51ba9aa",
"1ab79e91-d36c-4940-b1f4-b270f082549f",
"528cae8c-2c0a-4dc7-8364-b2f6f7a3fd10",
"af99cf2e-ca5e-47aa-8623-7edfb38c766d",
"6d5c0f09-0ff7-4d86-822c-3dee3e33eff0",
"425951b0-4d52-405d-9f7b-c4d6634854e2"]

ALL_LOCATION_SPECS_IDS = ["1404d726-425d-4064-8bd4-0c7e07421d8c",
"080ebe2a-66ca-4bf5-82d7-5390a54ee6da",
"46a4c402-9f11-4fa0-ac91-e71bbe0ec58e",
"0b319ccc-34b4-4bd1-8b24-22d7922747b8",
"89e7c0fc-ea98-4b48-9272-b39421e0d1f8",
"7baec123-1fd3-45a8-9547-9782fac76f33",
"b98d1b9b-d864-45e9-8a16-d9b79d18479b",
"61b2c19a-545a-4e83-97d9-2d47623570f5",
"6cd0ec79-1675-499e-8d3d-50fc1d1a7af0",
"e2ccf52c-d9f0-48af-b856-5a3e2a312ff7",
"b52cb6c2-00f0-44d7-9be2-14bb6151b349",
"f911a935-8cc7-405e-855c-ce8e2726ed54",
"e5d8c8a0-ba9e-466a-ade0-6249740c29e6",
"e30fcec0-3167-49fc-bf30-6614606c19d1",
"e001f7fe-b073-451e-97a1-327793217339",
"8865eb31-b919-40ac-9d1e-046690328865",
"045db2a2-fe8c-4deb-a3b5-1ce9e06fa7ba",
"b2798dc4-46e8-4118-b2a2-b2fee36214c3",
"8121721b-ea42-496c-b7c8-78ae706069fd",
"6db0cd8d-fabb-47f4-beae-12554a7881f2",
"5394356b-f19f-414d-ba0d-e19b1bbdf3dc",
"0ac9c8e0-4946-4309-870b-4c7d485fcf14",
"ac72fa8b-aec5-4e21-ab86-f1da0e6bdaef",
"384dc924-17b8-43c7-93c0-2c11e8f205e4",
"0df25541-93b3-4998-9f43-173d5ecc14e2",
"fabbbb83-d959-45f1-947f-a4f3180cb896",
"a13ccaee-a240-425e-898e-d5615c611444",
"68a80367-e879-4b4c-a667-cbba724b4e8e",
"4446b3d3-9687-42f3-930a-18d6e5f09a06",
"baf0b6b3-9490-4c44-951c-b0c42a512d34"]


def generate_random_string(length: int = 5) -> str:
    """
    Generates a string with random uppercase letters, lowercase letters, and numbers with
    the length given.

    :param length: how many characters long the string will be
    :type length: int

    :returns: a random string
    :rtype: str

    """
    return (''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length)))


def generate_random_float(lowerlim: int = 0.0, upperlim: int = 50.0) -> int:
    """
    Generates a random float between the limits given.

    :param lowerlim: the minimum value for the random float
    :type lowerlim: float

    :param upperlim: the maximum value for the random float
    :type upperlim: float

    :returns: a random float
    :rtype: float

    """

    return (round(random.uniform(lowerlim, upperlim), 2))


def generate_random_int(lowerlim: int = -10, upperlim: int = 10) -> int:
    """
    Generates a random integer between the limits given.

    :param lowerlim: the minimum value for the random integer
    :type lowerlim: int

    :param upperlim: the maximum value for the random integer
    :type upperlim: int

    :returns: a random integer
    :rtype: int

    """

    return (random.randint(lowerlim, upperlim))

conn = psycopg2.connect(dbname = "postgres",
						user = "admin_iot",
						host =  "test-asset-classification.c2z9g3wxqyud.us-east-1.rds.amazonaws.com",
						password = "X+c+8CV%%Mpbr3hG")

cursor = conn.cursor()

# # -- FOR CONVEYOR_SPECS TABLE --
# for identification in ALL_CONVEYOR_SPECS_IDS:
# 	query = f""" 
# 	UPDATE conveyor_specifications
# 	SET
# 		width = {generate_random_float()},
# 		length = {generate_random_float()},
# 		manufacturer = '{generate_random_string()}'
# 	WHERE
# 		id = '{identification}';
# 	"""
# 	cursor.execute(query)


# # -- FOR CONVEYOR_SPECS TABLE -- (edited to make manufacturer "real strings")
# for identification in ALL_CONVEYOR_SPECS_IDS:
#   query = f""" 
#   UPDATE conveyor_specifications
#   SET
#       width = {generate_random_float(0.0,10.0)},
#       length = {generate_random_float()},
#       manufacturer = '{random.choice(FAKE_CONVEYOR_SPECIFICATIONS['manufacturer'])}'
#   WHERE
#       id = '{identification}';
#   """
#   cursor.execute(query)


# # -- FOR MASTER_CONVEYOR TABLE --
# for i in range(15):
# 	query = f"""
# 	INSERT INTO master_conveyor(name, conveyor_id)
# 		VALUES
# 		('{generate_random_string()}', '{random.choice(ALL_CONVEYOR_SPECS_IDS)}')

# 	"""
# 	cursor.execute(query)


# # -- FOR MASTER_CONVEYOR TABLE -- (edited to make manufacturer "real strings")
# for identification in ALL_CONVEYOR_IDS:
#   query = f""" 
#   UPDATE master_conveyor
#   SET
#       name = '{random.choice(FAKE_MASTER_CONVEYOR['name'])}'
#   WHERE
#       id = '{identification}';
#   """
#   cursor.execute(query)


# # -- FOR MASTER_SENSOR TABLE --
# for identification in ALL_SENSOR_IDS:

# 	facility_query = f"""
# 		SELECT facility_id FROM master_sensor
# 		WHERE id = '{identification}'
# 	"""

# 	cursor.execute(facility_query)
# 	facility = cursor.fetchone()[0]

# 	choices = []
# 	if facility == "bf757ebf-f4d3-43e3-a267-55f5ffd6d7eb":
# 		choices = ALL_CONVEYOR_IDS[0:3]
# 	elif facility == "50ebb3a8-cdca-4dba-beb0-1559f32ed636":
# 		choices = ALL_CONVEYOR_IDS[3:6]
# 	elif facility == "3d923777-e271-4820-8075-c8f2e84b1c17":
# 		choices = ALL_CONVEYOR_IDS[6:9]
# 	elif facility == "115ac72f-d65c-4ae5-a96c-8328de11a3e8":
# 		choices = ALL_CONVEYOR_IDS[9:12]
# 	elif facility == "5ae8391a-b46f-4a23-999c-b6ae8eb3c736":
# 		choices = ALL_CONVEYOR_IDS[12:15]	
# 	conveyor = random.choice(choices)
# 	insert_query = f"""
# 	UPDATE master_sensor
# 	SET
# 		conveyor_id = '{conveyor}'
# 	WHERE
# 		id = '{identification}'
# 	"""
# 	cursor.execute(insert_query)


# # -- FOR WORK ORDER INFORMATION TABLE --
# # for however many rows we need
# for i in range(35):
#     # randomly select a sensor or conveyor
#     reference_id = random.choice(ALL_SENSOR_IDS + ALL_CONVEYOR_IDS)

#     # assign the values to the corresponding column
#     if reference_id in ALL_SENSOR_IDS:
#         sensor_id = reference_id
#         conveyor_id = 'NULL'
#         initial_query = f"INSERT INTO work_order_information (sensor_id, conveyor_id) VALUES ('{sensor_id}',{conveyor_id}) RETURNING id"
#     elif reference_id in ALL_CONVEYOR_IDS:
#         conveyor_id = reference_id
#         sensor_id = 'NULL'
#         initial_query = f"INSERT INTO work_order_information (sensor_id, conveyor_id) VALUES ({sensor_id},'{conveyor_id}') RETURNING id"

#     # place these values in the table and return what uuid is generated
#     cursor.execute(initial_query)
#     identification = cursor.fetchone()[0]

#     # iterate through the columns in this table
#     for column in WORK_ORDER_INFORMATION:
#         if  WORK_ORDER_INFORMATION_TYPES[column] == 'uuid':
#             continue
#         # generate a value based on the data type
#         value = 'NULL'
#         if WORK_ORDER_INFORMATION_TYPES[column] == str:
#             value = f"{generate_random_string()}"
#         elif WORK_ORDER_INFORMATION_TYPES[column] == float:
#             value = generate_random_float()
#         elif WORK_ORDER_INFORMATION_TYPES[column] == int:
#             value = generate_random_int()
#         elif WORK_ORDER_INFORMATION_TYPES[column] == 'date':
#             value = '2019-08-07'
#         elif WORK_ORDER_INFORMATION_TYPES[column] == bool:
#             value = random.choice(['true', 'false'])

#         insert_value_query = f"""
#             UPDATE work_order_information
#             SET {column} = '{value}'
#             WHERE
#                 id = '{identification}'
#         """
#         cursor.execute(insert_value_query)


# # -- FOR WORK ORDER INFORMATION TABLE -- (edited to have "real strings")
# # for however many rows we need
# for i in range(35):
#     # randomly select a sensor or conveyor
#     reference_id = random.choice(ALL_SENSOR_IDS + ALL_CONVEYOR_IDS)

#     # assign the values to the corresponding column
#     if reference_id in ALL_SENSOR_IDS:
#         sensor_id = reference_id
#         conveyor_id = 'NULL'
#         initial_query = f"INSERT INTO work_order_information (sensor_id, conveyor_id) VALUES ('{sensor_id}',{conveyor_id}) RETURNING id"
#     elif reference_id in ALL_CONVEYOR_IDS:
#         conveyor_id = reference_id
#         sensor_id = 'NULL'
#         initial_query = f"INSERT INTO work_order_information (sensor_id, conveyor_id) VALUES ({sensor_id},'{conveyor_id}') RETURNING id"

#     # place these values in the table and return what uuid is generated
#     cursor.execute(initial_query)
#     identification = cursor.fetchone()[0]

#     # iterate through the columns in this table
#     for column in FAKE_WORK_ORDER_INFORMATION:
#         # generate a value based on the data type
#         value = 'NULL'
#         if isinstance(FAKE_WORK_ORDER_INFORMATION[column], list):
#             value = random.choice(FAKE_WORK_ORDER_INFORMATION[column])
#         elif FAKE_WORK_ORDER_INFORMATION[column] == str:
#             value = f"{generate_random_string()}"
#         elif FAKE_WORK_ORDER_INFORMATION[column] == float:
#             value = generate_random_float()
#         elif FAKE_WORK_ORDER_INFORMATION[column] == int:
#             value = generate_random_int()
#         elif FAKE_WORK_ORDER_INFORMATION[column] == 'date':
#             value = '2019-08-08'
#         elif FAKE_WORK_ORDER_INFORMATION[column] == bool:
#             value = random.choice(['true', 'false'])

#         insert_value_query = f"""
#             UPDATE work_order_information
#             SET {column} = '{value}'
#             WHERE
#                 id = '{identification}'
#         """
#         cursor.execute(insert_value_query)


# # -- FOR LOCATION_SPECIFICATIONS TABLE --
# for i in range(30):
#     query = f""" 
#     INSERT INTO location_specifications(country, region, climate_zone, biome, average_temperature, average_humidity_level)
#     VALUES
#         ('{random.choice(FAKE_LOCATION_SPECIFICATIONS['country'])}',
#         '{random.choice(FAKE_LOCATION_SPECIFICATIONS['region'])}',
#         '{random.choice(FAKE_LOCATION_SPECIFICATIONS['climate_zone'])}',
#         '{random.choice(FAKE_LOCATION_SPECIFICATIONS['biome'])}',
#         {generate_random_float(-20,120)},
#         {generate_random_float(0,100)});
#     """
#     cursor.execute(query)


# # -- FOR LOCATION_SPECIFICATIONS TABLE (edit for new biome) --
# for identification in ALL_LOCATION_SPECS_IDS:
#     query = f""" 
#     UPDATE location_specifications
#     SET
#     country = '{random.choice(FAKE_LOCATION_SPECIFICATIONS['country'])}',
#     region = '{random.choice(FAKE_LOCATION_SPECIFICATIONS['region'])}',
#     biome = '{random.choice(FAKE_LOCATION_SPECIFICATIONS['biome'])}'
#     WHERE
#     id = '{identification}'
#     """
#     cursor.execute(query)


# # -- FOR FACILITY_SPECIFICATIONS TABLE --
# for identification in ALL_FACILITY_SPECS_IDS:
#     query = f""" 
#     UPDATE facility_specifications
#     SET
#         square_footage = {generate_random_float(50000, 3000000)},
#         location_id = '{random.choice(ALL_LOCATION_SPECS_IDS)}'
#     WHERE
#         id = '{identification}';
#     """
#     cursor.execute(query)


# # -- THIS COULD BE USED TO POPULATE FACILITY_ID COLUMN IN MASTER_CONVEYOR --
# for identification in ALL_CONVEYOR_IDS:
#     find_query = f"""
#     SELECT facility_id FROM master_sensor
#     WHERE conveyor_id = '{identification}';
#     """
#     cursor.execute(find_query)

#     facility_id = cursor.fetchone()

#     if facility_id is None:
#         continue

#     update_query = f"""
#     UPDATE master_conveyor
#     SET facility_id = '{facility_id[0]}'
#     WHERE id = '{identification}';
#     """
#     cursor.execute(update_query)


# # -- FOR FACILITY_SPECIFICATIONS TABLE WHERE LOCATION_SPECS DON'T EXIST ANYMORE
# for identification in ALL_FACILITY_SPECS_IDS:
#     update_query = f"""
#     UPDATE facility_specifications
#     SET 
#         country = '{random.choice(FAKE_FACILITY_SPECIFICATIONS['country'])}',
#         region = '{random.choice(FAKE_FACILITY_SPECIFICATIONS['region'])}',
#         biome = '{random.choice(FAKE_FACILITY_SPECIFICATIONS['biome'])}'
#     WHERE id = '{identification}'
#     """
#     cursor.execute(update_query)

cursor.close()
conn.commit()
conn.close()