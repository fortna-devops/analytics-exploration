"""
This is a script that connects to the PostgreSQL database and runs the SQL
file with queries for table creation.
"""


import psycopg2

conn = psycopg2.connect(dbname = "postgres",
						user = "admin_iot",
						host =  "test-asset-classification.c2z9g3wxqyud.us-east-1.rds.amazonaws.com",
						password = "X+c+8CV%%Mpbr3hG")

cursor = conn.cursor()


sqlfile = open('add_columns.sql', 'r')

cursor.execute(sqlfile.read())

cursor.close()
conn.commit()
conn.close()