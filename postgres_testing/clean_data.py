"""
cleans data by:
- converting mixed fractions to decimal values
- standardizing units
- keeping only the first number from 'xx/xx' or 'xx-xx/xx' formats (for now)
- getting rid of any unit specifications at the end (ex: (in), ", or %)
- making sure that the value matches the set data type
- converting dates to the proper format
- dates where there are fractions was done by excel and are not right

"""

import os
import csv
import unicodecsv

from fractions import Fraction
from table_types import ALL_SPECS_TYPES

PATH = "Documents/assets-info/postgres_testing/specs_as_csv_testing/"

def convert_mixed_fraction_to_float(mixed_fraction):
	"""
	this takes a mixed fraction as a stirng and converts it to a 
	float(decimal number)
	"""
	return float(sum(Fraction(term) for term in mixed_fraction.split()))

def verify_if_correct_types(table):
	"""
	iterate though the table and make sure that the data inside matches the
	data type that it was meant to be. if not, errors are raised.

	:param table: the table with all specifications
	:type table: list of lists

	:returns: true if all types are as needed
	:rtype: bool
	"""
	for column_index, column in enumerate(zip(*table)):
			column_name = column[0]
			required_type = ALL_SPECS_TYPES[column_name]
			for row_index, row_value in enumerate(column):
				if row_index == 0 or row_value == '':
					continue

				#TODO: what if required type is date or uuid?
				if not isinstance(row_value, required_type):
					print(f'{row_value} under {column_name} is not a {required_type} as required')




for filename in os.listdir(PATH):
		if not filename.endswith('.csv'):
			continue
		file_path = PATH + filename

		#this way of reading gets rid of the BOM (byte order mark)
		with open(file_path, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			original_csv = [row for row in reader]

		updated_csv = original_csv
		"""
		how to know when to standardize units? look for parenthesis, let the human
		know, and write code to convert units?

		maybe check if there is a special character and based on what kind of 
		character different actions will be taken
		if parenthesis: idk bc that likely means that there are differing units
		if /: there is probably mixed fractions
		"""
		for column_index, column in enumerate(zip(*updated_csv)):
			column_name = column[0]
			required_type = ALL_SPECS_TYPES[column_name]
			for row_index, row_value in enumerate(column):
				if row_index == 0 or row_value == '':
					continue
				try:
					if required_type == float:
						new_value = float(row_value)
					elif required_type == int:
						new_value = int(row_value)
					elif required_type == str:
						new_value = str(row_value)
				except ValueError:
					raise ValueError(f'could not convert {row_value} to {required_type} located under {column_name} in {filename}')

				updated_csv[row_index][column_index] = new_value

		#TODO: before this, you need to actually change the values to what they are supposed to be
		verify_if_correct_types(updated_csv)

		with open(file_path, 'w') as csvfl:
			writer = csv.writer(csvfl, delimiter=',')
			writer.writerows(updated_csv)