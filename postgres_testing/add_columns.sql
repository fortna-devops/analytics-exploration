/*
This is a script with solely queries. These queries create tables if they
do not already exist with columns that are *required*, defines primary keys,
and defines any foreign keys that may be necessary. The query after table
creation adds other additional columns if they do not already exist. These
types of columns are mainly specifications, which are likely to be changed.
*/


CREATE TABLE IF NOT EXISTS motor_specifications(
    id uuid DEFAULT uuid_generate_v4(),
    PRIMARY KEY (id)
);
-- fractions are everywhere
ALTER TABLE motor_specifications
ADD COLUMN IF NOT EXISTS order_number TEXT,
ADD COLUMN IF NOT EXISTS manufacturer TEXT,
ADD COLUMN IF NOT EXISTS model TEXT,
ADD COLUMN IF NOT EXISTS serial_number TEXT,
ADD COLUMN IF NOT EXISTS frame TEXT,
ADD COLUMN IF NOT EXISTS frame_material TEXT,
ADD COLUMN IF NOT EXISTS hp DECIMAL,
ADD COLUMN IF NOT EXISTS rpm DECIMAL, -- possibility of a range in here (fedex CT RPM range)
ADD COLUMN IF NOT EXISTS speed DECIMAL,
ADD COLUMN IF NOT EXISTS speed_code TEXT,
ADD COLUMN IF NOT EXISTS voltage DECIMAL, -- TAKE THE LOWEST NUMBER FOR NOW
ADD COLUMN IF NOT EXISTS amperage DECIMAL, -- TAKE THE LOWEST NUMBER FOR NOW
ADD COLUMN IF NOT EXISTS motor_size TEXT,
ADD COLUMN IF NOT EXISTS class TEXT,
ADD COLUMN IF NOT EXISTS full_load_amps DECIMAL, -- TAKE THE LOWEST NUMBER FOR NOW
ADD COLUMN IF NOT EXISTS term_prot TEXT,
ADD COLUMN IF NOT EXISTS weight DECIMAL, --saw some units in parenthesis here!!
ADD COLUMN IF NOT EXISTS perf_data DECIMAL,
ADD COLUMN IF NOT EXISTS dim_data DECIMAL,
ADD COLUMN IF NOT EXISTS ph DECIMAL,
ADD COLUMN IF NOT EXISTS hz DECIMAL,
ADD COLUMN IF NOT EXISTS insulation_class TEXT,
ADD COLUMN IF NOT EXISTS ratio DECIMAL,
ADD COLUMN IF NOT EXISTS standards TEXT,
ADD COLUMN IF NOT EXISTS length DECIMAL, --units all over the place here in parentheses and with a " at the end
ADD COLUMN IF NOT EXISTS ambient_temperature numrange, --units here and ambient temperature ranges
ADD COLUMN IF NOT EXISTS service_factor DECIMAL,
ADD COLUMN IF NOT EXISTS design_code TEXT,
ADD COLUMN IF NOT EXISTS auxillary_box TEXT,
ADD COLUMN IF NOT EXISTS drip_cover TEXT,
ADD COLUMN IF NOT EXISTS blower TEXT,
ADD COLUMN IF NOT EXISTS duty_rating TEXT,
ADD COLUMN IF NOT EXISTS nominal_efficiency DECIMAL,
ADD COLUMN IF NOT EXISTS efficiency_at_100_percent_load DECIMAL, --saw some percent signs here
ADD COLUMN IF NOT EXISTS electrically_isolated_bearing TEXT,
ADD COLUMN IF NOT EXISTS front_face_code TEXT,
ADD COLUMN IF NOT EXISTS feedback_device TEXT,
ADD COLUMN IF NOT EXISTS high_voltage_full_load_amps DECIMAL, --saw some units here
ADD COLUMN IF NOT EXISTS front_shaft_indicator TEXT,
ADD COLUMN IF NOT EXISTS heater_indicator TEXT,
ADD COLUMN IF NOT EXISTS inverter_code TEXT,
ADD COLUMN IF NOT EXISTS kva_code TEXT,
ADD COLUMN IF NOT EXISTS lifting_lungs TEXT,
ADD COLUMN IF NOT EXISTS locked_bearing_indicator TEXT,
ADD COLUMN IF NOT EXISTS lead_exit TEXT,
ADD COLUMN IF NOT EXISTS lead_termination TEXT,
ADD COLUMN IF NOT EXISTS lead_quantity_and_wire_size TEXT,
ADD COLUMN IF NOT EXISTS type TEXT,
ADD COLUMN IF NOT EXISTS mounting_arragement TEXT,
ADD COLUMN IF NOT EXISTS number_of_poles INT,
ADD COLUMN IF NOT EXISTS power_factor DECIMAL,
ADD COLUMN IF NOT EXISTS product_family TEXT,
ADD COLUMN IF NOT EXISTS pulley_end_bearing_type TEXT,
ADD COLUMN IF NOT EXISTS pulley_end_face_code TEXT,
ADD COLUMN IF NOT EXISTS pulley_shaft_indicator TEXT,
ADD COLUMN IF NOT EXISTS rodent_screen TEXT,
ADD COLUMN IF NOT EXISTS shaft_diameter DECIMAL,
ADD COLUMN IF NOT EXISTS shaft_size DECIMAL,
ADD COLUMN IF NOT EXISTS shaft_extension_location TEXT,
ADD COLUMN IF NOT EXISTS shaft_ground_indicator TEXT,
ADD COLUMN IF NOT EXISTS shaft_rotation TEXT,
ADD COLUMN IF NOT EXISTS shaft_slinger_indicator TEXT,
ADD COLUMN IF NOT EXISTS starting_method TEXT,
ADD COLUMN IF NOT EXISTS constant_torque_speed_range numrange, -- ratios are here but can be converted to range (?)
ADD COLUMN IF NOT EXISTS torque_rating DECIMAL,
ADD COLUMN IF NOT EXISTS variable_torque_speed_range numrange, -- ratios are here but can be converted to range (?)
ADD COLUMN IF NOT EXISTS series TEXT,
ADD COLUMN IF NOT EXISTS thermal_class TEXT, --only value i found in this section looks like "155(F)"
ADD COLUMN IF NOT EXISTS ingress_protection INT,
ADD COLUMN IF NOT EXISTS gear_unit_ratio DECIMAL,
ADD COLUMN IF NOT EXISTS maximum_torque DECIMAL,
ADD COLUMN IF NOT EXISTS oil_type TEXT,
ADD COLUMN IF NOT EXISTS oil_fill_volume DECIMAL, --some units here
ADD COLUMN IF NOT EXISTS enclosure_type TEXT,
ADD COLUMN IF NOT EXISTS lubrication_type TEXT,
ADD COLUMN IF NOT EXISTS gear_unit_type TEXT,
ADD COLUMN IF NOT EXISTS gear_unit_option TEXT,
ADD COLUMN IF NOT EXISTS gear_unit_size DECIMAL,
ADD COLUMN IF NOT EXISTS install_date DATE,
ADD COLUMN IF NOT EXISTS last_replaced DATE;



CREATE TABLE IF NOT EXISTS bearing_specifications(
    id uuid DEFAULT uuid_generate_v4(),
    PRIMARY KEY (id)
);
--there are fractions all over the place for this one
ALTER TABLE bearing_specifications
ADD COLUMN IF NOT EXISTS order_number TEXT,
ADD COLUMN IF NOT EXISTS manufacturer TEXT,
ADD COLUMN IF NOT EXISTS model TEXT,
ADD COLUMN IF NOT EXISTS bore_diameter DECIMAL,
ADD COLUMN IF NOT EXISTS base_to_centerline_height DECIMAL,
ADD COLUMN IF NOT EXISTS distance_from_bearing_centerline_to_inner_ring_long_hub_face DECIMAL,
ADD COLUMN IF NOT EXISTS distance_from_bearing_centerline_to_cap_face DECIMAL,
ADD COLUMN IF NOT EXISTS bolt_center_to_center_length DECIMAL,
ADD COLUMN IF NOT EXISTS mounting_pad_length DECIMAL,
ADD COLUMN IF NOT EXISTS mounting_pad_width DECIMAL,
ADD COLUMN IF NOT EXISTS mounting_pad_height DECIMAL,
ADD COLUMN IF NOT EXISTS mounting_bolt_size DECIMAL,
ADD COLUMN IF NOT EXISTS mouting_bolt_hole_slot_length DECIMAL,
ADD COLUMN IF NOT EXISTS overall_length_or_diameter DECIMAL,
ADD COLUMN IF NOT EXISTS overall_housing_height DECIMAL,
ADD COLUMN IF NOT EXISTS bore_type TEXT,
ADD COLUMN IF NOT EXISTS duty_type TEXT,
ADD COLUMN IF NOT EXISTS mounting_type TEXT,
ADD COLUMN IF NOT EXISTS housing_material TEXT,
ADD COLUMN IF NOT EXISTS locking_device TEXT,
ADD COLUMN IF NOT EXISTS locking_collar_outside_diameter DECIMAL,
ADD COLUMN IF NOT EXISTS expansion_type TEXT,
ADD COLUMN IF NOT EXISTS dynamic_load_capacity DECIMAL, --differing units here
ADD COLUMN IF NOT EXISTS static_load_capacity DECIMAL, --differeing units here
ADD COLUMN IF NOT EXISTS seal_type TEXT,
ADD COLUMN IF NOT EXISTS length_through_bore DECIMAL,
ADD COLUMN IF NOT EXISTS operating_temperature_range numrange, --it's a range
ADD COLUMN IF NOT EXISTS maximum_temperature DECIMAL, --differing units here. issue with column name here!
ADD COLUMN IF NOT EXISTS finish_or_coating TEXT,
ADD COLUMN IF NOT EXISTS maximum_rpm DECIMAL,
ADD COLUMN IF NOT EXISTS vibration_frequency_inner_ring_defect DECIMAL,
ADD COLUMN IF NOT EXISTS vibration_frequency_roller_spin DECIMAL,
ADD COLUMN IF NOT EXISTS vibration_frequency_fundamental_train DECIMAL,
ADD COLUMN IF NOT EXISTS vibration_frequency_outer_ring_defect DECIMAL,
ADD COLUMN IF NOT EXISTS lubrication_type TEXT,
ADD COLUMN IF NOT EXISTS grease_fitting_size TEXT,
ADD COLUMN IF NOT EXISTS series TEXT,
ADD COLUMN IF NOT EXISTS replacement_bearing TEXT,
ADD COLUMN IF NOT EXISTS replacement_seal_kit TEXT,
ADD COLUMN IF NOT EXISTS set_scerw_torque DECIMAL,
ADD COLUMN IF NOT EXISTS shaft_diameter DECIMAL,
ADD COLUMN IF NOT EXISTS install_date DATE,
ADD COLUMN IF NOT EXISTS last_replaced DATE;



CREATE TABLE IF NOT EXISTS gearbox_specifications(
    id uuid DEFAULT uuid_generate_v4(),
    PRIMARY KEY (id)
);
ALTER TABLE gearbox_specifications
ADD COLUMN IF NOT EXISTS order_number TEXT,
ADD COLUMN IF NOT EXISTS manufacturer TEXT,
ADD COLUMN IF NOT EXISTS model TEXT,
ADD COLUMN IF NOT EXISTS serial_number TEXT,
ADD COLUMN IF NOT EXISTS ratio DECIMAL,
ADD COLUMN IF NOT EXISTS assembly TEXT,
ADD COLUMN IF NOT EXISTS input_adaptor TEXT,
ADD COLUMN IF NOT EXISTS hollow_output_shaft_bore_size DECIMAL,
ADD COLUMN IF NOT EXISTS accessory_and_mounting TEXT,
ADD COLUMN IF NOT EXISTS complete_nomenclature TEXT,
ADD COLUMN IF NOT EXISTS weight DECIMAL, --some units are specified here
ADD COLUMN IF NOT EXISTS hp DECIMAL,
ADD COLUMN IF NOT EXISTS rpm DECIMAL,
ADD COLUMN IF NOT EXISTS sf DECIMAL,
ADD COLUMN IF NOT EXISTS install_date DATE,
ADD COLUMN IF NOT EXISTS last_replaced DATE;



CREATE TABLE IF NOT EXISTS conveyor_specifications(
    id uuid DEFAULT uuid_generate_v4(),
    PRIMARY KEY (id)
);
ALTER TABLE conveyor_specifications
ADD COLUMN IF NOT EXISTS manufacturer TEXT,
ADD COLUMN IF NOT EXISTS description TEXT,
ADD COLUMN IF NOT EXISTS width DECIMAL,
ADD COLUMN IF NOT EXISTS length DECIMAL;



CREATE TABLE IF NOT EXISTS facility_specifications(
    id uuid DEFAULT uuid_generate_v4(),
    facility_name TEXT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (facility_name)
);
ALTER TABLE facility_specifications
ADD COLUMN IF NOT EXISTS organization TEXT,
ADD COLUMN IF NOT EXISTS number_of_sensors INT,
ADD COLUMN IF NOT EXISTS square_footage DECIMAL,
ADD COLUMN IF NOT EXISTS country TEXT,
ADD COLUMN IF NOT EXISTS region TEXT,
ADD COLUMN IF NOT EXISTS biome TEXT;



CREATE TABLE IF NOT EXISTS master_conveyor (
    id uuid DEFAULT uuid_generate_v4(),
    name TEXT, -- as specified in the location table
    conveyor_id uuid,
    facility_id uuid,
    PRIMARY KEY (id),
    FOREIGN KEY (conveyor_id) REFERENCES conveyor_specifications(id),
    FOREIGN KEY (facility_id) REFERENCES facility_specifications(id)
);



CREATE TABLE IF NOT EXISTS master_sensor (
    id uuid DEFAULT uuid_generate_v4(),
    sensor TEXT,
    equipment_type TEXT,
    facility_id uuid,
    conveyor_id uuid,
    motor_id uuid,
    bearing_id uuid,
    gearbox_id uuid,
    PRIMARY KEY (id),
    FOREIGN KEY (facility_id) REFERENCES facility_specifications(id),
    FOREIGN KEY (conveyor_id) REFERENCES master_conveyor(id),
    FOREIGN KEY (motor_id) REFERENCES motor_specifications(id),
    FOREIGN KEY (bearing_id) REFERENCES bearing_specifications(id),
    FOREIGN KEY (gearbox_id) REFERENCES gearbox_specifications(id)
);



CREATE TABLE IF NOT EXISTS work_order_information(
    id uuid DEFAULT uuid_generate_v4(),
    sensor_id uuid,
    conveyor_id uuid,
    PRIMARY KEY (id),
    FOREIGN KEY (sensor_id) REFERENCES master_sensor(id),
    FOREIGN KEY (conveyor_id) REFERENCES master_conveyor(id)
);
ALTER TABLE work_order_information
ADD COLUMN IF NOT EXISTS reason TEXT,
ADD COLUMN IF NOT EXISTS problem TEXT,
ADD COLUMN IF NOT EXISTS requester_id TEXT,
ADD COLUMN IF NOT EXISTS name TEXT,
ADD COLUMN IF NOT EXISTS phone TEXT,
ADD COLUMN IF NOT EXISTS email TEXT,
ADD COLUMN IF NOT EXISTS asset_location TEXT,
ADD COLUMN IF NOT EXISTS procedure TEXT,
ADD COLUMN IF NOT EXISTS target_date DATE,
ADD COLUMN IF NOT EXISTS target_hours DECIMAL,
ADD COLUMN IF NOT EXISTS type TEXT,
ADD COLUMN IF NOT EXISTS category TEXT,
ADD COLUMN IF NOT EXISTS account TEXT,
ADD COLUMN IF NOT EXISTS priority INT,
ADD COLUMN IF NOT EXISTS project TEXT,
ADD COLUMN IF NOT EXISTS repair_center TEXT,
ADD COLUMN IF NOT EXISTS shop TEXT,
ADD COLUMN IF NOT EXISTS supervisor TEXT,
ADD COLUMN IF NOT EXISTS shift TEXT,
ADD COLUMN IF NOT EXISTS customer TEXT,
ADD COLUMN IF NOT EXISTS status TEXT,
ADD COLUMN IF NOT EXISTS sub_status TEXT,
ADD COLUMN IF NOT EXISTS approval BOOLEAN,
ADD COLUMN IF NOT EXISTS assignments TEXT,
ADD COLUMN IF NOT EXISTS warranty BOOLEAN,
ADD COLUMN IF NOT EXISTS attachments BOOLEAN,
ADD COLUMN IF NOT EXISTS chargeable BOOLEAN,
ADD COLUMN IF NOT EXISTS printed_or_e_mailed BOOLEAN,
ADD COLUMN IF NOT EXISTS shutdown_required BOOLEAN,
ADD COLUMN IF NOT EXISTS lockout_tagout BOOLEAN,
ADD COLUMN IF NOT EXISTS follow_up_work BOOLEAN
