/*
THIS IS NOT A SCRIPT!

These are query samples to help explain how certain queries work and is used as
a reference tool.
*/


--how the master table was created
CREATE TABLE master (
    id uuid DEFAULT uuid_generate_v4(),
    sensor TEXT,
    equipment_type TEXT,
    facility_id uuid,
    conveyor_id uuid,
    motor_id uuid,
    bearing_id uuid,
    gearbox_id uuid,
    PRIMARY KEY (id),
    FOREIGN KEY (facility_id) REFERENCES facility_specifications(facility_id),
    FOREIGN KEY (conveyor_id) REFERENCES conveyor_specifications(conveyor_id),
    FOREIGN KEY (motor_id) REFERENCES motor_specifications(motor_id),
    FOREIGN KEY (bearing_id) REFERENCES bearing_specifications(bearing_id),
    FOREIGN KEY (gearbox_id) REFERENCES gearbox_specifications(gearbox_id)
)

--example of how to insert into tables, in this case I was manually inserting data into the master table
INSERT INTO master (sensor, equipment_type, facility_id, conveyor_id, bearing_id)
VALUES
	('2', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '3d98b8de-8318-4e29-9c0e-6432a9e60a87', '6218472f-3493-49fd-801b-6eed0a44eae6'),
    ('3', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '6ef57ad3-531a-4208-bfef-2758935dce28', '6218472f-3493-49fd-801b-6eed0a44eae6'),
    ('5', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '3ce941c1-7388-4379-85f7-ca497b144ae5', '6218472f-3493-49fd-801b-6eed0a44eae6'),
    ('7', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '61e0625e-914a-4880-b8ce-f090a26392f9', '6218472f-3493-49fd-801b-6eed0a44eae6'),
    ('8', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '4fabff9f-c549-4009-aeed-20edfa847cce', '6218472f-3493-49fd-801b-6eed0a44eae6'),
    ('12', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', 'a96f97a2-9ddd-46be-9c9c-03dee02c1a02', '6218472f-3493-49fd-801b-6eed0a44eae6'),
    ('15', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', 'a6dc7fd9-fc3a-4a3c-a07f-3750f4df59a5', 'dd0478ec-c01b-4c6f-9ed0-3af796ad66ca'),
    ('16', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '7a25f60d-1085-423b-bb94-4456f408d7ff', 'dd0478ec-c01b-4c6f-9ed0-3af796ad66ca'),
    ('20', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', '627c49e4-a7ae-4cb1-bc19-aa4bc87d7049', 'dd0478ec-c01b-4c6f-9ed0-3af796ad66ca'),
    ('24', 'B', '3d923777-e271-4820-8075-c8f2e84b1c17', 'f4454e79-5d12-4e07-b6b6-9f1b65a0dd69', '6218472f-3493-49fd-801b-6eed0a44eae6')

--another example of how to insert into tables, in this case I was again manually inserting into the master table
INSERT INTO master (sensor, equipment_type, facility_id, conveyor_id, gearbox_id)
VALUES
    ('6', 'G', '3d923777-e271-4820-8075-c8f2e84b1c17', '2dc61013-93cd-48d4-9c29-00fdee014c36', '1b5bc2cf-d2f5-4b3f-856c-1ad03f45979f'),
    ('10', 'G', '3d923777-e271-4820-8075-c8f2e84b1c17', '66d9ef0c-9ec6-4b01-ae3a-b61cda80ebf0', '1b5bc2cf-d2f5-4b3f-856c-1ad03f45979f'),
    ('55', 'G', 'bf757ebf-f4d3-43e3-a267-55f5ffd6d7eb', '769dd217-fa52-413c-9f60-d0fdf3af0153', 'a076b1ee-0a82-4874-a5b8-ed4418e11aa4'),
    ('41', 'G', 'bf757ebf-f4d3-43e3-a267-55f5ffd6d7eb', '6dd661e3-e778-4bbd-a86a-126ee9c7de02', 'f50bd0f4-66e5-430b-b86b-34de7f2ca141')

--example of how to insert into tables, in this case I was inserting data into the motor_specifications table
--outline: INSERT INTO table_name (column_name1, column_name2, column_name3, column_name4, ...)
--not all column names need to be included, only the ones you have data for
INSERT INTO motor_specifications (manufacturer, model, serial_number, frame, frame_material, horsepower, rpm, voltage, amperage)
VALUES
    --data in the same order to correspond to the column names specified
    --every "()," is a new row
    ('Baldor', 'VUHM 3558', NULL, '56C', 'Steel', '2', '1750', '208-230/460', '6.7-6.2/3.1'),
    ('Van Der Graaf', 'TM215B40-430', NULL, NULL, NULL, NULL, NULL, '480', '4.125'),
    ('Toshiba', '0034SDSR44A-P', NULL, '182TC', NULL, '3', '1800', '230/460', '8/4'),
    ('Nord', 'SK 112MP/4 CUS TW', '21455484', '184TC', NULL, '5', '1755', '230/460', '13.0/6.50'),
    ('SEW Eurodrive', 'KA107/T DRN 225M4', '87.7434319801.0001.17.50', 'KA107T', NULL, '60', '1735/107', '230/460', '140.00/70.00'),
    ('SEW-EURODRIVE', 'SA57TDRE90L4', NULL, NULL, NULL, '2', NULL, NULL, NULL),
    ('SEW-EURODRIVE', 'SA47TDRE90M4', NULL, NULL, NULL, '1.5', '1740/1775', NULL, '4.50/2.25')

--example of how to insert into tables, in this case I was manually inserting data into the master table
INSERT INTO master (sensor, equipment_type, facility_id, conveyor_id, motor_id)
VALUES
    ('13', 'M', '3d923777-e271-4820-8075-c8f2e84b1c17', 'a96f97a2-9ddd-46be-9c9c-03dee02c1a02', '834d9ac6-4bc3-4fec-9905-7227162787b2'),
    ('14', 'M', '3d923777-e271-4820-8075-c8f2e84b1c17', 'b9c53aa2-b36b-4f0e-8f79-bc82c475fbc0', 'edc2545d-07db-4250-9963-5e205fb5376f'),
    ('18', 'M', '3d923777-e271-4820-8075-c8f2e84b1c17', '67d993c2-5416-400d-bafd-ec61d810dcec', '76bf8434-a6e4-44fc-91a9-f7ff2eacc3e2'),
    ('36', 'M', 'bf757ebf-f4d3-43e3-a267-55f5ffd6d7eb', '6d7ae6a6-8508-40d4-a75a-f3e736525e48', '4f0d0bfc-0435-4977-92df-7d8ab58b9162'),
    ('20', 'M', 'bf757ebf-f4d3-43e3-a267-55f5ffd6d7eb', 'e67dea43-8056-4718-97d1-53518eb675db', '20b8ad7a-8c9c-4995-960e-8341480090ad'),
    ('59', 'M', '115ac72f-d65c-4ae5-a96c-8328de11a3e8', '038ac770-85d8-42d6-9fbc-a22a7943103a', 'bbdcaa13-436f-46a1-bddc-4a924e8c4a0f'),
    ('64', 'M', '115ac72f-d65c-4ae5-a96c-8328de11a3e8', '99079388-84e2-4971-9859-f5cef45429bc', 'afa2b554-c20a-4962-8f80-eabb0b8bd422')

-- how we would end up querying data
SELECT * FROM master
LEFT JOIN facility_specifications ON facility_specifications.facility_id = master.facility_id
WHERE
facility_specifications.number_of_sensors > 30

--updating data in tables
UPDATE bearing_specifications --table name here
SET bore_diameter = '2.4375', -- column_name = 'new value'
    base_to_centerline_height = '2.75',
    distance_from_bearing_centerline_to_inner_ring_long_hub_face = '0.1875',
    mounting_pad_length = '0.125'
WHERE --rows that need updating
    manufacturer = 'Rexnord';

--altering table datatypes
ALTER TABLE bearing_specifications --table_name
ALTER COLUMN bore_diameter TYPE decimal USING bore_diameter::numeric,
ALTER COLUMN base_to_centerline_height TYPE decimal USING base_to_centerline_height::numeric,
ALTER COLUMN distance_from_bearing_centerline_to_inner_ring_long_hub_face TYPE decimal USING distance_from_bearing_centerline_to_inner_ring_long_hub_face::numeric,
ALTER COLUMN mounting_pad_length TYPE decimal USING mounting_pad_length::numeric;


INSERT INTO master(sensor, equipment_type, facility_id, conveyor_id, bearing_id)
VALUES
    ('T', 'B', '5ae8391a-b46f-4a23-999c-b6ae8eb3c736', 'e67dea43-8056-4718-97d1-53518eb675db', '5e4d0cd6-d0af-4105-b513-840bc483b911'),
    ('E', 'B', '50ebb3a8-cdca-4dba-beb0-1559f32ed636', '769dd217-fa52-413c-9f60-d0fdf3af0153', '962a6870-59f8-438a-b3a8-39802d0533b2')

INSERT INTO gearbox_specifications (manufacturer, model, serial_number, ratio, assembly, input_adaptor, accessory_and_mounting)
VALUES
    ('GBManu', 'xxx', '45.4.2', '8', 'M5', 'H4', 'M3'),
    ('SunG', 'sunny', '88888', '75', 'there to there', 'w', 'M9')

INSERT INTO master (sensor, equipment_type, facility_id, conveyor_id, gearbox_id)
VALUES
    ('33', 'G', '50ebb3a8-cdca-4dba-beb0-1559f32ed636', '3ce941c1-7388-4379-85f7-ca497b144ae5', '598cfb8e-b89c-4105-a7aa-5a6edcdc3096'),
    ('20', 'G', '50ebb3a8-cdca-4dba-beb0-1559f32ed636', '4fabff9f-c549-4009-aeed-20edfa847cce', '9ed8fda2-0a21-4818-b220-2b93a4a9cb3e'),
    ('72', 'G', '5ae8391a-b46f-4a23-999c-b6ae8eb3c736', '7a25f60d-1085-423b-bb94-4456f408d7ff', '598cfb8e-b89c-4105-a7aa-5a6edcdc3096'),
    ('13', 'G', '115ac72f-d65c-4ae5-a96c-8328de11a3e8', '66d9ef0c-9ec6-4b01-ae3a-b61cda80ebf0', '9ed8fda2-0a21-4818-b220-2b93a4a9cb3e')

ALTER TABLE motor_specifications
    ADD COLUMN new_column data_type;

--inserting range type data
INSERT INTO gearbox_specifications(test_ranges)
VALUES
    ('[4.0,4.5]'), -- brackets are inclusive
    ('(2,3)'), -- parenthesis are exclusive
    ('(0,10]') -- you can also mix them

SELECT * FROM gearbox_specifications
WHERE test_ranges @> 3::decimal -- where the number 3 (decimal type) is included

-- altering a table and adding a FK
ALTER TABLE master
ADD CONSTRAINT master_conveyor_id_fkey FOREIGN KEY (conveyor_id) REFERENCES master_conveyor(id);

-- returns something after inserting!!
INSERT INTO work_order_information (sensor_id, conveyor_id)
VALUES
    ({sensor_id},{conveyor_id}) RETURNING id

-- how to rename a column
ALTER TABLE table_name
RENAME COLUMN old_name TO new_name




-- ** HERE DOWN IS WHAT IS GOING ON THE PPT ** --
/*if you want distinct stuff:
SELECT
    DISTINCT ON (table_name.column_name) table_name.can_be_alias_column_name
FROM whatever tables
WHERE whatever conditions
ORDER BY table_name.column_name*/

/* What are all of the work orders, at the conveyor level, 
where the conveyor’s manufacturer is Conveyor Systems? */

SELECT master_conveyor.name, conveyor_specifications.manufacturer, work_order_information.*
FROM work_order_information INNER JOIN master_conveyor ON work_order_information.conveyor_id = master_conveyor.id
INNER JOIN conveyor_specifications ON master_conveyor.conveyor_id = conveyor_specifications.id
WHERE conveyor_specifications.manufacturer LIKE '%Conveyor Systems%'


/*What conveyors are at sites with nore than 50 esnsors installed
and had a work order with a target date on August 8th, 2019?*/
-- attempt 1
SELECT master_conveyor.name
--  facility_specifications.facility_name, 
--  facility_specifications.number_of_sensors, 
--  work_order_information.target_date,
--  master_conveyor.name
--  conveyor_specifications.*
FROM master_conveyor
INNER JOIN work_order_information ON master_conveyor.id = work_order_information.conveyor_id
INNER JOIN master_sensor ON master_conveyor.id = master_sensor.conveyor_id
INNER JOIN facility_specifications ON master_sensor.facility_id = facility_specifications.id
WHERE work_order_information.target_date = current_date
AND facility_specifications.number_of_sensors > 50

-- attempt 2 after adding facility_id to master_conveyor table
SELECT
    facility_specifications.facility_name, 
    facility_specifications.number_of_sensors, 
    work_order_information.target_date,
    master_conveyor.name,
    conveyor_specifications.*
FROM master_conveyor
INNER JOIN work_order_information ON master_conveyor.id = work_order_information.conveyor_id
INNER JOIN facility_specifications ON master_conveyor.facility_id = facility_specifications.id
INNER JOIN conveyor_specifications ON master_conveyor.conveyor_id = conveyor_specifications.id
WHERE work_order_information.target_date = '2019-08-08'
AND facility_specifications.number_of_sensors > 50


/*What bearings from any DHL site have a work order with a priority of 3?*/
SELECT
    facility_specifications.organization,
    work_order_information.priority,
    master_sensor.sensor,
    master_sensor.equipment_type,
    bearing_specifications.*
FROM master_sensor
INNER JOIN facility_specifications ON master_sensor.facility_id = facility_specifications.id
INNER JOIN work_order_information ON master_sensor.id = work_order_information.sensor_id
INNER JOIN bearing_specifications ON master_sensor.bearing_id = bearing_specifications.id
WHERE
    master_sensor.equipment_type = 'B'
    AND
    work_order_information.priority = 3
    AND
    facility_specifications.organization = 'DHL'


/*What motors with a horsepower greater than 2 have a warranty,
as specified on the work order?*/
SELECT
    work_order_information.warranty,
    master_sensor.sensor,
    master_sensor.equipment_type,
    motor_specifications.*
FROM master_sensor
INNER JOIN motor_specifications ON master_sensor.motor_id = motor_specifications.id
INNER JOIN work_order_information ON master_sensor.id = work_order_information.sensor_id
WHERE
    master_sensor.equipment_type = 'M'
    AND
    motor_specificaitons.hp > 2
    AND
    work_order_information.warranty = true
    

/*What conveyors have a length of less than 10 feet and have a 
work order with target hours above 8?*/
SELECT
    work_order_information.target_hours,
    master_conveyor.name,
    conveyor_specifications.*
FROM master_conveyor
INNER JOIN conveyor_specifications ON master_conveyor.conveyor_id = conveyor_specifications.id
INNER JOIN work_order_information ON master_conveyor.id = work_order_information.conveyor_id
WHERE
    conveyor_specifications.length > 10
    AND
    work_order_information.target_hours > 8

/*What are all of the work orders for gearboxes located at a distribution
center in a forest ecosystem with the keyword "worn" in the problem
description?*/
-- attempt 1
SELECT
    master_sensor.equipment_type,
    location_specifications.biome,
    work_order_information.*
FROM work_order_information
INNER JOIN master_sensor ON work_order_information.sensor_id = master_sensor.id
INNER JOIN facility_specifications ON master_sensor.facility_id = facility_specifications.id
INNER JOIN location_specifications ON facility_specifications.location_id = location_specifications.id
WHERE
    master_sensor.equipment_type = 'G'
    AND
    location_specifications.biome = 'forest'
    AND
    work_order_information.problem LIKE '%worn%'

-- attempt 2 where the facility table is reunited and loc_specs doesn't exist anymore
-- CHANGED QUESTION TO CHAPPARAL INSTEAD OF FOREST
SELECT
    master_sensor.equipment_type,
    facility_specifications.facility_name,
    facility_specifications.biome,
    work_order_information.*
FROM work_order_information
INNER JOIN master_sensor ON work_order_information.sensor_id = master_sensor.id
INNER JOIN facility_specifications ON master_sensor.facility_id = facility_specifications.id
WHERE
    master_sensor.equipment_type = 'G'
    AND
    facility_specifications.biome = 'chaparral'
    AND
    work_order_information.problem LIKE '%worn%'
