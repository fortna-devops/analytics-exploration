"""
script that cleans data by:
- converting mixed fractions to decimal values
- standardizing units
- keeping only the first number from 'xx/xx' or 'xx-xx/xx' formats (for now)
- getting rid of any unit specifications at the end (ex: (in), ", or %)
- making sure that the value matches the set data type
- converting dates to the proper format
- dates where there are fractions was done by excel and are not right

"""


"""
general outline?:

import the maps
import the table types
import the modules you may need
import units manager from toolbox


loop through the files

	check what kind of table it is
	go through the table zipped
		if the column is supposed to hold strings then 
			cast the value as a string
		elif the column is supposed to hold numbers, check if it is a problem column
			if so, fix it --> different functions for different issues
			then convert to the data type it is supposed to be
			TEST FUNCTIONS ONE BY ONE, AS THERE ARE MANY-- and some will probably be
			the same for multiple tables

	verify that everything is the type it should be
	write to the csv the fixed up data

QUESTIONS:
- should i go through the non-equipment tables too (like master, facility, conveyor)?
- how to get the data back into the row, column format?
"""

import os
import re
import csv
import unicodecsv

from constants.column_mapping import motor_columns, bearing_columns, gearbox_columns
from constants.table_types import MOTOR_SPECIFICATIONS_TYPES, BEARING_SPECIFICATIONS_TYPES, GEARBOX_SPECIFICATIONS_TYPES, CONVEYOR_SPECIFICATIONS_TYPES, FACILITY_SPECIFICATIONS_TYPES, MASTER_SENSOR_TYPES, MASTER_CONVEYOR_TYPES

#path to the split up csvs
PATH = 'Documents/assets-info/postgres_testing/split_specs/'


def decide_on_motor_rule(column_name, column):
	"""
	based on the column, different rules will be implemented. this function
	determines what rule should be used for the motor specs table

	:param column_name: the name of the column
	:type column_name: str

	:param column: the column as a list where the first element is the column \
	name and the other elements are the values under that column
	:type column: list

	:returns: the result from a different function that applies the appropriate\
	rule to the column given
	:rtype: list
	"""

	#some partial cleaning if applicable
	if column_name in motor_columns['wrong_units']:
		#do column = call_function() do not return!!!
		"""
		#TODO: should the values be casted as strings?? for the rest of the rules below..
		but then that is counterproductive so maybe the columns in two rules need to be
		removed from the other one and taken care of in such a way that nothing else needs
		to be done
		"""
		pass
	if column_name in motor_columns['with_units']:
		column = rid_of_units(column)
	if column_name in motor_columns['with_discrete_options']:
		column = keep_first_option(column)
	if column_name in motor_columns['with_fractions']:
		#do column = call_function() do not return!!!
		pass
	if column_name in motor_columns['not_in_range_format']:
		#do column = call_function() do not return!!!
		pass

	#final cleaning applied to all columns
	if MOTOR_SPECIFICATIONS_TYPES[column_name] == str:
		return cast_as_string(column)
	elif MOTOR_SPECIFICATIONS_TYPES[column_name] == int:
		return cast_as_integer(column)
	elif MOTOR_SPECIFICATIONS_TYPES[column_name] == float:
		return cast_as_float(column)
	elif MOTOR_SPECIFICATIONS_TYPES[column_name] =='uuid':
		#TODO: make sure it's a uuid (though nothing should be there so maybe i should delete this??)
		#use return statement
		pass
	elif MOTOR_SPECIFICATIONS_TYPES[column_name] == 'date':
		#make sure the date format is what postgres wants
		#use return statement
		pass
	else:
		#bad news if it gets to this point
		raise ValueError(f'No cleaning was applied to {column_name} in motor table.')


def cast_as_string(column):
	"""
	takes the column, casts the values as strings, and returnes the cleaned column
	
	:param column: the column where the first element is the column name and \
	the rest are the values under that column
	:type column: list

	:returns: the clean column
	:rtype: list of strings
	"""
	
	for row_index, row_value in enumerate(column):
		#skip the first row because it's the column name
		if row_index == 0:
			continue
		column[row_index] = str(row_value)
	return column


def cast_as_float(column):
	"""
	takes the column, casts the values as floats, and returnes the cleaned column
	
	:param column: the column where the first element is the column name and \
	the rest are the values under that column
	:type column: list

	:returns: the clean column
	:rtype: list of floats
	"""
	
	for row_index, row_value in enumerate(column):
		#skip the first row because it's the column name
		if row_index == 0:
			continue
		column[row_index] = float(row_value)
	return column


def cast_as_integer(column):
	"""
	takes the column, casts the values as integers, and returnes the cleaned column
	
	:param column: the column where the first element is the column name and \
	the rest are the values under that column
	:type column: list

	:returns: the clean column
	:rtype: list of ints
	"""
	
	for row_index, row_value in enumerate(column):
		#skip the first row because it's the column name
		if row_index == 0:
			continue
		column[row_index] = int(row_value)
	return column

def rid_of_units(column):
	"""
	takes the column that is deemed as having units present and takes them away

	:param column: the column where the first element is the column name and \
	the rest are the values(which have units inside) under that column
	:type column: list

	:returns: partially cleaned column without units
	:rtype: list of strings
	"""

	for row_index, row_value in enumerate(column):
		#skip the first row because it's the column name
		if row_index == 0:
			continue

		#get rid of any letters then stripped
		row_value = re.sub(r'[A-z]', '', row_value).strip()

		#get rid of special characters that indicate units
		#cannot get rid of all special characters bc '-' and '..' will be needed later
		row_value = re.sub(r'[()"%]', '', row_value).strip()

		column[row_index] = row_value
	return column


def keep_first_option(column):
	"""
	takes the column that has distinct options (such as xx/xx or xx-xx/xx voltage)
	and keeps only the first value

	:param column: a column where the first element is the column name and the \
	rest are the values in the distinct option format under this column
	:type column: list

	:returns: partially cleaned column keeping just one value
	:rtype: list of strings
	"""

	for row_index, row_value in enumerate(column):
		#skip the first row bc that is the column name
		if row_index == 0:
			continue

		row_value = row_value.split('/')[0]
		if '-' in row_value:
			row_value = row_value.split('-')[0]

		column[row_index] = row_value
	return column


#iterate through the directories(each site) and the files(tables) inside
for root, dirs, files in os.walk(PATH):
	#go to next iteration because we want to see what's inside
	if root == PATH:
		continue
	#iterating though the files
	for file in files:

		#---FOR TESTING PURPOSES ONLY---
		if 'motor' not in file or 'blank' in root:
			continue

		#opening the file
		path_to_csv = root + '/' + file
		with open(path_to_csv, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			original_table = [row for row in reader]

		#iterating through the table BY COLUMN not by row
		for column_index, column in enumerate(zip(*original_table)):
			column_name = column[0]
			#zip creates tuples but we need a list
			column = [column]

			"""
			based on the file, a function is called to decide on the rule that 
			should be used for this particular column
			"""

			cleaned_column = []
			if file.startswith('motor'):
				cleaned_column = decide_on_motor_rule(column_name, column)

			#creating empty table
			updated_table = [[] for i in range(len(original_table))]

			if cleaned_column:
				#fill in new values in empty table
				for row_index, row_value in enumerate(cleaned_column):
					updated_table[row_index].append(row_value)
			else:
				raise ValueError(f'The column {column_name} in {file} under {root} was not passed in to be cleaned.')

			#write to file the updated table
			#TODO: consider creating a new folder for all of the cleaned data so that testing
				#would be easier

		


