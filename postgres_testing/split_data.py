"""
this script will transfer data from the csvs with all specifications to 
split up csvs that match the postgres table format
"""

import os
import csv
import unicodecsv

from constants.table_schemas import MASTER, FACILITY_SPECIFICATIONS, CONVEYOR_SPECIFICATIONS, MOTOR_SPECIFICATIONS, BEARING_SPECIFICATIONS, GEARBOX_SPECIFICATIONS

SITES = ['amazon-zappos', 'dhl-miami', 'dhl-milano', 'fedex-louisville', 'dhl-cincinnati', 'blank-templates']
PATH = 'Documents/assets-info/postgres_testing/split_specs/'
ALL_SPECS_PATH = 'Documents/assets-info/postgres_testing/specs_as_csv_testing/'
FILES = ['master.csv', 'facility_specifications.csv', 'conveyor_specifications.csv', 'motor_specifications.csv', 'bearing_specifications.csv', 'gearbox_specifications.csv']

def insert_schemas(dir_path):
	"""
	inserts all of the tables with columns only into the site folder given

	:param dir_path: path to the directory that needs the asset spec tables
	:type dir_path: str

	:returns: nothing
	"""

	for file in FILES:
		path_to_table = dir_path + '/' + file
		with open(path_to_table, 'w') as csvfile:
			writer = csv.writer(csvfile, delimiter=',')
			if 'master' in file:
				writer.writerow(MASTER)
			elif 'facility' in file:
				writer.writerow(FACILITY_SPECIFICATIONS)
			elif 'conveyor' in file:
				writer.writerow(CONVEYOR_SPECIFICATIONS)
			elif 'motor' in file:
				writer.writerow(MOTOR_SPECIFICATIONS)
			elif 'bearing' in file:
				writer.writerow(BEARING_SPECIFICATIONS)
			elif 'gearbox' in file:
				writer.writerow(GEARBOX_SPECIFICATIONS)


def transfer_data(dir_path, directory):
	"""
	transfer the data from the CSVs with all specifications data to the CSVs split up
	by table

	:param dir_path: path to the directory that data will be transferred to
	:type dir_path: str

	:returns: nothing
	"""

	#hence the name, 'blank-templates' we don't want data here
	if directory == 'blank-templates':
		return

	for file in FILES:
		path_to_table = dir_path + '/' + file
		path_to_site_all_specs = ALL_SPECS_PATH + directory + '.csv'

		with open(path_to_site_all_specs, 'rb', ) as csvfl:
			reader = unicodecsv.reader(csvfl, delimiter=',', encoding='utf-8-sig')
			all_specs_csv = [row for row in reader]

		data = []
		#start with empty row for every sensor
		for i in range(len(all_specs_csv)):
			data.append([])


		#distributing the data by iterating though the columns
		for column_index, column in enumerate(zip(*all_specs_csv)):
			column_name = column[0] 
			for row_index, row_value in enumerate(column):
				#skip the first row because those are the column names
				if row_index == 0:
					continue
				equipment_type = all_specs_csv[row_index][3]
				if file.startswith('master'):
					if column_name.startswith('master') or column_name.startswith('all_'):
						data[row_index].append(row_value)
				elif file.startswith('facility'):
					if column_name.startswith('facility') or column_name.startswith('all_'):
						data[row_index].append(row_value)
				elif file.startswith('conveyor'):
					if column_name.startswith('conveyor') or column_name.startswith('all_'):
						data[row_index].append(row_value)
				elif file.startswith('motor') and equipment_type == 'M':
					if column_name.startswith('motor') or column_name.startswith('all'):
						data[row_index].append(row_value)
				elif file.startswith('bearing') and equipment_type == 'B':
					if column_name.startswith('bearing') or column_name.startswith('all'):
						data[row_index].append(row_value)
				elif file.startswith('gearbox') and equipment_type == 'G':
					if column_name.startswith('gearbox') or column_name.startswith('all'):
						data[row_index].append(row_value)

		final_data = [row for row in data if row != []]
		

		with open(path_to_table, 'a') as csvfile:
			writer = csv.writer(csvfile, delimiter=',')
			writer.writerows(final_data)

#creates a new folder for every site
for site in SITES:
	create_dir_path = PATH + site
	os.mkdir(create_dir_path)

#going through each site folder and placing the necessary files in each
for root, dirs, files in os.walk(PATH):
	for directory in dirs:
		dir_path = os.path.join(root,directory)

		insert_schemas(dir_path)
		transfer_data(dir_path, directory)